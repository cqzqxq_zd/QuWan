package quickcore.webview;

import android.graphics.Bitmap;
import android.net.Uri;

import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebView;

public interface WebViewListener {
    void setTitleView(String title);
    void pageStart(WebView view, String url, Bitmap favicon);
    void pageEnd(WebView view, String url);
    boolean loadUrl(WebView webView,boolean isWebScheme, String scheme ,String url);
    void onDownload(String url);
    void onOpenFile(boolean is, ValueCallback<Uri[]> callback, ValueCallback<Uri> callback2);
    interface TitleListener{
        void onLoadTitle(String title);
    }
}
