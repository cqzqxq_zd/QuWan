package quickcore.webview;


import com.tencent.smtt.sdk.DownloadListener;

public class WebViewDownloadImpl implements DownloadListener {
    private WebViewListener webViewListener;
    public WebViewDownloadImpl(WebViewListener webViewListener) {
        this.webViewListener = webViewListener;
    }

    @Override
    public void onDownloadStart(String s, String s1, String s2, String s3, long l) {
        webViewListener.onDownload(s);
    }
}
