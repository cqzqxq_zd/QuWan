package quickcore.webview;

import android.net.Uri;

import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

public class WebChromeClientImpl extends WebChromeClient {
    private WebViewListener listener;

    public WebChromeClientImpl(WebViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        listener.setTitleView(title);
    }

    @Override
    public void onGeolocationPermissionsShowPrompt(String s, GeolocationPermissionsCallback geolocationPermissionsCallback) {
        geolocationPermissionsCallback.invoke(s, true, false);
        super.onGeolocationPermissionsShowPrompt(s, geolocationPermissionsCallback);
    }


    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        listener.onOpenFile(true,filePathCallback,null);
        return true;
    }

    // For Android < 3.0
    public void openFileChooser(ValueCallback<Uri> uploadMsg) {
        listener.onOpenFile(false,null,uploadMsg);
    }

    // For Android  > 4.1.1
    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
        listener.onOpenFile(false,null,uploadMsg);
    }
}
