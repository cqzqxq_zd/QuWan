package quickcore.webview;

import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

public class WebViewClientImpl extends WebViewClient {
    private WebViewListener listener;

    public WebViewClientImpl(WebViewListener listener) {
        this.listener = listener;
    }
    //解决HTTPS链接问题


    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        listener.pageStart(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        listener.pageEnd(view, url);
        String title = view.getTitle();
        if (!TextUtils.isEmpty(title)) {
            listener.setTitleView(title);
        }
    }

    //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Uri uri = Uri.parse(url);
        Log.i("result", "shouldOverrideUrlLoading: ");
        String scheme = uri.getScheme();
        boolean isControl = true;
        if (scheme.equals("http") || scheme.equals("https")) {
            isControl = listener.loadUrl(view, true, scheme, url);
        } else {
            isControl = listener.loadUrl(view, false, scheme, url);
        }
        return isControl;
    }
}
