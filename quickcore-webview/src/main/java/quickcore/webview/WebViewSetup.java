package quickcore.webview;

import android.os.Build;

import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WebViewSetup{
    private WebViewClientImpl webViewClient;
    private WebChromeClientImpl webChromeClient;
    private WebViewDownloadImpl webViewDownload;
    private String locationDbPath;
    private int cacheMode;
    private String initUrl;
    private WebViewSetup(SetUp setUp) {
        webViewClient = new WebViewClientImpl(setUp.webViewListener);
        webChromeClient = new WebChromeClientImpl(setUp.webViewListener);
        webViewDownload = new WebViewDownloadImpl(setUp.webViewListener);
        locationDbPath = setUp.locationDbPath;
        cacheMode = setUp.cacheMode;
        initUrl = setUp.initUrl;
    }
    public static SetUp builder(){
        return new SetUp();
    }

    public void setupWebView(WebView webView){

        settingWebView(webView);
        webView.setWebViewClient(webViewClient);
        webView.setWebChromeClient(webChromeClient);
        webView.setDownloadListener(webViewDownload);
        webView.loadUrl(initUrl);

    }

    private void settingWebView(WebView webView){
        WebSettings settings = webView.getSettings();
        //启用地理定位
        settings.setGeolocationEnabled(true);
        //设置定位的数据库路径
        settings.setGeolocationDatabasePath(locationDbPath);
        settings.setDomStorageEnabled(true);//是否开启本地DOM存储
        settings.setJavaScriptEnabled(true);//启用支持javascript
        settings.setBlockNetworkImage(false);
        settings.setCacheMode(cacheMode);//优先使用缓存
        settings.setNeedInitialFocus(true); //当webview调用requestFocus时为webview设置节点
        settings.setLoadsImagesAutomatically(true);  //支持自动加载图片
        settings.setDisplayZoomControls(false);//不显示缩放控制按钮（3.0以上有效）
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);//支持内容重新布局
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            settings.setAllowUniversalAccessFromFileURLs(true);
        }else{
            try {
                Class<?> clazz = webView.getSettings().getClass();
                Method method = clazz.getMethod("setAllowUniversalAccessFromFileURLs", boolean.class);
                if (method != null) {
                    method.invoke(webView.getSettings(), true);
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        webView.requestFocusFromTouch();//支持获取手势焦点，便于用户输入

//        if (Build.VERSION.SDK_INT >= 21) {
//            settings.setMixedContentMode(MIXED_CONTENT_ALWAYS_ALLOW);
//        }
    }

    public static class SetUp{
        private WebViewListener webViewListener;
        private String locationDbPath;
        private int cacheMode;
        private String initUrl;
        private SetUp(){
        }

        public SetUp setWebViewClient(WebViewListener webViewClient) {
            this.webViewListener = webViewClient;
            return this;
        }

        public SetUp setLocationDbPath(String locationDbPath) {
            this.locationDbPath = locationDbPath;
            return this;
        }

        public SetUp setCacheMode(int cacheMode) {
            this.cacheMode = cacheMode;
            return this;
        }

        public SetUp setInitUrl(String initUrl) {
            this.initUrl = initUrl;
            return this;
        }

        public WebViewSetup build(){
            return new WebViewSetup(this);
        }
    }
}
