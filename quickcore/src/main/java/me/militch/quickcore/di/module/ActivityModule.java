package me.militch.quickcore.di.module;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import me.militch.quickcore.di.ActivityScope;

@Module
public class ActivityModule {
    private Activity activity;
    public ActivityModule(Activity activity) {
        this.activity = activity;
    }
    @Provides @ActivityScope
    Activity provideActivity(){
        return activity;
    }
}
