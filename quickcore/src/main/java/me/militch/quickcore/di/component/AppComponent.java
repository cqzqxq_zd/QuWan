package me.militch.quickcore.di.component;

import javax.inject.Singleton;

import dagger.Component;
import me.militch.quickcore.core.impl.AppTarget;
import me.militch.quickcore.di.module.GlobalConfigModule;
import me.militch.quickcore.di.module.HttpConfigModule;
import me.militch.quickcore.repository.impl.RepositoryStore;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
@Singleton
@Component(modules = {HttpConfigModule.class,GlobalConfigModule.class})
public interface AppComponent {
    OkHttpClient okhttp();
    Retrofit retrofit();
    RepositoryStore repositoryStore();
    void inject(AppTarget appTarget);
}
