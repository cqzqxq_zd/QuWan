package me.militch.quickcore.di.component;

import android.app.Activity;
import android.support.v4.app.Fragment;

import me.militch.quickcore.mvp.presenter.Presenter;
import me.militch.quickcore.mvp.presenter.QuickPresenter;

public interface IComponent{
    Activity activity();
    Fragment fragment();
}
