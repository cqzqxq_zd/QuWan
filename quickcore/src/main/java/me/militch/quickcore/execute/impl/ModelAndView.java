package me.militch.quickcore.execute.impl;

import io.reactivex.Flowable;
import me.militch.quickcore.event.RespEvent;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.execute.IModelAndView;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.ResPreHandler;
import me.militch.quickcore.util.RespBase;

public class ModelAndView<V>implements IModelAndView<V> {
    private ModelHelper modelHelper;
    private V view;
    public static <V> ModelAndView<V> create(V view,ModelHelper modelHelper){
        return new ModelAndView<V>(view,modelHelper);
    }
    ModelAndView(V view,ModelHelper modelHelper) {
        this.view = view;
        this.modelHelper = modelHelper;
    }
    @Override
    public <T> void request(Flowable<RespBase<T>> respResult,
                            final ViewEvent<V, T> okVet,
                            final ViewEvent<V, ApiException> badVet) {
        modelHelper.request(respResult, new RespEvent<T>() {
            @Override
            public void isOk(T t) {
                if(okVet != null){
                    okVet.call(view,t);
                }
            }

            @Override
            public void isError(ApiException apiException) {
                if(badVet != null){
                    badVet.call(view,apiException);
                }
            }
        });
    }

    @Override
    public <T> void request(Flowable<RespBase<T>> respResult, ResPreHandler<RespBase<T>, T> preHandler, final ViewEvent<V, T> okVet, final ViewEvent<V, ApiException> badVet) {
        modelHelper.requestByPreHandler(respResult, new RespEvent<T>() {
            @Override
            public void isOk(T t) {
                if(okVet != null){
                    okVet.call(view,t);
                }
            }

            @Override
            public void isError(ApiException apiException) {
                if(badVet != null){
                    badVet.call(view,apiException);
                }
            }
        },preHandler);
    }


    @Override
    public <T> void request(Flowable<RespBase<T>> respResult, ViewEvent<V, T> okVet) {
        this.request(respResult,okVet,null);
    }
}
