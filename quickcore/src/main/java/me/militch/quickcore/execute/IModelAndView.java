package me.militch.quickcore.execute;

import io.reactivex.Flowable;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.ResPreHandler;
import me.militch.quickcore.util.RespBase;

public interface IModelAndView<V> {
    <T> void request(Flowable<RespBase<T>> respResult, ViewEvent<V,T> okVet, ViewEvent<V,ApiException> badVet);
    <T> void request(Flowable<RespBase<T>> respResult, ResPreHandler<RespBase<T>,T> preHandler, ViewEvent<V, T> okVet, ViewEvent<V,ApiException> badVet);
    <T> void request(Flowable<RespBase<T>> respResult, ViewEvent<V,T> okVet);
}
