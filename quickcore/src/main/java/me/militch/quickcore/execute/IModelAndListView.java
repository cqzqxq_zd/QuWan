package me.militch.quickcore.execute;

import java.util.List;

import io.reactivex.Flowable;
import me.militch.quickcore.event.ListViewEvent;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.mvp.view.AbsListView;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.RespBase;

public interface IModelAndListView<T>{
    void requestList(Flowable<RespBase<List<T>>> respResult, ListViewEvent<T> okVet, ViewEvent<AbsListView<T>,ApiException> badVet);
}
