package me.militch.quickcore.execute.impl;

import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.mvp.view.AbsListView;

public class ModelAndListView<T> extends ModelAndView<AbsListView<T>> {
    private ModelAndListView(AbsListView<T> view, ModelHelper modelHelper) {
        super(view, modelHelper);
    }
    public static <T> ModelAndListView<T> create(AbsListView<T> view,ModelHelper modelHelper){
        return new ModelAndListView<T>(view,modelHelper);
    }
}
