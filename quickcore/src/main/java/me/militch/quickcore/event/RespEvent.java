package me.militch.quickcore.event;

import me.militch.quickcore.util.ApiException;

public interface RespEvent<T> {
    void  isOk(T t);
    void isError(ApiException apiException);
}
