package me.militch.quickcore.util;

import java.net.SocketTimeoutException;

import io.reactivex.subscribers.ResourceSubscriber;
import me.militch.quickcore.event.RespEvent;

public class TargetSubscribe<T> extends ResourceSubscriber<T> {
    private RespEvent<T> respEvent;
    public TargetSubscribe(RespEvent<T> respEvent) {
        this.respEvent = respEvent;
    }

    @Override
    public void onNext(T t) {
        if(respEvent != null)
            respEvent.isOk(t);
    }

    @Override
    public void onError(Throwable t) {
        if(respEvent != null&&t instanceof ApiException)
            respEvent.isError((ApiException) t);
        else if (respEvent!=null && t instanceof SocketTimeoutException){
            ApiException e = new ApiException("网络超时",t);
            respEvent.isError(e);
        }else if(respEvent!=null){
            ApiException e = new ApiException("未知错误",t);
            respEvent.isError(e);
        }
    }

    @Override
    public void onComplete() {

    }
}
