package me.militch.quickcore.util;
public interface EventCall<T> {
    void call(T t);
}
