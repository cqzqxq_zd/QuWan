package me.militch.quickcore.util;

public class Optional<T> {
    private final T t;
    public Optional(T t) {
        this.t = t;
    }
    public T get(){
        return t;
    }
}
