package me.militch.quickcore.util;

/**
 * Created by WYZ on 2018/3/27.
 */

public interface ResPreHandler<T,D> {
    D process(T t) throws Exception;
}
