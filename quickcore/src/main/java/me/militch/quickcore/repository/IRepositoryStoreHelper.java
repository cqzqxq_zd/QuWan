package me.militch.quickcore.repository;

import me.militch.quickcore.repository.impl.RepositoryStore;

public interface IRepositoryStoreHelper {
    void setRepository(RepositoryStore repository);
}
