package me.militch.quickcore.core;

public interface HasDaggerInject<T> {
    void inject(T t);
}
