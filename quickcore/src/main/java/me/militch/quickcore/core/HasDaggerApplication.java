package me.militch.quickcore.core;


import me.militch.quickcore.di.component.AppComponent;
import me.militch.quickcore.di.module.GlobalConfigModule;
import me.militch.quickcore.di.module.HttpConfigModule;

public interface HasDaggerApplication<T> {
    GlobalConfigModule setupGlobalConfigModule();
    HttpConfigModule setupHttpConfigModule();
    T activityComponent(AppComponent appComponent);
}
