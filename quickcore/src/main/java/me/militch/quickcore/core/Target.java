package me.militch.quickcore.core;

import me.militch.quickcore.di.component.AppComponent;

public interface Target {
    AppComponent getAppComponent();
}
