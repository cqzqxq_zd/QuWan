package me.militch.quickcore.core.impl;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import me.militch.quickcore.core.HasDaggerApplication;
import me.militch.quickcore.core.HasDaggerInject;
import me.militch.quickcore.di.component.AppComponent;
import me.militch.quickcore.di.module.ActivityModule;

public class DaggerActivityCallback<T> implements Application.ActivityLifecycleCallbacks {
    private AppComponent appComponent;
    private HasDaggerApplication<T> hasDaggerApplication;
    private T call;
    public DaggerActivityCallback(HasDaggerApplication<T> hasDaggerApplication,AppComponent appComponent){
        this.hasDaggerApplication = hasDaggerApplication;
        this.appComponent = appComponent;
    }
    @Override
    @SuppressWarnings("unchecked")
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if(activity instanceof HasDaggerInject){
            HasDaggerInject<T> callActivity = (HasDaggerInject<T>) activity;
            call = hasDaggerApplication.activityComponent(appComponent);
            callActivity.inject(call);
        }
        call = hasDaggerApplication.activityComponent(appComponent);
        DaggerFragmentCallback<T> daggerFragmentCallback = new DaggerFragmentCallback<>(call);
        if(activity instanceof AppCompatActivity){
            ((AppCompatActivity) activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(daggerFragmentCallback,true);
        }
    }


    @Override
    public void onActivityStarted(Activity activity) {


    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
