package me.militch.quickcore.mvp.model;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import me.militch.quickcore.event.RespEvent;
import me.militch.quickcore.repository.impl.RepositoryStore;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.EventCall;
import me.militch.quickcore.util.RXUtil;
import me.militch.quickcore.util.ResPreHandler;
import me.militch.quickcore.util.RespBase;
import me.militch.quickcore.util.TargetSubscribe;
import me.militch.quickcore.util.TargetSubscribeEvent;

public class  ModelHelper implements IModelHelper {
    private CompositeDisposable compositeDisposable;
    private RepositoryStore repositoryStore;

    @Override
    public String toString() {
        return super.toString();
    }

    @Inject
    public ModelHelper(RepositoryStore repositoryStore) {
        compositeDisposable = new CompositeDisposable();
        this.repositoryStore = repositoryStore;
    }
    @Override
    public <T> T getService(Class<T> serviceClass){
        return repositoryStore.getRetrofitService(serviceClass);
    }
    @Override
    public <T> Flowable<T> pack(Flowable<RespBase<T>> respResult){
        return respResult
                //统一处理线程切换
                .compose(RXUtil.<RespBase<T>>rxSchedulerHelper())
                //统一处理返回对象
                .compose(RXUtil.<T>handleRespBaseResult());
    }
    @Override
    public <T> void request(Flowable<RespBase<T>> respResult, RespEvent<T> event){
        //将RXJava流对象绑定的observer对象加入compositeDisposable统一管理
        compositeDisposable.add(pack(respResult)
                //自定义事件回调
                .subscribeWith(new TargetSubscribe<T>(event)));
    }



    @Override
    public <T> void requestByPreHandler(Flowable<RespBase<T>> respResult, RespEvent<T> event, ResPreHandler<RespBase<T>,T> preHandler){
        //将RXJava流对象绑定的observer对象加入compositeDisposable统一管理
        compositeDisposable.add(pack2(respResult,preHandler)
                //自定义事件回调
                .subscribeWith(new TargetSubscribe<T>(event)));
    }

    @Override
    public <T> Flowable<T> pack2(Flowable<RespBase<T>> respResult,ResPreHandler<RespBase<T>,T> preHandler) {
        return respResult
                //统一处理线程切换
                .compose(RXUtil.<RespBase<T>>rxSchedulerHelper())
                //统一处理返回对象
                .compose(RXUtil.<T>handleRespBaseResult2(preHandler));
    }

    @Override
    public <T> void request(Flowable<RespBase<T>> respResult, EventCall<? super T> okEvent, EventCall<? super ApiException> badEvent){
        //将RXJava流对象绑定的observer对象加入compositeDisposable统一管理
        compositeDisposable.add(pack(respResult)
                //自定义事件回调
                .subscribeWith(new TargetSubscribeEvent<T>(okEvent,badEvent)));
    }


    @Override
    public <T> void request(Flowable<RespBase<T>> respResult,EventCall<? super T> okEvent){
        this.request(respResult,okEvent,null);
    }


    @Override
    public void unBind(){
        compositeDisposable.clear();
    }
}
