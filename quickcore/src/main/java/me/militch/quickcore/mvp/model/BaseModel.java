package me.militch.quickcore.mvp.model;

import me.militch.quickcore.mvp.IModel;
import me.militch.quickcore.repository.IRepositoryStore;
import me.militch.quickcore.repository.impl.RepositoryStore;

public abstract class BaseModel implements IModel{
    private IRepositoryStore repositoryStore;
    public BaseModel(RepositoryStore repositoryStore){
        this.repositoryStore = repositoryStore;
    }
    @Override
    public IRepositoryStore store(){
        return repositoryStore;
    }
}
