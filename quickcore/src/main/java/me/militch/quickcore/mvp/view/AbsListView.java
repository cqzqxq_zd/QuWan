package me.militch.quickcore.mvp.view;

import java.util.List;

import me.militch.quickcore.mvp.BaseView;

public interface AbsListView<T> extends BaseView {
    void bindData(List<T> data);
}
