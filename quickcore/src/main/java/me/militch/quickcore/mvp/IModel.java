package me.militch.quickcore.mvp;

import me.militch.quickcore.repository.IRepositoryStore;

public interface IModel {
    IRepositoryStore store();
}
