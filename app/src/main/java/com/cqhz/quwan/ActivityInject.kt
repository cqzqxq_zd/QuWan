package com.cqhz.quwan

import com.cqhz.quwan.ui.activity.*
import com.cqhz.quwan.ui.base.WebViewActivity
import com.cqhz.quwan.ui.buy.*
import com.cqhz.quwan.ui.login.BindMobilePhoneActivity
import com.cqhz.quwan.ui.login.LoginActivity
import com.cqhz.quwan.ui.main.MainActivity
import com.cqhz.quwan.ui.main.fragment.*
import com.cqhz.quwan.ui.main.match.MatchAnalysisActivity
import com.cqhz.quwan.ui.main.match.fragment.AnalysisFragment
import com.cqhz.quwan.ui.main.match.fragment.PredictionFragment
import com.cqhz.quwan.ui.main.match.fragment.child.IntegralInfoFragment
import com.cqhz.quwan.ui.main.match.fragment.child.OddsAsiaFragment
import com.cqhz.quwan.ui.main.match.fragment.child.OddsBallFragment
import com.cqhz.quwan.ui.main.match.fragment.child.OddsEuropeFragment
import com.cqhz.quwan.ui.main.order.MyPushOrderActivity
import com.cqhz.quwan.ui.main.order.OrderListActivity
import com.cqhz.quwan.ui.mine.*
import com.cqhz.quwan.ui.mine.fragment.TransactionRecordFragment
import com.cqhz.quwan.ui.mine.fragment.WonRecordFragment
import com.cqhz.quwan.wxapi.WXEntryActivity
import dagger.Component
import me.militch.quickcore.di.ActivityScope
import me.militch.quickcore.di.component.AppComponent

@ActivityScope
@Component(dependencies = [AppComponent::class])
interface ActivityInject {
    /*******************************activity start******************************/
    fun inject(activity: LoginActivity)

    fun inject(activity: GoPaymentActivity)

    fun inject(activity: LotteryBettingActivity)

    fun inject(activity: PayPasswordActivity)

    fun inject(activity: RechargeActivity)

    fun inject(activity: WithdrawsCashActivity)

    fun inject(activity: SettingsActivity)

    fun inject(activity: BindMobilePhoneActivity)

    fun inject(activity: RealNameAuthActivity)

    fun inject(activity: InformationActivity)

    fun inject(activity: PaymentActivity)

    fun inject(activity: DoPayActivity)

    fun inject(activity: MessageActivity)

    fun inject(activity: WebViewActivity)

    fun inject(activity: WXEntryActivity)

    fun inject(activity: MatchAnalysisActivity)

    fun inject(activity: AddressActivity)

    fun inject(activity: MallActivity)

    fun inject(activity: DiamondRecordActivity)

    fun inject(activity: ProductDetailActivity)

    fun inject(activity: BeanActivity)

    fun inject(activity: BallSelectorActivity)

    fun inject(activity: MainActivity)

    fun inject(activity: LotteryDrawActivity)

    fun inject(activity: MyExchangeActivity)

    fun inject(activity: OrderListActivity)

    fun inject(activity: ESportActivity)

    fun inject(activity: MyPredictionActivity)

    fun inject(activity: ESportDetailActivity)

    fun inject(activity: MyCommissionActivity)

    fun inject(activity: MyPushOrderActivity)

    fun inject(activity: ExpertCertificationActivity)

    fun inject(activity: FootballLotteryActivity)

    fun inject(activity: FootballSelectedActivity)

    fun inject(activity: FootballSelectHHActivity)

    fun inject(activity: FootballSelectBFActivity)

    fun inject(activity: OrderDetailActivity)

    fun inject(activity: ESportOrderDetailActivity)

    fun inject(activity: ExpertDetailActivity)

    fun inject(activity: PlanDetailActivity)

    fun inject(activity: MyCouponActivity)

    fun inject(activity: SelectCouponActivity)

    fun inject(activity: BasketballLotteryActivity)

    fun inject(activity: BasketballSelectHHActivity)

    fun inject(activity: BasketballSelectSFCActivity)

    fun inject(activity: BasketballSelectedActivity)

    fun inject(activity: BonusOptimizationBasketballActivity)

    fun inject(activity: BonusOptimizationFootballActivity)
    /*******************************activity end******************************/
    /*******************************fragment start****************************/
    fun inject(fragment: IntegralInfoFragment)

    fun inject(fragment: AnalysisFragment)

    fun inject(fragment: OddsAsiaFragment)

    fun inject(fragment: OddsEuropeFragment)

    fun inject(fragment: PredictionFragment)

    fun inject(fragment: OddsBallFragment)

    fun inject(fragment: MineFragment)

    fun inject(fragment: LiveFragment)

    fun inject(fragment: GuessFragment)

    fun inject(fragment: CouponsActivity.CouponNotUsedFragment)

    fun inject(fragment: CouponsActivity.CouponNotDispatchedFragment)

    fun inject(fragment: CouponsActivity.CouponExpiredFragment)

    fun inject(fragment: NewsFragment)

    fun inject(fragment: WonRecordFragment)

    fun inject(fragment: TransactionRecordFragment)

    fun inject(fragment: MallFragment)

    fun inject(fragment: DocumentaryFragment)
    /*******************************fragment end****************************/
    fun inject(dialog: CouponDialog)
}
