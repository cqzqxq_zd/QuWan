package com.cqhz.quwan.ui.interfaces

import com.cqhz.quwan.model.ESportItemBean


interface SelectTeamClickListener {
    /**
     * 点击选择获胜队伍
     * @param  teamPosition 选中位置的坐标
     * @param  bean
     */
    fun onTeamClick(teamPosition: Int, bean: ESportItemBean)
}