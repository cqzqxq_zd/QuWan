package com.cqhz.quwan.ui.widget.dialog;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cqhz.quwan.R;

/**
 * 赠送趣豆弹出窗口
 * Created by Guojing on 2018/10/22.
 */
public class RewardDialog {

    private Context context;
    private Dialog dialog;
    private LinearLayout llRewardDialog;
    private View actionClose;
    private TextView tvMsg;
    private TextView tvBean;
    private Button actionConfirm;// 确认按钮

    public RewardDialog(Context context) {
        this.context = context;
    }

    public RewardDialog builder() {
        // 获取Dialog布局
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_reward, null);
        view.setMinimumWidth(10000);// 设置dialog全屏显示

        // 获取自定义Dialog布局中的控件
        llRewardDialog = view.findViewById(R.id.ll_reward_dialog);
        actionClose = view.findViewById(R.id.action_close);
        tvMsg = view.findViewById(R.id.tv_msg);
        tvBean = view.findViewById(R.id.tv_bean);
        actionConfirm = view.findViewById(R.id.action_confirm);

        actionClose.setOnClickListener(v -> dialog.dismiss());

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.DialogStyle);
        dialog.setContentView(view);
        llRewardDialog.setLayoutParams(new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

        return this;
    }

    /**
     * 设置内容
     *
     * @param msg
     * @return
     */
    public RewardDialog setMsg(@NonNull CharSequence msg) {
        tvMsg.setText("".equals(msg) ? "邀请成功" : msg);
        return this;
    }

    /**
     * 设置趣豆数
     *
     * @param bean
     * @return
     */
    public RewardDialog setBean(@NonNull CharSequence bean) {
        tvBean.setText("".equals(bean) ? "0.00" : bean);
        return this;
    }

    /**
     * 设置按钮文字和事件
     *
     * @param text
     * @return
     */
    public RewardDialog setActionConfirm(@NonNull CharSequence text) {
        actionConfirm.setText("".equals(text) ? "确定" : text);
        actionConfirm.setOnClickListener(v ->
        {
            dialog.dismiss();
        });
        return this;
    }

    /**
     * 设置按钮文字和事件
     *
     * @param text
     * @param listener
     * @return
     */
    public RewardDialog setActionConfirm(@NonNull CharSequence text, @NonNull final View.OnClickListener listener) {
        actionConfirm.setText("".equals(text) ? "确定" : text);
        actionConfirm.setOnClickListener(v ->
        {
            listener.onClick(v);
            dialog.dismiss();
        });
        return this;
    }

    /**
     * 显示
     */
    public void show() {
        dialog.setCancelable(false);
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
