package com.cqhz.quwan.ui.main.match.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebViewClient;

import com.blankj.utilcode.util.LogUtils;
import com.cqhz.quwan.R;
import com.cqhz.quwan.service.API;
import com.cqhz.quwan.ui.base.AbsFragment;
import com.cqhz.quwan.util.JSBridgeInterface;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;

import java.text.MessageFormat;

import quickcore.webview.WebViewListener;
import quickcore.webview.WebViewSetup;

/**
 * 赛况
 *
 * @author whamu2
 * @date 2018/7/17
 */
public class MatchSituationFragment extends AbsFragment implements WebViewListener {
    private static final String TAG = MatchSituationFragment.class.getSimpleName();
    private static final String KEY_CODE = "code";

    private static final String URL = "saikaung/saishi.html";

    private WebView mWebView;

    private String code;

    public static MatchSituationFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putString(KEY_CODE, code);
        MatchSituationFragment fragment = new MatchSituationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int layout() {
        return R.layout.fragment_match_situation;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_CODE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_CODE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWebView = view.findViewById(R.id.webview);

        String dir = getContext().getDir("database", Context.MODE_PRIVATE).getPath();
        WebViewSetup.builder()
                .setWebViewClient(this)
                .setLocationDbPath(dir)
                .setCacheMode(WebSettings.LOAD_NO_CACHE)
                .build().setupWebView(mWebView);
        mWebView.addJavascriptInterface(new JSBridgeInterface(), "Android");
//        WebSettings.initWebViewSettings(mWebView);
    }

    @Override
    public void onResume() {
        LogUtils.e(TAG, "onResume");
        super.onResume();
//        http://192.168.1.107:8021/cpweb/saikaung/saikuang.html?m_id=108951
        String link = MessageFormat.format("{0}{1}?m_id={2}", API.WEB_HOST, URL, code);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_CODE);
        }
//        String link = MessageFormat.format("{0}?m_id={1}", "http://h5.zhanguo.fun/saikaung/saikuang.html", code);
        LogUtils.e(TAG, link);
        mWebView.loadUrl(link);
        mWebView.reload();

    }

    public void setupLoad(String code) {
        String link = MessageFormat.format("{0}{1}?m_id={2}", API.WEB_HOST, URL, code);
        LogUtils.e(TAG, link);
        mWebView.loadUrl(link);
    }

    @Override
    public void setTitleView(String title) {

    }

    @Override
    public void pageStart(com.tencent.smtt.sdk.WebView view, String url, Bitmap favicon) {

    }

    @Override
    public void pageEnd(com.tencent.smtt.sdk.WebView view, String url) {

    }

    @Override
    public boolean loadUrl(com.tencent.smtt.sdk.WebView webView, boolean isWebScheme, String scheme, String url) {
        if (!isWebScheme) {
        } else {
            mWebView.loadUrl(url);
        }
        return true;
    }

    @Override
    public void onDownload(String url) {

    }

    @Override
    public void onOpenFile(boolean is, ValueCallback<Uri[]> callback, ValueCallback<Uri> callback2) {

    }


//    @Override
//    public void setUserVisibleHint(boolean isVisible) {
//        super.setUserVisibleHint(isVisible);
//        if (mRootView == null) {
//            return;
//        }
//        hasView = true;
//        if (isVisible) {
//            onVisibleChange(true);
//            isFragmentVisible = true;
//            return;
//        }
//        if (isFragmentVisible) {
//            onVisibleChange(false);
//            isFragmentVisible = false;
//        }
//    }
//
//    // 可见与不可见的处理
//    protected void onVisibleChange(boolean isVisible) {
//        if (isVisible) {
//            String link = MessageFormat.format("{0}{1}?m_id={2}", API.WEB_HOST, URL, code);
//            LogUtils.e(TAG, link);
//            mWebView.loadUrl(link);
//        }
//    }

}
