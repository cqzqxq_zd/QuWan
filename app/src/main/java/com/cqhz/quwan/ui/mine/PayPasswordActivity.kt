package com.cqhz.quwan.ui.mine

import android.widget.EditText
import android.widget.TextView
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.mvp.mine.ResetPayPasswordContract
import com.cqhz.quwan.mvp.mine.ResetPayPasswordPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.util.md5
import com.cqhz.quwan.util.v
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import me.militch.quickcore.core.HasDaggerInject
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PayPasswordActivity : GoBackActivity(),ResetPayPasswordContract.View,HasDaggerInject<ActivityInject>{
    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    private val numberEdit:TextView by v(R.id.ed_pay_password)
    private val getVerCodeBtn:TextView by v(R.id.btn_login_get_ver_code)
    private val verCodeEdit:EditText by v(R.id.edit_ver_code)
    private val passwordEdit:EditText by v(R.id.reset_password)
    private val password2Edit:EditText by v(R.id.reset_password2)
    private val resetBtn:TextView by v(R.id.reset_btn)
    var mSubscription: Subscription? = null
    @Inject lateinit var presenter: ResetPayPasswordPresenter
    override fun lockInputVerCode() {
//        timer()
    }
    private fun timer() {
        val count = 59L
        Flowable.interval(0, 1, TimeUnit.SECONDS)//设置0延迟，每隔一秒发送一条数据
                .onBackpressureBuffer()//加上背压策略
                .take(count) //设置循环次数
                .map{ aLong ->
                    count - aLong //
                }
                .observeOn(AndroidSchedulers.mainThread())//操作UI主要在UI线程
                .subscribe(object : Subscriber<Long> {
                    override fun onSubscribe(s: Subscription?) {
                        getVerCodeBtn.isEnabled = false
                        mSubscription = s
                        s?.request(Long.MAX_VALUE)//设置请求事件的数量，重要，必须调用
                    }

                    override fun onNext(aLong: Long?) {
                        getVerCodeBtn.text = "${aLong}s后重发" //接受到一条就是会操作一次UI
                    }

                    override fun onComplete() {
                        getVerCodeBtn.text = "点击重发"
                        getVerCodeBtn.isEnabled = true
                        mSubscription?.cancel()//取消订阅，防止内存泄漏
                    }

                    override fun onError(t: Throwable?) {
                        t?.printStackTrace()
                    }
                })
    }
    override fun titleBarText(): String? {
        return "支付密码"
    }
    override fun layout(): Int {
        return R.layout.activity_set_pay_password
    }

    override fun initView() {
        presenter.attachView(this)
        val loginInfo = APP.get()!!.loginInfo
        if(loginInfo!=null){
            numberEdit.text = loginInfo.phoneNumber
//            getVerCodeBtn.setOnClickListener {
//                showLoading("正在发送验证码")
//                presenter.getVerCode(loginInfo.phoneNumber!!)
//            }
            resetBtn.setOnClickListener {
                val vcode =verCodeEdit.text.toString()
                val pass1 = passwordEdit.text.toString()
                val pass2 = password2Edit.text.toString()
//                if(vcode.length<6){
//                    showToast("请输入正确的验证码")
//                }else
                if(pass1 != pass2){
                    showToast("两次密码不一样")
                }else if(pass1.length<6||pass2.length<6){
                    showToast("最少6位数字")
                }else{
                    showLoading("正在提交修改")
                    presenter.doReset(loginInfo.userId!!,pass1.md5(),"666666")
                }
            }
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

}