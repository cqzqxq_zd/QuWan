package com.cqhz.quwan.ui.main.match.adapter.tree.ball;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.whamu2.treeview.base.ViewHolder;
import com.whamu2.treeview.factory.ItemHelperFactory;
import com.whamu2.treeview.item.TreeItem;
import com.whamu2.treeview.item.TreeItemGroup;
import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.main.match.entity.AsiaOdds;

import java.util.Arrays;
import java.util.List;

/**
 * 赔率标题
 *
 * @author whamu2
 * @date 2018/7/19
 */
public class OddsAsiaTitleTree extends TreeItemGroup<AsiaOdds> {

    @Nullable
    @Override
    protected List<TreeItem> initChildList(AsiaOdds data) {
        return ItemHelperFactory.createTreeItemList(data.getResult(), OddsAsiaContentTree.class, this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.tree_odds_title_item;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder) {
        holder.setText(R.id.tv_company, R.string.str_company);

        holder.setText(R.id.tv_chupan, R.string.str_chupan);
        holder.setText(R.id.tv_c1, "主水");
        holder.setText(R.id.tv_c2, R.string.str_pankou);
        holder.setText(R.id.tv_c3, "客水");

        holder.setText(R.id.tv_jishipan, R.string.str_jishipan);
        holder.setText(R.id.tv_j1, "主水");
        holder.setText(R.id.tv_j2, R.string.str_pankou);
        holder.setText(R.id.tv_j3, "客水");
    }
}
