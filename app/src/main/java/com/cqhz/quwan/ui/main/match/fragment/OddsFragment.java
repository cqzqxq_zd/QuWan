package com.cqhz.quwan.ui.main.match.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RadioGroup;

import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.base.AbsFragment;
import com.cqhz.quwan.ui.main.match.fragment.child.OddsAsiaFragment;
import com.cqhz.quwan.ui.main.match.fragment.child.OddsBallFragment;
import com.cqhz.quwan.ui.main.match.fragment.child.OddsEuropeFragment;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * 赔率
 *
 * @author whamu2
 * @date 2018/7/18
 */
public class OddsFragment extends AbsFragment implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener {
    private static final String TAG = OddsFragment.class.getSimpleName();
    private static final String KEY_CODE = "code";

    private SegmentedGroup mSegmentedGroup;
    private ViewPager mViewPager;

    private String code;

    public static OddsFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putString(KEY_CODE, code);
        OddsFragment fragment = new OddsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int layout() {
        return R.layout.fragment_match_odds;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_CODE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSegmentedGroup = view.findViewById(R.id.sg);
        mViewPager = view.findViewById(R.id.viewpager);
        mSegmentedGroup.setOnCheckedChangeListener(this);

        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(OddsAsiaFragment.newInstance(code));
        adapter.addFragment(OddsEuropeFragment.newInstance(code));
        adapter.addFragment(OddsBallFragment.newInstance(code));
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(this);
        mSegmentedGroup.check(R.id.rb_asian_plate);
        mViewPager.setCurrentItem(0);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_asian_plate:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.rb_europe_plate:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.rb_twin_ball:
                mViewPager.setCurrentItem(2);
                break;
            default:
                mViewPager.setCurrentItem(0);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                mSegmentedGroup.check(R.id.rb_asian_plate);
                break;
            case 1:
                mSegmentedGroup.check(R.id.rb_europe_plate);
                break;
            case 2:
                mSegmentedGroup.check(R.id.rb_twin_ball);
                break;
            default:
                mSegmentedGroup.check(R.id.rb_asian_plate);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private static class PagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments = new ArrayList<>();

        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        void addFragment(Fragment fragment) {
            fragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
