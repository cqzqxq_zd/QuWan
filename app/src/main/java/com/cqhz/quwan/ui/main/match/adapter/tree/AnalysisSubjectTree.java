package com.cqhz.quwan.ui.main.match.adapter.tree;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.widget.TextView;

import com.whamu2.treeview.base.ViewHolder;
import com.whamu2.treeview.factory.ItemHelperFactory;
import com.whamu2.treeview.item.TreeItem;
import com.whamu2.treeview.item.TreeItemGroup;
import com.cqhz.quwan.APP;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.AnalysisRecord;
import com.cqhz.quwan.ui.main.match.common.Key;
import com.cqhz.quwan.util.CharSequenceManager;
import com.cqhz.quwan.util.PreferenceHelper;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 分析 - 球队
 *
 * @author whamu2
 * @date 2018/7/19
 */
public class AnalysisSubjectTree extends TreeItemGroup<AnalysisRecord.RecordPOListBean> {

    @Nullable
    @Override
    protected List<TreeItem> initChildList(AnalysisRecord.RecordPOListBean data) {
        return ItemHelperFactory.createTreeItemList(data.getRecord_detail_result(), AnalysisContentTree.class, this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.tree_analysis_subject;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder) {
        AnalysisRecord.RecordPOListBean data = getData();
        int type = data.getRecord_type();

        SpannableString text;
        if (data.getRecord_simple_result() == null) {
            String format = MessageFormat.format("近{0}场", 0);
            text = new SpannableString(format);
        } else {
            AnalysisRecord.RecordPOListBean.RecordSimpleResult result = data.getRecord_simple_result();
            AnalysisRecord.RecordPOListBean.RecordSimpleResult.RecordBean record = result.getRecord();
            String win = MessageFormat.format("{0}胜", record.getWin());
            String draw = MessageFormat.format("{0}平", record.getDraw());
            String lose = MessageFormat.format("{0}负", record.getLose());

            String l_win = null;
            String l_draw = null;
            String l_lose = null;
            String l_record = null;
            if (result.getH_record() != null) {
                AnalysisRecord.RecordPOListBean.RecordSimpleResult.HostRecord h_record = result.getH_record();
                l_win = MessageFormat.format("{0}胜", h_record.getWin());
                l_draw = MessageFormat.format("{0}平", h_record.getDraw());
                l_lose = MessageFormat.format("{0}负", h_record.getLose());
                l_record = MessageFormat.format("主场{0} {1} {2}", l_win, l_draw, l_lose);
            }
            if (result.getG_record() != null) {
                AnalysisRecord.RecordPOListBean.RecordSimpleResult.HostRecord g_record = result.getG_record();
                l_win = MessageFormat.format("{0}胜", g_record.getWin());
                l_draw = MessageFormat.format("{0}平", g_record.getDraw());
                l_lose = MessageFormat.format("{0}负", g_record.getLose());
                l_record = MessageFormat.format("客场{0} {1} {2}", l_win, l_draw, l_lose);
            }

            String team;
            if (type == Key.Analysis.ZKDLSJF || type == Key.Analysis.ZDJQZJ) {
                team = PreferenceHelper.getInstance(APP.Companion.get().getApplicationContext())
                        .getString("host_team", "");
            } else {
                team = PreferenceHelper.getInstance(APP.Companion.get().getApplicationContext())
                        .getString("guest_team", "");
            }

            String format = MessageFormat.format("近{0}场，{1}{2} {3} {4}；{5}", result.getTotal(), team, win, draw, lose, TextUtils.isEmpty(l_record) ? "" : l_record);

            HashMap<String, Integer> map = new HashMap<String, Integer>();
            map.put(win, 0xFFD60000);
            map.put(draw, 0xFF0D84C6);
            map.put(lose, 0xFF009D48);
            if (!TextUtils.isEmpty(l_win)) {
                map.put(l_win, 0xFFD60000);
            }
            if (!TextUtils.isEmpty(l_draw)) {
                map.put(l_draw, 0xFF0D84C6);
            }
            if (!TextUtils.isEmpty(l_lose)) {
                map.put(l_lose, 0xFF009D48);
            }
            text = CharSequenceManager.getKeyWordSpanArray(format, map);
        }

        if (type == Key.Analysis.ZKDLSJF) { // 主客队历史交锋
            TextView textView = viewHolder.getTextView(R.id.tv);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.res_title_icon, 0, 0, 0);
            textView.setText(text);
            viewHolder.setVisible(R.id.ll, true);

        } else if (type == Key.Analysis.ZDJQZJ) { // 主队近期战绩
            TextView textView = viewHolder.getTextView(R.id.tv);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.res_title_icon, 0, 0, 0);
            textView.setText(text);
            viewHolder.setVisible(R.id.ll, true);

        } else if (type == Key.Analysis.ZDWLSS) { // 主队未来赛事
            TextView textView = viewHolder.getTextView(R.id.tv);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.res_title_default_icon, 0, 0, 0);
            textView.setText("未来赛事");
            viewHolder.setVisible(R.id.ll, false);

        } else if (type == Key.Analysis.KDJQZJ) { // 客队近期战绩
            TextView textView = viewHolder.getTextView(R.id.tv);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.res_title_icon, 0, 0, 0);
            textView.setText(text);
            viewHolder.setVisible(R.id.ll, true);

        } else if (type == Key.Analysis.KDWLSS) { // 客队未来赛事
            TextView textView = viewHolder.getTextView(R.id.tv);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.res_title_default_icon, 0, 0, 0);
            textView.setText("未来赛事");
            viewHolder.setVisible(R.id.ll, false);

        }

    }
}
