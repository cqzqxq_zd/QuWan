package com.cqhz.quwan.ui.main.match.common;

import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class Utils {

    public static String getSingleScore(@NonNull String score, @IntRange(from = 0, to = 1) int index) {
        String regex = ":";
        String[] s = score.split(regex, 0);
        if (s.length > 1) {
            return s[index];
        }
        return "-";
    }

    public static String required(String val, String placeholder) {
        return TextUtils.isEmpty(val) ? placeholder : val;
    }

    public static String required(String val) {
        return required(val, "");
    }

    @ColorInt
    public static int getStateColor(@Nullable String change) {
        if (TextUtils.isEmpty(change)) {
            return Color.parseColor("#353535");
        } else {
            if (TextUtils.equals(change, Key.Odds.UP)) {
                return 0xFFD60000;
            } else if (TextUtils.equals(change, Key.Odds.DOWN)) {
                return 0xFF009D48;
            } else if (TextUtils.equals(change, Key.Odds.EQUAL)) {
                return Color.parseColor("#353535");
            } else {
                return Color.parseColor("#353535");
            }
        }
    }
}
