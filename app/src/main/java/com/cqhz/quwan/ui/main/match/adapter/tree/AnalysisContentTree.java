package com.cqhz.quwan.ui.main.match.adapter.tree;

import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.TimeUtils;
import com.whamu2.treeview.base.ViewHolder;
import com.whamu2.treeview.item.TreeItem;
import com.whamu2.treeview.item.TreeItemGroup;
import com.cqhz.quwan.APP;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.AnalysisRecord;
import com.cqhz.quwan.ui.main.match.common.Key;
import com.cqhz.quwan.ui.main.match.common.Utils;
import com.cqhz.quwan.util.CharSequenceManager;
import com.cqhz.quwan.util.PreferenceHelper;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Objects;

/**
 * 分析-内容
 *
 * @author whamu2
 * @date 2018/7/19
 */
public class AnalysisContentTree extends TreeItem<AnalysisRecord.RecordPOListBean.RecordDetailResultBean> {

    @Override
    public int getLayoutId() {
        return R.layout.tree_analysis_content;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder) {
        AnalysisRecord.RecordPOListBean.RecordDetailResultBean data = getData();

        long l = TimeUtils.string2Millis(data.getTime(), new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()));
        viewHolder.setText(R.id.tv_time, Utils.required(TimeUtils.millis2String(l, new SimpleDateFormat("yy-MM-dd", Locale.getDefault()))));
        viewHolder.setText(R.id.tv_league, Utils.required(data.getLeague()));

        TreeItemGroup parentItem = getParentItem();
        AnalysisRecord.RecordPOListBean bean = (AnalysisRecord.RecordPOListBean) Objects.requireNonNull(parentItem).getData();
        String host_team = PreferenceHelper.getInstance(APP.Companion.get().getApplicationContext())
                .getString("host_team", "");
        String guest_team = PreferenceHelper.getInstance(APP.Companion.get().getApplicationContext())
                .getString("guest_team", "");

        AnalysisRecord.RecordPOListBean.RecordDetailResultBean.ScoreBean score = data.getScore();
        if (score != null) { // 近期
            String name = MessageFormat.format("{0} {1} {2}",
                    Utils.required(score.getHost()),
                    Utils.required(score.getSco(), "VS"),
                    Utils.required(score.getGuest()));

            TextView view = viewHolder.getView(R.id.tv_bf);
            if (!TextUtils.isEmpty(host_team) && !TextUtils.isEmpty(guest_team)) {
                SpannableString keyWordSpan;
                if (bean.getRecord_type() == Key.Analysis.ZKDLSJF
                        || bean.getRecord_type() == Key.Analysis.ZDJQZJ
                        || bean.getRecord_type() == Key.Analysis.ZDWLSS) {

                    keyWordSpan = CharSequenceManager.getKeyWordSpan(0xFFD60000, name, host_team);
                } else if (bean.getRecord_type() == Key.Analysis.KDJQZJ
                        || bean.getRecord_type() == Key.Analysis.KDWLSS) {
                    keyWordSpan = CharSequenceManager.getKeyWordSpan(0xFFD60000, name, guest_team);
                } else {
                    keyWordSpan = CharSequenceManager.getKeyWordSpan(0xFFD60000, name, "@");
                }
                view.setText(keyWordSpan);
            } else {
                view.setText(name);
            }
        }

        AnalysisRecord.RecordPOListBean.RecordDetailResultBean.TeamBean team = data.getTeam();
        if (team != null) { // 未来
            String name = MessageFormat.format("{0} {1} {2}",
                    Utils.required(team.getHost()),
                    Utils.required(team.getSco(), "VS"),
                    Utils.required(team.getGuest()));

            TextView view = viewHolder.getView(R.id.tv_bf);
            if (!TextUtils.isEmpty(host_team) && !TextUtils.isEmpty(guest_team)) {
                SpannableString keyWordSpan;
                if (bean.getRecord_type() == Key.Analysis.ZDJQZJ
                        || bean.getRecord_type() == Key.Analysis.ZDWLSS
                        || bean.getRecord_type() == Key.Analysis.ZKDLSJF) {
                    keyWordSpan = CharSequenceManager.getKeyWordSpan(0xFFD60000, name, host_team);
                } else if (bean.getRecord_type() == Key.Analysis.KDJQZJ
                        || bean.getRecord_type() == Key.Analysis.KDWLSS) {

                    keyWordSpan = CharSequenceManager.getKeyWordSpan(0xFFD60000, name, guest_team);
                } else {
                    keyWordSpan = CharSequenceManager.getKeyWordSpan(0xFFD60000, name, "@");
                }
                view.setText(keyWordSpan);
            } else {
                view.setText(name);
            }
        }

        TextView result = viewHolder.getTextView(R.id.tv_result);
        result.setText(Utils.required(data.getResult()));
        if (!TextUtils.isEmpty(data.getResult())) {
            if (TextUtils.equals(data.getResult(), "胜")) {
                result.setTextColor(0xFFD60000);
            } else if (TextUtils.equals(data.getResult(), "平")) {
                result.setTextColor(0xFF0D84C6);
            } else if (TextUtils.equals(data.getResult(), "负")) {
                result.setTextColor(0xFF009D48);
            }
        }
    }

}
