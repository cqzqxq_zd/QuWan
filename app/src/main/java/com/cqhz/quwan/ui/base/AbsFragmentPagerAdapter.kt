package com.cqhz.quwan.ui.base

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * FragmentPagerAdapter 抽象封装类
 * Created by WYZ on 2018/3/21.
 */
class AbsFragmentPagerAdapter(
        fm: FragmentManager?,
        private val frags:List<Fragment>
) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return frags[position]
    }

    override fun getCount(): Int {
        return frags.size
    }
}