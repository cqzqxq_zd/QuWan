package com.cqhz.quwan.ui.widget.dialog;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cqhz.quwan.R;

/**
 * 中奖弹出窗口
 * Created by Guojing on 2018/10/22.
 */
public class PrizeDialog {

    private Context context;
    private Dialog dialog;
    private LinearLayout llPrizeDialog;
    private View actionClose;
    private TextView tvMsg;
    private TextView tvBean;
    private ImageView actionClosed;
    private Button actionConfirm;// 确认按钮

    public PrizeDialog(Context context) {
        this.context = context;
    }

    public PrizeDialog builder() {
        // 获取Dialog布局
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_prize, null);
        view.setMinimumWidth(10000);// 设置dialog全屏显示

        // 获取自定义Dialog布局中的控件
        llPrizeDialog = view.findViewById(R.id.ll_prize_dialog);
        actionClose = view.findViewById(R.id.action_close);
        tvMsg = view.findViewById(R.id.tv_msg);
        tvBean = view.findViewById(R.id.tv_bean);
        actionClosed = view.findViewById(R.id.action_closed);
        actionConfirm = view.findViewById(R.id.action_confirm);

        actionClose.setOnClickListener(v -> dialog.dismiss());
        actionClosed.setOnClickListener(v -> dialog.dismiss());

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.DialogStyle);
        dialog.setContentView(view);
        llPrizeDialog.setLayoutParams(new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

        return this;
    }

    /**
     * 设置内容
     *
     * @param msg
     * @return
     */
    public PrizeDialog setMsg(@NonNull CharSequence msg) {
        tvMsg.setText("".equals(msg) ? "恭喜您中了" : msg);
        return this;
    }

    /**
     * 设置趣豆数
     *
     * @param bean
     * @return
     */
    public PrizeDialog setBean(@NonNull CharSequence bean) {
        tvBean.setText("".equals(bean) ? "0.00" : bean);
        return this;
    }

    /**
     * 设置按钮文字和事件
     *
     * @param text
     * @param listener
     * @return
     */
    public PrizeDialog setActionConfirm(@NonNull CharSequence text, @NonNull final View.OnClickListener listener) {
        actionConfirm.setText("".equals(text) ? "查看中奖订单" : text);
        actionConfirm.setOnClickListener(v ->
        {
            listener.onClick(v);
            dialog.dismiss();
        });
        return this;
    }

    /**
     * 显示
     */
    public void show() {
        dialog.setCancelable(false);
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

}
