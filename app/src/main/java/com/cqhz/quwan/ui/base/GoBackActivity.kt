package com.cqhz.quwan.ui.base

import com.cqhz.quwan.R

abstract class GoBackActivity : AbsTitleActivity(){

    override fun titleBarLeftIconResId(): Int? {
        return R.drawable.icon_back
    }

    override fun onTitleBarLeftClick() {
        finish()
    }
}