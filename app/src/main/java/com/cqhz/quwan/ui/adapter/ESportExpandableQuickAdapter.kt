package com.cqhz.quwan.ui.adapter

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.cqhz.quwan.R
import com.cqhz.quwan.model.ESportBean
import com.cqhz.quwan.model.ESportItemBean
import com.cqhz.quwan.ui.interfaces.SelectTeamClickListener
import com.cqhz.quwan.util.formatTime2Show
import com.cqhz.quwan.util.setLoadImage

/**
 * 电竞赛事列表数据适配器
 * Created by Guojing on 2018/11/07.
 */
class ESportExpandableQuickAdapter(context: Context, data: List<MultiItemEntity>, listener: SelectTeamClickListener) : BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder>(data) {

    private var context: Context = context
    private var listener: SelectTeamClickListener = listener

    init {
        addItemLayout()
    }

    /**
     * 绑定不同布局
     */
    private fun addItemLayout() {
        addItemType(TYPE_LEVEL_0, R.layout.item_esport_head)
        addItemType(TYPE_LEVEL_1, R.layout.item_esport)
    }

    /**
     * Implement this method and use the helper to adapt the view to the given item.
     *
     * @param helper A fully initialized helper.
     * @param item   The item that needs to be displayed.
     */
    override fun convert(helper: BaseViewHolder, item: MultiItemEntity) {
        when (helper.itemViewType) {
            TYPE_LEVEL_0 -> setGroupData(helper, item as ESportBean)
            TYPE_LEVEL_1 -> setChildData(helper, item as ESportItemBean)
        }
    }

    /**
     * 设置组信息
     * @param item
     */
    private fun setGroupData(helper: BaseViewHolder, item: ESportBean) {
        if (item.score!!.isNotEmpty()) {
            val scores = item!!.score!!.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            helper.setText(R.id.tv_host_score, scores[0])
            helper.setText(R.id.tv_guest_score, scores[1])
            Log.e("groupBean：" + item.leftTeam + item.rightTeam, item.score)
        } else {
            helper.setText(R.id.tv_host_score, "")
            helper.setText(R.id.tv_guest_score, "")
        }

        helper.setGone(R.id.iv_host_victory, item.result == "1")
        helper.setGone(R.id.iv_guest_victory, item.result == "2")

        helper.getView<ImageView>(R.id.iv_host_logo).setLoadImage(context, item.leftTeamLogo, R.drawable.esports_img_default)// 主队logo
        helper.getView<ImageView>(R.id.iv_guest_logo).setLoadImage(context, item.rightTeamLogo, R.drawable.esports_img_default)// 客队logo

        helper.setText(R.id.tv_host_name, item.leftTeam)
        helper.setText(R.id.tv_guest_name, item.rightTeam)
        helper.setText(R.id.tv_game_info, item.matchName)
        helper.setText(R.id.tv_game_round, "BO" + item.round)

        var tvGameStatus = helper.getView<TextView>(R.id.tv_game_status)
        tvGameStatus!!.text = ""
        tvGameStatus!!.setTextColor(Color.parseColor("#333333"))
        tvGameStatus!!.setBackgroundResource(R.drawable.sp_gray_x12)
        when {
            item.betStatus == "0" -> {
                tvGameStatus!!.text = "可预测"
                tvGameStatus!!.setTextColor(Color.parseColor("#FFFFFF"))
                tvGameStatus!!.setBackgroundResource(R.drawable.sp_orange_x12)
            }
            item.betStatus != "0" && item.status == "0" -> {
                tvGameStatus!!.text = "未开始"
                tvGameStatus!!.setTextColor(Color.parseColor("#333333"))
                tvGameStatus!!.setBackgroundResource(R.drawable.sp_gray_x12)
            }
            item.betStatus != "0" && item.status == "1" -> {
                tvGameStatus!!.text = "进行中"
                tvGameStatus!!.setTextColor(Color.parseColor("#FF513A"))
                tvGameStatus!!.setBackgroundResource(R.drawable.sp_stroke_orange_x12)
            }
            item.betStatus != "0" && item.status == "2" -> {
                tvGameStatus!!.text = "已结束"
                tvGameStatus!!.setTextColor(Color.parseColor("#333333"))
                tvGameStatus!!.setBackgroundResource(R.drawable.sp_gray_x12)
            }
            item.betStatus != "0" && item.status == "3" -> {
                tvGameStatus!!.text = "已取消"
                tvGameStatus!!.setTextColor(Color.parseColor("#333333"))
                tvGameStatus!!.setBackgroundResource(R.drawable.sp_gray_x12)
            }
            else -> {
                tvGameStatus!!.text = "未知"
                tvGameStatus!!.setTextColor(Color.parseColor("#333333"))
                tvGameStatus!!.setBackgroundResource(R.drawable.sp_gray_x12)
            }
        }
        helper.setText(R.id.tv_game_time, item.matchTime!!.formatTime2Show(item!!.localTimestamp!!))

        helper.itemView.setOnClickListener {
            for (i in 0 until data.size) {
                var pos = helper.adapterPosition
                if (pos == i && !item.isExpanded) {
                    expand(i)
                } else {
                    collapse(i)
                }
            }
        }
    }

    /**
     * 设置子项展开信息
     * @param item
     */
    private fun setChildData(helper: BaseViewHolder, item: ESportItemBean) {
        if (item.showScore == "0" && item.score!!.isNotEmpty()) {
            val scores = item.score!!.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            helper.setText(R.id.tv_host_score, scores[0])
            helper.setText(R.id.tv_guest_score, scores[1])
        } else {
            helper.setText(R.id.tv_host_score, "")
            helper.setText(R.id.tv_guest_score, "")
        }

        // 结果 1:左队胜 2:右队胜
        helper.setGone(R.id.iv_host_victory, item.result == "1")
        helper.setGone(R.id.iv_guest_victory, item.result == "2")

        if (item.showOdds == "1") {
            helper.setText(R.id.tv_host_odds, item.playOddsMap!!["1"]!!["partOdds"]!!)
            helper.setText(R.id.tv_guest_odds, item.playOddsMap!!["2"]!!["partOdds"]!!)
            helper.setText(R.id.tv_host_odds_des, "赔率")
            helper.setText(R.id.tv_guest_odds_des, "赔率")
        } else {
            helper.setText(R.id.tv_host_odds, "")
            helper.setText(R.id.tv_guest_odds, "")
            helper.setText(R.id.tv_host_odds_des, "")
            helper.setText(R.id.tv_guest_odds_des, "")
        }

        // 玩法类型
        var tvGameDesc = helper.getView<TextView>(R.id.tv_game_desc)
        var tvGameTime = helper.getView<TextView>(R.id.tv_game_time)
        tvGameDesc!!.text = item.playName
        tvGameDesc!!.setTextColor(Color.parseColor("#333333"))
        tvGameDesc!!.setBackgroundResource(R.drawable.sp_gray_x12)
        when (item.status) {
            "0" -> {
                tvGameDesc!!.setTextColor(Color.parseColor("#ffffff"))
                tvGameDesc!!.setBackgroundResource(R.drawable.sp_orange_x12)
                tvGameTime!!.text = "未开始"
            }
            "1" -> {
                tvGameDesc!!.setTextColor(Color.parseColor("#FF513A"))
                tvGameDesc!!.setBackgroundResource(R.drawable.sp_stroke_orange_x12)
                tvGameTime!!.text = "进行中"
            }
            "2" -> {
                tvGameDesc!!.setTextColor(Color.parseColor("#333333"))
                tvGameDesc!!.setBackgroundResource(R.drawable.sp_gray_x12)
                tvGameTime!!.text = "已结束"
            }
            "3" -> {
                tvGameDesc!!.setTextColor(Color.parseColor("#333333"))
                tvGameDesc!!.setBackgroundResource(R.drawable.sp_gray_x12)
                tvGameTime!!.text = "已取消"
            }
            else -> {
                tvGameDesc!!.setTextColor(Color.parseColor("#333333"))
                tvGameDesc!!.setBackgroundResource(R.drawable.sp_gray_x12)
                tvGameTime!!.text = "未知"
            }
        }

        when {
            item.playType == "2" -> {
                tvGameDesc!!.text = "让分局 " + (if (item.handicapTeam == "1") item.leftTeam else item.rightTeam) + " " + item.handicap
            }
            else -> {
                var title = ""
                // type 游戏类型 0:全部 1:LOL 2:DOTA2 3:CSGO 4:守望先锋 5:篮球 6:足球 7:王者荣耀 8:绝地求生 9:魔兽争霸3 10:炉石传说 11:彩虹6号  12:星际争霸1 13:星际争霸2 14:风暴英雄 15:堡垒之夜 16:FIFA Online 17:穿越火线 18:传说对决 19:使命召唤 20:火箭联盟 21:NBA2K'
                when (item.type) {
                    "5", "21" -> {
                        title = when (item.round) {
                            "0.5" -> "上半场 "
                            "1.5" -> "下半场 "
                            "1" -> "第一节 "
                            "2" -> "第二节 "
                            "3" -> "第三节 "
                            "4" -> "第四节 "
                            else -> ""
                        }
                    }
                    "6", "16" -> {
                        title = when (item.round) {
                            "0.5" -> "上半场 "
                            "1.5" -> "下半场 "
                            else -> ""
                        }
                    }
                    else -> {
                        title = when (item.round) {
                            "1" -> "第一局 "
                            "2" -> "第二局 "
                            "3" -> "第三局 "
                            "4" -> "第四局 "
                            "5" -> "第五️局 "
                            "6" -> "第六局 "
                            "7" -> "第七局 "
                            "8" -> "第八局 "
                            "9" -> "第九局 "
                            "10" -> "第十局 "
                            else -> ""
                        }
                    }
                }
                tvGameDesc!!.text = title + item.playName
            }
        }

        helper.itemView.setOnClickListener {
            var pos = helper.adapterPosition
            listener?.onTeamClick(pos, item)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        when (holder.itemViewType) {
            TYPE_LEVEL_0, TYPE_LEVEL_1 -> setFullSpan(holder)
        }
    }

    companion object {
        const val TYPE_LEVEL_0 = 0
        const val TYPE_LEVEL_1 = 1
    }
}