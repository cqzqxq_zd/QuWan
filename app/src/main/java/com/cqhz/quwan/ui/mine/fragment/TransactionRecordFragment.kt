package com.cqhz.quwan.ui.mine.fragment

import android.widget.ListView
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.model.TransactionRecordBean
import com.cqhz.quwan.service.CashService
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.ui.base.AbsFragment
import com.cqhz.quwan.ui.widget.StateLayout
import com.cqhz.quwan.util.findView
import com.cqhz.quwan.util.formatMoney
import me.militch.quickcore.core.HasDaggerInject
import me.militch.quickcore.mvp.model.ModelHelper
import javax.inject.Inject

class TransactionRecordFragment : AbsFragment(), HasDaggerInject<ActivityInject> {
    private val stateLayout by findView<StateLayout>(R.id.stateLayout)
    private val mListView by findView<ListView>(R.id.lv_content)
    private val refreshLayout by findView<SmartRefreshLayout>(R.id.refreshLayout)
    private lateinit var mAdapter: AbsAdapter<TransactionRecordBean>
    @Inject
    lateinit var modelHelper: ModelHelper
    private var pageIndex = 1
    private var visible = false
    private var create = false

    companion object {
        fun new() = TransactionRecordFragment()
    }

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun layout(): Int {
        return R.layout.layout_listview
    }

    override fun initView() {
        mAdapter = object : AbsAdapter<TransactionRecordBean>(
                context(), R.layout.item_bean_record) {
            override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: TransactionRecordBean?) {
                viewHolder.setText(R.id.tv_time, itemData?.createTime ?: "0000-00-00")
                viewHolder.setText(R.id.tv_behavior, itemData?.behavior)
                viewHolder.setText(R.id.tv_pay_amount, when (itemData?.type ?: "-1") {
                    "4", "8", "12" -> "-" + itemData?.payAmount!!.formatMoney() ?: "0.00"
                    "6" -> itemData?.payAmount!!.formatMoney() ?: "0.00"
                    else -> "+" + itemData?.payAmount!!.formatMoney() ?: "0.00"
                })
                viewHolder.setTextColor(R.id.tv_pay_amount, when (itemData?.type ?: "-1") {
                    "4", "6", "8", "12" -> 0XFF3333330.toInt()
                    else -> 0XFFd60000.toInt()
                })
                viewHolder.setText(R.id.tv_type, when (itemData?.type ?: "-1") {
                    "1" -> "充值"
                    "2" -> "提现"
                    "3" -> "奖金"
                    "4" -> "竞猜"
                    "5" -> "退款"
                    "6" -> "佣金"
                    "7" -> "赠送"
                    "8" -> "换钻"
                    "9" -> "换物"
                    "10" -> "抽奖"
                    "11" -> "兑换"
                    "12" -> "打赏"
                    else -> "其他"
                })
            }
        }
        refreshLayout?.setOnLoadMoreListener {
            requestByPage(++pageIndex)
        }
        refreshLayout?.setOnRefreshListener {
            pageIndex = 1
            requestByPage(pageIndex)
        }
        mListView?.adapter = mAdapter
        create = true
        lazyLoad()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        visible = isVisibleToUser
        lazyLoad()
    }

    fun lazyLoad() {
        if (visible && create) {
            pageIndex = 1
            requestByPage(1)
        }
    }

    private fun requestByPage(pageNum: Int) {
        val userId = APP.get()!!.loginInfo?.userId
        modelHelper.request(
                modelHelper.getService(CashService::class.java)
                        ?.transactionRecord(userId ?: "", pageNum, 10), {
            if (pageNum == 1 && it?.records?.isEmpty() == true) {
                stateLayout?.showDataNoneLayout()
            } else if (pageNum == 1) {
                mAdapter.setData(it?.records)
            } else {
                mAdapter.addData(it?.records)
            }
            if (refreshLayout?.state?.isHeader!!) {
                refreshLayout?.finishRefresh()
            }
            if (refreshLayout?.state?.isFooter!!) {
                refreshLayout?.finishLoadMore()
            }
        }, {
            showRequestError(it)
            if (refreshLayout?.state?.isHeader!!) {
                refreshLayout?.finishRefresh(false)
            }
            if (refreshLayout?.state?.isFooter!!) {
                refreshLayout?.finishLoadMore(false)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        modelHelper.unBind()
    }
}