package com.cqhz.quwan.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.GridView
import com.cqhz.quwan.R

/**
 * 自适应高度的 GridView
 * Created by WYZ on 2018/3/19.
 */
class AutoHeightFormatGridView(
        context: Context?,
        attrs: AttributeSet?) :
        GridView(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE shr 2, MeasureSpec.AT_MOST)
        super.onMeasure(widthMeasureSpec, height)
    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        for (childIndex in 0..childCount){
            val layout= getChildAt(childIndex) as FrameLayout?
            val btl = layout?.findViewById<FrameLayout>(R.id.bet_type_layout)
            val lp = btl?.layoutParams as FrameLayout.LayoutParams?
//            val tv = btl?.findViewById<TextView>(R.id.bet_type_text)
            if (childIndex%numColumns == 0){
                lp?.gravity = Gravity.END
                lp?.setMargins(0,0,10,0)
                btl?.layoutParams = lp
            }else if ((childIndex+1)%numColumns == 0){
                lp?.gravity = Gravity.START
                lp?.setMargins(10,0,0,0)
            }
            btl?.layoutParams = lp
        }
    }
}