package com.cqhz.quwan.ui.widget

import com.chad.library.adapter.base.loadmore.LoadMoreView
import com.cqhz.quwan.R

class LoadMoreDoneView : LoadMoreView() {
    /**
     * load more layout
     *
     * @return
     */
    override fun getLayoutId(): Int {
        return R.layout.layout_load_end
    }

    /**
     * loading view
     *
     * @return
     */
    override fun getLoadingViewId(): Int {
        return R.id.tv_end_tips
    }

    /**
     * load end view, you can return 0
     *
     * @return
     */
    override fun getLoadEndViewId(): Int {
        return R.id.tv_end_tips
    }

    /**
     * load fail view
     *
     * @return
     */
    override fun getLoadFailViewId(): Int {
        return R.id.tv_end_tips
    }
}