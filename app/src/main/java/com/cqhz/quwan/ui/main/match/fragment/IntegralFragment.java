package com.cqhz.quwan.ui.main.match.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RadioGroup;

import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.base.AbsFragment;
import com.cqhz.quwan.ui.main.match.common.Key;
import com.cqhz.quwan.ui.main.match.fragment.child.IntegralInfoFragment;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * 积分
 *
 * @author whamu2
 * @date 2018/7/17
 */
public class IntegralFragment extends AbsFragment implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener {
    private static final String KEY_INTEGRAL = "integral";
    private static final String KEY_LEAGUE = "league";

    private SegmentedGroup mSegmentedGroup;
    private ViewPager mViewPager;

    private String code;
    private String leagueId;

    public static IntegralFragment newInstance(String code,String leagueId) {
        Bundle args = new Bundle();
        args.putString(KEY_INTEGRAL, code);
        args.putString(KEY_LEAGUE, leagueId);
        IntegralFragment fragment = new IntegralFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int layout() {
        return R.layout.fragment_match_integral;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_INTEGRAL);
            leagueId = getArguments().getString(KEY_LEAGUE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSegmentedGroup = view.findViewById(R.id.sg);
        mViewPager = view.findViewById(R.id.viewpager);
        mSegmentedGroup.setOnCheckedChangeListener(this);

        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(IntegralInfoFragment.newInstance(code, leagueId, Key.IntegralType.ALL));
        adapter.addFragment(IntegralInfoFragment.newInstance(code, leagueId, Key.IntegralType.HOST));
        adapter.addFragment(IntegralInfoFragment.newInstance(code, leagueId, Key.IntegralType.GUEST));
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(this);
        mSegmentedGroup.check(R.id.rb_all);
        mViewPager.setCurrentItem(0);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_all:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.rb_host:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.rb_guest:
                mViewPager.setCurrentItem(2);
                break;
            default:
                mViewPager.setCurrentItem(0);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                mSegmentedGroup.check(R.id.rb_all);
                break;
            case 1:
                mSegmentedGroup.check(R.id.rb_host);
                break;
            case 2:
                mSegmentedGroup.check(R.id.rb_guest);
                break;
            default:
                mSegmentedGroup.check(R.id.rb_all);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private static class PagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments = new ArrayList<>();

        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        void addFragment(Fragment fragment) {
            fragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
