package com.cqhz.quwan.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.ListView

class AutoHeightListView(
        context: Context?,
        attrs: AttributeSet?) :
        ListView(context, attrs) {
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE shr 2, MeasureSpec.AT_MOST)
        super.onMeasure(widthMeasureSpec, height)
    }
}