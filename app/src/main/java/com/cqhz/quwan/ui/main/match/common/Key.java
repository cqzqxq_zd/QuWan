package com.cqhz.quwan.ui.main.match.common;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class Key {

    /**
     * 赛事状态0：未开始; 1：进行中;2：暂停;3：已完成;4：推迟;5：取消
     */
    public class MatchState {
        public static final int STATE_NO = 0;
        public static final int STATE_START = 1;
        public static final int STATE_PAUSE = 2;
        public static final int STATE_COMPLATE = 3;
        public static final int STATE_DELAY = 4;
        public static final int STATE_CANCEL = 5;
    }

    /**
     * 积分类型 0-总积分；1-主场积分；2-客场积分
     */
    public class IntegralType {
        public static final int ALL = 0;
        public static final int HOST = 1;
        public static final int GUEST = 2;

    }

    /**
     * 投票类型(0-主胜,1-平,3-客胜；)
     */
    public class Analysis {
        public static final int VOTE_DEFAULT = -1;
        public static final int VOTE_HOST = 0;
        public static final int VOTE_LEVEL = 1;
        public static final int VOTE_GUEST = 3;

        /**
         * 主客队历史交锋
         */
        public static final int ZKDLSJF = 0;
        /**
         * 主队近期战绩
         */
        public static final int ZDJQZJ = 1;
        /**
         * 主队未来赛事
         */
        public static final int ZDWLSS = 2;
        /**
         * 客队近期战绩
         */
        public static final int KDJQZJ = 3;
        /**
         * 客队未来赛事
         */
        public static final int KDWLSS = 4;
    }

    /**
     * 赔率类型(0-亚盘,1-欧赔,2-大小球)
     */
    public class Odds {
        public static final int ASIA = 0;
        public static final int EUROPE = 1;
        public static final int BALL = 2;

        public static final String UP = "up";
        public static final String DOWN = "down";
        public static final String EQUAL = "equal";
    }
}
