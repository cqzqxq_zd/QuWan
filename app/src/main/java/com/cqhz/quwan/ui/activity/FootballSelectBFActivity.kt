package com.cqhz.quwan.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.CheckBox
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.BallMatchBean
import com.cqhz.quwan.util.getCheckBoxTextValue2
import com.cqhz.quwan.util.getCheckedStyleText
import com.cqhz.quwan.util.getMapKeyByPosition
import kotlinx.android.synthetic.main.activity_football_select_bf.*
import me.militch.quickcore.core.HasDaggerInject

/**
 * 足彩混合投注选择页面
 * Created by Guojing on 2018/11/19.
 */
class FootballSelectBFActivity : Activity(), HasDaggerInject<ActivityInject> {
    private lateinit var ballMatchBean: BallMatchBean
    private var checkBoxes = ArrayList<CheckBox>()

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_football_select_bf)
        addCheckBox()
        initView()
    }

    private fun addCheckBox() {
        checkBoxes.add(action_select0)
        checkBoxes.add(action_select1)
        checkBoxes.add(action_select2)
        checkBoxes.add(action_select3)
        checkBoxes.add(action_select4)
        checkBoxes.add(action_select5)
        checkBoxes.add(action_select6)
        checkBoxes.add(action_select7)
        checkBoxes.add(action_select8)
        checkBoxes.add(action_select9)
        checkBoxes.add(action_select10)
        checkBoxes.add(action_select11)
        checkBoxes.add(action_select12)
        checkBoxes.add(action_select13)
        checkBoxes.add(action_select14)
        checkBoxes.add(action_select15)
        checkBoxes.add(action_select16)
        checkBoxes.add(action_select17)
        checkBoxes.add(action_select18)
        checkBoxes.add(action_select19)
        checkBoxes.add(action_select20)
        checkBoxes.add(action_select21)
        checkBoxes.add(action_select22)
        checkBoxes.add(action_select23)
        checkBoxes.add(action_select24)
        checkBoxes.add(action_select25)
        checkBoxes.add(action_select26)
        checkBoxes.add(action_select27)
        checkBoxes.add(action_select28)
        checkBoxes.add(action_select29)
        checkBoxes.add(action_select30)
    }

    fun initView() {
        ballMatchBean = intent.getParcelableExtra<BallMatchBean>(KeyContract.Bean)
        setData()
        setOnClick()
    }

    /**
     * 设置控件的数据
     */
    private fun setData() {
        tv_host_team.text = ballMatchBean.hostTeam
        tv_guest_team.text = ballMatchBean.guestTeam

        val detailStatus = ballMatchBean.detailStatus?.split(',')?.map {
            try {
                it.toInt()
            } catch (e: NumberFormatException) {
                1
            }
        }
        var isEnable = detailStatus[2] == 0

        for (i in 0 until checkBoxes.size) {
            checkBoxes[i].isEnabled = isEnable
            checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
            var realPosition = i.getMapKeyByPosition(true)
            checkBoxes[i].isChecked = ballMatchBean.selected!![realPosition] == true
            checkBoxes[i].text = if (isEnable) i.getCheckedStyleText(this, true, ballMatchBean.oddsMap, checkBoxes[i].isChecked, 13, 12) else "未开售"
            checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                ballMatchBean.selected!![realPosition] = isChecked
                checkBoxes[i].text = if (isEnable) i.getCheckedStyleText(this, true, ballMatchBean.oddsMap, checkBoxes[i].isChecked, 13, 12) else "未开售"

                var selectedNum = 0
                var sbLabel = StringBuffer()
                for (j in 0 until ballMatchBean.selected!!.size) {
                    if (ballMatchBean.selected!![j] == true) {
                        selectedNum++
                        sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                    }
                }
                ballMatchBean.selectedNumber = selectedNum
                ballMatchBean.selectedLabel = sbLabel.toString().substring(0, if (ballMatchBean.selectedNumber > 0) sbLabel.length - 2 else 0)
            }
        }
    }

    /**
     * 设置控件的点击事件
     */
    private fun setOnClick() {
        action_closed.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        action_cancel.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        action_confirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(KeyContract.Bean, ballMatchBean)
            setResult(RESULT_OK, intent)
            finish()
            overridePendingTransition(0, 0)
        }
    }
}
