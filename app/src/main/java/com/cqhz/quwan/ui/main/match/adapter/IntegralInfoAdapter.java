package com.cqhz.quwan.ui.main.match.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cqhz.quwan.APP;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.AnalysisIntegral;
import com.cqhz.quwan.ui.base.BaseRecyclerViewAdapter;
import com.cqhz.quwan.ui.main.match.common.Utils;
import com.cqhz.quwan.util.PreferenceHelper;

import java.text.MessageFormat;

/**
 * 积分详情适配器
 *
 * @author whamu2
 * @date 2018/7/17
 */
public class IntegralInfoAdapter extends BaseRecyclerViewAdapter<AnalysisIntegral.PointBean.PointResultBean, IntegralInfoAdapter.ViewHolder> {

    public IntegralInfoAdapter(Context c) {
        super(c);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_integral_info, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position), position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mLayout;
        TextView mTextView1, mTextView2, mTextView3,
                mTextView4, mTextView5, mTextView6, mTextView7;

        public ViewHolder(View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mTextView1 = itemView.findViewById(R.id.tv_1);
            mTextView2 = itemView.findViewById(R.id.tv_2);
            mTextView3 = itemView.findViewById(R.id.tv_3);
            mTextView4 = itemView.findViewById(R.id.tv_4);
            mTextView5 = itemView.findViewById(R.id.tv_5);
            mTextView6 = itemView.findViewById(R.id.tv_6);
            mTextView7 = itemView.findViewById(R.id.tv_7);
        }

        void bind(AnalysisIntegral.PointBean.PointResultBean item, int position) {
            String host_team = PreferenceHelper.getInstance(APP.Companion.get().getApplicationContext())
                    .getString("host_team", "");
            String guest_team = PreferenceHelper.getInstance(APP.Companion.get().getApplicationContext())
                    .getString("guest_team", "");
            if (TextUtils.equals(host_team, item.getTeam()) || TextUtils.equals(guest_team, item.getTeam())) {
                mLayout.setBackgroundColor(0xFFF7D572);
            } else {
                mLayout.setBackgroundColor(0xFFFFFFFF);
            }

            mTextView1.setText(Utils.required(item.getRank()));
            mTextView2.setText(Utils.required(item.getTeam()));
            mTextView3.setText(Utils.required(item.getMatchTimes()));
            mTextView4.setText(MessageFormat.format("{0}/{1}/{2}", Utils.required(item.getWin()), Utils.required(item.getDraw()), Utils.required(item.getLose())));
            mTextView5.setText(MessageFormat.format("{0}/{1}", Utils.required(item.getGoal()), Utils.required(item.getLose_goal())));
            mTextView6.setText(Utils.required(item.getProfit()));
            mTextView7.setText(Utils.required(item.getScore()));
        }
    }
}
