package com.cqhz.quwan.ui.activity

import android.content.Intent
import android.graphics.Color
import android.view.View
import cn.jpush.android.e.a.b.showToast
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.R.id.iv_code
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.CodeBean
import com.cqhz.quwan.model.ESportOrderBetInfoBean
import com.cqhz.quwan.model.OrderInfoBean
import com.cqhz.quwan.mvp.OrderDetailContract
import com.cqhz.quwan.mvp.OrderDetailPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.util.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_esport_order_detail.*
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject

/**
 * 电竞订单详情
 * Created by Guojing on 2018/11/29.
 */
class ESportOrderDetailActivity : GoBackActivity(), OrderDetailContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var orderDetailPresenter: OrderDetailPresenter
    private var orderInfo: OrderInfoBean? = null
    private var orderId = ""
    private var playType = ""

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun titleBarText(): String? {
        return "订单详情"
    }

    override fun layout(): Int {
        return R.layout.activity_esport_order_detail
    }

    override fun initView() {
        orderDetailPresenter.attachView(this)
        orderId = intent.getStringExtra(KeyContract.OrderId)
        orderDetailPresenter.getOrderDetail(orderId)
        if (APP.get()!!.loginInfo != null) {
            orderDetailPresenter.getCodeImg(APP.get()!!.loginInfo!!.userId!!)
        }
        setOnClick()
    }


    /**
     * 设置点击事件
     */
    private fun setOnClick() {
        setTitleBarRightText("截图") {
            val bitmap = getNestedScrollViewBitmap(sv_content)
            bitmap.saveImage("天天趣玩", "${System.currentTimeMillis()}.jpg") { it ->
                scanPhoto(it)
                showToast("保存成功: $it")
            }
        }
        action_cancel.setOnClickListener {
            orderDetailPresenter.updateOrderStatus(orderId, "7")// 取消订单
        }
        action_commit.setOnClickListener {
            val intent = Intent(this, GoPaymentActivity::class.java)
            intent.putExtra(KeyContract.Amount, orderInfo!!.payAmount?.toDouble())
            intent.putExtra(KeyContract.ActualAmount, orderInfo!!.payAmount?.toDouble())
            intent.putExtra(KeyContract.OrderId, orderInfo!!.id)
            intent.putExtra(KeyContract.LotteryName, playType)
            startActivity(intent)
        }
    }

    override fun setOrderDetail(bean: OrderInfoBean) {
        val orderBetInfo = Gson().fromJson(bean.orderBetJson, object : TypeToken<ESportOrderBetInfoBean>() {}.type) as? ESportOrderBetInfoBean
        var orderStatus = ""
        ll_operation.visibility = View.GONE
        when (bean.status) {
            0, 1 -> {
                orderStatus = "待下单"
                ll_operation.visibility = View.VISIBLE
            }
            2 -> {
                orderStatus = "待开奖"
            }
            3 -> orderStatus = "未中奖"
            4 -> orderStatus = "已中奖"
            5 -> orderStatus = "待领奖"
            6 -> orderStatus = "已领奖"
            7 -> orderStatus = "已取消"
            9 -> orderStatus = "已下单"
            else -> orderStatus = "已失效"
        }
        tv_order_status.text = orderStatus

        var gameName = when (orderBetInfo!!.type) {
            1 -> "LOL"
            2 -> "DOTA2"
            3 -> "CSGO"
            4 -> "守望先锋"
            5 -> "篮球"
            6 -> "足球"
            7 -> "王者荣耀"
            8 -> "绝地求生"
            9 -> "魔兽争霸3"
            10 -> "炉石传说"
            11 -> "彩虹6号"
            12 -> "星际争霸1"
            13 -> "星际争霸2"
            14 -> "风暴英雄"
            15 -> "堡垒之夜"
            16 -> "FIFA Online"
            17 -> "穿越火线"
            18 -> "传说对决"
            19 -> "使命召唤"
            20 -> "火箭联盟"
            21 -> "NBA2K"
            else -> "电子竞技"
        }

        iv_ball.setImageResource(when (orderBetInfo!!.type) {
            5, 6 -> R.drawable.order_logo_hlcq
            else -> R.drawable.order_logo_esports
        })
        iv_prize.visibility = when (bean.status) {
            4, 5, 6 -> View.VISIBLE
            else -> View.GONE
        }
        tv_game_type_title.text = when (orderBetInfo!!.type) {
            5, 6 -> "欢乐猜球"
            else -> "电子竞技"
        }
        tv_game_name.text = gameName
        playType = gameName + " " + orderBetInfo.matchName
        tv_match_name.text = playType
        tv_game_round.text = orderBetInfo.round

        var title = ""
        // type 游戏类型 0:全部 1:LOL 2:DOTA2 3:CSGO 4:守望先锋 5:篮球 6:足球 7:王者荣耀 8:绝地求生 9:魔兽争霸3 10:炉石传说 11:彩虹6号  12:星际争霸1 13:星际争霸2 14:风暴英雄 15:堡垒之夜 16:FIFA Online 17:穿越火线 18:传说对决 19:使命召唤 20:火箭联盟 21:NBA2K'
        when (orderBetInfo.type) {
            5, 21 -> {
                title = when (orderBetInfo.rounds) {
                    "0" -> "全场 "
                    "0.5" -> "上半场 "
                    "1.5" -> "下半场 "
                    "1" -> "第一节 "
                    "2" -> "第二节 "
                    "3" -> "第三节 "
                    "4" -> "第四节 "
                    else -> ""
                }
            }
            6, 16 -> {
                title = when (orderBetInfo.rounds) {
                    "0" -> "全场 "
                    "0.5" -> "上半场 "
                    "1.5" -> "下半场 "
                    else -> ""
                }
            }
            else -> {
                title = when (orderBetInfo.rounds) {
                    "0" -> "总局 "
                    "1" -> "第一局 "
                    "2" -> "第二局 "
                    "3" -> "第三局 "
                    "4" -> "第四局 "
                    "5" -> "第五️局 "
                    "6" -> "第六局 "
                    "7" -> "第七局 "
                    "8" -> "第八局 "
                    "9" -> "第九局 "
                    "10" -> "第十局 "
                    else -> ""
                }
            }
        }
        tv_match_title.text = title + orderBetInfo.playDescribe

        tv_left_name.text = orderBetInfo.leftTeam
        tv_right_name.text = orderBetInfo.rightTeam
        tv_bet_name.text = orderBetInfo.betDescribe
        tv_odds.text = orderBetInfo.betOdds
        iv_left_logo.setLoadImage(this, orderBetInfo.leftTeamLogo, R.drawable.esports_img_default)// 主队logo
        iv_right_logo!!.setLoadImage(this, orderBetInfo.rightTeamLogo, R.drawable.esports_img_default)// 副队logo

        tv_amount_pay.text = bean.payAmount.formatNoZero()
        tv_amount_profit.text = bean.prize.formatNoZero()
        tv_amount_profit.setTextColor(Color.parseColor(if (bean.prize.toDouble() > 0) "#FF0000" else "#333333"))
        tv_time.text = "下单时间：" + bean.createTime
    }

    override fun setCodeImg(bean: CodeBean) {
        iv_code.setLoadImage(this, bean.image, 0)
    }

    override fun updateSuccess() {
        showToast("取消成功")
        orderDetailPresenter.getOrderDetail(orderId)
    }

    override fun onResume() {
        super.onResume()
        orderDetailPresenter.getOrderDetail(orderId)
    }

    override fun onDestroy() {
        super.onDestroy()
        orderDetailPresenter.detachView()
    }
}
