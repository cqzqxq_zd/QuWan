package com.cqhz.quwan.ui.buy.fragment

import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.model.FootBallEntity
import com.cqhz.quwan.model.LeagueEntity
import com.cqhz.quwan.ui.base.AbsFragment
import me.militch.quickcore.core.HasDaggerInject

abstract class AbsFootBallSelectorFragment : AbsFragment(), HasDaggerInject<ActivityInject> {
    private var selectedChangeListener: ((List<FootBallEntity.Match>) -> Unit)? = null
    fun _selectedListener(): ((List<FootBallEntity.Match>) -> Unit)? {
        return selectedChangeListener
    }

    fun setSelectedChangeListener(l: ((List<FootBallEntity.Match>) -> Unit)?) {
        this.selectedChangeListener = l
    }

    abstract fun clearSelected()
    abstract fun getSelectedData(): List<FootBallEntity>

    abstract fun filterLeague(leagues: List<LeagueEntity>)
}