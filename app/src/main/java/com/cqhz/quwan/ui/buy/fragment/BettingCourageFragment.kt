package com.cqhz.quwan.ui.buy.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.BettingItem
import com.cqhz.quwan.model.Lottery
import com.cqhz.quwan.model.LottoType
import com.cqhz.quwan.ui.base.AbsFragment
import com.cqhz.quwan.ui.buy.BettingMachineActivity
import com.cqhz.quwan.ui.buy.LotteryBettingActivity
import com.cqhz.quwan.ui.buy.adapter.BallAdapter
import com.cqhz.quwan.ui.widget.AutoHeightGridView
import com.cqhz.quwan.util.combine
import com.cqhz.quwan.util.findView

/**
 * 胆拖投注
 */
class BettingCourageFragment:AbsFragment() {

    // 红球-胆码区
    private val redDMLabel by findView<TextView>(R.id.betting_courage_red_dm_label)
    private val redDMTip by findView<TextView>(R.id.betting_courage_red_dm_tip)
    private val redDMGv by findView<AutoHeightGridView>(R.id.red_ball_selector_dm)
    // 红球-拖码区
    private val redTMLabel by findView<TextView>(R.id.betting_courage_red_tm_label)
    private val redTMTip by findView<TextView>(R.id.betting_courage_red_tm_tip)
    private val redTMGv by findView<AutoHeightGridView>(R.id.red_ball_selector_tm)
    // 蓝球-胆码区
    private val blueDMLayout by findView<LinearLayout>(R.id.betting_courage_blue_dm_layout)
    private val blueDMLabel by findView<TextView>(R.id.betting_courage_blue_dm_label)
    private val blueDMTip by findView<TextView>(R.id.betting_courage_blue_dm_tip)
    private val blueDMGv by findView<AutoHeightGridView>(R.id.blue_ball_selector_dm)
    // 蓝球-拖码区
    private val blueTMLayout by findView<LinearLayout>(R.id.betting_courage_blue_tm_layout)
    private val blueTMLabel by findView<TextView>(R.id.betting_courage_blue_tm_label)
    private val blueTMTip by findView<TextView>(R.id.betting_courage_blue_tm_tip)
    private val blueTMGv by findView<AutoHeightGridView>(R.id.blue_ball_selector_tm)
    // 蓝球区
    private val blueLayout by findView<LinearLayout>(R.id.betting_courage_blue_layout)
    private val blueLabel by findView<TextView>(R.id.betting_courage_blue_label)
    private val blueTip by findView<TextView>(R.id.betting_courage_blue_tip)
    private val blueGv by findView<AutoHeightGridView>(R.id.blue_ball_selector)
    private val clearBtn by findView<TextView>(R.id.tv_ball_clear)
    private val confirmBtn by findView<TextView>(R.id.btn_confirm)
    private val resultLabel by findView<TextView>(R.id.label_result)
    private lateinit var redBalls:LottoType.Balls
    private lateinit var blueBalls:LottoType.Balls
    private val redDMSelected = ArrayList<Int>()
    private val redTMSelected = ArrayList<Int>()
    private val blueDMSelected = ArrayList<Int>()
    private val blueTMSelected = ArrayList<Int>()
    private val blueSelected = ArrayList<Int>()
    private lateinit var redDMBallAdapter: BallAdapter
    private lateinit var redTMBallAdapter: BallAdapter
    private lateinit var blueDMBallAdapter: BallAdapter
    private lateinit var blueTMBallAdapter: BallAdapter
    private lateinit var blueBallAdapter: BallAdapter
    private var allStake = 0L

    companion object {
        fun new(redBalls: LottoType.Balls?, blueBalls: LottoType.Balls?):BettingCourageFragment{
            val f = BettingCourageFragment()
            val b = Bundle()
            b.putSerializable(KeyContract.RedBalls,redBalls)
            b.putSerializable(KeyContract.BlueBalls,blueBalls)
            f.arguments = b
            return f
        }
    }
    override fun layout(): Int {
        return R.layout.layout_betting_courage
    }

    override fun initView() {
        redBalls = arguments!![KeyContract.RedBalls] as LottoType.Balls
        blueBalls = arguments!![KeyContract.BlueBalls] as LottoType.Balls
        // 初始化显示红球-胆码区
        redDMLabel?.text = "${redBalls.dmLabel}"
        redDMTip?.text = "${redBalls.dmTip}"
        // 初始化显示红球-拖码区
        redTMLabel?.text = "${redBalls.tmLabel}"
        redTMTip?.text = "${redBalls.tmTip}"
        // 初始化数据适配器
        redDMBallAdapter = BallAdapter(context(),R.layout.item_red_ball)
        redTMBallAdapter = BallAdapter(context(),R.layout.item_red_ball)
        blueDMBallAdapter = BallAdapter(context(),R.layout.item_blue_ball)
        blueTMBallAdapter = BallAdapter(context(),R.layout.item_blue_ball)
        blueBallAdapter = BallAdapter(context(),R.layout.item_blue_ball)
        redDMBallAdapter.selectMax = redBalls.selectDMMax?:-1
        redTMBallAdapter.selectMax = redBalls.selectTMMax?:-1
        redDMGv?.adapter = redDMBallAdapter
        redTMGv?.adapter = redTMBallAdapter
        redDMBallAdapter.listener = {
            val ls = it
            redDMSelected.clear()
            redDMSelected.addAll(ls)
            val news = redTMSelected.filter {
                !ls.contains(it)
            }
            redTMSelected.clear()
            redTMSelected.addAll(news)
            redTMBallAdapter.setChecks(redTMSelected,false)
            resetView()
        }
        redTMBallAdapter.listener = {
            val ls = it
            redTMSelected.clear()
            redTMSelected.addAll(ls)
            val news = redDMSelected.filter {
                !ls.contains(it)
            }
            redDMSelected.clear()
            redDMSelected.addAll(news)
            redDMBallAdapter.setChecks(redDMSelected,false)
            resetView()
        }
        blueDMBallAdapter.listener = {
            val ls = it
            blueDMSelected.clear()
            blueDMSelected.addAll(ls)
            val news = blueTMSelected.filter {
                !ls.contains(it)
            }
            blueTMSelected.clear()
            blueTMSelected.addAll(news)
            blueTMBallAdapter.setChecks(blueTMSelected,false)
            resetView()
        }
        blueTMBallAdapter.listener = {
            val ls = it
            blueTMSelected.clear()
            blueTMSelected.addAll(ls)
            val news = blueDMSelected.filter {
                !ls.contains(it)
            }
            blueDMSelected.clear()
            blueDMSelected.addAll(news)
            blueDMBallAdapter.setChecks(blueDMSelected,false)
            resetView()
        }
        blueBallAdapter.listener = {
            blueSelected.clear()
            blueSelected.addAll(it)
            resetView()
        }
        confirmBtn?.setOnClickListener {
            if (allStake<1){
                showToast("请至少选择一注")
                return@setOnClickListener
            }
            val allRed = redDMSelected.plus(redTMSelected)
            val allBlue = if(blueBalls.selectMin?:0>1){
                blueDMSelected.plus(blueTMSelected)
            }else{
                blueSelected
            }
            val item = BettingItem(allRed,allBlue,Lottery.TYPE_LOTTO_COURAGE,allStake)
            item.redDMIndex = redDMSelected.size -1
            item.redTMIndex = redDMSelected.size + redTMSelected.size - 1
            item.blueDMIndex = if (blueBalls.selectMin?:0>1) blueDMSelected.size-1 else -1
            item.blueTMIndex = if (blueBalls.selectMin?:0>1) blueDMSelected.size + blueTMSelected.size - 1 else -1
            val intent = Intent(context,LotteryBettingActivity::class.java)
            val lotteryId = (activity as BettingMachineActivity).currentLotteryId
            val isReset = (activity as BettingMachineActivity).isReset
            val resetIndex = (activity as BettingMachineActivity).resetIndex
            val periodNum = (activity as BettingMachineActivity).currentPeriodNum
            val lotteryName = (activity as BettingMachineActivity).currentLotteryName
            intent.putExtra(KeyContract.LotteryId,lotteryId)
            intent.putExtra(KeyContract.LotteryName,lotteryName)
            intent.putExtra(KeyContract.PeriodNum,periodNum)
            intent.putExtra(KeyContract.BettingItem, item)
            intent.putExtra(KeyContract.IsReset,isReset)
            intent.putExtra(KeyContract.ResetIndex,resetIndex)
            startActivity(intent)
        }
        clearBtn?.setOnClickListener {
            redDMBallAdapter.clearChecks()
            redTMBallAdapter.clearChecks()
            if (blueBalls.selectMin?:0>1){
                blueDMBallAdapter.clearChecks()
                blueTMBallAdapter.clearChecks()
            }else{
                blueBallAdapter.clearChecks()
            }
        }
        redDMBallAdapter.setData(redBalls.range!!)
        redTMBallAdapter.setData(redBalls.range!!)
        redDMBallAdapter.setChecks(redBalls.dmSelected)
        redTMBallAdapter.setChecks(redBalls.tmSelected)
        if (blueBalls.selectMin?:0>1){
            blueDMLayout?.visibility = View.VISIBLE
            blueTMLayout?.visibility = View.VISIBLE
            blueLayout?.visibility = View.GONE
            // 初始化显示蓝球-胆码区
            blueDMLabel?.text = "${blueBalls.dmLabel}"
            blueDMTip?.text = "${blueBalls.dmTip}"
            blueDMBallAdapter.selectMax = blueBalls.selectDMMax?:-1
            blueDMGv?.adapter = blueDMBallAdapter
            // 初始化显示蓝球-拖码区
            blueTMLabel?.text = "${blueBalls.tmLabel}"
            blueTMTip?.text = "${blueBalls.tmTip}"
            blueTMBallAdapter.selectMax = blueBalls.selectTMMax?:-1
            blueTMGv?.adapter = blueTMBallAdapter
            // 初始化数据适配器
            blueDMBallAdapter.setData(blueBalls.range!!)
            blueTMBallAdapter.setData(blueBalls.range!!)
            blueDMBallAdapter.setChecks(blueBalls.dmSelected)
            blueTMBallAdapter.setChecks(blueBalls.tmSelected)
        }else{
            blueDMLayout?.visibility = View.GONE
            blueTMLayout?.visibility = View.GONE
            blueLayout?.visibility = View.VISIBLE
            blueLabel?.text = "${blueBalls.label}"
            blueTip?.text = "${blueBalls.tip}"
            blueBallAdapter.selectMax = blueBalls.selectMax?:-1
            blueGv?.adapter = blueBallAdapter
            blueBallAdapter.setData(blueBalls.range!!)
            blueBallAdapter.setChecks(blueBalls.selected)
        }
    }

    private fun resetView(){
        val redSelectedSize = redDMSelected.size + redTMSelected.size
        val red = if(redSelectedSize <= redBalls.selectMin?:0){
            0
        }else{
            redTMSelected.size.combine(redBalls.selectMin!! - redDMSelected.size)
        }

        val blue = if(blueBalls.selectMin?:0>1){
            blueTMSelected.size.combine(2-blueDMSelected.size)
        }else{
            blueSelected.size.combine(1)
        }
        allStake = red * blue
        val text = "已选 $allStake 注\n一共 ${allStake*2} 元"
        resultLabel?.text = text
    }
}