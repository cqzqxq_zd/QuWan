package com.cqhz.quwan.ui.mine

import com.blankj.utilcode.util.RegexUtils.isIDCard18
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.mvp.mine.RealNameAuthContract
import com.cqhz.quwan.mvp.mine.RealNameAuthPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.util.isIDCard18
import kotlinx.android.synthetic.main.activity_real_name_auth.*
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject

/**
 * 实名认证
 * Created by Guojing on 2018/9/6.
 */
class RealNameAuthActivity : GoBackActivity(), RealNameAuthContract.View, HasDaggerInject<ActivityInject> {
    @Inject
    lateinit var realNameAuthPresenter: RealNameAuthPresenter

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun layout(): Int {
        return R.layout.activity_real_name_auth
    }

    override fun titleBarText(): String? {
        return "实名认证"
    }

    override fun onDestroy() {
        realNameAuthPresenter.detachView()
        super.onDestroy()
    }

    override fun initView() {
        realNameAuthPresenter.attachView(this)
        action_auth.setOnClickListener {
            val name = et_auth_name.text.toString()
            val idCard = et_auth_id.text.toString()
            when {
                name.length < 2 -> showToast("请输入有效的姓名")
                !idCard.isIDCard18() -> showToast("请输入有效的身份证号码")
                else -> {
                    showLoading("正在上传")
                    realNameAuthPresenter.doAuth(name, idCard)
                }
            }
        }
    }
}