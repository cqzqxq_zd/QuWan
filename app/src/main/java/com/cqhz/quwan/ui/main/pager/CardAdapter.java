package com.cqhz.quwan.ui.main.pager;

/**
 * @author whamu2
 * @date 2018/6/11
 */
public interface CardAdapter {

    int getCount();
}
