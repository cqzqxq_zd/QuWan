package com.cqhz.quwan.ui.main.pager;

import com.cqhz.quwan.model.FocusBean;

/**
 * @author whamu2
 * @date 2018/6/11
 */
public class CardItem {

    private FocusBean data;

    public CardItem(FocusBean data) {
        this.data = data;
    }

    public FocusBean getData() {
        return data;
    }
}
