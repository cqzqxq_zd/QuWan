package com.cqhz.quwan.ui.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.CheckBox
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.BallMatchBean
import com.cqhz.quwan.util.getCheckBoxTextValueLabel
import com.cqhz.quwan.util.getCheckBoxTextValueShow
import com.cqhz.quwan.util.getCheckedStyleText2
import kotlinx.android.synthetic.main.activity_basketball_select_hh.*
import me.militch.quickcore.core.HasDaggerInject


/**
 * 竞猜篮球混合选择页面
 * Created by Guojing on 2018/12/18.
 */
class BasketballSelectHHActivity : Activity(), HasDaggerInject<ActivityInject> {

    private lateinit var ballMatchBean: BallMatchBean
    private var checkBoxes = ArrayList<CheckBox>()


    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basketball_select_hh)
        addCheckBox()
        initView()
    }

    private fun addCheckBox() {
        checkBoxes.add(action_select0)
        checkBoxes.add(action_select1)
        checkBoxes.add(action_select2)
        checkBoxes.add(action_select3)
        checkBoxes.add(action_select4)
        checkBoxes.add(action_select5)
        checkBoxes.add(action_select6)
        checkBoxes.add(action_select7)
        checkBoxes.add(action_select8)
        checkBoxes.add(action_select9)
        checkBoxes.add(action_select10)
        checkBoxes.add(action_select11)
        checkBoxes.add(action_select12)
        checkBoxes.add(action_select13)
        checkBoxes.add(action_select14)
        checkBoxes.add(action_select15)
        checkBoxes.add(action_select16)
        checkBoxes.add(action_select17)
    }

    fun initView() {
        ballMatchBean = intent.getParcelableExtra<BallMatchBean>(KeyContract.Bean)
        setData()
        setOnClick()
    }

    /**
     * 设置控件的数据
     */
    private fun setData() {
        tv_host_team.text = ballMatchBean.hostTeam
        tv_guest_team.text = ballMatchBean.guestTeam
        tv_total_score.text = ballMatchBean.totalScore
        tv_match_score.text = ballMatchBean.score
        tv_match_score.setTextColor(if (ballMatchBean.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))
        tv_match_score2.setBackgroundColor(if (ballMatchBean.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))


        val passStatus = ballMatchBean.passStatus?.split(',')?.map {
            try {
                it.toInt()
            } catch (e: NumberFormatException) {
                1
            }
        }
        val detailStatus = ballMatchBean.detailStatus?.split(',')?.map {
            try {
                it.toInt()
            } catch (e: NumberFormatException) {
                1
            }
        }
        for (i in 0 until checkBoxes.size) {
            var isEnable = when (i) {
                0, 1 -> if (ballMatchBean.pagePosition == 1) passStatus[0] == 1 else detailStatus[0] == 0
                2, 3 -> if (ballMatchBean.pagePosition == 1) passStatus[1] == 1 else detailStatus[1] == 0
                4, 5 -> if (ballMatchBean.pagePosition == 1) passStatus[2] == 1 else detailStatus[2] == 0
                in 6..17 -> if (ballMatchBean.pagePosition == 1) passStatus[3] == 1 else detailStatus[3] == 0
                else -> true
            }
            checkBoxes[i].isEnabled = isEnable
            checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
            checkBoxes[i].isChecked = ballMatchBean.selected!![i] == true
            checkBoxes[i].text = if (isEnable) i.getCheckedStyleText2(this, ballMatchBean.oddsMap, checkBoxes[i].isChecked) else "未开售"
            checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                ballMatchBean.selected!![i] = isChecked
                checkBoxes[i].text = if (isEnable) i.getCheckedStyleText2(this, ballMatchBean.oddsMap, checkBoxes[i].isChecked) else "未开售"

                var selectedNum = 0
                var sbLabel = StringBuffer()
                var list = arrayListOf<String>()
                for (j in 0 until ballMatchBean.selected!!.size) {
                    if (ballMatchBean.selected!![j] == true) {
                        selectedNum++
                        sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                        if (j > 3) {
                            list.add(checkBoxes[j].text.toString())
                        }
                    }
                }
                ballMatchBean.selectedNumber = selectedNum
                ballMatchBean.selectedLabel = sbLabel.toString().substring(0, if (ballMatchBean.selectedNumber > 0) sbLabel.length - 2 else 0)
                ballMatchBean.selectedBasketString = list
            }
        }
    }

    /**
     * 设置控件的点击事件
     */
    private fun setOnClick() {
        action_closed.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        action_cancel.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        action_confirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(KeyContract.Bean, ballMatchBean)
            setResult(RESULT_OK, intent)
            finish()
            overridePendingTransition(0, 0)
        }
    }
}
