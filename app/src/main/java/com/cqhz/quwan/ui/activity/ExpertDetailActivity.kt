package com.cqhz.quwan.ui.activity

import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.blankj.utilcode.util.ConvertUtils.dp2px
import com.blankj.utilcode.util.StringUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.ExpertDetailBean
import com.cqhz.quwan.model.Order
import com.cqhz.quwan.mvp.ExpertDetailContract
import com.cqhz.quwan.mvp.ExpertDetailPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.ui.widget.GridSpacingItemDecoration
import com.cqhz.quwan.util.CommonAdapterHelper
import com.cqhz.quwan.util.formatNoZero
import com.cqhz.quwan.util.setLoadImage
import kotlinx.android.synthetic.main.activity_expert_detail.*
import me.militch.quickcore.core.HasDaggerInject
import java.util.ArrayList
import javax.inject.Inject

/**
 * 专家详情
 * Created by Guojing on 2018/11/29.
 */
class ExpertDetailActivity : GoBackActivity(), ExpertDetailContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var expertDetailPresenter: ExpertDetailPresenter
    private var followStatus = 0
    private var expertId: String? = null

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun titleBarText(): String? {
        return "专家详情"
    }

    override fun layout(): Int {
        return R.layout.activity_expert_detail
    }

    override fun initView() {
        expertDetailPresenter.attachView(this)
        if (APP.get()!!.loginInfo != null) {
            val userId = intent.getStringExtra(KeyContract.UserId)
            expertId = userId
            expertDetailPresenter.getExpertDetail(userId, APP.get()!!.loginInfo!!.userId!!)
        } else {
            finish()
            showToast("请登录再试")
        }
        action_follow.setOnClickListener {
            val userId = intent.getStringExtra(KeyContract.UserId)
            var tempStatus = if (followStatus == 1) 0 else 1
            expertDetailPresenter.updateFollowStatus(userId, APP.get()!!.loginInfo!!.userId!!, tempStatus.toString())
        }
    }

    /**
     * 初始化列表适配器
     */
    private fun initAdapter(list: List<Order>) {
        // 当前页面列表的空布局
        var emptyView = LayoutInflater.from(this).inflate(R.layout.layout_none, rv_plan_list.parent as? ViewGroup, false) as View
        var ivNone = emptyView.findViewById(R.id.iv_none) as ImageView
        ivNone.setImageResource(0)

        // 当前页面列表的数据和适配器
        var adapter = CommonAdapterHelper.getPlanListAdapter(list)
        adapter.emptyView = emptyView

        // 当前页面列表适配器的事件
        rv_plan_list.layoutManager = LinearLayoutManager(this)
        rv_plan_list.isNestedScrollingEnabled = false// 让NestedScrollView滑动顺滑
        rv_plan_list.adapter = adapter
        rv_plan_list.addOnItemTouchListener(object : OnItemClickListener() {
            override fun onSimpleItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
                val item = adapter.getItem(position) as Order
                val intent = Intent(context(), PlanDetailActivity::class.java)
                intent.putExtra(KeyContract.OrderId, item.orderId)
                intent.putExtra(KeyContract.ExpertId, expertId)
                startActivity(intent)
            }
        })
    }

    /**
     * 初始化列表适配器
     */
    private fun initHitAdapter(list: List<String>) {
        // 当前页面列表的数据和适配器
        var adapter = CommonAdapterHelper.getHitListAdapter(list)
        // 当前页面列表适配器的事件
        rv_hit_list.layoutManager = GridLayoutManager(this, 7)
        rv_hit_list.addItemDecoration(GridSpacingItemDecoration(7, dp2px(10f), false))
        rv_hit_list.isNestedScrollingEnabled = false// 让NestedScrollView滑动顺滑
        rv_hit_list.adapter = adapter
    }

    override fun setExpertDetail(bean: ExpertDetailBean) {
        followStatus = bean.followStatus

        iv_avatar.setLoadImage(this, bean.headUrl, R.drawable.default_head)
        val level = getDrawable(
                when (bean.rank) {
                    1 -> R.drawable.details_level_1
                    2 -> R.drawable.details_level_2
                    3 -> R.drawable.details_level_3
                    else -> R.drawable.details_level_1
                })
        level.setBounds(0, 0, level.minimumWidth, level.minimumHeight)
        tv_name.setCompoundDrawables(null, null, level, null)
        tv_name.text = bean.nick
        tv_desc.text = bean.introduce
        action_follow.text = if (followStatus == 1) "取消关注" else "关注"
        tv_week_orders.text = "发" + bean.createOrders + "中" + bean.hitNums
        tv_week_hits.text = bean.hitOdds.formatNoZero() + "%"
        tv_week_profits.text = bean.profitOdds.formatNoZero() + "%"

        if (!StringUtils.isEmpty(bean.lastStatus)) {
            val hits = ArrayList(bean.lastStatus.split(","))
            initHitAdapter(hits)
        }

        initAdapter(bean.orderList)
    }

    override fun updateSuccess() {
        followStatus = if (followStatus == 1) 0 else 1
        action_follow.text = if (followStatus == 1) "取消关注" else "关注"
    }

    override fun onDestroy() {
        super.onDestroy()
        expertDetailPresenter.detachView()
    }
}
