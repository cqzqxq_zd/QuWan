package com.cqhz.quwan.ui.main;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.cqhz.quwan.APP;
import com.cqhz.quwan.util.CLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author whamu2
 * @date 2018/6/12
 */
public class SplashActivity extends AppCompatActivity {

    CountDownTimer mCountDownTimer;
    private static int RQC_PERMISSIONS = 0x01;
    private boolean allowAccess = true;
    private CLogger log = new CLogger(this.getClass());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!((APP) getApplication()).isStart()) {
            checkPermissions();
        } else {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }

        String s = "100";
        double ss = Double.parseDouble(s);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    private void allowAccess() {
        mCountDownTimer = new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                ((APP) getApplication()).setStart(true);
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }.start();
    }

    private Dialog createPermissionTipDialog() {
        return new AlertDialog.Builder(this)
                .setTitle("权限授权提示").setMessage("APP当前部分存储、相机、电话和位置权限调用失败，可能会影响您的正常使用。")
                .setNegativeButton("取消", (dialog, which) -> {
                    finish();
                }).setPositiveButton("确认", ((dialog, which) -> {
                    allowAccess();
                }))
                .setCancelable(false)
                .create();
    }

    private void checkPermissions() {
        // 检查权限
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // 判断权限是否被禁止
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_PHONE_STATE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                createPermissionTipDialog().show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, RQC_PERMISSIONS);
            }
        } else {
            allowAccess();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RQC_PERMISSIONS) {
            List<String> temp = new ArrayList<>();

            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i])) {
                        createPermissionTipDialog().show();
                        return;
                    } else {
                        temp.add(permissions[i]);
                    }
                }
            }
            String[] tempArray = new String[temp.size()];
            if (tempArray.length > 0) {
                ActivityCompat.requestPermissions(this, temp.toArray(tempArray), RQC_PERMISSIONS);
                return;
            }
            allowAccess();
//            if (grantResults.length>=3){
//                // 判断权限是否被禁止
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
//                        ActivityCompat.shouldShowRequestPermissionRationale(this,
//                                Manifest.permission.READ_PHONE_STATE) ||
//                        ActivityCompat.shouldShowRequestPermissionRationale(this,
//                                Manifest.permission.ACCESS_FINE_LOCATION)){
//                    createPermissionTipDialog().show();
//                }
//                if (grantResults[0] == -1){
//                    ActivityCompat.requestPermissions(this,new String[]{
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE
//                    },RQC_PERMISSIONS);
//                }else if(grantResults[1] == -1){
//                    ActivityCompat.requestPermissions(this,new String[]{
//                            Manifest.permission.READ_PHONE_STATE
//                    },RQC_PERMISSIONS);
//                }else if(grantResults[2] == -1){
//                    ActivityCompat.requestPermissions(this,new String[]{
//                            Manifest.permission.ACCESS_FINE_LOCATION
//                    },RQC_PERMISSIONS);
//                }
//            }else if(grantResults.length == 1){
//                if(grantResults[0] == -1){
//                    ActivityCompat.requestPermissions(this,permissions,RQC_PERMISSIONS);
//                }
//            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        mCountDownTimer = null;
    }
}
