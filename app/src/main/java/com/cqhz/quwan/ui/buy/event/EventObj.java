package com.cqhz.quwan.ui.buy.event;

/**
 * <p>事件实体</p>
 *
 * @author whamu2
 * @date 2018/6/9
 */
public class EventObj {

    private int key;
    private Object value;

    public EventObj(int key, Object value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
