package com.cqhz.quwan.ui.widget

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cqhz.quwan.R


class ProgressDialogFragment : DialogFragment() {
    companion object {
        fun new():ProgressDialogFragment{
            return ProgressDialogFragment()
        }
    }

    private lateinit var loadingToastView:TextView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(R.layout.layout_loading,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingToastView = view.findViewById(R.id.tv_loading)

    }

    fun setText(toastText:String?){
        if (toastText?.isEmpty() == true){
            loadingToastView.visibility = View.GONE
        }else{
            loadingToastView.visibility = View.VISIBLE
            loadingToastView.text = toastText
        }
    }
    override fun onStart() {
        super.onStart()
        val window = dialog.window
        val windowParams = window!!.attributes
        windowParams.dimAmount = 0.5f
        window.attributes = windowParams
        isCancelable = false
    }
}