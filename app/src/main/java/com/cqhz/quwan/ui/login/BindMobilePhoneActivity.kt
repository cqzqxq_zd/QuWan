package com.cqhz.quwan.ui.login

import android.widget.EditText
import android.widget.TextView
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.mvp.login.BindMobileContract
import com.cqhz.quwan.mvp.login.BindMobilePresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.util.v
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject


class BindMobilePhoneActivity:GoBackActivity(),BindMobileContract.View,HasDaggerInject<ActivityInject>{
    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    private val numberEt by v<EditText>(R.id.et_bind_mobile_phone_number)
    private val bindBtn by v<TextView>(R.id.btn_bind)
    @Inject lateinit var presenter:BindMobilePresenter
    override fun titleBarText(): String? {
        return "绑定手机号码"
    }
    override fun layout(): Int {
        return R.layout.activity_bind_mobile_phone
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
    override fun initView() {
        presenter.attachView(this)
        val alipayAuthData = intent.getSerializableExtra(KeyContract.AlipayAuthData) as AlipayAuthData?
        bindBtn.setOnClickListener {
            val num = numberEt.text.toString()
            if(num.length>=11&&alipayAuthData!=null){
                showLoading("正在登录中...")
                presenter.bindAlipay(num,alipayAuthData)
            }else{
                showToast("请输入正确的手机号码")
            }
        }
    }

}