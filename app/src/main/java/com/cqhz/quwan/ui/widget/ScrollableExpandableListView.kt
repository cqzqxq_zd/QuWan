package com.cqhz.quwan.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ExpandableListView

/**
 * 可以放在ScrollView中的ExpandableListView
 * Created by Guojing on 2018/9/20.
 */
class ScrollableExpandableListView : ExpandableListView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val expandSpec = View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE shr 2, View.MeasureSpec.AT_MOST)
        super.onMeasure(widthMeasureSpec, expandSpec)
    }
}