package com.cqhz.quwan.ui.base

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.cqhz.quwan.ui.widget.flowlayout.TagAdapter

abstract class AbsAdapter<T>(val context: Context, private val layoutResId: Int) : BaseAdapter() {
    open val mData = ArrayList<T>()
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View? = convertView
        val viewHolder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(layoutResId, parent, false)
            viewHolder = ViewHolder(view)
            handlerViewHolderPre(viewHolder, position, getItem(position))
            view.tag = viewHolder
        } else {
            viewHolder = view!!.tag as ViewHolder
        }
        handlerViewHolder(viewHolder, position, getItem(position))
        return view!!
    }

    override fun getItem(position: Int): T {
        return mData[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mData.size
    }

    open fun handlerViewHolderPre(viewHolder: ViewHolder, position: Int, itemData: T?) {

    }

    abstract fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: T?)
    fun removeItem(t: T) {
        mData.remove(t)
        notifyDataSetChanged()
    }

    fun removeItemIndex(index: Int) {
        mData.removeAt(index)
        notifyDataSetChanged()
    }

    fun addData(data: List<T>?) {
        if (data != null) {
            mData.addAll(data)
            notifyDataSetChanged()
        }
    }

    fun addData(data: List<T>, index: Int) {
        mData.addAll(index, data)
        notifyDataSetChanged()
    }

    fun addItem(t: T) {
        mData.add(t)
        notifyDataSetChanged()
    }

    fun addItem(t: T, index: Int) {
        mData.add(index, t)
        notifyDataSetChanged()
    }

    fun setData(data: List<T>?) {
        if (data != null) {
            mData.clear()
            mData.addAll(data)
            notifyDataSetChanged()
        }
    }

    fun removeData(t: T?) {
        if (t != null) {
            mData.remove(t)
        }
    }

    fun removeDataIndex(index: Int) {
        mData.remove(mData[index])
    }

    fun clearData() {
        mData.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(private val parent: View) {
        var adapter: TagAdapter<String>? = null

        fun setText(id: Int, text: String?) {
            with(parent) {
                if (text != null) {
                    findViewById<TextView>(id).text = text
                }
            }
        }

        fun setVisible(id: Int, visible: Boolean) {
            with(parent) {
                findViewById<TextView>(id).visibility = if (visible) View.VISIBLE else View.GONE
            }
        }

        fun setHtmlText(id: Int, text: String?) {
            with(parent) {
                if (text != null) {
                    findViewById<TextView>(id).text = Html.fromHtml(text)
                }
            }
        }

        fun setTextColor(id: Int, color: Int) {
            with(parent) {
                findViewById<TextView>(id).setTextColor(color)
            }
        }

        fun setTextAndColor(id: Int, text: String?, color: Int) {
            setText(id, text)
            with(parent) {
                findViewById<TextView>(id).setTextColor(color)
            }
        }

        fun setGridViewAdapter(id: Int, adapter: BaseAdapter) {
            with(parent) {
                findViewById<GridView>(id).adapter = adapter
            }
        }

        fun loadImageUrl(id: Int, imageUrl: String?) {
            with(parent) {
                val view = findViewById<ImageView>(id)
                if (imageUrl != null) {
                    Glide.with(context).load(imageUrl).into(view)
                }
            }
        }

        fun <T : View> find(id: Int): T {
            with(parent) {
                return findViewById(id)
            }
        }

        fun checked(id: Int, flg: Boolean) {
            with(parent) {
                findViewById<CheckBox>(id).isChecked = flg
            }
        }

        fun setItemClick(lin: (v: View) -> Unit) {
            parent.setOnClickListener {
                lin.invoke(it)
            }
        }
    }

    open fun refreshLayoutIdRes(): Int {
        return -1
    }
}