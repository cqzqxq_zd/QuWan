package com.cqhz.quwan.ui.main.match.fragment.child;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cqhz.quwan.ActivityInject;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.AnalysisIntegral;
import com.cqhz.quwan.mvp.match.IntegralContract;
import com.cqhz.quwan.mvp.match.IntegralPresenter;
import com.cqhz.quwan.ui.base.AbsFragment;
import com.cqhz.quwan.ui.main.match.adapter.IntegralInfoAdapter;
import com.cqhz.quwan.ui.main.match.common.Utils;
import com.cqhz.quwan.ui.widget.FullyLinearLayoutManager;

import javax.inject.Inject;

import me.militch.quickcore.core.HasDaggerInject;

/**
 * 积分详情
 *
 * @author whamu2
 * @date 2018/7/17
 */
public class IntegralInfoFragment extends AbsFragment implements IntegralContract.View, HasDaggerInject<ActivityInject> {
    private static final String TAG = "IntegralInfoFragment";
    private static final String KEY_INTEGRAL = "integral";
    private static final String KEY_TYPE = "type";
    private static final String KEY_LEAGUE = "league";

    private TextView mTitle;
    private View mLargeView;
    private View mBarView;
    private RecyclerView mRecyclerView;
    private View mEmptyView;

    private String code;
    private int type;
    private String leagueId;

    @Inject
    IntegralPresenter mPresenter;
    private IntegralInfoAdapter mAdapter;

    @Override
    public void inject(ActivityInject inject) {
        inject.inject(this);
    }

    public static IntegralInfoFragment newInstance(String code, String leagueId, int type) {
        Bundle args = new Bundle();
        args.putString(KEY_INTEGRAL, code);
        args.putString(KEY_LEAGUE, leagueId);
        args.putInt(KEY_TYPE, type);
        IntegralInfoFragment fragment = new IntegralInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int layout() {
        return R.layout.fragment_match_integral_info;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_INTEGRAL);
            type = getArguments().getInt(KEY_TYPE);
            leagueId = getArguments().getString(KEY_LEAGUE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mPresenter.attachView(this);
        super.onViewCreated(view, savedInstanceState);
        mTitle = view.findViewById(R.id.tv_title);
        mRecyclerView = view.findViewById(R.id.rv);
        mLargeView = view.findViewById(R.id.tv_large);
        mBarView = view.findViewById(R.id.layout);
        mEmptyView = view.findViewById(R.id.tv_empty);

        FullyLinearLayoutManager mLayoutManager = new FullyLinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new IntegralInfoAdapter(getContext());
        mRecyclerView.setAdapter(mAdapter);

        mPresenter.getIntegralToType(type);

    }

    @Override
    public void getData(AnalysisIntegral data) {
        if (data != null) {
            AnalysisIntegral.PointBean bean = data.getPoint().get(0);
            mTitle.setText(Utils.required(bean.getLeague()));
            mAdapter.setData(bean.getPoint_result());
            mTitle.setVisibility(View.VISIBLE);
            mLargeView.setVisibility(View.VISIBLE);
            mBarView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFail() {
        mTitle.setVisibility(View.GONE);
        mLargeView.setVisibility(View.GONE);
        mBarView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public String getMatchId() {
        return code;
    }

    @Override
    public String getLeague() {
        return leagueId;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mRecyclerView = null;
        mAdapter = null;
    }
}
