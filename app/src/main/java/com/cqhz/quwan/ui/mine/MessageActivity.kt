package com.cqhz.quwan.ui.mine

import android.view.View
import android.widget.ImageView
import android.widget.ListView
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.model.Msg2Entity
import com.cqhz.quwan.model.MsgItemBean
import com.cqhz.quwan.mvp.mine.MessageContract
import com.cqhz.quwan.mvp.mine.MessagePresenter
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.util.v
import me.militch.quickcore.util.ApiException
import javax.inject.Inject


class MessageActivity : GoBackActivity(), MessageContract.View {
    override fun showMsgData2(data: List<Msg2Entity>) {
        if (data.isNotEmpty()) {
            mListView.visibility = View.VISIBLE
            noneIv.visibility = View.GONE
            mAdapter.setData(data)
        } else {
            mListView.visibility = View.GONE
            mListView.visibility = View.VISIBLE
        }
        if (refreshLayout.state.isOpening) {
            refreshLayout.finishRefresh()
        }
    }


    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun showMsgData(data: List<MsgItemBean>) {

    }

    private var userId: String? = null
    private val mListView by v<ListView>(R.id.lv_content)
    private val noneIv by v<ImageView>(R.id.iv_data_none)
    private val refreshLayout by v<SmartRefreshLayout>(R.id.refreshLayout)
    private lateinit var mAdapter: AbsAdapter<Msg2Entity>
    @Inject
    lateinit var mPresenter: MessagePresenter

    override fun layout(): Int {
        return R.layout.activity_message
    }

    override fun showRequestError(e: ApiException) {
        super.showRequestError(e)
        if (refreshLayout.state.isOpening) {
            refreshLayout.finishRefresh(false)
        }
    }

    override fun titleBarText(): String? {
        return "消息中心"
    }

    override fun onDestroy() {
        mPresenter.detachView()
        super.onDestroy()
    }

    override fun initView() {
        mPresenter.attachView(this)
        val loginInfo = APP.get()!!.loginInfo
        userId = loginInfo?.userId
        mAdapter = object : AbsAdapter<Msg2Entity>(this, R.layout.item_message) {
            override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: Msg2Entity?) {
                viewHolder.setText(R.id.tv_item_message_theme, itemData?.title ?: "")
                viewHolder.setText(R.id.tv_item_message_content, itemData?.content ?: "")
                viewHolder.setText(R.id.tv_item_message_createTime, itemData?.createTime ?: "")
                val iconImage = viewHolder.find<ImageView>(R.id.msg_icon)
                val alreadyView = viewHolder.find<View>(R.id.msg_already)
                // 类型 1-充值 2-提现 3-中奖 4-系统 5.兑换 11-系统维护 12-系统升级
                iconImage.setImageResource(when (itemData?.type) {
                    1 -> R.drawable.icon_success
                    2 -> R.drawable.icon_success
                    3 -> R.drawable.icon_winning
                    5 -> R.drawable.message_ic_exchange
                    4, 11, 12 -> R.drawable.icon_system
                    else -> R.drawable.icon_success
                })
                alreadyView.visibility = if (itemData?.status ?: -1 == 0) View.VISIBLE else View.GONE
            }
        }
        refreshLayout.setOnRefreshListener {
            mPresenter.getUserMsg(userId!!)
        }
        mListView.adapter = mAdapter
        mPresenter.getUserMsg(userId!!)
    }
}