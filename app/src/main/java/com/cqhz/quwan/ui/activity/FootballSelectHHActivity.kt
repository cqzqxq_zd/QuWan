package com.cqhz.quwan.ui.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.CheckBox
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.BallMatchBean
import com.cqhz.quwan.util.getCheckBoxTextValue2
import com.cqhz.quwan.util.getCheckedStyleText
import com.cqhz.quwan.util.getMapKeyByPosition
import kotlinx.android.synthetic.main.activity_football_select_hh.*
import me.militch.quickcore.core.HasDaggerInject

/**
 * 足彩混合投注选择页面
 * Created by Guojing on 2018/11/16.
 */
class FootballSelectHHActivity : Activity(), HasDaggerInject<ActivityInject> {

    private lateinit var ballMatchBean: BallMatchBean
    private var checkBoxes = ArrayList<CheckBox>()


    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_football_select_hh)
        addCheckBox()
        initView()
    }

    private fun addCheckBox() {
        checkBoxes.add(action_select0)
        checkBoxes.add(action_select1)
        checkBoxes.add(action_select2)
        checkBoxes.add(action_select3)
        checkBoxes.add(action_select4)
        checkBoxes.add(action_select5)
        checkBoxes.add(action_select6)
        checkBoxes.add(action_select7)
        checkBoxes.add(action_select8)
        checkBoxes.add(action_select9)
        checkBoxes.add(action_select10)
        checkBoxes.add(action_select11)
        checkBoxes.add(action_select12)
        checkBoxes.add(action_select13)
        checkBoxes.add(action_select14)
        checkBoxes.add(action_select15)
        checkBoxes.add(action_select16)
        checkBoxes.add(action_select17)
        checkBoxes.add(action_select18)
        checkBoxes.add(action_select19)
        checkBoxes.add(action_select20)
        checkBoxes.add(action_select21)
        checkBoxes.add(action_select22)
        checkBoxes.add(action_select23)
        checkBoxes.add(action_select24)
        checkBoxes.add(action_select25)
        checkBoxes.add(action_select26)
        checkBoxes.add(action_select27)
        checkBoxes.add(action_select28)
        checkBoxes.add(action_select29)
        checkBoxes.add(action_select30)
        checkBoxes.add(action_select31)
        checkBoxes.add(action_select32)
        checkBoxes.add(action_select33)
        checkBoxes.add(action_select34)
        checkBoxes.add(action_select35)
        checkBoxes.add(action_select36)
        checkBoxes.add(action_select37)
        checkBoxes.add(action_select38)
        checkBoxes.add(action_select39)
        checkBoxes.add(action_select40)
        checkBoxes.add(action_select41)
        checkBoxes.add(action_select42)
        checkBoxes.add(action_select43)
        checkBoxes.add(action_select44)
        checkBoxes.add(action_select45)
        checkBoxes.add(action_select46)
        checkBoxes.add(action_select47)
        checkBoxes.add(action_select48)
        checkBoxes.add(action_select49)
        checkBoxes.add(action_select50)
        checkBoxes.add(action_select51)
        checkBoxes.add(action_select52)
        checkBoxes.add(action_select53)
    }

    fun initView() {
        ballMatchBean = intent.getParcelableExtra<BallMatchBean>(KeyContract.Bean)
        setData()
        setOnClick()
    }

    /**
     * 设置控件的数据
     */
    private fun setData() {
        tv_host_team.text = ballMatchBean.hostTeam
        tv_guest_team.text = ballMatchBean.guestTeam
        tv_match_score.text = ballMatchBean.score
        tv_match_score.setBackgroundColor(if (ballMatchBean.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#d60000"))

        val passStatus = ballMatchBean.passStatus?.split(',')?.map {
            try {
                it.toInt()
            } catch (e: NumberFormatException) {
                1
            }
        }
        val detailStatus = ballMatchBean.detailStatus?.split(',')?.map {
            try {
                it.toInt()
            } catch (e: NumberFormatException) {
                1
            }
        }
        for (i in 0 until checkBoxes.size) {
            var isEnable = when (i) {
                in 0..2 -> if (ballMatchBean.pagePosition == 1) passStatus[0] == 1 else detailStatus[0] == 0
                in 3..5 -> if (ballMatchBean.pagePosition == 1) passStatus[1] == 1 else detailStatus[1] == 0
                in 6..36 -> if (ballMatchBean.pagePosition == 1) passStatus[2] == 1 else detailStatus[2] == 0
                in 37..44 -> if (ballMatchBean.pagePosition == 1) passStatus[3] == 1 else detailStatus[3] == 0
                in 45..53 -> if (ballMatchBean.pagePosition == 1) passStatus[4] == 1 else detailStatus[4] == 0
                else -> true
            }
            checkBoxes[i].isEnabled = isEnable
            checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
            var realPosition = i.getMapKeyByPosition(false)
            checkBoxes[i].isChecked = ballMatchBean.selected!![realPosition] == true
            checkBoxes[i].text = if (isEnable) i.getCheckedStyleText(this, false, ballMatchBean.oddsMap, checkBoxes[i].isChecked, 13, 12) else "未开售"
            checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                ballMatchBean.selected!![realPosition] = isChecked
                checkBoxes[i].text = if (isEnable) i.getCheckedStyleText(this, false, ballMatchBean.oddsMap, checkBoxes[i].isChecked, 13, 12) else "未开售"

                var selectedNum = 0
                var sbLabel = StringBuffer()
                for (j in 0 until ballMatchBean.selected!!.size) {
                    if (ballMatchBean.selected!![j] == true) {
                        selectedNum++
                        sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                    }
                }
                ballMatchBean.selectedNumber = selectedNum
                ballMatchBean.selectedLabel = sbLabel.toString().substring(0, if (ballMatchBean.selectedNumber > 0) sbLabel.length - 2 else 0)
            }
        }
    }

    /**
     * 设置控件的点击事件
     */
    private fun setOnClick() {
        action_closed.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        action_cancel.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        action_confirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(KeyContract.Bean, ballMatchBean)
            setResult(RESULT_OK, intent)
            finish()
            overridePendingTransition(0, 0)
        }
    }
}
