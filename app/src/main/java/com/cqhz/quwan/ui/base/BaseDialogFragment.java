package com.cqhz.quwan.ui.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cqhz.quwan.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author whamu2
 * @date 2018/6/9
 */
public abstract class BaseDialogFragment extends DialogFragment {

    private Unbinder unbinder;
    private Handler handler = null;

    public BaseDialogFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, gentleDialogStyle() == 0 ? R.style.BaseDialogStyle : gentleDialogStyle());
    }

    @StyleRes
    protected abstract int gentleDialogStyle();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return gentleLayout(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        gentleViewCreated(view, savedInstanceState);
    }

    protected abstract View gentleLayout(LayoutInflater inflater,
                                         @Nullable ViewGroup container,
                                         @Nullable Bundle savedInstanceState);


    protected abstract void gentleViewCreated(View view,
                                              @Nullable Bundle savedInstanceState);

    @Override
    public void dismiss() {
        super.dismiss();
//        KeyboardUtils.hideKeyboard(getActivity());
    }

    public Handler getHandler() {
        if (null == handler) {
            handler = new Handler(Looper.getMainLooper());
        }
        return handler;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null && unbinder != Unbinder.EMPTY) {
            unbinder.unbind();
        }
        unbinder = null;
    }
}
