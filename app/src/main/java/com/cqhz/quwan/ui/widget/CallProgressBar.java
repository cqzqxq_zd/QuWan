package com.cqhz.quwan.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.cqhz.quwan.R;


/**
 * 支持反应柱状图
 *
 * @author whamu2
 * @date 2018/7/17
 */
public class CallProgressBar extends View {
    private static final String TAG = CallProgressBar.class.getSimpleName();
    private static final int DEFAULT_CORNERS = 0;
    private static final int DEFAULT_SOLID_COLOR = Color.BLUE;
    private static final int DEFAULT_PROGRESS = 0;
    private static final int DEFAULT_MAX = 100;
    private static final int DEFAULT_FIXED_HEIGHT = 0;
    public static final int VERTICAL = 0;
    private static final int HORIZONTAL = 1;

    /**
     * 圆角
     */
    private int corners = DEFAULT_CORNERS;
    /**
     * 填充色
     */
    private int solid = DEFAULT_SOLID_COLOR;
    /**
     * 进度
     */
    private float progress = DEFAULT_PROGRESS;
    /**
     * 进度最大值
     */
    private float max = DEFAULT_MAX;
    /**
     * 方向
     */
    private int orientation = VERTICAL;

    /**
     * 固定高度
     */
    private int fixedHeight = DEFAULT_FIXED_HEIGHT;

    //View宽度
    private int width;

    //View高度
    private int height;

    //默认宽高值
    private int result = 0;
    private Paint mPaint;
    private Paint mBgPaint;
    private RectF drawRect;

    public CallProgressBar(Context context) {
        this(context, null);
    }

    public CallProgressBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CallProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CallProgressBar, defStyleAttr, 0);
        corners = typedArray.getDimensionPixelSize(R.styleable.CallProgressBar_call_corners, DEFAULT_CORNERS);
        solid = typedArray.getColor(R.styleable.CallProgressBar_call_solid, DEFAULT_SOLID_COLOR);
        progress = typedArray.getFloat(R.styleable.CallProgressBar_call_progress, DEFAULT_PROGRESS);
        max = typedArray.getFloat(R.styleable.CallProgressBar_call_max, DEFAULT_MAX);
        orientation = typedArray.getInt(R.styleable.CallProgressBar_call_orientation, VERTICAL);
        fixedHeight = typedArray.getDimensionPixelSize(R.styleable.CallProgressBar_call_fixed, DEFAULT_FIXED_HEIGHT);

        typedArray.recycle();
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(solid);
        drawRect = new RectF();

        mBgPaint = new Paint();
        mBgPaint.setAntiAlias(true);
        mBgPaint.setStyle(Paint.Style.FILL);
        mBgPaint.setColor(Color.TRANSPARENT);

        result = dp2px(100);
    }

    public void setCorners(int corners) {
        if (this.corners == corners) {
            return;
        }
        this.corners = corners;
        //
        invalidate();
    }

    public void setSolid(@ColorInt int solid) {
        if (this.solid == solid) {
            return;
        }
        mPaint.setColor(solid);
        invalidate();

    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        if (progress < 0) {
            throw new IllegalArgumentException("The progress of 0");
        }
        if (progress > max) {
            progress = max;
        }
        if (progress <= max) {
            this.progress = progress;
            postInvalidate();
        }
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        if (this.max == max) {
            return;
        }
        this.max = max;
        //
        invalidate();
    }

    /**
     * 设置方向
     *
     * @param orientation VERTICAL and HORIZONTAL
     */
    public void setOrientation(int orientation) {
        if (this.orientation == orientation) {
            return;
        }
        this.orientation = orientation;
        invalidate();
    }

    /**
     * 底部自定义视图搞得
     *
     * @param fixedHeight int
     */
    public void setFixedHeight(int fixedHeight) {
        if (this.fixedHeight == fixedHeight) {
            return;
        }
        this.fixedHeight = fixedHeight;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (orientation == VERTICAL) {
            drawVertical(canvas);
        } else if (orientation == HORIZONTAL) {
            drawHorizontal(canvas);
        } else {
            drawVertical(canvas);
        }
    }

    private void drawVertical(Canvas canvas) {
        RectF rectF = new RectF(0, 0, width, height);
        canvas.drawRoundRect(rectF, corners, corners, mBgPaint);
        float realHeight = height - fixedHeight;
        float scale = progress / max;
        drawRect.set(0, realHeight - realHeight * scale, width, height);
        canvas.drawRoundRect(drawRect, corners, corners, mPaint);

    }

    private void drawHorizontal(Canvas canvas) {
        RectF rectF = new RectF(0, 0, width, height);
        canvas.drawRoundRect(rectF, corners, corners, mBgPaint);
        float realWidth = width - fixedHeight;
        float scale = progress / max;
        drawRect.set(0, 0, (realWidth * scale) + fixedHeight, height);
        canvas.drawRoundRect(drawRect, corners, corners, mPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //获取宽高的mode和size
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        //测量宽度
        if (widthMode == MeasureSpec.AT_MOST) {
            width = result;
        } else {
            width = widthSize;
        }

        //测量高度
        if (heightMode == MeasureSpec.AT_MOST) {
            height = result;
        } else {
            height = heightSize;
        }

        //设置测量的宽高值
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //确定View的宽高
        width = w;
        height = h;
    }

    /**
     * dp转px
     */
    public int dp2px(int dp) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return (int) (dp * density + 0.5f);
    }
}
