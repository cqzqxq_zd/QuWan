package com.cqhz.quwan.ui.main.match.adapter.tree;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.whamu2.treeview.base.ViewHolder;
import com.whamu2.treeview.factory.ItemHelperFactory;
import com.whamu2.treeview.item.TreeItem;
import com.whamu2.treeview.item.TreeItemGroup;
import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.main.match.entity.RecordData;

import java.util.List;

/**
 * 分析主标题
 *
 * @author whamu2
 * @date 2018/7/19
 */
public class AnalysisMainTitleTree extends TreeItemGroup<RecordData> {

    @Nullable
    @Override
    protected List<TreeItem> initChildList(RecordData data) {
        return ItemHelperFactory.createTreeItemList(data.getData(), AnalysisSubjectTree.class, this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.tree_analysis_decoration;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder) {
        holder.setText(R.id.tv, getData().getTitle());
    }
}
