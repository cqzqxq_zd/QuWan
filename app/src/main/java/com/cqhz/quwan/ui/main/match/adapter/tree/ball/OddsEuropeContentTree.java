package com.cqhz.quwan.ui.main.match.adapter.tree.ball;

import android.support.annotation.NonNull;
import android.widget.TextView;

import com.whamu2.treeview.base.ViewHolder;
import com.whamu2.treeview.item.TreeItem;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.OddsEuropeData;
import com.cqhz.quwan.ui.main.match.common.Utils;

/**
 * 赔率内容 欧盘
 *
 * @author whamu2
 * @date 2018/7/19
 */
public class OddsEuropeContentTree extends TreeItem<OddsEuropeData.OddsResultBean> {

    @Override
    public int getLayoutId() {
        return R.layout.tree_odds_europe_content;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder) {
        //        cn: 公司
        //        win_s: 初始奖金-胜
        //        draw_s: 初始奖金-平
        //        lose_s: 初始奖金-负
        //        win: 即时奖金-胜
        //        draw: 即时奖金-平
        //        lose: 即时奖金-负
        //        win_s_index: 凯利指数-初始-胜
        //        draw_s_index: 凯利指数-初始-平
        //        lose_s_index: 凯利指数-初始-负
        //        win_index: 凯利指数-即时-胜
        //        draw_index: 凯利指数-即时—平
        //        lose_index: 凯利指数-即时-负
        OddsEuropeData.OddsResultBean data = getData();
        viewHolder.setText(R.id.tv_company, Utils.required(data.getCn()));

        viewHolder.setText(R.id.tv_pl_c_zs, Utils.required(data.getWin_s()));
        viewHolder.setText(R.id.tv_pl_c_p, Utils.required(data.getDraw_s()));
        viewHolder.setText(R.id.tv_pl_c_ks, Utils.required(data.getLose_s()));
        viewHolder.setText(R.id.tv_pl_j_zs, Utils.required(data.getWin()));
        viewHolder.setText(R.id.tv_pl_j_p, Utils.required(data.getDraw()));
        viewHolder.setText(R.id.tv_pl_j_ks, Utils.required(data.getLose()));

        TextView winTextView = viewHolder.getTextView(R.id.tv_pl_j_zs);
        winTextView.setTextColor(Utils.getStateColor(data.getWin_change()));
        TextView drawTextView = viewHolder.getTextView(R.id.tv_pl_j_p);
        drawTextView.setTextColor(Utils.getStateColor(data.getDraw_change()));
        TextView loseTextView = viewHolder.getTextView(R.id.tv_pl_j_ks);
        loseTextView.setTextColor(Utils.getStateColor(data.getLose_change()));

        viewHolder.setText(R.id.tv_klzs_c_zs, Utils.required(data.getWin_s_index()));
        viewHolder.setText(R.id.tv_klzs_c_p, Utils.required(data.getDraw_s_index()));
        viewHolder.setText(R.id.tv_klzs_c_ks, Utils.required(data.getLose_s_index()));
        viewHolder.setText(R.id.tv_klzs_j_zs, Utils.required(data.getWin_index()));
        viewHolder.setText(R.id.tv_klzs_j_p, Utils.required(data.getDraw_index()));
        viewHolder.setText(R.id.tv_klzs_j_ks, Utils.required(data.getLose_index()));

        int layoutPosition = viewHolder.getLayoutPosition();
        if (layoutPosition % 2 == 0) {
            viewHolder.itemView.setBackgroundColor(0xFFEEEEEE);
        } else {
            viewHolder.itemView.setBackgroundColor(0xFFFFFFFF);

        }
    }
}
