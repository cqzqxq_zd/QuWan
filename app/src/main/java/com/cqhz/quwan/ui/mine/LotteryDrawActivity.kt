package com.cqhz.quwan.ui.mine

import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.model.OrderDetailBean
import com.cqhz.quwan.mvp.mall.LotteryDrawContract
import com.cqhz.quwan.mvp.mall.LotteryDrawPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import kotlinx.android.synthetic.main.activity_lottery_draw.*
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject


/**
 * 抽奖成功
 * Created by Guojing on 2018/9/14.
 */
class LotteryDrawActivity : GoBackActivity(), LotteryDrawContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var lotteryDrawPresenter: LotteryDrawPresenter

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun layout(): Int {
        return R.layout.activity_lottery_draw
    }

    override fun initView() {
        lotteryDrawPresenter.attachView(this)
        var tradeId = intent.getStringExtra("orderId")
        lotteryDrawPresenter.getOrderDetail(tradeId)

        action_confirm.setOnClickListener { finish() }// 完成
    }

    override fun showOrderDetail(orderDetailBean: OrderDetailBean) {
        tv_product_name.text = orderDetailBean.productName// 商品名称
        tv_product_number.text = orderDetailBean.num.toString() + "份"// 数量
        tv_name.text = orderDetailBean.receiverName// 收货人
        tv_phone.text = orderDetailBean.receiverMobile// 联系电话
        tv_address.text = orderDetailBean.address// 收货地址
    }

    override fun onDestroy() {
        lotteryDrawPresenter.detachView()
        super.onDestroy()
    }
}
