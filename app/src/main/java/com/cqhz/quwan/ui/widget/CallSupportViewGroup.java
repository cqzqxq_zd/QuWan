package com.cqhz.quwan.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cqhz.quwan.R;

/**
 * @author whamu2
 * @date 2018/7/18
 */
public class CallSupportViewGroup extends LinearLayout implements CompoundButton.OnCheckedChangeListener {
    private static final int DEFAULT_CORNERS = 0;
    private static final int DEFAULT_SOLID_COLOR = Color.BLUE;
    private static final int DEFAULT_PROGRESS = 0;
    private static final int DEFAULT_MAX = 100;
    private static final String DEFAULT_PERCENT = "10%";
    private static final int DEFAULT_FIXED_HEIGHT = 0;

    /**
     * 圆角
     */
    private int corners = DEFAULT_CORNERS;

    /**
     * 填充色
     */
    private int solid = DEFAULT_SOLID_COLOR;
    /**
     * 进度
     */
    private float progress = DEFAULT_PROGRESS;
    /**
     * 进度最大值
     */
    private float max = DEFAULT_MAX;
    /**
     * 百分比
     */
    private String percent = DEFAULT_PERCENT;

    /**
     * 是否选中
     */
    private boolean mChecked;
    /**
     * 方向
     */
    private int orientation = CallProgressBar.VERTICAL;

    /**
     * 固定高度
     */
    private int fixedHeight = DEFAULT_FIXED_HEIGHT;

    private OnCheckedChangeListener mOnCheckedChangeListener;

    private CallProgressBar mProgressBar;
    private TextView mPercentTextView;
    private CheckBox mSupportView;

    public CallSupportViewGroup(Context context) {
        this(context, null);
    }

    public CallSupportViewGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CallSupportViewGroup);
        corners = typedArray.getDimensionPixelSize(R.styleable.CallSupportViewGroup_cs_corners, DEFAULT_CORNERS);
        solid = typedArray.getColor(R.styleable.CallSupportViewGroup_cs_solid, DEFAULT_SOLID_COLOR);
        progress = typedArray.getFloat(R.styleable.CallSupportViewGroup_cs_progress, DEFAULT_PROGRESS);
        max = typedArray.getFloat(R.styleable.CallSupportViewGroup_cs_max, DEFAULT_MAX);
        percent = typedArray.getString(R.styleable.CallSupportViewGroup_cs_percent);
        mChecked = typedArray.getBoolean(R.styleable.CallSupportViewGroup_cs_checked, false);
        orientation = typedArray.getInt(R.styleable.CallSupportViewGroup_cs_orientation, CallProgressBar.VERTICAL);
        fixedHeight = typedArray.getDimensionPixelSize(R.styleable.CallSupportViewGroup_cs_fixed, DEFAULT_FIXED_HEIGHT);
        typedArray.recycle();


    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.call_support_layout, null);
        mProgressBar = view.findViewById(R.id.callprogressbar);
        mPercentTextView = view.findViewById(R.id.tv_amount);
        mSupportView = view.findViewById(R.id.checkbox);
        mSupportView.setOnCheckedChangeListener(this);

        setupCallView();
        addView(view);
    }

    private void setupCallView() {
        mProgressBar.setSolid(solid);
        mProgressBar.setMax(max);
        mProgressBar.setProgress(progress);
        mProgressBar.setCorners(corners);
        mProgressBar.setFixedHeight(fixedHeight);
        mProgressBar.setOrientation(orientation);

        mPercentTextView.setText(percent);
        mSupportView.setChecked(mChecked);
        mSupportView.setText(mChecked ? "已支持" : "支持");

        invalidate();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mChecked = isChecked;
        if (mOnCheckedChangeListener != null) {
            mOnCheckedChangeListener.onCheckedChanged(this, isChecked);
            mSupportView.setText(mChecked ? "已支持" : "支持");
        }
    }

    public interface OnCheckedChangeListener {
        void onCheckedChanged(CallSupportViewGroup group, boolean isChecked);
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        mOnCheckedChangeListener = listener;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        this.mChecked = checked;
        mSupportView.setChecked(mChecked);
        mSupportView.setText(mChecked ? "已支持" : "支持");
        postInvalidate();
    }

    public void setProgress(float progress) {
        mProgressBar.setProgress(progress);
        postInvalidate();
    }

    public void setPercent(String percent) {
        mPercentTextView.setText(percent);
        postInvalidate();
    }

    public void setEnabled(boolean enabled) {
        mSupportView.setEnabled(enabled);
    }

    public void setClickable(boolean clickable){
        mSupportView.setClickable(clickable);
    }
}
