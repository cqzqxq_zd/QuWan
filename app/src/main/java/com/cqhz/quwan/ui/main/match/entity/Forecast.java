package com.cqhz.quwan.ui.main.match.entity;

/**
 * @author whamu2
 * @date 2018/7/26
 */
public class Forecast {

    /**
     * win_per : 32%
     * draw_per : 34%
     * lose_per : 34%
     */
    private int type;
    private String title;
    private String win;
    private String draw;
    private String lose;

    public String getWin() {
        return win;
    }

    public void setWin(String win) {
        this.win = win;
    }

    public String getDraw() {
        return draw;
    }

    public void setDraw(String draw) {
        this.draw = draw;
    }

    public String getLose() {
        return lose;
    }

    public void setLose(String lose) {
        this.lose = lose;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
