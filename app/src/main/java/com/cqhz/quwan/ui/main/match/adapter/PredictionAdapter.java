package com.cqhz.quwan.ui.main.match.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.base.BaseRecyclerViewAdapter;
import com.cqhz.quwan.ui.main.match.entity.Forecast;
import com.cqhz.quwan.ui.main.match.holder.DataViewHolder;
import com.cqhz.quwan.ui.main.match.holder.NewsViewHolder;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/18
 */
public class PredictionAdapter extends BaseRecyclerViewAdapter<Forecast, RecyclerView.ViewHolder> {
    private static final int DATA = 759;
    private static final int NEWS = 167;

    public PredictionAdapter(Context c) {
        super(c);
    }

    @Override
    public int getItemViewType(int position) {
        Forecast item = getItem(position);
        if (item.getType() == 0) {
            return DATA;
        } else {
            return NEWS;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == DATA) {
            return new DataViewHolder(LayoutInflater.from(getContext())
                    .inflate(R.layout.item_prediction_data, parent, false));
        } else {
            return new NewsViewHolder(LayoutInflater.from(getContext())
                    .inflate(R.layout.item_prediction_news, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DataViewHolder) {
            ((DataViewHolder) holder).bind(getItem(position), position);
        } else if (holder instanceof NewsViewHolder) {
            ((NewsViewHolder) holder).bind(getItem(position), position);

        }
    }
}
