package com.cqhz.quwan.ui.widget.guide.lifecycle;

public abstract class FragmentLifecycleAdapter implements FragmentLifecycle {
    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onDestroyView() {
    }

    @Override
    public void onDestroy() {
    }
}