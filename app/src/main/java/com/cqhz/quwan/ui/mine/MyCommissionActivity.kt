package com.cqhz.quwan.ui.mine

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.blankj.utilcode.util.StringUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.CommissionBean
import com.cqhz.quwan.model.CommissionInfoBean
import com.cqhz.quwan.model.LoginBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.mvp.buy.CommissionContract
import com.cqhz.quwan.mvp.buy.CommissionPresenter
import com.cqhz.quwan.service.API
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.ui.base.WebViewActivity
import com.cqhz.quwan.ui.login.LoginActivity
import com.cqhz.quwan.util.CommonAdapterHelper
import kotlinx.android.synthetic.main.activity_my_commission.*
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject

/**
 * 我的佣金
 * Created by Guojing on 2018/10/29.
 */
class MyCommissionActivity : GoBackActivity(), CommissionContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var commissionPresenter: CommissionPresenter
    private var loginBean: LoginBean? = null
    private var commissions = ArrayList<CommissionBean>()
    private lateinit var commissionListAdapter: BaseQuickAdapter<*, *>

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun titleBarText(): String? {
        return "我的佣金"
    }

    override fun layout(): Int {
        return R.layout.activity_my_commission
    }

    override fun initView() {
        commissionPresenter.attachView(this)
        if (APP.get()!!.loginInfo != null) {
            loginBean = APP.get()!!.loginInfo
        }

        initAdapter()
        loadData()
    }

    /**
     * 初始化适配器
     */
    private fun initAdapter() {
        commissionListAdapter = CommonAdapterHelper.getCommissionRecordListAdapter(commissions)
        rv_content.layoutManager = LinearLayoutManager(this)
        rv_content.adapter = commissionListAdapter
        // 加载更多
        commissionListAdapter.setOnLoadMoreListener({
            if (commissions.size > 0) {
                if (commissions.size % KeyContract.pageSize == 0) {
                    val pageNo = commissions.size / KeyContract.pageSize + 1
                    refreshLayout.isEnableRefresh = false
                    commissionPresenter.getRecordList(loginBean!!.userId!!, pageNo, KeyContract.pageSize)
                } else {
                    commissionListAdapter.loadMoreEnd()
                }
            } else {
                refreshLayout.isEnableRefresh = false
                commissionPresenter.getRecordList(loginBean!!.userId!!, 1, KeyContract.pageSize)
            }
        }, rv_content)
        rv_content.addOnItemTouchListener(object : OnItemClickListener() {
            override fun onSimpleItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
                val item = commissionListAdapter.getItem(position) as CommissionBean

                val intent = Intent(context(), WebViewActivity::class.java)
                intent.putExtra(KeyContract.Title, "订单详情")
                val url = "${API.DDXQ}?id=${item.id}"
                intent.putExtra(KeyContract.Url, url)
                intent.putExtra(KeyContract.EnableRefresh, false)
                startActivity(intent)
            }
        })
        // 刷新
        refreshLayout.setOnRefreshListener {
            loadData()
        }
    }

    /**
     * 加载数据
     */
    private fun loadData() {
        if (loginBean == null || StringUtils.isEmpty(loginBean!!.userId)) {
            startActivity(Intent(this, LoginActivity::class.java))
            return
        }
        commissionPresenter.getCommissionInfo(loginBean!!.userId!!)
        commissionPresenter.getRecordList(loginBean!!.userId!!, 1, KeyContract.pageSize)
    }

    override fun showCommissionInfo(commissionInfoBean: CommissionInfoBean) {
        tv_commission_today.text = commissionInfoBean.todayCommission.toString()
        tv_commission_mouth.text = commissionInfoBean.thirtyCommission.toString()
        tv_total_commission.text = "累计佣金：" + commissionInfoBean.accumulatedCommission.toString()
    }

    override fun loadRecordList(pageNo: Int, pageResultBean: PageResultBean<CommissionBean>) {
        if (pageNo == 1) {
            commissions.clear()
            commissionListAdapter.setEnableLoadMore(true)
            if (refreshLayout.state.isHeader!!) {
                refreshLayout.finishRefresh()
            }
        }

        if (pageResultBean.records.isNotEmpty()) {
            commissions.addAll(pageResultBean.records)
        }

        if (pageResultBean.total > commissions.size) {
            commissionListAdapter.loadMoreComplete()
        } else {
            commissionListAdapter.loadMoreEnd()
        }

        commissionListAdapter.notifyDataSetChanged()
        refreshLayout.isEnableRefresh = true
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    override fun onDestroy() {
        super.onDestroy()
        commissionPresenter.detachView()
    }
}
