package com.cqhz.quwan.ui.buy

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.CouponConsume
import com.cqhz.quwan.mvp.mine.DoPayContract
import com.cqhz.quwan.mvp.mine.DoPayPresenter
import com.cqhz.quwan.ui.base.AbsActivity
import com.cqhz.quwan.util.md5
import com.cqhz.quwan.util.v
import javax.inject.Inject

class DoPayActivity : AbsActivity(),DoPayContract.View, TextWatcher {
    override fun close4resultCode(resultCode: Int) {
        hintLoading()
        setResult(resultCode)
        finish()
    }


    @Inject lateinit var doPayPresenter:DoPayPresenter
    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        val len = p0!!.length
        resetCb(len)
        if(len == 6){
            val pass = p0.toString().md5()
            if(!isGetCash&&((orderId!=null&&money>=2||couponConsume != null)) ){
                //支付
                hintInput()
                showLoading("正在支付中...")
                doPayPresenter.doPay2(couponConsume!!,pass)
            }else if(isGetCash&&money>=1){
                //提现
                hintInput()
                showLoading("数据请求中...")
                when(payWay){
                    KeyContract.PAY_WAY_ALIPAY -> {
                        doPayPresenter.doGetCash(money,pass)
                    }
                    KeyContract.PAY_WAY_WECHAT -> {
                        doPayPresenter.doGetCash(money,pass,payWay=6)
                    }
                }
            }else{
                showToast("操作失败，请重试")
                finish()
            }
        }
    }


    private fun hintInput(){
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(passwordEdit.windowToken, 0)
//        passwordEdit.isEnabled = false
//        disableShowSoftInput()
    }
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(p0: Editable?) {
    }

    private val closeIv by v<ImageView>(R.id.iv_close)
    private val pl by v<LinearLayout>(R.id.pay_plan)
    private val passwordEdit by v<EditText>(R.id.et_pay_password)
    private val cb1 by v<CheckBox>(R.id.cb_1)
    private val cb2 by v<CheckBox>(R.id.cb_2)
    private val cb3 by v<CheckBox>(R.id.cb_3)
    private val cb4 by v<CheckBox>(R.id.cb_4)
    private val cb5 by v<CheckBox>(R.id.cb_5)
    private val cb6 by v<CheckBox>(R.id.cb_6)
    private val cbs = ArrayList<CheckBox>()

    private var money:Double = 0.0
    private var orderId:String? = null
    private var isGetCash:Boolean = false
    private var payWay:Int = KeyContract.PAY_WAY_ALIPAY
    private var couponConsume: CouponConsume? = null
    override fun layout(): Int {
        return R.layout.activity_pay_password
    }

    override fun onDestroy() {
        doPayPresenter.detachView()
        super.onDestroy()
    }

    override fun initView() {
        doPayPresenter.attachView(this)
        money = intent.getDoubleExtra(KeyContract.ActualAmount,0.0)
        orderId = intent.getStringExtra(KeyContract.OrderId)
        couponConsume = intent.getSerializableExtra(KeyContract.CouponConsume) as CouponConsume?
        isGetCash = intent.getBooleanExtra(KeyContract.IsGetCash,false)
        payWay = intent.getIntExtra(KeyContract.PAY_WAY,KeyContract.PAY_WAY_ALIPAY)
        cbs.add(cb1)
        cbs.add(cb2)
        cbs.add(cb3)
        cbs.add(cb4)
        cbs.add(cb5)
        cbs.add(cb6)
        closeIv.setOnClickListener {
            finish()
        }
        passwordEdit.addTextChangedListener(this)
    }

    private fun resetCb(len:Int){
        for (i in cbs.indices){
            cbs[i].isChecked = i+1<=len
        }
    }

    override fun closePage() {
        setResult(Activity.RESULT_OK)
        finish()
    }

}