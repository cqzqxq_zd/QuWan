package com.cqhz.quwan.ui.mine

import android.app.Activity
import android.content.Intent
import android.widget.EditText
import android.widget.TextView
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.mine.WithdrawsCashContract
import com.cqhz.quwan.mvp.mine.WithdrawsCashPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.ui.buy.DoPayActivity
import com.cqhz.quwan.util.CashierInputFilter
import com.cqhz.quwan.util.format
import com.cqhz.quwan.util.v
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject

class WithdrawsCashActivity : GoBackActivity(), WithdrawsCashContract.View, HasDaggerInject<ActivityInject> {

    companion object {
        const val DO_PAY = 0x11
    }

    override fun showUserInfo(userInfoBean: UserInfoBean) {
        this.userInfoBean = userInfoBean
        val text = "可提现余额：${(userInfoBean.cashAble ?: "0").toDouble().format()} 元"
        balanceTv.text = text
    }

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    private val balanceTv by v<TextView>(R.id.label_withdraws_cash_balance)
    private val withdrawsCashAllBtn by v<TextView>(R.id.tv_withdraws_cash_all)
    private val moneyInputEdit by v<EditText>(R.id.ed_money_input)
    private val getCashBtn by v<TextView>(R.id.btn_get_cash)
    private var userInfoBean: UserInfoBean? = null
    @Inject
    lateinit var presenter: WithdrawsCashPresenter

    override fun layout(): Int {
        return R.layout.activity_withdraws_cash
    }

    override fun titleBarText(): String? {
        return "提现"
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun initView() {
        presenter.attachView(this)
        moneyInputEdit.filters = arrayOf(CashierInputFilter())
        withdrawsCashAllBtn.setOnClickListener {
            if (userInfoBean != null) {
                moneyInputEdit.setText((userInfoBean!!.cashAble ?: "0").toDouble().format())
            }
        }
        getCashBtn.setOnClickListener {
            if (userInfoBean != null) {
                getCashBtn.isEnabled = false
                val maxMoney = (userInfoBean!!.cashAble ?: "0").toDouble()
                val moneyStr = moneyInputEdit.text.toString()
                if (moneyStr.isNotEmpty()) {
                    val money = moneyStr.toDouble()
                    if (money <= maxMoney && money != 0.0) {
                        val intent = Intent(this, DoPayActivity::class.java)
                        intent.putExtra(KeyContract.IsGetCash, true)
                        intent.putExtra(KeyContract.ActualAmount, money)
                        startActivityForResult(intent, DO_PAY)
                    } else if (money == 0.0) {
                        showToast("没有余额可以提现")
                        getCashBtn.isEnabled = true
                    } else {
                        showToast("提现金额超过可用余额")
                        getCashBtn.isEnabled = true
                    }
                } else {
                    val a = showToast("请输入有效的金额")
                    getCashBtn.isEnabled = true
                }
            } else {
                getCashBtn.isEnabled = true
            }
        }
        showLoading("数据加载中")
        presenter.getUserInfo(APP.get()!!.loginInfo!!.userId!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DO_PAY && resultCode == Activity.RESULT_OK) {
            finish()
        } else if (requestCode == DO_PAY && resultCode == Activity.RESULT_CANCELED) {
            getCashBtn.isEnabled = true
        }
    }
}