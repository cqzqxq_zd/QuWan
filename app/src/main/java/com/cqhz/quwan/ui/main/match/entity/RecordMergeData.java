package com.cqhz.quwan.ui.main.match.entity;

import com.cqhz.quwan.model.AnalysisRecord;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class RecordMergeData {

    private String title;
    private List<AnalysisRecord.RecordPOListBean> data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AnalysisRecord.RecordPOListBean> getData() {
        return data;
    }

    public void setData(List<AnalysisRecord.RecordPOListBean> data) {
        this.data = data;
    }
}
