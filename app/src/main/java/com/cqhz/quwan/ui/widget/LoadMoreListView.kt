package com.cqhz.quwan.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.AbsListView
import android.widget.ListView


class LoadMoreListView(context: Context?, attrs: AttributeSet?) : ListView(context, attrs), AbsListView.OnScrollListener {
    override fun onScroll(view: AbsListView?, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
        Log.e(javaClass.simpleName,"onScroll$totalItemCount")
    }

    override fun onScrollStateChanged(view: AbsListView?, scrollState: Int) {
        Log.e(javaClass.simpleName,"onScrollStateChanged$scrollState")
    }
    init {
        setOnScrollListener(this)
    }
}