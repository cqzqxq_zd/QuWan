package com.cqhz.quwan.ui.activity

import android.content.Intent
import android.view.View
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.LoginBean
import com.cqhz.quwan.model.SystemConfigBean
import com.cqhz.quwan.mvp.mall.RechargeContract
import com.cqhz.quwan.mvp.mall.RechargePresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.ui.buy.event.EventObj
import com.cqhz.quwan.ui.login.LoginActivity
import com.cqhz.quwan.util.formatNoZero
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_recharge.*
import me.militch.quickcore.core.HasDaggerInject
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import javax.inject.Inject
import com.google.gson.reflect.TypeToken


/**
 * 充值
 * Created by Guojing on 2018/9/11.
 */
class RechargeActivity : GoBackActivity(), RechargeContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var presenter: RechargePresenter
    private var loginInfo: LoginBean? = null
    private var moneys: Array<String>? = null
    private var amountNum: Double = 0.00
    private var map = HashMap<Int, Boolean>()// 位置，选中状态
    private var isAlipay: Boolean = true

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun layout(): Int {
        return R.layout.activity_recharge
    }

    override fun titleBarText(): String? {
        return "充值"
    }

    override fun onDestroy() {
        presenter.detachView()
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    override fun initView() {
        presenter.attachView(this)
        loginInfo = APP.get()!!.loginInfo
        presenter.getUserInfo(loginInfo!!.userId!!)
        presenter.getSystemConfig(KeyContract.RECHARGE_GIFT_RATIO)
        presenter.getSystemConfig(KeyContract.RECHARGE_HINTS)
        EventBus.getDefault().register(this)

        // 初始化选中状态
        for (i in 0..5) map.set(key = i, value = (i == 0))

        // 点击事件
        action_select_bean0.setOnClickListener { setSelectStyle(0) }
        action_select_bean1.setOnClickListener { setSelectStyle(1) }
        action_select_bean2.setOnClickListener { setSelectStyle(2) }
        action_select_bean3.setOnClickListener { setSelectStyle(3) }
        action_select_bean4.setOnClickListener { setSelectStyle(4) }
        action_select_bean5.setOnClickListener { setSelectStyle(5) }
        // 支付宝
        action_select_alipay.setOnClickListener {
            isAlipay = true
            iv_alipay.setImageResource(R.drawable.ic_selected)
            iv_wechat.setImageResource(R.drawable.ic_default)
        }
        // 微信
        action_select_wechat.setOnClickListener {
            isAlipay = false
            iv_alipay.setImageResource(R.drawable.ic_default)
            iv_wechat.setImageResource(R.drawable.ic_selected)
        }
        // 支付
        action_pay.setOnClickListener {
            val loginInfo = APP.get()!!.loginInfo
            if (loginInfo != null) {
                if (isAlipay) presenter.doRecharge(loginInfo.userId!!, amountNum.toString()) else presenter.doRecharge4wechat(loginInfo.userId!!, amountNum.toString())
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
    }

    override fun setBean(bean: String) {
        tv_funny_bean.text = bean.formatNoZero()
    }

    override fun setMoney(systemConfigBean: SystemConfigBean) {
        val gson = Gson()
        val maps: Map<String, String> = gson.fromJson(systemConfigBean.cval, object : TypeToken<Map<String, String>>() {}.type)
        moneys = maps.keys.toTypedArray()

        tv_bean0.text = (Integer.parseInt(moneys!![0]) * 100).toString()
        tv_bean0_send.text = "赠送" + Integer.parseInt(moneys!![0]) * Integer.parseInt(maps[moneys!![0]]) + "豆"
        tv_bean0_send.visibility = if (Integer.parseInt(maps[moneys!![0]]) > 0) View.VISIBLE else View.GONE
        tv_money0.text = "¥" + moneys!![0]

        tv_bean1.text = (Integer.parseInt(moneys!![1]) * 100).toString()
        tv_bean1_send.text = "赠送" + Integer.parseInt(moneys!![1]) * Integer.parseInt(maps[moneys!![1]]) + "豆"
        tv_bean1_send.visibility = if (Integer.parseInt(maps[moneys!![1]]) > 0) View.VISIBLE else View.GONE
        tv_money1.text = "¥" + moneys!![1]

        tv_bean2.text = (Integer.parseInt(moneys!![2]) * 100).toString()
        tv_bean2_send.text = "赠送" + Integer.parseInt(moneys!![2]) * Integer.parseInt(maps[moneys!![2]]) + "豆"
        tv_bean2_send.visibility = if (Integer.parseInt(maps[moneys!![2]]) > 0) View.VISIBLE else View.GONE
        tv_money2.text = "¥" + moneys!![2]

        tv_bean3.text = (Integer.parseInt(moneys!![3]) * 100).toString()
        tv_bean3_send.text = "赠送" + Integer.parseInt(moneys!![3]) * Integer.parseInt(maps[moneys!![3]]) + "豆"
        tv_bean3_send.visibility = if (Integer.parseInt(maps[moneys!![3]]) > 0) View.VISIBLE else View.GONE
        tv_money3.text = "¥" + moneys!![3]

        tv_bean4.text = (Integer.parseInt(moneys!![4]) * 100).toString()
        tv_bean4_send.text = "赠送" + Integer.parseInt(moneys!![4]) * Integer.parseInt(maps[moneys!![4]]) + "豆"
        tv_bean4_send.visibility = if (Integer.parseInt(maps[moneys!![4]]) > 0) View.VISIBLE else View.GONE
        tv_money4.text = "¥" + moneys!![4]

        tv_bean5.text = (Integer.parseInt(moneys!![5]) * 100).toString()
        tv_bean5_send.text = "赠送" + Integer.parseInt(moneys!![5]) * Integer.parseInt(maps[moneys!![5]]) + "豆"
        tv_bean5_send.visibility = if (Integer.parseInt(maps[moneys!![5]]) > 0) View.VISIBLE else View.GONE
        iv_big_sale5.visibility = if (Integer.parseInt(maps[moneys!![5]]) > 0) View.VISIBLE else View.GONE
        tv_money5.text = "¥" + moneys!![5]

        amountNum = if (moneys != null) moneys!![0].toDouble() else 50.00
        action_pay.text = "立即支付 " + amountNum.toString().formatNoZero() + "元"
    }

    override fun setTips(systemConfigBean: SystemConfigBean) {
        tv_tips.text = systemConfigBean.cval
    }

    /**
     * 设置选中的状态
     */
    private fun setSelectStyle(position: Int) {
        // 设置列表的状态
        for (i in 0..5) map[i] = i == position


        // 设置选中的付款值
        when (position) {
            0 -> amountNum = if (moneys != null) moneys!![0].toDouble() else 50.00
            1 -> amountNum = if (moneys != null) moneys!![1].toDouble() else 100.00
            2 -> amountNum = if (moneys != null) moneys!![2].toDouble() else 500.00
            3 -> amountNum = if (moneys != null) moneys!![3].toDouble() else 1000.00
            4 -> amountNum = if (moneys != null) moneys!![4].toDouble() else 2000.00
            5 -> amountNum = if (moneys != null) moneys!![5].toDouble() else 5000.00
        }

        // 设置文件资源
        iv_bg0.setImageResource(if (map[0]!!) R.drawable.recharge_img_selected else R.drawable.recharge_img_default)
        iv_bg1.setImageResource(if (map[1]!!) R.drawable.recharge_img_selected else R.drawable.recharge_img_default)
        iv_bg2.setImageResource(if (map[2]!!) R.drawable.recharge_img_selected else R.drawable.recharge_img_default)
        iv_bg3.setImageResource(if (map[3]!!) R.drawable.recharge_img_selected else R.drawable.recharge_img_default)
        iv_bg4.setImageResource(if (map[4]!!) R.drawable.recharge_img_selected else R.drawable.recharge_img_default)
        iv_bg5.setImageResource(if (map[5]!!) R.drawable.recharge_img_selected else R.drawable.recharge_img_default)
//        tv_bean0.setTextColor(if (map[0]!!) Color.parseColor("#EC0411") else Color.parseColor("#333333"))
//        tv_bean1.setTextColor(if (map[1]!!) Color.parseColor("#EC0411") else Color.parseColor("#333333"))
//        tv_bean2.setTextColor(if (map[2]!!) Color.parseColor("#EC0411") else Color.parseColor("#333333"))
//        tv_bean3.setTextColor(if (map[3]!!) Color.parseColor("#EC0411") else Color.parseColor("#333333"))
//        tv_bean4.setTextColor(if (map[4]!!) Color.parseColor("#EC0411") else Color.parseColor("#333333"))
//        tv_bean5.setTextColor(if (map[5]!!) Color.parseColor("#EC0411") else Color.parseColor("#333333"))
//        tv_money0.setTextColor(if (map[0]!!) Color.parseColor("#EC0411") else Color.parseColor("#666666"))
//        tv_money1.setTextColor(if (map[1]!!) Color.parseColor("#EC0411") else Color.parseColor("#666666"))
//        tv_money2.setTextColor(if (map[2]!!) Color.parseColor("#EC0411") else Color.parseColor("#666666"))
//        tv_money3.setTextColor(if (map[3]!!) Color.parseColor("#EC0411") else Color.parseColor("#666666"))
//        tv_money4.setTextColor(if (map[4]!!) Color.parseColor("#EC0411") else Color.parseColor("#666666"))
//        tv_money5.setTextColor(if (map[5]!!) Color.parseColor("#EC0411") else Color.parseColor("#666666"))

        action_pay.text = "立即支付 " + amountNum.toString().formatNoZero() + "元"
    }

    override fun alipayCallBack() {
        finish()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun payEvent(eventObj: EventObj) {
        if (eventObj.key == 0x998) {
            finish()
        }
    }

}
