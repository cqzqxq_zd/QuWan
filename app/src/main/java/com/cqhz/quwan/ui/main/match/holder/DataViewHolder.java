package com.cqhz.quwan.ui.main.match.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.main.match.entity.Forecast;

/**
 * @author whamu2
 * @date 2018/7/18
 */
public class DataViewHolder extends RecyclerView.ViewHolder {

    private TextView mTitleTextView;
    private TextView mZSTextView;
    private TextView mZSTitleTextView;
    private TextView mPTextView;
    private TextView mPTitleTextView;
    private TextView mKSTextView;
    private TextView mKSTitleTextView;

    public DataViewHolder(View itemView) {
        super(itemView);
        mTitleTextView = itemView.findViewById(R.id.tv_title);
        mZSTextView = itemView.findViewById(R.id.tv_zs);
        mPTextView = itemView.findViewById(R.id.tv_p);
        mKSTextView = itemView.findViewById(R.id.tv_ks);
        mZSTitleTextView = itemView.findViewById(R.id.tv_zs_title);
        mPTitleTextView = itemView.findViewById(R.id.tv_p_title);
        mKSTitleTextView = itemView.findViewById(R.id.tv_ks_title);
    }

    public void bind(Forecast item, int position) {
        mTitleTextView.setText(item.getTitle());
        mZSTextView.setText(item.getWin());
        mPTextView.setText(item.getDraw());
        mKSTextView.setText(item.getLose());
//        String win = item.getWin().replaceAll("%", "");
//        String draw = item.getDraw().replaceAll("%", "");
//        String lose = item.getLose().replaceAll("%", "");
//
//        double winD = Double.parseDouble(win);
//        double drawD = Double.parseDouble(draw);
//        double loseD = Double.parseDouble(lose);


    }
}
