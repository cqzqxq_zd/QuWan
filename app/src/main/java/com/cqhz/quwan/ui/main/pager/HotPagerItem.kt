package com.cqhz.quwan.ui.main.pager

import com.cqhz.quwan.model.HotBean

class HotPagerItem(data: HotBean) {

    var data: HotBean = data

    companion object {
        fun getData(hotPagerItem: HotPagerItem): HotBean? {
            return hotPagerItem.data
        }
    }
}