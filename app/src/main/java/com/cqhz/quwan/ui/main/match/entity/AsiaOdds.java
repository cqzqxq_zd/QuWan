package com.cqhz.quwan.ui.main.match.entity;

import com.cqhz.quwan.model.OddsData;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/26
 */
public class AsiaOdds {
    private String flag;
    private List<OddsData.OddsResultBean> results;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<OddsData.OddsResultBean> getResult() {
        return results;
    }

    public void setResult(List<OddsData.OddsResultBean> results) {
        this.results = results;
    }
}
