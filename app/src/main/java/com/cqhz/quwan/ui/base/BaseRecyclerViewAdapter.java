package com.cqhz.quwan.ui.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author whamu2
 * @date 2018/6/9
 */
public abstract class BaseRecyclerViewAdapter<D, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private List<D> Data = new ArrayList<>();
    private Context mContext;

    public Context getContext() {
        return mContext;
    }

    public BaseRecyclerViewAdapter(Context c) {
        this.mContext = c;
    }

    public void cleanData() {
        Data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setData(List<D> data) {
        if (data == null) Data = new ArrayList<>();
        else Data = data;
        notifyDataSetChanged();
    }

    public void setDataArray(D[] data) {
        Data = new ArrayList<>();
        if (data != null)
            Collections.addAll(Data, data);
        notifyDataSetChanged();
    }

    public List<D> getData() {
        return Data;
    }

    /**
     * 追加数据
     */
    public void addData(D data) {
        if (data != null) {
            Data.add(data);
            notifyDataSetChanged();
        }
    }

    /**
     * 追加数据
     */
    public void addData(List<D> data) {
        if (data != null) {
            Data.addAll(data);
            notifyDataSetChanged();
        }
    }

    /**
     * 追加数据
     */
    public void addData(int index, D data) {
        if (data != null) {
            Data.add(index, data);
            notifyDataSetChanged();
        }
    }

    /**
     * 追加数据
     */
    public void addData(int index, List<D> data) {
        if (data != null) {
            Data.addAll(index, data);
            notifyDataSetChanged();
        }
    }

    /**
     * 删除数据
     *
     * @param position
     */
    public void remove(int position) {
        if (position < 0) {
            return;
        } else {
            Data.remove(position);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return Data.size();
    }

    public D getItem(int position) {
        return Data.get(position);
    }
}
