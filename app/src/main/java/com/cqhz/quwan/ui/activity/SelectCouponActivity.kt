package com.cqhz.quwan.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.CouponBean
import com.cqhz.quwan.util.CommonAdapterHelper
import kotlinx.android.synthetic.main.activity_select_coupon.*
import me.militch.quickcore.core.HasDaggerInject

/**
 * 选择红包
 * Created by Guojing on 2018/12/12.
 */
class SelectCouponActivity : Activity(), HasDaggerInject<ActivityInject> {

    private lateinit var couponListAdapter: BaseQuickAdapter<*, *>
    private var couponList = ArrayList<CouponBean>()
    private var couponBean: CouponBean? = null
    private var isUseCoupon: Boolean = true// 是否使用优惠券

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_coupon)
        initAdapter()
        setOnClick()
    }

    /**
     * 初始化列表适配器
     */
    private fun initAdapter() {
        couponList.addAll(intent.getParcelableArrayListExtra(KeyContract.List))
        couponBean = intent.getParcelableExtra<CouponBean>(KeyContract.Bean)
        isUseCoupon = intent.getBooleanExtra(KeyContract.Selected, true)

        // 设置选中状态
        if (isUseCoupon) {
            for (i in 0 until couponList.size) {
                if (couponBean != null && couponList[i].couponId == couponBean!!.couponId) {
                    couponList[i].isSelected = couponBean!!.isSelected
                } else {
                    couponList[i].isSelected = false
                }
            }
        }
        setSelectNoneStyle()

        // 当前页面列表的空布局
        var emptyView = LayoutInflater.from(this).inflate(R.layout.layout_none, rv_content.parent as? ViewGroup, false) as View
        var ivNone = emptyView.findViewById(R.id.iv_none) as ImageView
        ivNone.setImageResource(0)

        // 初始化适配器
        couponListAdapter = CommonAdapterHelper.getSelectCouponListAdapter(couponList)
        couponListAdapter.emptyView = emptyView

        // 当前页面列表适配器的事件
        rv_content.layoutManager = LinearLayoutManager(this)
        rv_content.adapter = couponListAdapter
        rv_content.addOnItemTouchListener(object : OnItemClickListener() {
            override fun onSimpleItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
                val item = couponListAdapter.getItem(position) as CouponBean
                couponBean = item
                for (i in 0 until couponList.size) {
                    couponList[i].isSelected = i == position
                }
                couponListAdapter.notifyDataSetChanged()
                isUseCoupon = true
                setSelectNoneStyle()
            }
        })
    }

    /**
     * 设置选中状态
     */
    private fun setSelectNoneStyle() {
        val select = resources.getDrawable(if (!isUseCoupon) R.drawable.checkout_choose_selected else R.drawable.checkout_choose_default)
        select.setBounds(0, 0, select.minimumWidth, select.minimumHeight)
        action_select_none.setCompoundDrawables(null, null, select, null)
    }

    /**
     * 设置控件的点击事件
     */
    private fun setOnClick() {
        action_closed.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        action_select_none.setOnClickListener {
            isUseCoupon = false
            if (!isUseCoupon) {
                for (i in 0 until couponList.size) {
                    couponList[i].isSelected = false
                }
                couponListAdapter.notifyDataSetChanged()
            }
            setSelectNoneStyle()
        }
        action_confirm.setOnClickListener {
            val intent = Intent()
            if (isUseCoupon && couponBean != null && couponBean!!.status == 0) {
                intent.putExtra(KeyContract.Bean, couponBean)
            }
            intent.putExtra(KeyContract.Selected, isUseCoupon)
            setResult(RESULT_OK, intent)
            finish()
            overridePendingTransition(0, 0)
        }
    }
}
