package com.cqhz.quwan.ui.main.match.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.main.match.entity.Forecast;

import java.text.MessageFormat;

/**
 * @author whamu2
 * @date 2018/7/18
 */
public class NewsViewHolder extends RecyclerView.ViewHolder {

    private TextView mSubjectTextView;
    private ImageView mIcon;
    private LinearLayout mSubjectLayout;

    private TextView mTitle;
    private TextView mSource;
    private TextView mTime;

    public NewsViewHolder(View itemView) {
        super(itemView);
        mSubjectLayout = itemView.findViewById(R.id.ll_subject);
        mSubjectTextView = itemView.findViewById(R.id.tv_subject);
        mIcon = itemView.findViewById(R.id.iv_icon);
        mTitle = itemView.findViewById(R.id.tv_title);
        mSource = itemView.findViewById(R.id.tv_source);
        mTime = itemView.findViewById(R.id.tv_time);
    }

    public void bind(Forecast item, int position) {

    }
}
