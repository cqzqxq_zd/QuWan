package com.cqhz.quwan.ui.base

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.cqhz.quwan.R
import com.cqhz.quwan.util.v


abstract class AbsTitleActivity : AbsActivity() {
    private val titleBarLeftIV: ImageView by v(R.id.bar_left)
    private val titleBarTextView: TextView by v(R.id.bar_text)
    private val titleBarRightIV: ImageView by v(R.id.bar_right)
    private val barRightText: TextView by v(R.id.bar_right_text)
    private val titleBarFilterIv: ImageView? by v(R.id.bar_right_filter)

    override fun beforeInitView() {
        super.beforeInitView()
        if (titleBarLeftIconResId() == null) {
            titleBarLeftIV.visibility = View.INVISIBLE
        } else {
            titleBarLeftIV.visibility = View.VISIBLE
            titleBarLeftIV.setImageResource(titleBarLeftIconResId()!!)
            titleBarLeftIV.setOnClickListener {
                onTitleBarLeftClick()
            }
        }
        if (titleBarRightIconResId() == null) {
            titleBarRightIV.visibility = View.INVISIBLE
        } else {
            titleBarRightIV.visibility = View.VISIBLE
            titleBarRightIV.setImageResource(titleBarRightIconResId()!!)
            titleBarRightIV.setOnClickListener {
                onTitleBarRightClick(it)
            }
        }
        if (titleBarText() == null) {
            titleBarTextView.visibility = View.VISIBLE
        } else {
            titleBarTextView.visibility = View.VISIBLE
            if (onTitleTextClickFlag()) {
                titleBarTextView.setOnClickListener { onTitleTextClick(it) }
            }
            titleBarTextView.text = titleBarText()
        }

        if (titleRightFilterIconResId() == null) {
            titleBarFilterIv?.visibility = View.GONE
        } else {
            titleBarFilterIv?.visibility = View.VISIBLE
            titleBarFilterIv?.setImageResource(titleRightFilterIconResId()!!)
            if (handlerRightFilterClick() != null) {
                titleBarFilterIv?.setOnClickListener(handlerRightFilterClick())
            }
        }
    }

    fun setTitleBarRightText(text: String, on: ((View) -> Unit)? = null) {
        barRightText.visibility = View.VISIBLE
        barRightText.text = text
        barRightText.setOnClickListener(on)
    }

    fun setTitleBarText(text: String) {
        this.titleBarTextView.text = text
    }

    open fun titleBarLeftIconResId(): Int? {
        return null
    }

    open fun titleBarRightIconResId(): Int? {
        return null
    }

    open fun titleBarText(): String? {
        return this.titleBarTextView.text.toString()
    }

    open fun onTitleTextClickFlag(): Boolean {
        return true
    }

    open fun onTitleBarLeftClick() {

    }

    open fun onTitleBarRightClick(view: View) {

    }

    open fun onTitleTextClick(view: View) {}

    open fun titleRightFilterIconResId(): Int? {
        return null
    }

    open fun handlerRightFilterClick(): ((View) -> Unit)? {
        return null
    }
}