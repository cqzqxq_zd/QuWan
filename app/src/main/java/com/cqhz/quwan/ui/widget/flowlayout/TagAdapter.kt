package com.cqhz.quwan.ui.widget.flowlayout


import android.util.Log
import android.view.View
import java.util.*

/**
 * 流式布局-TagAdapter
 * Created by Guojing on 2018/9/12.
 */
abstract class TagAdapter<T> {
    private var mTagDatas: List<T>? = null
    private var mOnDataChangedListener: OnDataChangedListener? = null
    @Deprecated("")
    @get:Deprecated("")
    internal val preCheckedList = HashSet<Int>()

    val count: Int get() = if (mTagDatas == null) 0 else mTagDatas!!.size

    constructor(datas: List<T>) {
        mTagDatas = datas
    }

    @Deprecated("")
    constructor(datas: Array<T>) {
        mTagDatas = ArrayList(Arrays.asList(*datas))
    }

    internal interface OnDataChangedListener {
        fun onChanged()
    }

    internal fun setOnDataChangedListener(listener: OnDataChangedListener) {
        mOnDataChangedListener = listener
    }

    @Suppress("DEPRECATION")
    @Deprecated("")
    fun setSelectedList(vararg poses: Int) {
        val set = HashSet<Int>()
        for (pos in poses) {
            set.add(pos)
        }
        setSelectedList(set)
    }

    @Suppress("DEPRECATION")
    @Deprecated("")
    fun setSelectedList(set: Set<Int>?) {
        preCheckedList.clear()
        if (set != null) {
            preCheckedList.addAll(set)
        }
        notifyDataChanged()
    }

    fun notifyDataChanged() {
        if (mOnDataChangedListener != null)
            mOnDataChangedListener!!.onChanged()
    }

    fun getItem(position: Int): T {
        return mTagDatas!![position]
    }

    abstract fun getView(parent: TagFlowLayout, position: Int, t: Any): View

    fun onSelected(position: Int, view: View) {
        Log.d("TagAdapter", "onSelected $position")
    }

    fun unSelected(position: Int, view: View) {
        Log.d("TagAdapter", "unSelected $position")
    }

    fun setSelected(position: Int, t: Any): Boolean {
        return false
    }
}