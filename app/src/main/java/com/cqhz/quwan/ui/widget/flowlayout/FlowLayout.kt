package com.cqhz.quwan.ui.widget.flowlayout

import android.content.Context
import android.support.v4.text.TextUtilsCompat
import android.util.AttributeSet
import android.util.LayoutDirection
import android.view.View
import android.view.ViewGroup
import com.cqhz.quwan.R
import java.util.*

/**
 * 流式布局-FlowLayout
 * Created by Guojing on 2018/9/12.
 */
open class FlowLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : ViewGroup(context, attrs, defStyle) {

    protected var mAllViews: MutableList<List<View>> = ArrayList<List<View>>()
    private var mLineHeight: MutableList<Int> = ArrayList()
    private var mLineWidth: MutableList<Int> = ArrayList()
    private var mGravity: Int = 0
    private var lineViews: MutableList<View> = ArrayList()

    init {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.TagFlowLayout)
        mGravity = ta.getInt(R.styleable.TagFlowLayout_tag_gravity, LEFT)
        val layoutDirection = TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault())
        if (layoutDirection == LayoutDirection.RTL) {
            if (mGravity == LEFT) {
                mGravity = RIGHT
            } else {
                mGravity = LEFT
            }
        }
        ta.recycle()
    }

    protected override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val sizeWidth = MeasureSpec.getSize(widthMeasureSpec)
        val modeWidth = MeasureSpec.getMode(widthMeasureSpec)
        val sizeHeight = MeasureSpec.getSize(heightMeasureSpec)
        val modeHeight = MeasureSpec.getMode(heightMeasureSpec)

        // wrap_content
        var width = 0
        var height = 0

        var lineWidth = 0
        var lineHeight = 0

        val cCount = getChildCount()

        for (i in 0 until cCount) {
            val child = getChildAt(i)
            if (child.getVisibility() === View.GONE) {
                if (i == cCount - 1) {
                    width = Math.max(lineWidth, width)
                    height += lineHeight
                }
                continue
            }
            measureChild(child, widthMeasureSpec, heightMeasureSpec)
            val lp = child
                    .getLayoutParams() as MarginLayoutParams

            val childWidth = (child.getMeasuredWidth() + lp.leftMargin
                    + lp.rightMargin)
            val childHeight = (child.getMeasuredHeight() + lp.topMargin
                    + lp.bottomMargin)

            if (lineWidth + childWidth > sizeWidth - getPaddingLeft() - getPaddingRight()) {
                width = Math.max(width, lineWidth)
                lineWidth = childWidth
                height += lineHeight
                lineHeight = childHeight
            } else {
                lineWidth += childWidth
                lineHeight = Math.max(lineHeight, childHeight)
            }
            if (i == cCount - 1) {
                width = Math.max(lineWidth, width)
                height += lineHeight
            }
        }
        setMeasuredDimension(
                //
                if (modeWidth == MeasureSpec.EXACTLY) sizeWidth else width + getPaddingLeft() + getPaddingRight(),
                if (modeHeight == MeasureSpec.EXACTLY) sizeHeight else height + getPaddingTop() + getPaddingBottom()//
        )

    }


    protected override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        mAllViews.clear()
        mLineHeight.clear()
        mLineWidth.clear()
        lineViews.clear()

        val width = getWidth()

        var lineWidth = 0
        var lineHeight = 0

        val cCount = getChildCount()

        for (i in 0 until cCount) {
            val child = getChildAt(i)
            if (child.getVisibility() === View.GONE) continue
            val lp = child.getLayoutParams() as MarginLayoutParams

            val childWidth = child.getMeasuredWidth()
            val childHeight = child.getMeasuredHeight()

            if (childWidth + lineWidth + lp.leftMargin + lp.rightMargin > width - getPaddingLeft() - getPaddingRight()) {
                mLineHeight.add(lineHeight)
                mAllViews.add(lineViews)
                mLineWidth.add(lineWidth)

                lineWidth = 0
                lineHeight = childHeight + lp.topMargin + lp.bottomMargin
                lineViews = ArrayList<View>()
            }
            lineWidth += childWidth + lp.leftMargin + lp.rightMargin
            lineHeight = Math.max(lineHeight, childHeight + lp.topMargin
                    + lp.bottomMargin)
            lineViews.add(child)

        }
        mLineHeight.add(lineHeight)
        mLineWidth.add(lineWidth)
        mAllViews.add(lineViews)


        var left = getPaddingLeft()
        var top = getPaddingTop()

        val lineNum = mAllViews.size

        for (i in 0 until lineNum) {
            lineViews = mAllViews[i].toMutableList()
            lineHeight = mLineHeight[i]

            // set gravity
            val currentLineWidth = this.mLineWidth[i]
            when (this.mGravity) {
                LEFT -> left = getPaddingLeft()
                CENTER -> left = (width - currentLineWidth) / 2 + getPaddingLeft()
                RIGHT -> {
                    //  适配了rtl，需要补偿一个padding值
                    left = width - (currentLineWidth + getPaddingLeft()) - getPaddingRight()
                    //  适配了rtl，需要把lineViews里面的数组倒序排
                    Collections.reverse(lineViews)
                }
            }

            for (j in lineViews.indices) {
                val child = lineViews[j]
                if (child.getVisibility() === View.GONE) {
                    continue
                }

                val lp = child.getLayoutParams() as MarginLayoutParams

                val lc = left + lp.leftMargin
                val tc = top + lp.topMargin
                val rc = lc + child.getMeasuredWidth()
                val bc = tc + child.getMeasuredHeight()

                child.layout(lc, tc, rc, bc)

                left += (child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin)
            }
            top += lineHeight
        }

    }

    override fun generateLayoutParams(attrs: AttributeSet): LayoutParams {
        return MarginLayoutParams(getContext(), attrs)
    }

    protected override fun generateDefaultLayoutParams(): LayoutParams {
        return MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
    }

    protected override fun generateLayoutParams(p: LayoutParams): LayoutParams {
        return MarginLayoutParams(p)
    }

    companion object {
        private val TAG = "FlowLayout"
        private val LEFT = -1
        private val CENTER = 0
        private val RIGHT = 1
    }
}