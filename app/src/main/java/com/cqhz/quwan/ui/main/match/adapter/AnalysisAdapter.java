package com.cqhz.quwan.ui.main.match.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cqhz.quwan.R;
import com.cqhz.quwan.ui.base.BaseRecyclerViewAdapter;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/18
 */
public class AnalysisAdapter extends BaseRecyclerViewAdapter<String, AnalysisAdapter.ViewHolder> {

    public AnalysisAdapter(Context c) {
        super(c);
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            strings.add(MessageFormat.format("第{0}行", i + 1));
        }
        setData(strings);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_analysis_info, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position), position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text);
        }

        void bind(String item, int position) {
            textView.setText(item);
        }
    }
}
