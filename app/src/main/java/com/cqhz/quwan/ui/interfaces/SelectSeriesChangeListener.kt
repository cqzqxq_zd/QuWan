package com.cqhz.quwan.ui.interfaces

import com.cqhz.quwan.util.BallBunch

/**
 * 足彩串法选择事件
 * Created by Guojing on 2018/11/22.
 */
interface SelectSeriesChangeListener {

    /**
     * 选择或取消
     * @param  type            0上面默认显示的列表 1更多列表
     * @param  adapterPosition
     * @param  bean
     */
    fun onItemSelectChange(type: Int, adapterPosition: Int, bean: BallBunch)
}