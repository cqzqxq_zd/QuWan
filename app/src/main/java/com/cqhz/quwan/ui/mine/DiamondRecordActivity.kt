package com.cqhz.quwan.ui.mine

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import com.blankj.utilcode.util.StringUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.LoginBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.model.TransactionRecordBean
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.mine.DiamondsRecordContract
import com.cqhz.quwan.mvp.mine.DiamondsRecordPresenter
import com.cqhz.quwan.service.API
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.ui.base.WebViewActivity
import com.cqhz.quwan.ui.login.LoginActivity
import com.cqhz.quwan.util.CommonAdapterHelper
import com.cqhz.quwan.util.formatMoney
import kotlinx.android.synthetic.main.activity_diamond_record.*
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject

/**
 * 钻石记录
 * Created by Guojing on 2018/9/10.
 */
class DiamondRecordActivity : GoBackActivity(), DiamondsRecordContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var diamondsRecordPresenter: DiamondsRecordPresenter
    private var loginInfo: LoginBean? = null
    private lateinit var diamondsRecordListAdapter: BaseQuickAdapter<*, *>
    private val records = ArrayList<TransactionRecordBean>()

    override fun layout(): Int {
        return R.layout.activity_diamond_record
    }

    override fun titleBarText(): String? {
        return "我的钻石"
    }

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun initView() {
        diamondsRecordPresenter.attachView(this)
        loginInfo = APP.get()!!.loginInfo
        // 获取和设置适配器
        diamondsRecordListAdapter = CommonAdapterHelper.getDiamondsRecordListAdapter(this, records)
        rv_content.layoutManager = LinearLayoutManager(this)
        rv_content.adapter = diamondsRecordListAdapter
        // 加载更多
        diamondsRecordListAdapter.setOnLoadMoreListener({
            if (records.size > 0) {
                if (records.size % KeyContract.pageSize == 0) {
                    val pageNo = records.size / KeyContract.pageSize + 1
                    refreshLayout.isEnableRefresh = false
                    diamondsRecordPresenter.getRecordList(loginInfo!!.userId!!, pageNo, KeyContract.pageSize)
                } else {
                    diamondsRecordListAdapter.loadMoreEnd()
                }
            } else {
                refreshLayout.isEnableRefresh = false
                diamondsRecordPresenter.getRecordList(loginInfo!!.userId!!, 1, KeyContract.pageSize)
            }
        }, rv_content)
        // 刷新
        refreshLayout.setOnRefreshListener {
            loadData()
        }
        action_goto_web.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(KeyContract.Title, "趣豆寻宝")
            val url = "${API.findTreasure}?userId=${loginInfo!!.userId}"
            intent.putExtra(KeyContract.Url, url)
            startActivity(intent)
        }
        // 加载数据
        loadData()
    }

    /**
     * 加载数据
     */
    private fun loadData() {
        if (loginInfo == null || StringUtils.isEmpty(loginInfo!!.userId)) {
            startActivity(Intent(this, LoginActivity::class.java))
            return
        }
        diamondsRecordPresenter.getUserInfo(loginInfo!!.userId!!)
        diamondsRecordPresenter.getRecordList(loginInfo!!.userId!!, 1, KeyContract.pageSize)
    }

    override fun loadRecordList(pageNo: Int, pageResultBean: PageResultBean<TransactionRecordBean>) {
        if (pageNo == 1) {
            records.clear()
            diamondsRecordListAdapter.setEnableLoadMore(true)
            diamondsRecordListAdapter.notifyDataSetChanged()
            if (refreshLayout?.state?.isHeader!!) {
                refreshLayout?.finishRefresh()
            }
        }

        if (pageResultBean.records.isNotEmpty()) {
            records.addAll(pageResultBean.records)
        }

        if (pageResultBean.total > records.size) {
            diamondsRecordListAdapter.loadMoreComplete()
        } else {
            diamondsRecordListAdapter.loadMoreEnd()
        }

        refreshLayout.isEnableRefresh = true
    }

    override fun showUserInfo(userInfoBean: UserInfoBean) {
        tv_diamonds.text = userInfoBean.diamonds.formatMoney()
    }

    override fun onDestroy() {
        diamondsRecordPresenter.detachView()
        super.onDestroy()
    }
}
