package com.cqhz.quwan.ui.mine

import android.view.View
import cn.jpush.android.e.a.b.showToast
import cn.qqtheme.framework.entity.City
import cn.qqtheme.framework.entity.County
import cn.qqtheme.framework.entity.Province
import com.blankj.utilcode.util.StringUtils
import com.blankj.utilcode.util.StringUtils.isEmpty
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.R.id.*
import com.cqhz.quwan.model.AddressBean
import com.cqhz.quwan.mvp.mine.AddressContract
import com.cqhz.quwan.mvp.mine.AddressPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.util.AddressPickTask
import kotlinx.android.synthetic.main.activity_address.*
import javax.inject.Inject


/**
 * 收货地址
 * Created by Guojing on 2018/9/6.
 */
class AddressActivity : GoBackActivity(), AddressContract.View {

    @Inject
    lateinit var addressPresenter: AddressPresenter
    private var isEdit: Boolean = false
    private var addressBean: AddressBean = AddressBean()

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun layout(): Int {
        return R.layout.activity_address
    }

    override fun initView() {
        addressPresenter.attachView(this)
        val loginInfo = APP.get()!!.loginInfo
        var userId = loginInfo!!.userId
        if (loginInfo?.userId != null) {
            addressPresenter.getUserInfo(loginInfo.userId!!)
        }

        setTitleBarRightText("修改", ({
            isEdit = true
            setEditStyle()
        }))

        // 选择收货地址
        action_goto_select_address.setOnClickListener {
            if (!isEdit) return@setOnClickListener
            selectAddress()
        }

        // 保存
        action_save.setOnClickListener {
            var receiverName = et_name.text.toString().trim()
            var phoneNumber = et_phone.text.toString().trim()
            var address = tv_address.text.toString().trim()
            var detailAddress = et_address_detail.text.toString().trim()

            if (!isEdit) return@setOnClickListener
            when {
                isEmpty(receiverName) -> {
                    showToast("请输入收货人姓名")
                    return@setOnClickListener
                }
                isEmpty(phoneNumber) -> {
                    showToast("请输入收货人手机号码")
                    return@setOnClickListener
                }
                isEmpty(address) -> {
                    showToast("请选择所在地区")
                    return@setOnClickListener
                }
                isEmpty(detailAddress) -> {
                    showToast("请补充详细地址")
                    return@setOnClickListener
                }
                else -> {
                    addressBean.receiverName = receiverName
                    addressBean.phoneNumber = phoneNumber
                    addressBean.detailAddress = detailAddress
                    addressPresenter.saveAddress(userId!!, addressBean)// 保存收货地址
                }
            }
        }
    }

    /**
     *  设置编辑样式
     */
    private fun setEditStyle() {
        setTitleBarRightText(if (isEdit) "" else "修改") {
            isEdit = true
            setEditStyle()
        }

        // 收货人
        et_name.isCursorVisible = isEdit
        et_name.isFocusable = isEdit
        et_name.isFocusableInTouchMode = isEdit
        // 手机号码
        et_phone.isCursorVisible = isEdit
        et_phone.isFocusable = isEdit
        et_phone.isFocusableInTouchMode = isEdit
        // 详细地址
        et_address_detail.isCursorVisible = isEdit
        et_address_detail.isFocusable = isEdit
        et_address_detail.isFocusableInTouchMode = isEdit
        action_save.visibility = if (isEdit) View.VISIBLE else View.GONE
    }

    override fun titleBarText(): String? {
        return "收货地址"
    }

    /**
     * 选择地址
     */
    private fun selectAddress() {
        val task = AddressPickTask(this)
        task.setHideProvince(false)
        task.setHideCounty(false)
        task.setCallback(object : AddressPickTask.Callback {
            override fun onAddressInitFailed() {
                showToast("数据初始化失败")
            }

            override fun onAddressPicked(province: Province, city: City, county: County) {
                addressBean.province = province.areaName
                addressBean.city = city.areaName
                addressBean.county = county.areaName
                tv_address.text = addressBean.province + addressBean.city + addressBean.county// 所在地区
            }
        })

        if (StringUtils.isEmpty(addressBean.province) || StringUtils.isEmpty(addressBean.city) || StringUtils.isEmpty(addressBean.county)) {
            task.execute("重庆市", "重庆市", "江北区")
        } else {
            task.execute(addressBean.province, addressBean.city, addressBean.county)
        }
    }

    override fun setAddress(address: AddressBean) {
        this.addressBean = address
        et_name.setText(addressBean.receiverName)// 收货人
        et_phone.setText(addressBean.phoneNumber)// 手机号码
        tv_address.text = addressBean.province + addressBean.city + addressBean.county// 所在地区
        et_address_detail.setText(addressBean.detailAddress)// 详细地址
    }

    override fun updateSuccess() {
        showToast("保存成功")
        finish()
    }

    override fun onDestroy() {
        addressPresenter.detachView()
        super.onDestroy()
    }

}
