package com.cqhz.quwan.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.cqhz.quwan.R
import kotlinx.android.synthetic.main.widget_button_attribute.view.*

/**
 * 商品属性按钮
 * Created by Guojing on 2018/9/12.
 */
class AttributeButton : LinearLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.widget_button_attribute, this)
    }

    /**
     * 设置显示的文字
     */
    fun setText(text: String) {
        tv_attribute_name.text = text
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        tv_attribute_name.isEnabled = enabled
        if (enabled) {
            isSelected = false
        }
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        tv_attribute_name.isSelected = selected
    }
}
