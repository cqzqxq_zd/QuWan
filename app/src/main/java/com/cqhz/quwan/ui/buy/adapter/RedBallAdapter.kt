package com.cqhz.quwan.ui.buy.adapter

import android.content.Context
import android.widget.CheckBox
import com.cqhz.quwan.R
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.util.slice

class RedBallAdapter(
        context: Context
) : AbsAdapter<Int>(
        context, R.layout.item_red_ball) {
    private val checks = ArrayList<Int>()
    private var listener: ((List<Int>) -> Unit)? = null
    override fun handlerViewHolder(viewHolder: ViewHolder,position: Int, itemData: Int?) {
        viewHolder.setText(R.id.item_ball_cb,itemData?.slice(2))
        if(checks.contains(itemData)){
            viewHolder.checked(R.id.item_ball_cb,true)
        }else {
            viewHolder.checked(R.id.item_ball_cb,false)
        }
        val cb = viewHolder.find<CheckBox>(R.id.item_ball_cb)
        cb.tag = itemData
        cb.setOnCheckedChangeListener{v,checked ->
            val i = v.tag as Int
            if(checks.contains(i)&&!checked){
                checks.remove(i)
                listener?.invoke(checks)
            }else if(!checks.contains(i)&&checked){
                checks.add(i)
                listener?.invoke(checks)
            }

        }

    }

    fun setOnSelectListener(listener: (List<Int>) -> Unit){
        this.listener = listener
    }
    fun addChecks(checks:List<Int>){
        this.checks.addAll(checks)
        notifyDataSetChanged()
    }
    fun setChecks(checks:List<Int>){
        this.checks.clear()
        this.checks.addAll(checks)
        notifyDataSetChanged()
    }
    fun clearChecks(){
        this.checks.clear()
        notifyDataSetChanged()
    }
}