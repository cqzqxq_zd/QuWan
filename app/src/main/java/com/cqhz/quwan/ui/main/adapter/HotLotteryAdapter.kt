package com.cqhz.quwan.ui.main.adapter

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import com.cqhz.quwan.R
import com.cqhz.quwan.model.HotLottery
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.util.setLoadImage

/**
 * 热门彩票-数据适配器
 * Created by Guojing on 2018/9/14.
 */
class HotLotteryAdapter(context: Context) : AbsAdapter<HotLottery>(context, R.layout.item_hot_lottery) {

    override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: HotLottery?) {
        viewHolder.setText(R.id.tv_lottery_name, itemData?.name)
        viewHolder.setText(R.id.tv_lottery_tips, itemData?.tags)
        viewHolder.setTextColor(R.id.tv_lottery_tips, if (itemData?.hot == 1) Color.parseColor("#D60000") else Color.parseColor("#666666"))
        val ivHot = viewHolder.find<ImageView>(R.id.iv_hot)
        ivHot.setImageResource(if (itemData?.hot == 1) R.drawable.ic_hot else R.drawable.ic_new)
        ivHot.visibility = if (itemData?.hot == 1 || itemData?.hot == 2) View.VISIBLE else View.GONE
        val imageView = viewHolder.find<ImageView>(R.id.iv_lottery)
        imageView.setLoadImage(context, itemData?.imgUrl, R.drawable.team_ic_default)
    }
}