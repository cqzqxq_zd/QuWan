package com.cqhz.quwan.ui.main.match.entity;

import com.cqhz.quwan.model.OddsEuropeData;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/26
 */
public class EuropeOdds {
    private String flag;
    private List<OddsEuropeData.OddsResultBean> results;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<OddsEuropeData.OddsResultBean> getResult() {
        return results;
    }

    public void setResult(List<OddsEuropeData.OddsResultBean> results) {
        this.results = results;
    }
}
