package com.cqhz.quwan.ui.main.pager;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cqhz.quwan.R;
import com.cqhz.quwan.common.KeyContract;
import com.cqhz.quwan.model.HotBean;
import com.cqhz.quwan.ui.activity.FootballLotteryActivity;
import com.cqhz.quwan.ui.main.match.MatchAnalysisActivity;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import me.militch.quickcore.mvp.model.ModelHelper;

/**
 * 热门赛事列表适配器
 * Created by Guojing on 2018/9/30.
 */
public class HotPagerAdapter extends PagerAdapter implements CardAdapter {

    private Context mContext;
    private List<View> mViews;
    private List<HotPagerItem> mData;
    private Context context;
    private ModelHelper modelHelper;

    public HotPagerAdapter(Context context, ModelHelper modelHelper) {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
        this.context = context;
        this.modelHelper = modelHelper;
    }

    public void addCardItem(Context context, HotPagerItem item) {
        mContext = context;
        mViews.add(null);
        mData.add(item);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_pager_hot, container, false);
        container.addView(view);
        bind(mData.get(position), view);
        View cardView = view.findViewById(R.id.cardView);

        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(HotPagerItem item, View view) {
        ViewHolder holder = null;
        if (holder == null) {
            holder = new ViewHolder(view);
        }
        holder.bind(item);
    }


    public class ViewHolder implements View.OnClickListener {
        ImageView ivHostTeam;
        TextView tvHostName;
        TextView tvGameName;
        TextView tvGameDes;
        TextView tvGameJoin;
        ImageView ivGuestTeam;
        TextView tvGuestName;
        private HotBean data;

        public ViewHolder(View itemView) {
            ivHostTeam = itemView.findViewById(R.id.iv_host_team);
            tvHostName = itemView.findViewById(R.id.tv_host_name);
            tvGameName = itemView.findViewById(R.id.tv_game_name);
            tvGameDes = itemView.findViewById(R.id.tv_game_des);
            tvGameJoin = itemView.findViewById(R.id.tv_game_join);
            ivGuestTeam = itemView.findViewById(R.id.iv_guest_team);
            tvGuestName = itemView.findViewById(R.id.tv_guest_name);
            itemView.findViewById(R.id.action_goto_more).setOnClickListener(this);
            itemView.findViewById(R.id.action_goto_match).setOnClickListener(this);
        }

        void bind(HotPagerItem item) {
            data = item.getData();
            if (data != null) {
                Glide.with(mContext).load(data.getHostTeamLogo()).apply(new RequestOptions().error(R.mipmap.icon_default_img)).into(ivHostTeam);
                Glide.with(mContext).load(data.getGuestTeamLogo()).apply(new RequestOptions().error(R.mipmap.icon_default_img)).into(ivGuestTeam);
                tvHostName.setText(data.getHostTeam());
                tvGuestName.setText(data.getGuestTeam());
                tvGameName.setText(data.getLeague());
                tvGameDes.setText(MessageFormat.format("{0}{1}截止", data.getWeek(), data.getCloseTime()));
                tvGameJoin.setText(data.getPeopleJoin() + "人参加");
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.action_goto_more:
                    Intent intent = new Intent(context, FootballLotteryActivity.class);
                    intent.putExtra(KeyContract.LotteryId, data.getLotteryId());
                    intent.putExtra(KeyContract.Position, 0);
                    context.startActivity(intent);
                    break;
                case R.id.action_goto_match:
                    Intent intent2 = new Intent(context, MatchAnalysisActivity.class);
                    intent2.putExtra("Analysis", data.getMatchId());
                    intent2.putExtra("league", data.getLid());
                    context.startActivity(intent2);
                    break;
                default:
            }
        }
    }
}
