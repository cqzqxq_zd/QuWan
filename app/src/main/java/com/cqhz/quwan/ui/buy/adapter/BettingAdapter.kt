package com.cqhz.quwan.ui.buy.adapter

import android.content.Context
import android.content.Intent
import android.widget.ImageView
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.BettingItem
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.ui.buy.BettingMachineActivity

class BettingAdapter(context: Context,private val lotteryId:String) :
        AbsAdapter<BettingItem>(context, R.layout.item_ball_list) {
    override fun handlerViewHolderPre(viewHolder: ViewHolder, position: Int, itemData: BettingItem?) {

    }

    override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: BettingItem?) {
//        val adapter = BallNumberAdapter(context)
//        adapter.redDMIndex = itemData?.redDMIndex?:-1
//        adapter.redTMIndex = itemData?.redTMIndex?:-1
//        adapter.blueDMIndex = itemData?.blueDMIndex?:-1
//        adapter.blueTMIndex = itemData?.blueTMIndex?:-1
//        adapter.setBallNumbers(itemData?.redBalls!!, itemData.blueBalls)
//        viewHolder.setGridViewAdapter(
//                R.id.ball_list_gv,
//                adapter
//        )
//        val allBall = itemData!!.redBalls.plus(itemData.blueBalls)
        viewHolder.setHtmlText(R.id.balls_tv,itemData!!.str())
        val closeIv = viewHolder.find<ImageView>(R.id.iv_betting_item_close)
        closeIv.setOnClickListener {
            removeItem(itemData)
        }

        val typeName = itemData.betName()
        val allAmount = itemData.allStake*2
        viewHolder.setText(R.id.tv_betting_item_stake, "${itemData.allStake} 注")
        viewHolder.setText(R.id.tv_betting_item_amount, "$allAmount 元")
        viewHolder.setText(R.id.tv_betting_item_type,typeName)
        viewHolder.setItemClick {
            val intent = Intent(context, BettingMachineActivity::class.java)
            intent.putExtra(KeyContract.IsReset,true)
            intent.putExtra(KeyContract.BettingItem,itemData)
            intent.putExtra(KeyContract.LotteryId,lotteryId)
            intent.putExtra(KeyContract.ResetIndex,position)
            context.startActivity(intent)
        }
    }

    fun getTotalStake(): Long {
        var total = 0L
        for (item: BettingItem in mData) {
            total += item.allStake
        }
        return total
    }
}