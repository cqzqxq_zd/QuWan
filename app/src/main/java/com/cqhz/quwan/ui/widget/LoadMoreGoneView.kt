package com.cqhz.quwan.ui.widget

import com.chad.library.adapter.base.loadmore.LoadMoreView
import com.cqhz.quwan.R

class LoadMoreGoneView : LoadMoreView() {
    /**
     * load more layout
     *
     * @return
     */
    override fun getLayoutId(): Int {
        return R.layout.layout_empty
    }

    /**
     * loading view
     *
     * @return
     */
    override fun getLoadingViewId(): Int {
        return R.id.iv_none
    }

    /**
     * load end view, you can return 0
     *
     * @return
     */
    override fun getLoadEndViewId(): Int {
        return R.id.iv_none
    }

    /**
     * load fail view
     *
     * @return
     */
    override fun getLoadFailViewId(): Int {
        return R.id.iv_none
    }
}