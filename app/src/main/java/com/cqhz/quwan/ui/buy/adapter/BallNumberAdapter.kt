package com.cqhz.quwan.ui.buy.adapter

import android.content.Context
import com.cqhz.quwan.R
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.util.slice


class BallNumberAdapter(context: Context)
    : AbsAdapter<Int>(context, R.layout.item_number){
    private var redBallCount:Int = 0
    var redDMIndex = -1
    var redTMIndex = -1
    var blueDMIndex = -1
    var blueTMIndex = -1
    override fun handlerViewHolder(viewHolder: ViewHolder,position: Int, itemData: Int?) {
        if (position < redBallCount){
            val numStr = itemData?.slice(2)
            val text = if(redDMIndex != -1 && position == 0){
                "胆码[$numStr"
            }else if(redDMIndex != -1 && position == redDMIndex){
                "$numStr]"
            }else{
                "$numStr"
            }
            viewHolder.setTextAndColor(R.id.number_tv,text,0XFFd60000.toInt())
        }else{
            viewHolder.setTextAndColor(R.id.number_tv,itemData?.slice(2),0XFF1c7ee3.toInt())
        }
    }
    fun setBallNumbers(redBalls:List<Int>,blueBalls:List<Int>){
        redBallCount = redBalls.size
        addData(redBalls.sorted().plus(blueBalls.sorted()))
    }
}