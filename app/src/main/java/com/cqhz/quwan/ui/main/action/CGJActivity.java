package com.cqhz.quwan.ui.main.action;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.cqhz.quwan.APP;
import com.cqhz.quwan.R;
import com.cqhz.quwan.common.KeyContract;
import com.cqhz.quwan.model.LoginBean;
import com.cqhz.quwan.model.OrderResp;
import com.cqhz.quwan.ui.base.GoBackActivity;
import com.cqhz.quwan.ui.activity.GoPaymentActivity;
import com.cqhz.quwan.ui.login.LoginActivity;

import quickcore.webview.WebViewListener;
import quickcore.webview.WebViewSetup;

/**
 * @author whamu2
 * @date 2018/6/12
 */
public class CGJActivity extends GoBackActivity implements WebViewListener {
    private static final String TAG = CGJActivity.class.getSimpleName();
    public static final String KEY_PATH = "path";

    private String base_url = null;

    private WebView mWebView;

    public static void start(Context context, String path) {
        Intent starter = new Intent(context, CGJActivity.class);
        starter.putExtra(KEY_PATH, path);
        context.startActivity(starter);
    }

    @Override
    public int layout() {
        return R.layout.activity_cgj_action;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void initView() {
        if (getIntent() != null) {
            base_url = getIntent().getStringExtra(KEY_PATH);
        }

        mWebView = findViewById(R.id.webview);
        String dir = getDir("database", Context.MODE_PRIVATE).getPath();
        WebViewSetup.builder()
                .setWebViewClient(this)
                .setLocationDbPath(dir)
                .setCacheMode(WebSettings.LOAD_NO_CACHE)
                .build().setupWebView(mWebView);

//        mWebView.setWebChromeClient(new WebChromeClientBase());
        mWebView.addJavascriptInterface(new JavascriptObject(this), "Android");

        if (!TextUtils.isEmpty(base_url)) {
            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    mWebView.loadUrl(base_url);
                }
            });
        }
    }

    @Override
    public void setTitleView(String title) {
        setTitleBarText(title);
    }

    @Override
    public void pageStart(WebView view, String url, Bitmap favicon) {

    }

    @Override
    public void pageEnd(WebView view, String url) {

    }

    @Override
    public boolean loadUrl(WebView webView, boolean isWebScheme, String scheme, String url) {
        webView.loadUrl(base_url);
        return true;
    }

    @Override
    public void onDownload(String url) {

    }

    @Override
    public void onOpenFile(boolean is, ValueCallback<Uri[]> callback, ValueCallback<Uri> callback2) {

    }

    private class WebChromeClientBase extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            //Tell the host application the current progress of loading a page. 页面加载进度
            //if (null != mProgressBar) {
            //    mProgressBar.setProgress(newProgress);
            //}
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            //Notify the host application of a change in the document title  文档标题
            setTitleBarText("猜冠军");
            super.onReceivedTitle(view, title);
        }
    }


    private static class JavascriptObject {
        Context mContext;

        JavascriptObject(Context context) {
            mContext = context;
        }

        @JavascriptInterface
        public void postOrder(final String json) {
            Log.e(TAG, json);
            Gson gson = new GsonBuilder().create();
            if (!TextUtils.isEmpty(json)) {
                OrderResp data = gson.fromJson(json, OrderResp.class);
                Intent intent = new Intent(mContext, GoPaymentActivity.class);
                intent.putExtra(KeyContract.Amount, Double.valueOf(data.getPayAmount()));
                intent.putExtra(KeyContract.ActualAmount, Double.valueOf(data.getPayAmount()));
                intent.putExtra(KeyContract.LotteryName, "竞猜足球-猜冠军");
                intent.putExtra(KeyContract.OrderId, data.getId());
                mContext.startActivity(intent);
            }
        }

        @JavascriptInterface
        public void toLogin() {
            LoginBean loginInfo = APP.Companion.get().getLoginInfo();
            if (loginInfo==null || loginInfo.getUserId() == null){
                Intent intent = new Intent(mContext, LoginActivity.class);
                mContext.startActivity(intent);
            }
        }
    }
}
