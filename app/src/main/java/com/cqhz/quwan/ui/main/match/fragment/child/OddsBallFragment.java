package com.cqhz.quwan.ui.main.match.fragment.child;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.whamu2.treeview.adpater.TreeRecyclerAdapter;
import com.whamu2.treeview.adpater.TreeRecyclerType;
import com.cqhz.quwan.ActivityInject;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.OddsData;
import com.cqhz.quwan.model.OddsEuropeData;
import com.cqhz.quwan.mvp.match.OddsContract;
import com.cqhz.quwan.mvp.match.OddsPresenter;
import com.cqhz.quwan.ui.base.AbsFragment;
import com.cqhz.quwan.ui.main.match.common.Key;

import javax.inject.Inject;

import me.militch.quickcore.core.HasDaggerInject;

/**
 * 赔率详情 大小球
 *
 * @author whamu2
 * @date 2018/7/20
 */
public class OddsBallFragment extends AbsFragment implements OddsContract.View, HasDaggerInject<ActivityInject> {
    private static final String TAG = OddsBallFragment.class.getSimpleName();
    private static final String KEY_CODE = "code";

    private RecyclerView mRecyclerView;
    private TreeRecyclerAdapter mAdapter;
    private View mEmptyView;

    private String code;
    @Inject
    OddsPresenter mPresenter;

    public static OddsBallFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putString(KEY_CODE, code);
        OddsBallFragment fragment = new OddsBallFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int layout() {
        return R.layout.item_odds_info;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_CODE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.attachView(this);
        mRecyclerView = view.findViewById(R.id.rv);
        mEmptyView = view.findViewById(R.id.tv_empty);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new TreeRecyclerAdapter(TreeRecyclerType.SHOW_ALL);
        mRecyclerView.setAdapter(mAdapter);

//        List<TreeItem> treeItemList = ItemHelperFactory.createTreeItemList(Collections.singletonList("1"), OddsBallTitleTree.class, null);
//        mAdapter.getItemManager().replaceAllItem(treeItemList);
        mEmptyView.setVisibility(View.VISIBLE);

        mPresenter.getAnalysisOddsBall(Key.Odds.BALL);
    }

    @Override
    public void getResult(OddsData data) {

    }

    @Override
    public void getResultEurope(OddsEuropeData data) {

    }

    @Override
    public void onErrorAndEmpty() {

    }

    @Override
    public String getMatchId() {
        return code;
    }

    @Override
    public void inject(ActivityInject inject) {
        inject.inject(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
