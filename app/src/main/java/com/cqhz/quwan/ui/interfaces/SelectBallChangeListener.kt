package com.cqhz.quwan.ui.interfaces

import com.cqhz.quwan.model.BallMatchBean

/**
 * 竞彩足球和篮球的选择事件
 * Created by Guojing on 2018/11/19.
 */
interface SelectBallChangeListener {

    /**
     * 选择或取消
     * @param  bean
     */
    fun onItemSelectChange(bean: BallMatchBean)
}