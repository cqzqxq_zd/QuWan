package com.cqhz.quwan.ui.main.match.adapter.tree.ball;

import android.support.annotation.NonNull;

import com.whamu2.treeview.base.ViewHolder;
import com.whamu2.treeview.item.TreeItem;
import com.cqhz.quwan.R;

/**
 * 赔率内容
 *
 * @author whamu2
 * @date 2018/7/19
 */
public class OddsBallContentTree extends TreeItem<String> {

    @Override
    public int getLayoutId() {
        return R.layout.tree_odds_content;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder) {
        viewHolder.setText(R.id.tv_company, "彩票辣鸡");

        viewHolder.setText(R.id.tv_cd, "2");
        viewHolder.setText(R.id.tv_cp, "2");
        viewHolder.setText(R.id.tv_cx, "2");

        viewHolder.setText(R.id.tv_jd, "2");
        viewHolder.setText(R.id.tv_jp, "2");
        viewHolder.setText(R.id.tv_jx, "2");

        int layoutPosition = viewHolder.getLayoutPosition();
        if (layoutPosition %2 == 0){
            viewHolder.itemView.setBackgroundColor(0xFFEEEEEE);
        } else {
            viewHolder.itemView.setBackgroundColor(0xFFFFFFFF);

        }
    }
}
