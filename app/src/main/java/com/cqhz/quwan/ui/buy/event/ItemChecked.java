package com.cqhz.quwan.ui.buy.event;

/**
 * @author whamu2
 * @date 2018/6/9
 */
public abstract class ItemChecked {
    private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
