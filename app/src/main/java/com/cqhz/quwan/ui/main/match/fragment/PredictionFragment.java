package com.cqhz.quwan.ui.main.match.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.cqhz.quwan.ActivityInject;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.AnalysisForecast;
import com.cqhz.quwan.mvp.match.ForecastContract;
import com.cqhz.quwan.mvp.match.ForecastPresenter;
import com.cqhz.quwan.ui.base.AbsFragment;
import com.cqhz.quwan.ui.main.match.adapter.PredictionAdapter;
import com.cqhz.quwan.ui.main.match.entity.Forecast;
import com.cqhz.quwan.ui.widget.FullyLinearLayoutManager;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import me.militch.quickcore.core.HasDaggerInject;

/**
 * 预测
 *
 * @author whamu2
 * @date 2018/7/18
 */
public class PredictionFragment extends AbsFragment implements ForecastContract.View, HasDaggerInject<ActivityInject> {
    private static final String TAG = PredictionFragment.class.getSimpleName();
    private static final String KEY_CODE = "code";

    private RecyclerView mRecyclerView;
    private View mEmptyView;

    private String code;
    private PredictionAdapter mAdapter;

    @Inject
    ForecastPresenter mPresenter;

    public static PredictionFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putString(KEY_CODE, code);
        PredictionFragment fragment = new PredictionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int layout() {
        return R.layout.fragment_match_prediction;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_CODE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.attachView(this);
        mRecyclerView = view.findViewById(R.id.rv);
        mEmptyView = view.findViewById(R.id.tv_empty);

        FullyLinearLayoutManager mLayoutManager = new FullyLinearLayoutManager(getContext());
        mLayoutManager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration decoration = new DividerItemDecoration(Objects.requireNonNull(getContext()), DividerItemDecoration.VERTICAL);
        decoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(getContext(), R.drawable.space)));
        mRecyclerView.addItemDecoration(decoration);
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new PredictionAdapter(getContext());
        mRecyclerView.setAdapter(mAdapter);


        mPresenter.getAnalysisForecast();
    }

    @Override
    public void getResult(AnalysisForecast data) {
        if (data != null) {
            List<Forecast> forecasts = new ArrayList<>();

            if (data.getSpf_forecast() != null) {
                Forecast forecast = new Forecast();
                AnalysisForecast.SpfForecastBean spfForecast = data.getSpf_forecast();
                forecast.setType(0);
                forecast.setTitle("胜平负预测指数");
                forecast.setWin(spfForecast.getWin_per());
                forecast.setDraw(spfForecast.getDraw_per());
                forecast.setLose(spfForecast.getLose_per());
                forecasts.add(forecast);
            }
            if (data.getRqspf_forecast() != null) {
                Forecast forecast = new Forecast();
                AnalysisForecast.RqspfForecastBean rqspf_forecast = data.getRqspf_forecast();
                forecast.setType(0);
                forecast.setTitle(MessageFormat.format("让球胜平负预测指数{0}",
                        TextUtils.isEmpty(data.getGoal_line()) ? "" : MessageFormat.format("({0})", data.getGoal_line())));

                forecast.setWin(rqspf_forecast.getWin_per());
                forecast.setDraw(rqspf_forecast.getDraw_per());
                forecast.setLose(rqspf_forecast.getLose_per());
                forecasts.add(forecast);
            }
            mAdapter.setData(forecasts);

            if (forecasts.size() > 0) {
                mEmptyView.setVisibility(View.GONE);
            } else {
                mEmptyView.setVisibility(View.VISIBLE);
            }
        } else {
            mEmptyView.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onFail() {
        mEmptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public String getMatchId() {
        return code;
    }

    @Override
    public void inject(ActivityInject inject) {
        inject.inject(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mRecyclerView = null;
        mAdapter = null;
    }
}
