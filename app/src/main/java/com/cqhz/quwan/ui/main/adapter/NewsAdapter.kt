package com.cqhz.quwan.ui.main.adapter

import android.content.Context
import android.content.Intent
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.HotNews
import com.cqhz.quwan.service.API
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.ui.base.WebViewActivity

class NewsAdapter(context: Context) : AbsAdapter<HotNews>(context, R.layout.item_hot_news) {
    var itemClickListener: ((Int, HotNews) -> Unit)? = null
    override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: HotNews?) {
        viewHolder.loadImageUrl(R.id.news_img, API.HOST + itemData?.imgUrl)
        viewHolder.setText(R.id.news_title, itemData?.title)
        viewHolder.setText(R.id.news_author, itemData?.author)
        val readerCount = itemData?.reader
        viewHolder.setText(R.id.news_reading_count, "阅读量：$readerCount")
        viewHolder.setItemClick {
            if (itemClickListener != null) {
                itemClickListener!!.invoke(position, itemData!!)
            }
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra(KeyContract.Title, "资讯详情")
            val url = "${API.ZXXQ}?query=${itemData?.id}"
            intent.putExtra(KeyContract.Url, url)
            context.startActivity(intent)
        }
    }
}