package com.cqhz.quwan.ui.mine

import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.orhanobut.hawk.Hawk
import com.tencent.mm.opensdk.modelmsg.SendAuth
import com.tencent.mm.opensdk.openapi.IWXAPI
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.common.KeyContract.Companion.WEICHAT_APP_ID
import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.mine.AlipayContract
import com.cqhz.quwan.mvp.mine.AlipayPresenter
import com.cqhz.quwan.mvp.mine.SettingsContract
import com.cqhz.quwan.mvp.mine.SettingsPresenter
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.util.LoginObserver
import com.cqhz.quwan.util.getVersionName
import kotlinx.android.synthetic.main.activity_settings.*
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject

/**
 * 设置
 * Created by Guojing on 2018/9/6.
 */
class SettingsActivity : GoBackActivity(), SettingsContract.View, AlipayContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var alipayPresenter: AlipayPresenter
    @Inject
    lateinit var settingsPresenter: SettingsPresenter
    private var userInfoBean: UserInfoBean? = null
    private lateinit var wxApi: IWXAPI

    override fun showUserInfo(userInfoBean: UserInfoBean) {
        this.userInfoBean = userInfoBean

        tv_real_auth_enabled.text = if (userInfoBean.realAuth == "1") "已认证" else "未实名";
        tv_bind_alipay_enabled.text = if (userInfoBean.alipayBind == "1") "已绑定" else "未绑定"
        tv_bind_wechat_enabled.text = if (userInfoBean.wechatBind == "1") "已绑定" else "未绑定"
        tv_qq.text = userInfoBean.officialQQ
    }

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun layout(): Int {
        return R.layout.activity_settings
    }

    override fun titleBarText(): String? {
        return "设置"
    }

    override fun onDestroy() {
        settingsPresenter.detachView()
        alipayPresenter.detachView()
        super.onDestroy()
    }

    override fun initView() {
        alipayPresenter.attachView(this)
        settingsPresenter.attachView(this)

        // 个人信息
        action_goto_setting.setOnClickListener {
            startActivity(Intent(this, InformationActivity::class.java))
        }
        // 实名认证
        action_goto_real_name.setOnClickListener {
            if (userInfoBean != null) {
                val isAuth = userInfoBean?.realAuth == "1"
                if (!isAuth) {
                    startActivity(Intent(this, RealNameAuthActivity::class.java))
                }
            }
        }
        // 绑定支付宝
        action_bind_alipay.setOnClickListener {
            if (userInfoBean != null) {
                val isBind = userInfoBean?.alipayBind == "1"
                if (!isBind) {
                    alipayPresenter.auth4alipay()
                }
            }
        }
        // 绑定微信
        wxApi = WXAPIFactory.createWXAPI(this, WEICHAT_APP_ID, false)
        wxApi.registerApp(WEICHAT_APP_ID)
        action_bind_wechat.setOnClickListener {
            if (wxApi.isWXAppInstalled) {
                if (userInfoBean != null && userInfoBean?.wechatBind != "1") {
                    // send oauth request
                    val req = SendAuth.Req()
                    req.scope = "snsapi_userinfo"
                    req.state = "wechat_sdk_auth"
                    wxApi.sendReq(req)
                    // todo 调不起微信，需要前往公众平台修改签名
                }
            } else {
                showToast("您还未安装微信")
            }
        }
        // 官方客服QQ
        action_qq.setOnClickListener {
            val cm = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cm.text = this.userInfoBean!!.officialQQ
            showToast("已复制到剪贴板")
        }
        // 版本号
        val versionText = "V${APP.get()!!.getVersionName()}"
        tv_vision.text = versionText
        // 退出登录
        action_exit.setOnClickListener {
            Hawk.delete(KeyContract.LoginInfo)
            LoginObserver.push(null)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    override fun refresh() {
        val loginInfo = APP.get()!!.loginInfo
        if (loginInfo != null) {
            showLoading("正在加载数据...")
            settingsPresenter.getUserInfo(loginInfo.userId!!)
        }
    }

    override fun alipayAuthCallBack(authData: AlipayAuthData) {
        settingsPresenter.bindAlipay(APP.get()!!.loginInfo!!.userId!!, authData)
    }
}