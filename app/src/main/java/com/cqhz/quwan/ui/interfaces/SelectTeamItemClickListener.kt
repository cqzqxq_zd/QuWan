package com.cqhz.quwan.ui.interfaces

import com.cqhz.quwan.model.ESportItemBean
import com.cqhz.quwan.model.ESportItemSelectBean


interface SelectTeamItemClickListener {
    /**
     * 点击选择获胜队伍
     * @param  teamPosition 选中位置的坐标
     * @param  bean
     */
    fun onTeamClick(eSportItemBean: ESportItemBean, eSportItemSelectBean: ESportItemSelectBean)
}