package com.cqhz.quwan.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.widget.GridView
import com.cqhz.quwan.R

/**
 * 自适应高度的 GridView
 * Created by WYZ on 2018/3/19.
 */
class AutoHeightGridView(
        context: Context?,
        attrs: AttributeSet?) :
        GridView(context, attrs) {
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE shr 2, MeasureSpec.AT_MOST)
        super.onMeasure(widthMeasureSpec, height)
    }
}