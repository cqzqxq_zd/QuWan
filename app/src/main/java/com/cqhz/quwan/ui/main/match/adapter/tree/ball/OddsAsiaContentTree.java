package com.cqhz.quwan.ui.main.match.adapter.tree.ball;

import android.support.annotation.NonNull;
import android.widget.TextView;

import com.whamu2.treeview.base.ViewHolder;
import com.whamu2.treeview.item.TreeItem;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.OddsData;
import com.cqhz.quwan.ui.main.match.common.Utils;

/**
 * 赔率内容 亚盘
 *
 * @author whamu2
 * @date 2018/7/19
 */
public class OddsAsiaContentTree extends TreeItem<OddsData.OddsResultBean> {

    @Override
    public int getLayoutId() {
        return R.layout.tree_odds_content;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder) {
        //        cn: 公司
        //        o1_s: 初盘-主水
        //        o3_s: 初盘-盘口
        //        o2_s: 初盘-客水
        //        o1: 即时盘-主水
        //        o3: 即时盘-盘口
        //        o2: 即时盘-客水
        OddsData.OddsResultBean data = getData();

        viewHolder.setText(R.id.tv_company, Utils.required(data.getCn()));

        viewHolder.setText(R.id.tv_cd, Utils.required(data.getO1_s()));
        viewHolder.setText(R.id.tv_cp, Utils.required(data.getO3_s()));
        viewHolder.setText(R.id.tv_cx, Utils.required(data.getO2_s()));

        viewHolder.setText(R.id.tv_jd, Utils.required(data.getO1()));
        viewHolder.setText(R.id.tv_jp, Utils.required(data.getO3()));
        viewHolder.setText(R.id.tv_jx, Utils.required(data.getO2()));

        TextView o1TextView = viewHolder.getTextView(R.id.tv_jd);
        o1TextView.setTextColor(Utils.getStateColor(data.getO1_change()));
        TextView o2TextView = viewHolder.getTextView(R.id.tv_jx);
        o2TextView.setTextColor(Utils.getStateColor(data.getO2_change()));

        int layoutPosition = viewHolder.getLayoutPosition();
        if (layoutPosition % 2 == 0) {
            viewHolder.itemView.setBackgroundColor(0xFFEEEEEE);
        } else {
            viewHolder.itemView.setBackgroundColor(0xFFFFFFFF);

        }
    }
}
