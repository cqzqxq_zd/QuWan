package com.cqhz.quwan.ui.activity

import android.content.Intent
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.cqhz.quwan.APP
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.LoginBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.model.ProductBean
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.mall.MallContract
import com.cqhz.quwan.mvp.mall.MallPresenter
import com.cqhz.quwan.service.API
import com.cqhz.quwan.ui.base.GoBackActivity
import com.cqhz.quwan.ui.base.WebViewActivity
import com.cqhz.quwan.ui.login.LoginActivity
import com.cqhz.quwan.ui.mine.DiamondRecordActivity
import com.cqhz.quwan.ui.widget.SameSpacesItemDecoration
import com.cqhz.quwan.util.CommonAdapterHelper
import com.cqhz.quwan.util.formatMoney
import kotlinx.android.synthetic.main.activity_mall.*
import me.militch.quickcore.core.HasDaggerInject
import javax.inject.Inject

/**
 * 商城
 * Created by Guojing on 2018/9/6.
 */
class MallActivity : GoBackActivity(), MallContract.View, HasDaggerInject<ActivityInject> {

    @Inject
    lateinit var mallPresenter: MallPresenter
    lateinit var loginBean: LoginBean
    private lateinit var productListAdapter: BaseQuickAdapter<*, *>
    private val products = ArrayList<ProductBean>()

    override fun layout(): Int {
        return R.layout.activity_mall
    }

    override fun titleBarText(): String? {
        return "商城"
    }

    override fun inject(t: ActivityInject?) {
        t?.inject(this)
    }

    override fun initView() {
        mallPresenter.attachView(this)
        loginBean = APP.get()!!.loginInfo!!
        mallPresenter.getUserInfo(loginBean.userId!!)

        // 获取和设置适配器
        productListAdapter = CommonAdapterHelper.getProductListAdapter(this, products)
        rv_content.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        rv_content.adapter = productListAdapter
        val decoration = SameSpacesItemDecoration(1)
        rv_content.addItemDecoration(decoration)
        rv_content.setPadding(1, 1, 1, 1)
        rv_content.itemAnimator = null// 去除瀑布流会换列跳动
        rv_content.addOnItemTouchListener(object : OnItemClickListener() {
            override fun onSimpleItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
                val item = productListAdapter.getItem(position) as ProductBean
                if (loginBean == null) {
                    startActivity(Intent(context(), LoginActivity::class.java))
                } else {
                    val intent = Intent(context(), ProductDetailActivity::class.java)
                    intent.putExtra("productId", item.id)
                    startActivity(intent)
                }
            }
        })
        // 加载更多
        productListAdapter.setOnLoadMoreListener({
            if (products.size > 0) {
                if (products.size % KeyContract.pageSize == 0) {
                    val pageNo = products.size / KeyContract.pageSize + 1
                    refreshLayout.isEnableRefresh = false
                    mallPresenter.getProductList(pageNo, KeyContract.pageSize)
                } else {
                    productListAdapter.loadMoreEnd()
                }
            } else {
                refreshLayout.isEnableRefresh = false
                mallPresenter.getProductList(0, KeyContract.pageSize)
            }
        }, rv_content)
        // 刷新
        refreshLayout.setOnRefreshListener {
            mallPresenter.getProductList(1, KeyContract.pageSize)
        }
        // 加载数据
        mallPresenter.getProductList(1, KeyContract.pageSize)

        // 趣豆寻宝
        action_goto_web.setOnClickListener { gotoExchangeDiamonds() }
        setTitleBarRightText("抽钻") { gotoExchangeDiamonds() }
        // 我的领奖（我的兑换）
        action_goto_my_exchange.setOnClickListener { gotoActivity(MyExchangeActivity::class.java) }
        // 钻石记录
        action_goto_diamond_record.setOnClickListener { gotoActivity(DiamondRecordActivity::class.java) }
    }

    /**
     * 去抽钻
     */
    private fun gotoExchangeDiamonds() {
        val intent = Intent(this, WebViewActivity::class.java)
        intent.putExtra(KeyContract.Title, "趣豆寻宝")
        val url = "${API.findTreasure}?userId=${loginBean.userId}"
        intent.putExtra(KeyContract.Url, url)
        startActivity(intent)
    }

    override fun showUserInfo(userInfoBean: UserInfoBean) {
        tv_diamonds.text = userInfoBean.diamonds.formatMoney()
    }

    override fun loadProductList(pageNo: Int, pageResultBean: PageResultBean<ProductBean>) {
        if (pageNo == 1) {
            products.clear()
            productListAdapter.setEnableLoadMore(true)
            if (refreshLayout?.state?.isHeader!!) {
                refreshLayout?.finishRefresh()
            }
        }

        if (pageResultBean.records.isNotEmpty()) {
            products.addAll(pageResultBean.records)
        }

        if (pageResultBean.total > products.size) {
            productListAdapter.loadMoreComplete()
        } else {
            productListAdapter.loadMoreEnd()
        }

        productListAdapter.notifyDataSetChanged()
        refreshLayout.isEnableRefresh = true
    }

    /**
     * 跳转到其他页面
     * @author Guojing
     */
    private fun gotoActivity(c: Class<*>) {
        startActivity(Intent(this, if (loginBean == null) LoginActivity::class.java else c))
    }

    override fun onDestroy() {
        mallPresenter.detachView()
        super.onDestroy()
    }
}
