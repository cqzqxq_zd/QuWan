package com.cqhz.quwan.ui.widget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import com.cqhz.quwan.R
import java.util.jar.Attributes

/**
 * 快速选球器
 * Created by WYZ on 2018/3/19.
 */
class FastBallSelector(context: Context) : View(context) {
    private val ballPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var ballCount: Int = 0
    private var blueBallCount: Int = 0
    private var redBallColor: Int = 0
    private var blueBallColor: Int = 0
    constructor(context: Context,attrs: AttributeSet):this(context){
        // 初始化解析参数
        resolveAttrs(context.obtainStyledAttributes(attrs, R.styleable.FastBallSelector))
        Log.e("aaaa","----")
    }

    private fun resolveAttrs(array: TypedArray){
        ballCount = array.getInt(R.styleable.FastBallSelector_ballCount,7)
        blueBallCount = array.getInt(R.styleable.FastBallSelector_blueBallCount,1)
        redBallColor = array.getColor(R.styleable.FastBallSelector_redBallColor,0xfffeefef.toInt())
        blueBallColor = array.getColor(R.styleable.FastBallSelector_blueBallColor,0xffe9f7fe.toInt())
        array.recycle()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawLine(10.toFloat(),10.toFloat(),30.toFloat(),10.toFloat(),ballPaint)
        Log.e("ab","dcdd")
    }
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        val width = getMeasuredWidth()
        val widthSpecMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightSpecMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)
        if (widthSpecMode == View.MeasureSpec.AT_MOST && heightSpecMode == View.MeasureSpec.AT_MOST) {
            setMeasuredDimension(100, 100)
        } else if (widthSpecMode == View.MeasureSpec.AT_MOST) {
            setMeasuredDimension(100, heightSize)
        } else if (heightSpecMode == View.MeasureSpec.AT_MOST) {
            setMeasuredDimension(widthSize, 100)
        }
//        setMeasuredDimension(widthSize, heightSize)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

}