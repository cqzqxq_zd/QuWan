package com.cqhz.quwan.ui.main.match.fragment.child;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.whamu2.treeview.adpater.TreeRecyclerAdapter;
import com.whamu2.treeview.adpater.TreeRecyclerType;
import com.whamu2.treeview.factory.ItemHelperFactory;
import com.whamu2.treeview.item.TreeItem;
import com.cqhz.quwan.ActivityInject;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.OddsData;
import com.cqhz.quwan.model.OddsEuropeData;
import com.cqhz.quwan.mvp.match.OddsContract;
import com.cqhz.quwan.mvp.match.OddsPresenter;
import com.cqhz.quwan.ui.base.AbsFragment;
import com.cqhz.quwan.ui.main.match.adapter.tree.ball.OddsEuropeTitleTree;
import com.cqhz.quwan.ui.main.match.common.Key;
import com.cqhz.quwan.ui.main.match.entity.EuropeOdds;
import com.cqhz.quwan.ui.widget.FullyLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import me.militch.quickcore.core.HasDaggerInject;

/**
 * 赔率详情 欧盘
 *
 * @author whamu2
 * @date 2018/7/20
 */
public class OddsEuropeFragment extends AbsFragment implements OddsContract.View, HasDaggerInject<ActivityInject> {
    private static final String TAG = OddsEuropeFragment.class.getSimpleName();
    private static final String KEY_CODE = "code";

    private RecyclerView mRecyclerView;
    private TreeRecyclerAdapter mAdapter;
    private View mEmptyView;

    private String code;
    @Inject
    OddsPresenter mPresenter;

    public static OddsEuropeFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putString(KEY_CODE, code);
        OddsEuropeFragment fragment = new OddsEuropeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int layout() {
        return R.layout.item_odds_info;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            code = getArguments().getString(KEY_CODE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.attachView(this);
        mRecyclerView = view.findViewById(R.id.rv);
        mEmptyView = view.findViewById(R.id.tv_empty);

        FullyLinearLayoutManager mLayoutManager = new FullyLinearLayoutManager(getContext());
        mLayoutManager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new TreeRecyclerAdapter(TreeRecyclerType.SHOW_ALL);

        mPresenter.getAnalysisOddsEurope(Key.Odds.EUROPE);
    }

    @Override
    public void getResult(OddsData data) {
        // do nothing
    }

    @Override
    public void getResultEurope(OddsEuropeData data) {
        if (data != null && data.getOdds_result().size() > 0) {
            List<EuropeOdds> odds = new ArrayList<>();
            EuropeOdds europeOdds = new EuropeOdds();
            europeOdds.setFlag("o");
            europeOdds.setResult(data.getOdds_result());
            odds.add(europeOdds);

            List<TreeItem> treeItemList = ItemHelperFactory.createTreeItemList(odds, OddsEuropeTitleTree.class, null);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.getItemManager().replaceAllItem(treeItemList);

            mEmptyView.setVisibility(View.GONE);
        } else {

            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onErrorAndEmpty() {
        mEmptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public String getMatchId() {
        return code;
//        return "108945";
    }

    @Override
    public void inject(ActivityInject inject) {
        inject.inject(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
