package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class SettingsPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), SettingsContract.Presenter {
    override fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(SettingsContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).bindAlipay(userId,alipayAuthData.userId,alipayAuthData.alipayOpenId,alipayAuthData.authCode),{_->""},
                { view, _ ->
                    view.refresh()
                }, {view, data ->  view.showRequestError(data)})
    }

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(SettingsContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, {view, data ->  view.showRequestError(data)})
    }
}