package com.cqhz.quwan.mvp.login

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.mvp.IBasicView


interface BindMobileContract {
    interface View:IBasicView{
    }
    interface Presenter {
        fun bindAlipay(number:String,alipayAuthData: AlipayAuthData)
    }
}