package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.model.Msg2Entity
import com.cqhz.quwan.model.MsgItemBean
import com.cqhz.quwan.mvp.IBasicView
import me.militch.quickcore.core.HasDaggerInject


interface MessageContract {
    interface View:IBasicView,HasDaggerInject<ActivityInject>{
        fun showMsgData(data:List<MsgItemBean>)
        fun showMsgData2(data:List<Msg2Entity>)
    }
    interface Presenter{
        fun getSysMsg()
        fun getUserMsg(userId:String)
        fun getBroadcast()
    }
}