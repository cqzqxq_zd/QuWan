package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.service.APIService
import com.cqhz.quwan.service.CommonService
import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class GoPaymentPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), GoPaymentContract.Presenter {

    /**
     * 订单支付
     */
    override fun orderPay(userId: String, orderId: String, couponId: String?) {
        ModelAndView.create(view(GoPaymentContract.View::class.java), modelHelper()).request(
                service(OrderService::class.java).orderPay(userId, orderId, couponId),
                { v, d ->
                    v.showOrderInfo(d)
                }, { v, e ->
            v.showRequestError(e)
        }
        )
    }

    /**
     * 获取用户信息
     */
    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(GoPaymentContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取弹窗信息
     */
    override fun getWindowList(userId: String) {
        ModelAndView.create(view(GoPaymentContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).getWindowList(userId), { v, data ->
            v.setWindowList(data)
        }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取优惠券列表
     * @param orderId
     */
    override fun getAvailableCouponList(orderId: String) {
        ModelAndView.create(view(GoPaymentContract.View::class.java), modelHelper())
                .request(service(APIService::class.java).getAvailableCouponList(orderId), { view, data ->
                    view.loadAvailableCouponList(data)
                }, { v, e -> v.showRequestError(e) })
    }
}