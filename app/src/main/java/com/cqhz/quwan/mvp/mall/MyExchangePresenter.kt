package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.service.MallService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class MyExchangePresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), MyExchangeContract.Presenter {

    /**
     * 获取兑换列表
     * @param userId 用户id
     * @param status 空，0，1
     * @param pageNo 1..
     * @param pageSize 10
     */
    override fun getOrderList(userId: Long, currentPage: Int, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(MyExchangeContract.View::class.java), modelHelper())
                .request(service(MallService::class.java).exchangeList(userId, when (currentPage) {
                    1 -> "0"
                    2 -> "1"
                    else -> ""
                }, pageNo, pageSize), { view, data ->
                    view.loadOrderList(currentPage, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}