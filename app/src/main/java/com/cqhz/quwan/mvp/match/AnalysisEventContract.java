package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisEventData;
import com.cqhz.quwan.mvp.IBasicView;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/24
 */
public interface AnalysisEventContract {

    interface View extends IBasicView {
        void matchEvent(AnalysisEventData data);
    }

    interface Presenter {
        void getAnalysisEvent(String code);

    }
}
