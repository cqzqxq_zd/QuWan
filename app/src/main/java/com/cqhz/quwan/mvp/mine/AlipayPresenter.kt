package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class AlipayPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper),AlipayContract.Presenter {

    override fun auth4alipay() {
        ModelAndView.create(view(AlipayContract.View::class.java),modelHelper()).request(
                service(UserService::class.java).auth4alipay(),
                {v,arg ->
                    v.callAlipayAuth(arg)
                }, {v,e ->
                    v.showRequestError(e)
                }
        )
    }
}