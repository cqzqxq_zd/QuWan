package com.cqhz.quwan.mvp.buy;

import com.cqhz.quwan.model.Coupon;
import com.cqhz.quwan.mvp.IBasicView;

import java.util.List;

import me.militch.quickcore.util.ApiException;

/**
 * @author whamu2
 * @date 2018/6/11
 */
public interface CouponContract {
    interface View {
        void showList(List<Coupon> data);

        void showRequestError(ApiException e);
    }

    interface Presenter {
        void getCouponList(String orderId);
    }
}
