package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.model.CouponConsume
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.service.WechatPayContract
import me.militch.quickcore.core.HasDaggerInject

/**
 * Created by WYZ on 2018/3/30.
 */
interface PaymentContract {
    interface View :WechatPayContract.View,AlipayContract.View,HasDaggerInject<ActivityInject> {
        fun showUserInfo(userInfoBean: UserInfoBean)
    }
    interface Presenter{
        fun getUserInfo(userId:String)
        fun aliPay(orderId:String)
        fun aliPay2(couponConsume: CouponConsume)
        fun wechatPay(couponConsume: CouponConsume)
    }
}