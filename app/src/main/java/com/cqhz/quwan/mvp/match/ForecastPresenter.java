package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisForecast;
import com.cqhz.quwan.service.AnalysisService;

import javax.inject.Inject;

import io.reactivex.Flowable;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.execute.impl.ModelAndView;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.mvp.presenter.QuickPresenter;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.RespBase;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class ForecastPresenter extends QuickPresenter implements ForecastContract.Presenter {

    @Inject
    public ForecastPresenter(ModelHelper modelHelper) {
        super(modelHelper);
    }


    @Override
    public void getAnalysisForecast() {
        ForecastContract.View view = view(ForecastContract.View.class);
        Flowable<RespBase<AnalysisForecast>> flowable = service(AnalysisService.class).getAnalysisForecast(view.getMatchId());
        ModelAndView.create(view, modelHelper())
                .request(flowable, new ViewEvent<ForecastContract.View, AnalysisForecast>() {
                    @Override
                    public void call(ForecastContract.View view, AnalysisForecast data) {
                        view.getResult(data);
                    }
                }, new ViewEvent<ForecastContract.View, ApiException>() {
                    @Override
                    public void call(ForecastContract.View view, ApiException e) {
                        view.showRequestError(e);
                        view.onFail();
                    }
                });
    }
}
