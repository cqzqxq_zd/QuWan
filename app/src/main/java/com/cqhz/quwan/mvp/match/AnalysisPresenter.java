package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisRecord;
import com.cqhz.quwan.model.AnalysisVote;
import com.cqhz.quwan.service.AnalysisService;

import javax.inject.Inject;

import io.reactivex.Flowable;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.execute.impl.ModelAndView;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.mvp.presenter.QuickPresenter;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.RespBase;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class AnalysisPresenter extends QuickPresenter implements AnalysisContract.Presenter {

    @Inject
    public AnalysisPresenter(ModelHelper modelHelper) {
        super(modelHelper);
    }

    @Override
    public void getAnalysisRecord() {
        AnalysisContract.View view = view(AnalysisContract.View.class);
        Flowable<RespBase<AnalysisRecord>> flowable = service(AnalysisService.class)
                .getAnalysisRecord(view.getUser(), view.getMatchId());
        ModelAndView.create(view, modelHelper())
                .request(flowable, new ViewEvent<AnalysisContract.View, AnalysisRecord>() {
                    @Override
                    public void call(AnalysisContract.View view, AnalysisRecord data) {
                        view.getRecord(data.getRecordPOList());
                        view.getVote(data.getVotePOList());
                    }
                }, new ViewEvent<AnalysisContract.View, ApiException>() {
                    @Override
                    public void call(AnalysisContract.View view, ApiException e) {
                        view.getRecordFail();
                        view.showRequestError(e);
                    }
                });
    }

    @Override
    public void addAnalysisVote(Integer type) {
        AnalysisContract.View view = view(AnalysisContract.View.class);
        Flowable<RespBase<AnalysisVote>> flowable = service(AnalysisService.class)
                .addAnalysisVote(view.getUser(), view.getMatchId(), type);
        ModelAndView.create(view, modelHelper())
                .request(flowable, new ViewEvent<AnalysisContract.View, AnalysisVote>() {
                    @Override
                    public void call(AnalysisContract.View view, AnalysisVote data) {
                        view.getVoteResult(data);
                    }
                }, new ViewEvent<AnalysisContract.View, ApiException>() {
                    @Override
                    public void call(AnalysisContract.View view, ApiException e) {
                        view.voteFail();
                        view.showRequestError(e);

                    }
                });
    }
}
