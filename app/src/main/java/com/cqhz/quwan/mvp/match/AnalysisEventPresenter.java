package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisEventData;
import com.cqhz.quwan.service.AnalysisService;
import com.cqhz.quwan.util.PreferenceHelper;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.execute.impl.ModelAndView;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.mvp.presenter.QuickPresenter;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.RespBase;

/**
 * @author whamu2
 * @date 2018/7/24
 */
public class AnalysisEventPresenter extends QuickPresenter implements AnalysisEventContract.Presenter {

    @Inject
    public AnalysisEventPresenter(ModelHelper modelHelper) {
        super(modelHelper);
    }

    @Override
    public void getAnalysisEvent(String code) {
        Flowable<RespBase<AnalysisEventData>> flowable = service(AnalysisService.class)
                .getAnalysisEvent(code);

        AnalysisEventContract.View view = view(AnalysisEventContract.View.class);

        ModelAndView.create(view, modelHelper())
                .request(flowable,
                        new ViewEvent<AnalysisEventContract.View, AnalysisEventData>() {
                            @Override
                            public void call(AnalysisEventContract.View view, AnalysisEventData data) {
                                view.matchEvent(data);
                            }
                        },
                        new ViewEvent<AnalysisEventContract.View, ApiException>() {
                            @Override
                            public void call(AnalysisEventContract.View view, ApiException data) {
                                PreferenceHelper.getInstance(view.context()).putString("host_team", "");
                                PreferenceHelper.getInstance(view.context()).putString("guest_team", "");
                                view.showRequestError(data);
                            }
                        });
    }
}
