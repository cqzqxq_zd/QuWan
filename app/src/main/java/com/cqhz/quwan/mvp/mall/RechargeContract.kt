package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.model.SystemConfigBean
import com.cqhz.quwan.mvp.mine.AlipayContract
import com.cqhz.quwan.service.WechatPayContract


interface RechargeContract {
    interface View : AlipayContract.View, WechatPayContract.View {
        fun setMoney(systemConfigBean: SystemConfigBean)
        fun setTips(systemConfigBean: SystemConfigBean)
        fun setBean(bean: String)
    }

    interface Presenter {
        fun doRecharge(userId: String, money: String)
        fun doRecharge4wechat(userId: String, money: String)
        fun getUserInfo(userId: String)
        fun getSystemConfig(key: String)
    }
}