package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.OddsData;
import com.cqhz.quwan.model.OddsEuropeData;
import com.cqhz.quwan.mvp.IBasicView;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public interface OddsContract {

    interface View extends IBasicView {
        void getResult(OddsData data);

        void getResultEurope(OddsEuropeData data);

        void onErrorAndEmpty();

        String getMatchId();
    }

    interface Presenter {
        void getAnalysisOdds(Integer type);

        void getAnalysisOddsEurope(Integer type);

        void getAnalysisOddsBall(Integer type);
    }
}
