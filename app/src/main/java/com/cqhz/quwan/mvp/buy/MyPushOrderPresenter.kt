package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.service.OrderService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

/**
 * Created by Guojing on 2018/10/30.
 */
class MyPushOrderPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), MyPushOrderContract.Presenter {
    /**
     * 获取订单列表
     * @param userId 用户id
     * @param type
     * @param status
     * @param pageNo 1..
     * @param pageSize 10
     */
    override fun getOrderList(userId: String, type: Int, currentPage: Int, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(MyPushOrderContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).getPushOrderList(userId, type, when (currentPage) {
                    0 -> 1
                    1 -> 2
                    2 -> 3
                    else -> 1
                }, pageNo, pageSize), { view, data ->
                    view.loadOrderList(currentPage, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}