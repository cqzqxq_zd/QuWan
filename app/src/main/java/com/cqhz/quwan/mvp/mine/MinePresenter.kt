package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.CommonService
import com.cqhz.quwan.service.HomeService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class MinePresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), MineContract.Presenter {

    override fun mineSign(userId: String) {
        ModelAndView.create(view(MineContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).signReward(userId), { view, data ->
            view.showTips(data)
        }, { view, data -> view.showRequestError(data) })
    }

    override fun hasMsgNew(userId: String) {
        ModelAndView.create(view(MineContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).hasNewMsg(userId), { view, entity ->
            view.showAlreadyMsg(entity > 0)
        }, { view, data -> view.showRequestError(data) })
    }

    override fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(MineContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).bindAlipay(userId, alipayAuthData.userId, alipayAuthData.alipayOpenId, alipayAuthData.authCode), { _ -> "" },
                { view, _ ->
                    view.hintLoading()
                    getUserInfo(userId)
                }, { view, data -> view.showRequestError(data) })
    }

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(MineContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取弹窗信息
     */
    override fun getWindowList(userId: String) {
        ModelAndView.create(view(MineContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).getWindowList(userId), { v, data ->
            v.setWindowList(data)
        }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取顶部广告的数据
     * @param  banner类型 0：竞猜 1：首页中部 2：个人中心 3：弹窗 4:资讯首页
     */
    override fun getBannerData() {
        ModelAndView.create(view(MineContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).queryBannerList(2), { view, data ->
                    view.loadBanner(data)
                }, { v, e -> v.showRequestError(e) })
    }


    /**
     * 获取竞猜开关
     */
    override fun getSystemConfig(key: String) {
        ModelAndView.create(view(MineContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).systemConfig(key), { v, data ->
            v.setSystemConfig(data)
        }, { v, e -> v.showRequestError(e) })
    }
}