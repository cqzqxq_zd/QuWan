package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.WechatPayContract
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class WechatPayPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper),WechatPayContract.Presenter{
    override fun getWechatPayReq() {
        ModelAndView.create(WechatPayContract.View::class,modelHelper())
    }

    override fun getWechatAppId() {

    }

}