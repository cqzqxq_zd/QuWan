package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.mvp.IBasicView

/**
 * Created by WYZ on 2018/3/30.
 */
interface RealNameAuthContract {
    interface View : IBasicView{

    }
    interface Presenter{
        fun doAuth(name:String,idCard:String)
    }
}