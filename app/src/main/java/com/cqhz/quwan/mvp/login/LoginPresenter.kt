package com.cqhz.quwan.mvp.login

import android.content.Intent
import com.orhanobut.hawk.Hawk
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.LoginService
import com.cqhz.quwan.ui.login.BindMobilePhoneActivity
import com.cqhz.quwan.util.LoginObserver
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class LoginPresenter @Inject constructor(modelHelper: ModelHelper) :
        QuickPresenter(modelHelper),
        LoginContract.Presenter {
    override fun login4alipay(alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(LoginContract.BasicView::class.java), modelHelper()).request(
                service(LoginService::class.java).alipayAuthLogin(alipayAuthData.alipayOpenId, alipayAuthData.userId, alipayAuthData.authCode, 4),
                { v, d ->
                    v.hintLoading()
                    if (d.userId == null) {
                        val intent = Intent(v.context(), BindMobilePhoneActivity::class.java)
                        intent.putExtra(KeyContract.AlipayAuthData, alipayAuthData)
                        v.context().startActivity(intent)
                    }else{
                        Hawk.put(KeyContract.LoginInfo,d)
                        LoginObserver.push(d)
                    }
                }, { v, e -> v.showRequestError(e) }
        )
    }

    override fun doLogin(number: String, verCode: String) {
        ModelAndView.create(
                view(LoginContract.BasicView::class.java), modelHelper()
        ).request(service(LoginService::class.java).doLogin(number, verCode), { view, d ->
            view.hintLoading()
            d.phoneNumber = number
            Hawk.put(KeyContract.LoginInfo, d)
            LoginObserver.push(d)
            view.closePage()
        }, { v, e ->
            v.showRequestError(e)
        })
    }

    override fun getVerCode(number: String) {

        ModelAndView.create(
                view(LoginContract.BasicView::class.java), modelHelper()
        ).request(service(LoginService::class.java).sendVerifyCode(number, 1), { d ->
            ""
        }, { view, d ->
            view.hintLoading()
            view.lockInputVerCode()
        }, { v, e ->
            v.showRequestError(e)
        })
    }


}