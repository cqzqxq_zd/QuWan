package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.service.OrderService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class LotteryBettingPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), LotteryBettingContract.Presenter{
    override fun postOrder(orderArgs: Map<String, String>) {
        ModelAndView.create(view(LotteryBettingContract.View::class.java),modelHelper())
                .request(service(OrderService::class.java).postOrder(orderArgs),{view,data ->
                    view.hintLoading()
                    if(data.id != null){
                        view.toPay(data)
                    }else {
                        view.showToast("创建订单失败")
                    }
                },{view,e->
                    view.showRequestError(e)
                })
    }
}