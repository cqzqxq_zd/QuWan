package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.model.DocumentaryBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.mvp.IBasicView


interface DocumentaryContract {
    interface View : IBasicView {
        fun loadOrderList(currentPage: Int, pageNo: Int, pageResultBean: PageResultBean<DocumentaryBean>)
        fun updateSuccess()
    }

    interface Presenter {
        fun getOrderList(pageNo: Int, pageSize: Int, type: Int, followUserId: String)
        fun updateFollowStatus(userId: String, fansId: String, status: String)
    }
}