package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.model.ExpertInfoBean
import com.cqhz.quwan.mvp.IBasicView


interface FootBallBetContract {
    interface View : IBasicView {
        fun setExpertInfo(bean: ExpertInfoBean)
    }

    interface Presenter {
        fun getExpertInfo(userId: String)
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
    }
}