package com.cqhz.quwan.mvp

import com.cqhz.quwan.service.APIService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class ExpertDetailPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), ExpertDetailContract.Presenter {

    /**
     * 获取专家详情
     * @param userId
     * @param followUserId
     */
    override fun getExpertDetail(userId: String, followUserId: String) {
        ModelAndView.create(view(ExpertDetailContract.View::class.java), modelHelper())
                .request(service(APIService::class.java).getExpertDetail(userId, followUserId), { view, data ->
                    view.setExpertDetail(data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 取消关注
     * @param userId
     * @param fansId 用户id
     * @param status 状态(0:取消关注,1:关注)
     */
    override fun updateFollowStatus(userId: String, fansId: String, status: String) {
        ModelAndView.create(view(ExpertDetailContract.View::class.java), modelHelper())
                .request(service(UserService::class.java).followExpert(userId, fansId, status), { view, data ->
                    view.updateSuccess()
                }, { v, e -> v.showRequestError(e) })
    }
}