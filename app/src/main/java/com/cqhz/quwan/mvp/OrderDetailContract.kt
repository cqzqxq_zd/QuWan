package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.CodeBean
import com.cqhz.quwan.model.OrderInfoBean


interface OrderDetailContract {
    interface View : IBasicView {
        fun setOrderDetail(bean: OrderInfoBean)
        fun setCodeImg(bean: CodeBean)
        fun updateSuccess()
    }

    interface Presenter {
        fun getOrderDetail(orderId: String)
        fun getCodeImg(userId: String)
        fun updateOrderStatus(orderId: String, status: String)
    }
}