package com.cqhz.quwan.mvp

import com.cqhz.quwan.service.APIService
import com.cqhz.quwan.service.OrderService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class OrderDetailPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), OrderDetailContract.Presenter {

    /**
     * 获取订单信息
     * @param orderId
     */
    override fun getOrderDetail(orderId: String) {
        ModelAndView.create(view(OrderDetailContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).getOrderDetail(orderId), { view, data ->
                    view.setOrderDetail(data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取订单详情二维码图片
     * @param userId
     */
    override fun getCodeImg(userId: String) {
        ModelAndView.create(view(OrderDetailContract.View::class.java), modelHelper())
                .request(service(APIService::class.java).getCodeImg(userId), { view, data ->
                    view.setCodeImg(data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 更新订单状态
     * @param orderId
     * @param status
     */
    override fun updateOrderStatus(orderId: String, status: String) {
        ModelAndView.create(view(OrderDetailContract.View::class.java), modelHelper())
                .request(service(APIService::class.java).updateOrderStatus(orderId, status), { view, data ->
                    view.updateSuccess()
                }, { v, e -> v.showRequestError(e) })
    }
}