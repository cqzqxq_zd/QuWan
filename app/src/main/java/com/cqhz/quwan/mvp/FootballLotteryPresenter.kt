package com.cqhz.quwan.mvp

import com.cqhz.quwan.service.APIService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class FootballLotteryPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), FootballLotteryContract.Presenter {

    /**
     * 获取竞猜足球列表
     * @param playType  玩法 1：胜平负；2：让球胜平负；3：比分；4：总进球；5：半全场；6：混合投注；7：单关固定；
     * @param leagueIds 联赛Ids（非必传参数）
     */
    override fun getFootballList(currentPage: Int, leagueIds: String?) {
        ModelAndView.create(view(FootballLotteryContract.View::class.java), modelHelper()).request(
                service(APIService::class.java).getBallMatchList(when (currentPage) {
                    0 -> 6
                    1 -> 7
                    2, 3, 6 -> currentPage - 1
                    4 -> currentPage
                    5 -> 3
                    else -> 6
                }, leagueIds, "0"), { v, data ->
            v.setFootballList(currentPage, data)
        }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取联赛列表
     */
    override fun getLeagueList() {
        ModelAndView.create(view(FootballLotteryContract.View::class.java), modelHelper()).request(
                service(APIService::class.java).getLeagueList(),
                { view, data ->
                    view.hintLoading()
                    view.setLeagueList(data)
                }, { view, data -> view.showRequestError(data) })
    }
}