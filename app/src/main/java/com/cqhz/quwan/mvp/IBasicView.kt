package com.cqhz.quwan.mvp

import android.content.Context
import android.content.Intent
import me.militch.quickcore.util.ApiException


interface IBasicView {
    fun showToast(toast:String)
    fun showRequestError(e:ApiException)
    fun toIntent(clazz: Class<*>)
    fun closePage()
    fun showLoading()
    fun showLoading(text:String)
    fun hintLoading()
    fun context():Context
    fun toIntent(intent:Intent)
}