package com.cqhz.quwan.mvp

import com.cqhz.quwan.service.APIService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class BasketballLotteryPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), BasketballLotteryContract.Presenter {

    /**
     * 获取竞猜篮球列表
     * @param playType  1：胜负；2：让分胜负；3：胜分差；4：大小分；5：混合过关；6：单关；
     */
    override fun getBasketballList(currentPage: Int) {
        ModelAndView.create(view(BasketballLotteryContract.View::class.java), modelHelper()).request(
                service(APIService::class.java).getBallMatchList(when (currentPage) {
                    0 -> 5
                    1 -> 6
                    2, 3 -> currentPage - 1
                    4 -> currentPage
                    5 -> 3
                    else -> 5
                }, null, "1"), { v, data ->
            v.setBasketballList(currentPage, data)
        }, { v, e -> v.showRequestError(e) })
    }
}