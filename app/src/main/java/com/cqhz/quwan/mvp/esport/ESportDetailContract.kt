package com.cqhz.quwan.mvp.esport

import com.cqhz.quwan.model.*
import com.cqhz.quwan.mvp.IBasicView


interface ESportDetailContract {
    interface View : IBasicView {
        fun setDetailHead(bean: ESportDetailBean)
        fun setBean(bean: String)
        fun loadGameItemList(currentPage: Int, list: ArrayList<ESportItemBean>)
        fun setWindowList(list: List<WindowBean>)
        fun bindAlipay()
        fun toPay(orderResp: OrderResp)
    }

    interface Presenter {
        fun getDetailHead(matchId: String)
        fun getUserInfo(userId: String)
        fun getGameItemList(currentPage: Int, matchId: String, round: String)
        fun getWindowList(userId: String)
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
        fun postOrder(orderArgs: Map<String, String>)
    }
}