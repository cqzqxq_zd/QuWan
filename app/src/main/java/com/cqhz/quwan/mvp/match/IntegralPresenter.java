package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisIntegral;
import com.cqhz.quwan.service.AnalysisService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.execute.impl.ModelAndView;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.mvp.presenter.QuickPresenter;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.RespBase;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class IntegralPresenter extends QuickPresenter implements IntegralContract.Presenter {

    @Inject
    public IntegralPresenter(ModelHelper modelHelper) {
        super(modelHelper);
    }

    @Override
    public void getIntegralToType(int type) {
        IntegralContract.View view = view(IntegralContract.View.class);
        Flowable<RespBase<AnalysisIntegral>> flowable = service(AnalysisService.class).getIntegralToType(type, view.getMatchId());
        ModelAndView.create(view, modelHelper())
                .request(flowable, new ViewEvent<IntegralContract.View, AnalysisIntegral>() {
                    @Override
                    public void call(IntegralContract.View view, AnalysisIntegral data) {
                        view.getData(data);
                    }
                }, new ViewEvent<IntegralContract.View, ApiException>() {
                    @Override
                    public void call(IntegralContract.View view, ApiException e) {
                        view.onFail();
                        view.showRequestError(e);
                    }
                });

    }
}
