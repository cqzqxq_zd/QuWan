package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.BallMatchListBean


interface BasketballLotteryContract {
    interface View : IBasicView {
        fun setBasketballList(currentPage: Int, list: List<BallMatchListBean>)
    }

    interface Presenter {
        fun getBasketballList(currentPage: Int)
    }
}