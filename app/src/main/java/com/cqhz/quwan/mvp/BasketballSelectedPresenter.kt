package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class BasketballSelectedPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), BasketballSelectedContract.Presenter {

    override fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(BasketballSelectedContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).bindAlipay(userId, alipayAuthData.userId, alipayAuthData.alipayOpenId, alipayAuthData.authCode), { _ -> "" },
                { view, _ ->
                    view.showToast("绑定成功")
                    view.setCommitEnable()
                }, { view, data -> view.showRequestError(data) })
    }

    override fun postOrder(orderArgs: Map<String, String>) {
        ModelAndView.create(view(BasketballSelectedContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).postOrder(orderArgs), { view, data ->
                    if (data.id != null) {
                        view.toPay(data)
                    } else {
                        view.showToast("创建订单失败")
                    }
                }, { view, e ->
                    if (e.code == 10008) {
                        view.bindAlipay()
                    } else {
                        view.setCommitEnable()
                        view.showRequestError(e)
                    }
                })
    }
}