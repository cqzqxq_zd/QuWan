package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.model.*
import com.cqhz.quwan.mvp.IBasicView


interface ProductDetailContract {
    interface View : IBasicView {
        fun showUserInfo(userInfoBean: UserInfoBean)
        fun setSystemConfig(configKey: String, data: SystemConfigBean)
        fun showProductDetail(productDetailBean: ProductDetailBean)
        fun showSuccess(commonResultBean: CommonResultBean)
        fun refresh()
    }

    interface Presenter {
        fun getUserInfo(userId: String)
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
        fun getSystemConfig(configKey: String)
        fun getProductDetail(userId: String, productId: String)
        fun addProductOrder(userId: String, productId: String, num: String, type: String, exchangeMethod: String)
    }
}