package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.APP
import com.cqhz.quwan.model.CouponConsume
import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class PaymentPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), PaymentContract.Presenter {
    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(PaymentContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    override fun wechatPay(couponConsume: CouponConsume) {
        ModelAndView.create(view(PaymentContract.View::class.java), modelHelper()).request(
                service(OrderService::class.java).wechatPay(
                        couponConsume.orderId,
                        couponConsume.couponId,
                        couponConsume.subBalanceMoney,
                        couponConsume.finalMoney),
                { v, d ->
                    v.callWechatPay(d)
                }, { view, e -> view.showRequestError(e) }
        )
    }

    override fun aliPay2(couponConsume: CouponConsume) {
        ModelAndView.create(view(PaymentContract.View::class.java), modelHelper()).request(
                service(OrderService::class.java).alipay2(
                        couponConsume.orderId,
                        couponConsume.couponId,
                        couponConsume.subBalanceMoney,
                        couponConsume.finalMoney),
                { v, d ->
                    v.callAlipay(d)
                }, { view, e -> view.showRequestError(e) }
        )
    }

    override fun aliPay(orderId: String) {
        ModelAndView.create(view(PaymentContract.View::class.java), modelHelper()).request(
                service(OrderService::class.java).alipay(APP.get()!!.loginInfo!!.userId!!, orderId),
                { v, d ->
                    v.callAlipay(d)
                }, { view, e -> view.showRequestError(e) }
        )
    }
}