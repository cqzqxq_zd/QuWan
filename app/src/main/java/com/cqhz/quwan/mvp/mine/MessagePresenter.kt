package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.HomeService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class MessagePresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper),MessageContract.Presenter {
    override fun getUserMsg(userId: String) {
        ModelAndView.create(view(MessageContract.View::class.java),modelHelper()).request(
                service(HomeService::class.java).msg2List(1,10,userId),
                {v,d->
                    v.showMsgData2(d.records)
//                    d.records
                },
                {v,e->
                    v.showRequestError(e)
                }
        )
    }

    override fun getBroadcast() {
        ModelAndView.create(view(MessageContract.View::class.java),modelHelper()).request(
                service(HomeService::class.java).msgList(1),
                {v,d->
                    v.showMsgData(d.records)
                },
                {v,e->
                    v.showRequestError(e)
                }
        )
    }

    override fun getSysMsg() {
        ModelAndView.create(view(MessageContract.View::class.java),modelHelper()).request(
                service(HomeService::class.java).msgList(0),
                {v,d->
                    v.showMsgData(d.records)
                },
                {v,e->
                    v.showRequestError(e)
                }
        )
    }
}