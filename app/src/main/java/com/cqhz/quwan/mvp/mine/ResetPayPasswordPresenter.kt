package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.mvp.login.LoginContract
import com.cqhz.quwan.service.LoginService
import com.cqhz.quwan.service.ResetPasswordService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class ResetPayPasswordPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper),ResetPayPasswordContract.Presenter {
    override fun doReset(userId: String, pass: String, verifyCode: String) {
        ModelAndView.create(
                view(ResetPayPasswordContract.View::class.java),modelHelper()
        ).request(service(ResetPasswordService::class.java).resetPayPassword(userId,pass,verifyCode),{_->
            ""
        },{view,_->
            view.hintLoading()
            view.showToast("修改成功")
            view.closePage()
        },{view,e ->
            view.showRequestError(e)
        })
    }


    override fun getVerCode(number: String) {
        ModelAndView.create(
                view(ResetPayPasswordContract.View::class.java), modelHelper()
        ).request(service(LoginService::class.java).sendVerifyCode(number, 2),{ d ->
            ""
        }, { view, d ->
            view.hintLoading()
            view.lockInputVerCode()
        }, { v, e ->
            v.showRequestError(e)
        })
    }
}