package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.CommonService
import com.cqhz.quwan.service.MallService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class ProductDetailPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), ProductDetailContract.Presenter {
    /**
     * 获取商品详情信息
     */
    override fun getProductDetail(userId: String, productId: String) {
        ModelAndView.create(view(ProductDetailContract.View::class.java), modelHelper())
                .request(service(MallService::class.java).payDataProcure(userId, productId), { view, data ->
                    view.showProductDetail(data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取用户信息，在其选择代卖换钱时，判断其是否绑定支付宝或微信
     * @param userId 用户id
     */
    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(ProductDetailContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 绑定支付宝
     */
    override fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(ProductDetailContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).bindAlipay(userId, alipayAuthData.userId, alipayAuthData.alipayOpenId, alipayAuthData.authCode), { _ -> "" },
                { view, _ ->
                    view.refresh()
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取商品详情信息
     */
    override fun addProductOrder(userId: String, productId: String, num: String, type: String, exchangeMethod: String) {
        ModelAndView.create(view(ProductDetailContract.View::class.java), modelHelper())
                .request(service(MallService::class.java).addProductOrder(userId, productId, num, type, exchangeMethod), { view, data ->
                    view.showSuccess(data)
                }, { v, e ->
                    v.showRequestError(e)
                })
    }

    /**
     * 获取充值金额
     */
    override fun getSystemConfig(configKey: String) {
        ModelAndView.create(view(ProductDetailContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).systemConfig(configKey), { v, data ->
            v.hintLoading()
            v.setSystemConfig(configKey, data)
        }, { v, e -> v.showRequestError(e) })
    }
}