package com.cqhz.quwan.mvp.home


import com.cqhz.quwan.model.MatchBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.mvp.IBasicView


interface LiveContract {
    interface View : IBasicView {
        fun loadMatchLiveList(currentPage: Int, pageNo: Int, pageResultBean: PageResultBean<MatchBean>)
    }

    interface Presenter {
        fun getMatchLiveList(pageNo: String, pageSize: String, status: Int, currentPage: Int)
    }
}