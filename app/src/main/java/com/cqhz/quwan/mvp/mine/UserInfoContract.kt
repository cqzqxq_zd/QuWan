package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView
import me.militch.quickcore.core.HasDaggerInject
import okhttp3.MultipartBody

interface UserInfoContract {
    interface View : IBasicView, HasDaggerInject<ActivityInject> {
        fun refresh()
        fun showUserInfo(userInfoBean: UserInfoBean)
        fun setHeadUrl(url: String?)
    }

    interface Presenter {
        fun getUserInfo(userId: String)
        fun updateUserInfo(userId: String, headAddress: String, nickName: String)
        fun updateHead(files: List<MultipartBody.Part>)
    }
}