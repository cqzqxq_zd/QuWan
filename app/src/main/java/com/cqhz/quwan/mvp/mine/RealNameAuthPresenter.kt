package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.APP
import com.cqhz.quwan.service.Test
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class RealNameAuthPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper),RealNameAuthContract.Presenter {
    override fun doAuth(name: String, idCard: String) {
        ModelAndView.create(view(RealNameAuthContract.View::class.java),modelHelper()).request(
                service(Test::class.java).realAuth(APP.get()!!.loginInfo!!.userId,name,idCard),{_->""},
                {v,d->
                    v.hintLoading()
                    v.closePage()
                }, {v,e-> v.showRequestError(e)}
        )
    }
}