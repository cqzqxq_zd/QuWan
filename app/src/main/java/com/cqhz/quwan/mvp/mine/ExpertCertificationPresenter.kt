package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UploadService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import okhttp3.MultipartBody
import javax.inject.Inject

/**
 * 专家认证ExpertCertificationPresenter
 * Created by Guojing on 2018/10/30.
 */
class ExpertCertificationPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), ExpertCertificationContract.Presenter {

    /**
     * 专家认证
     * @param userId 用户id
     * @param nick
     * @param headPortrait
     * @param introduce
     */
    override fun expertAuth(userId: String, nick: String, headPortrait: String, introduce: String) {
        ModelAndView.create(view(ExpertCertificationContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).expertAuth(userId.toLong(), nick, headPortrait, introduce), { view, data ->
                    view.commitSuccess()
                }, { v, e -> v.showRequestError(e) })
    }

    override fun updateHead(files: List<MultipartBody.Part>) {
        ModelAndView.create(view(ExpertCertificationContract.View::class.java), modelHelper()).request(
                service(UploadService::class.java).updateHead(files),
                { view, data ->
                    view.setHeadUrl(data)
                }, { view, data ->
            view.showRequestError(data)
        })
    }

    /**
     * 获取专家信息
     * @param userId
     */
    override fun getExpertInfo(userId: String) {
        ModelAndView.create(view(ExpertCertificationContract.View::class.java), modelHelper())
                .request(service(UserService::class.java).getExpertInfo(userId.toLong()), { view, data ->
                    view.setExpertInfo(data)
                }, { v, e -> v.showRequestError(e) })
    }
}