package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisRecord;
import com.cqhz.quwan.model.AnalysisVote;
import com.cqhz.quwan.mvp.IBasicView;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public interface AnalysisContract {

    interface View extends IBasicView {
        void getVoteResult(AnalysisVote vote);

        void voteFail();

        void getRecord(List<AnalysisRecord.RecordPOListBean> data);

        void getVote(AnalysisVote vote);

        void getRecordFail();

        String getUser();

        String getMatchId();
    }

    interface Presenter {
        void getAnalysisRecord();

        void addAnalysisVote(Integer type);
    }
}
