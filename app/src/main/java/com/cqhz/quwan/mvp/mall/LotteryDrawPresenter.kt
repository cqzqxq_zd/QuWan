package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.service.MallService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class LotteryDrawPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), LotteryDrawContract.Presenter {
    /**
     * 获取订单详情信息
     */
    override fun getOrderDetail(tradeId: String) {
        ModelAndView.create(view(LotteryDrawContract.View::class.java), modelHelper())
                .request(service(MallService::class.java).exchangeDetail(tradeId), { view, data ->
                    view.showOrderDetail(data)
                }, { v, e -> v.showRequestError(e) })
    }
}