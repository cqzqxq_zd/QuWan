package com.cqhz.quwan.mvp.esport

import com.cqhz.quwan.model.ESportOrderResultBean
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView


interface MyPredictionContract {
    interface View : IBasicView {
        fun setGuessStatistics(userInfoBean: UserInfoBean)
        fun loadOrderList(currentPage: Int, pageNo: Int, bean: ESportOrderResultBean)
    }

    interface Presenter {
        fun getUserInfo(userId: String)
        fun getOrderList(userId: String, currentPage: Int, pageNo: Int, pageSize: Int)
    }
}