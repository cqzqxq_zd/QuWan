package com.cqhz.quwan.mvp

import com.cqhz.quwan.service.APIService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class PlanDetailPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), PlanDetailContract.Presenter {


    /**
     * 获取方案详情
     * @param orderId
     * @param expertId
     * @param followUserId
     */
    override fun getProgrammeDetail(orderId: String, expertId: String, followUserId: String) {
        ModelAndView.create(view(PlanDetailContract.View::class.java), modelHelper())
                .request(service(APIService::class.java).getProgrammeDetail(orderId, expertId, followUserId), { view, data ->
                    view.setProgrammeDetail(data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 取消关注
     * @param userId
     * @param fansId 用户id
     * @param status 状态(0:取消关注,1:关注)
     */
    override fun updateFollowStatus(userId: String, fansId: String, status: String) {
        ModelAndView.create(view(PlanDetailContract.View::class.java), modelHelper())
                .request(service(UserService::class.java).followExpert(userId, fansId, status), { view, data ->
                    view.updateSuccess()
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 跟单
     * @param userId
     * @param orderId
     * @param betTimes
     */
    override fun addFollowOrder(userId: Long, orderId: Long, betTimes: Int) {
        ModelAndView.create(view(PlanDetailContract.View::class.java), modelHelper())
                .request(service(APIService::class.java).addFollowOrder(userId, orderId, betTimes), { view, data ->
                    view.commitSuccess(data)
                }, { v, e -> v.showRequestError(e) })
    }
}