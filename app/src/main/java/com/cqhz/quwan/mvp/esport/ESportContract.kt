package com.cqhz.quwan.mvp.esport

import com.cqhz.quwan.model.ESportBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.model.TitleBean
import com.cqhz.quwan.mvp.IBasicView


interface ESportContract {
    interface View : IBasicView {
        fun setTitleList(list: ArrayList<TitleBean>)
        fun requestTitleFail()
        fun setBean(bean: String)
        fun loadGameList(position: Int, pageNo: Int, pageResultBean: PageResultBean<ESportBean>)
    }

    interface Presenter {
        fun getTitleList(type: String)
        fun getUserInfo(userId: String)
        fun getGameList(pageNo: Int, pageSize: Int, type: Int, pagePosition: Int)
    }
}