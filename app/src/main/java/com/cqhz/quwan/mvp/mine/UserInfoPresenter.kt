package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.UploadService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import okhttp3.MultipartBody
import java.io.File
import javax.inject.Inject


/**
 * 个人信息
 */
class UserInfoPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), UserInfoContract.Presenter {
    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(UserInfoContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    override fun updateUserInfo(userId: String, headAddress: String, nickName: String) {
        ModelAndView.create(view(UserInfoContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).updateUserInfo(userId, headAddress, nickName),
                { view, _ ->
                    view.refresh()
                }, { view, data -> view.showRequestError(data) })
    }

    override fun updateHead(files: List<MultipartBody.Part>) {
        ModelAndView.create(view(UserInfoContract.View::class.java), modelHelper()).request(
                service(UploadService::class.java).updateHead(files),
                { view, data ->
                    view.setHeadUrl(data)
                }, { view, data ->
            view.showRequestError(data)
        })
    }
}