package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.mvp.IBasicView

/**
 * Created by WYZ on 2018/3/29.
 */
interface ResetPayPasswordContract {
    interface View:IBasicView{
        fun lockInputVerCode()
    }
    interface Presenter{
        fun getVerCode(number:String)
        fun doReset(userId:String,pass:String,verifyCode:String)
    }
}