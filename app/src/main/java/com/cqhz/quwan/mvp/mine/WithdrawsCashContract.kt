package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView


interface WithdrawsCashContract {
    interface View : IBasicView,AlipayContract.View{
        fun showUserInfo(userInfoBean: UserInfoBean)
    }
    interface Presenter {
        fun getUserInfo(userId:String)
        fun getCash(userId:String,money:String)
    }
}