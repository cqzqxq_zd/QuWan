package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.service.OrderService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class CommissionPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), CommissionContract.Presenter {

    override fun getCommissionInfo(userId: String) {
        ModelAndView.create(view(CommissionContract.View::class.java), modelHelper()).request(
                service(OrderService::class.java).getCommissionInfo(userId),
                { view, data ->
                    view.showCommissionInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取商品列表
     */
    override fun getRecordList(userId: String, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(CommissionContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).getCommissionList(userId, pageNo, pageSize), { view, data ->
                    view.loadRecordList(pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}