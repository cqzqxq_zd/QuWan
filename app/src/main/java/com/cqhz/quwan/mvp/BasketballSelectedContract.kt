package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.model.ExpertInfoBean
import com.cqhz.quwan.model.OrderResp


interface BasketballSelectedContract {
    interface View : IBasicView {
        fun bindAlipay()
        fun setCommitEnable()
        fun toPay(orderResp: OrderResp)
    }

    interface Presenter {
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
        fun postOrder(orderArgs: Map<String, String>)
    }
}