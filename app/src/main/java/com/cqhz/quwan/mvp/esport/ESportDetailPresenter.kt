package com.cqhz.quwan.mvp.esport

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.CommonService
import com.cqhz.quwan.service.HomeService
import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class ESportDetailPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), ESportDetailContract.Presenter {

    override fun getDetailHead(matchId: String) {
        ModelAndView.create(view(ESportDetailContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).eSportDetailHead(matchId), { view, data ->
                    view.setDetailHead(data)
                }, { v, e -> v.showRequestError(e) })
    }

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(ESportDetailContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.setBean(data.balance ?: "0")
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取电竞赛事列表的数据
     */
    override fun getGameItemList(currentPage: Int, matchId: String, round: String) {
        ModelAndView.create(view(ESportDetailContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).eSportGameItemList(matchId, round), { view, data ->
                    view.loadGameItemList(currentPage, data)
                }, { v, e -> v.showRequestError(e) })
    }

    override fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(ESportDetailContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).bindAlipay(userId, alipayAuthData.userId, alipayAuthData.alipayOpenId, alipayAuthData.authCode), { _ -> "" },
                { view, _ ->
                    view.showToast("绑定成功")
                }, { view, data -> view.showRequestError(data) })
    }

    override fun postOrder(orderArgs: Map<String, String>) {
        ModelAndView.create(view(ESportDetailContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).postOrder(orderArgs), { view, data ->
                    if (data.id != null) {
                        view.toPay(data)
                    } else {
                        view.showToast("创建订单失败")
                    }
                }, { view, e ->
                    if (e.code == 10008) {
                        view.bindAlipay()
                    } else {
                        view.showRequestError(e)
                    }
                })
    }

    /**
     * 获取弹窗信息
     */
    override fun getWindowList(userId: String) {
        ModelAndView.create(view(ESportDetailContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).getWindowList(userId), { v, data ->
            v.setWindowList(data)
        }, { v, e -> v.showRequestError(e) })
    }
}