package com.cqhz.quwan.mvp.buy;

import com.cqhz.quwan.model.Coupon;
import com.cqhz.quwan.service.CouponService;

import java.util.List;

import javax.inject.Inject;

import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.execute.impl.ModelAndView;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.mvp.presenter.QuickPresenter;
import me.militch.quickcore.util.ApiException;

/**
 * @author whamu2
 * @date 2018/6/11
 */

public class CouponPresenter extends QuickPresenter implements CouponContract.Presenter {

    @Inject
    public CouponPresenter(ModelHelper modelHelper) {
        super(modelHelper);
    }

    @Override
    public void getCouponList(String orderId) {
        ModelAndView.create(view(CouponContract.View.class), modelHelper())
                .request(service(CouponService.class).getList(orderId),
                        new ViewEvent<CouponContract.View, List<Coupon>>() {
                            @Override
                            public void call(CouponContract.View view, List<Coupon> data) {
                                view.showList(data);
                            }
                        }, new ViewEvent<CouponContract.View, ApiException>() {
                            @Override
                            public void call(CouponContract.View view, ApiException e) {
                                view.showRequestError(e);
                            }
                        });
    }
}
