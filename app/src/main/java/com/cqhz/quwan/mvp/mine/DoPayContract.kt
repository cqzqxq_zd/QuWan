package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.model.CouponConsume
import com.cqhz.quwan.mvp.IBasicView
import me.militch.quickcore.core.HasDaggerInject


interface DoPayContract {
    interface View : IBasicView,HasDaggerInject<ActivityInject>{
        fun close4resultCode(resultCode:Int)
    }
    interface Presenter{
        fun doPay(money:Double,orderId:String,payPassword:String)
        fun doPay2(couponConsume: CouponConsume,payPassword:String)
        fun doGetCash(money: Double,payPassword:String,payWay:Int=1)
    }
}