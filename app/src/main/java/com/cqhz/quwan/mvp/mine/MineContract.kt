package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.*
import com.cqhz.quwan.mvp.IBasicView

interface MineContract {
    interface View : IBasicView {
        fun showTips(msg: String)
        fun showUserInfo(userInfoBean: UserInfoBean)
        fun showAlreadyMsg(boolean: Boolean)
        fun loadBanner(list: List<BannerItem>)
        fun setSystemConfig(systemConfigBean: SystemConfigBean)
        fun setWindowList(list: List<WindowBean>)
    }

    interface Presenter {
        fun getUserInfo(userId: String)
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
        fun hasMsgNew(userId: String)
        fun getBannerData()
        fun getSystemConfig(key: String)
        fun getWindowList(userId: String)
        fun mineSign(userId: String)
    }
}