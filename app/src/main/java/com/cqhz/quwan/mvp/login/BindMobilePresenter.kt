package com.cqhz.quwan.mvp.login

import com.orhanobut.hawk.Hawk
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.LoginService
import com.cqhz.quwan.service.Test
import com.cqhz.quwan.util.LoginObserver
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class BindMobilePresenter @Inject constructor(modelHelper: ModelHelper) :
        QuickPresenter(modelHelper),
        BindMobileContract.Presenter {
    override fun bindAlipay(number: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(BindMobileContract.View::class.java),modelHelper()).request(
                service(LoginService::class.java).alipayAuthLogin(number,alipayAuthData.alipayOpenId,alipayAuthData.userId,alipayAuthData.authCode,4),
                {v,d->
                    v.hintLoading()
                    d.phoneNumber = number
                    Hawk.put(KeyContract.LoginInfo,d)
                    LoginObserver.push(d)
                    v.closePage()
                },{v,e->v.showRequestError(e)}
        )
    }
}