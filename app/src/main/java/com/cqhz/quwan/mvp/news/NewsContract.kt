package com.cqhz.quwan.mvp.news

import com.cqhz.quwan.model.*
import com.cqhz.quwan.mvp.IBasicView

/**
 * @author Guojing
 * @date 2018/9/13
 */
interface NewsContract {
    interface View : IBasicView {
        fun loadBanner(list: List<BannerItem>)
        fun loadNewsGames(list: List<NewsGameBean>)
        fun loadNewsList(type: Int, pageNo: Int, pageResultBean: PageResultBean<NewsBean>)
        fun setWindowList(list: List<WindowBean>)
        fun newsClickSuccess(newsId: String)
    }

    interface Presenter {
        fun getBannerData()
        fun getNewsGames()
        fun getNewsList(type: Int, pageNo: Int, pageSize: Int)
        fun getWindowList(userId: String)
        fun newsClick(newsId: String)
    }
}