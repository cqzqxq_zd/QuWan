package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.model.OrderResp
import com.cqhz.quwan.mvp.IBasicView


interface LotteryBettingContract {
    interface View : IBasicView{
        fun toPay(orderResp: OrderResp)
    }
    interface Presenter{
        fun postOrder(orderArgs:Map<String,String>)
    }
}