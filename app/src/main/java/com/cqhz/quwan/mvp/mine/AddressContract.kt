package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.model.AddressBean
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView
import me.militch.quickcore.core.HasDaggerInject
import java.io.File

interface AddressContract {
    interface View : IBasicView,HasDaggerInject<ActivityInject>{
        fun setAddress(addressBean: AddressBean)
        fun updateSuccess()
    }

    interface Presenter{
        fun getUserInfo(userId:String)
        fun getAddress(userId:String)
        fun saveAddress(userId: String, addressBean: AddressBean)
    }
}