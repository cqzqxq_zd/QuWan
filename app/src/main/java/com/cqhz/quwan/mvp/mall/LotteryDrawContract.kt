package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.model.OrderDetailBean
import com.cqhz.quwan.mvp.IBasicView


interface LotteryDrawContract {
    interface View : IBasicView {
        fun showOrderDetail(orderDetailBean: OrderDetailBean)
    }

    interface Presenter {
        fun getOrderDetail(tradeId: String)
    }
}