package com.cqhz.quwan.mvp.home

import com.cqhz.quwan.service.HomeService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class LivePresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), LiveContract.Presenter {

    /**
     * 获取顶部广告的数据
     * @param  status 赛事状态 1：即时（当日比赛）2：完场 3：赛程（次日及之后赛事）
     * @param  type 联赛 0:全部 1:竞彩 2:中超 3:欧冠 4:英超 5:德甲
     */
    override fun getMatchLiveList(pageNo: String, pageSize: String, status: Int, currentPage: Int) {

        ModelAndView.create(view(LiveContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).matchLiveList(pageNo, pageSize, status, currentPage), { view, data ->
                    view.loadMatchLiveList(currentPage, pageNo.toInt(), data)
                }, { v, e -> v.showRequestError(e) })
    }
}