package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.OddsData;
import com.cqhz.quwan.model.OddsEuropeData;
import com.cqhz.quwan.service.AnalysisService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import me.militch.quickcore.event.ViewEvent;
import me.militch.quickcore.execute.impl.ModelAndView;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.mvp.presenter.QuickPresenter;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.RespBase;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class OddsPresenter extends QuickPresenter implements OddsContract.Presenter {

    @Inject
    public OddsPresenter(ModelHelper modelHelper) {
        super(modelHelper);
    }

    @Override
    public void getAnalysisOdds(Integer type) {
        OddsContract.View view = view(OddsContract.View.class);
        Flowable<RespBase<List<OddsData>>> flowable = service(AnalysisService.class).getAnalysisOdds(view.getMatchId(), type);
        ModelAndView.create(view, modelHelper())
                .request(flowable, new ViewEvent<OddsContract.View, List<OddsData>>() {
                    @Override
                    public void call(OddsContract.View view, List<OddsData> data) {
                        if (data != null && data.size() > 0) {
                            view.getResult(data.get(0));
                        } else {
                            view.onErrorAndEmpty();
                        }
                    }
                }, new ViewEvent<OddsContract.View, ApiException>() {
                    @Override
                    public void call(OddsContract.View view, ApiException e) {
                        view.showRequestError(e);
                        view.onErrorAndEmpty();
                    }
                });
    }

    @Override
    public void getAnalysisOddsEurope(Integer type) {
        OddsContract.View view = view(OddsContract.View.class);
        Flowable<RespBase<List<OddsEuropeData>>> flowable = service(AnalysisService.class).getAnalysisEuropeOdds(view.getMatchId(), type);
        ModelAndView.create(view, modelHelper())
                .request(flowable, new ViewEvent<OddsContract.View, List<OddsEuropeData>>() {
                    @Override
                    public void call(OddsContract.View view, List<OddsEuropeData> data) {
                        if (data != null && data.size() > 0) {
                            view.getResultEurope(data.get(0));
                        } else {
                            view.onErrorAndEmpty();
                        }
                    }
                }, new ViewEvent<OddsContract.View, ApiException>() {
                    @Override
                    public void call(OddsContract.View view, ApiException e) {
                        view.showRequestError(e);
                        view.onErrorAndEmpty();
                    }
                });
    }

    @Override
    public void getAnalysisOddsBall(Integer type) {

    }
}
