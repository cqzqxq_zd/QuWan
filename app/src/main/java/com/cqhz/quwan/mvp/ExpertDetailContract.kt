package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.ExpertDetailBean


interface ExpertDetailContract {
    interface View : IBasicView {
        fun setExpertDetail(bean: ExpertDetailBean)
        fun updateSuccess()
    }

    interface Presenter {
        fun getExpertDetail(userId: String, followUserId: String)
        fun updateFollowStatus(userId: String, fansId: String, status: String)
    }
}