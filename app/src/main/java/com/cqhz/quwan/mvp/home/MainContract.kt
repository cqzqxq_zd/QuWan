package com.cqhz.quwan.mvp.home

import com.cqhz.quwan.model.SystemConfigBean
import com.cqhz.quwan.model.VersionBean
import com.cqhz.quwan.mvp.IBasicView


interface MainContract {
    interface View : IBasicView {
        fun setGuessButton(systemConfigBean: SystemConfigBean)
        fun setVersionInfo(versionBean: VersionBean)
    }

    interface Presenter {
        fun getSystemConfig()
        fun getVersionConfig()
    }
}