package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.service.MallService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class MallPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), MallContract.Presenter {

    /**
     * 获取商品列表
     */
    override fun getProductList(pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(MallContract.View::class.java), modelHelper())
                .request(service(MallService::class.java).getProductList(pageNo, pageSize), { view, data ->
                    view.loadProductList(pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取用户信息，显示钻石数目
     * @param userId 用户id
     */
    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(MallContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }
}