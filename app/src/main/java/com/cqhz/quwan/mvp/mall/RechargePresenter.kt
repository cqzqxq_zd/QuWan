package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.service.CommonService
import com.cqhz.quwan.service.RechargeService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class RechargePresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), RechargeContract.Presenter {

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(RechargeContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.setBean(data.balance ?: "0")
                }, { view, data -> view.showRequestError(data) })
    }

    override fun doRecharge4wechat(userId: String, money: String) {
        ModelAndView.create(view(RechargeContract.View::class.java), modelHelper()).request(
                service(RechargeService::class.java).doRecharge4wechat(userId, money), { v, d ->
            v.callWechatPay(d)
        }, { v, e -> v.showRequestError(e) })
    }

    override fun doRecharge(userId: String, money: String) {
        ModelAndView.create(view(RechargeContract.View::class.java), modelHelper()).request(
                service(RechargeService::class.java).doRecharge(userId, money), { v, d ->
            v.hintLoading()
            v.callAlipay(d)
        }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取充值金额
     */
    override fun getSystemConfig(key: String) {
        ModelAndView.create(view(RechargeContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).systemConfig(key), { v, data ->
            if (key == KeyContract.RECHARGE_GIFT_RATIO) {
                v.setMoney(data)
            } else {
                v.setTips(data)
            }
        }, { v, e -> v.showRequestError(e) })
    }
}