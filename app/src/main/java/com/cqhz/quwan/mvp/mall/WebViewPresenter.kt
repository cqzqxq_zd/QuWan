package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class WebViewPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), WebViewContract.Presenter {

    /**
     * 获取用户信息
     * @param userId 用户id
     */
    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(WebViewContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    override fun getOrderInfo(orderId: String, periodId: String, typeId: String) {
        ModelAndView.create(view(WebViewContract.View::class.java), modelHelper()).request(
                service(OrderService::class.java).getOrderInfo(orderId),
                { v, d ->
                    v.showOrderInfo(d, periodId, typeId)
                }, { v, e -> v.showRequestError(e) }
        )
    }
}