package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.ExpertInfoBean
import com.cqhz.quwan.mvp.IBasicView
import okhttp3.MultipartBody


interface ExpertCertificationContract {
    interface View : IBasicView {
        fun setExpertInfo(bean: ExpertInfoBean)
        fun commitSuccess()
        fun setHeadUrl(url: String?)
    }

    interface Presenter {
        fun getExpertInfo(userId: String)
        fun expertAuth(userId: String, nick: String, headPortrait: String, introduce: String)
        fun updateHead(files: List<MultipartBody.Part>)
    }
}