package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.model.ProductBean
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView


interface MallContract {
    interface View : IBasicView {
        fun showUserInfo(userInfoBean: UserInfoBean)
        fun loadProductList(pageNo: Int, pageResultBean: PageResultBean<ProductBean>)
    }

    interface Presenter {
        fun getUserInfo(userId:String)
        fun getProductList(pageNo: Int, pageSize: Int)
    }
}