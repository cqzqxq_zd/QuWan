package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.ExpertDetailBean
import com.cqhz.quwan.model.OrderInfoBean


interface PlanDetailContract {
    interface View : IBasicView {
        fun setProgrammeDetail(bean: ExpertDetailBean)
        fun updateSuccess()
        fun commitSuccess(orderInfo: OrderInfoBean)
    }

    interface Presenter {
        fun getProgrammeDetail(orderId: String, expertId: String, followUserId: String)
        fun updateFollowStatus(userId: String, fansId: String, status: String)
        fun addFollowOrder(userId: Long, orderId: Long, betTimes: Int)
    }
}