package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.BallMatchListBean
import com.cqhz.quwan.model.LeagueBean


interface FootballLotteryContract {
    interface View : IBasicView {
        fun setFootballList(currentPage: Int, list: List<BallMatchListBean>)
        fun setLeagueList(list: List<LeagueBean>)
    }

    interface Presenter {
        fun getFootballList(currentPage: Int, leagueIds: String?)
        fun getLeagueList()
    }
}