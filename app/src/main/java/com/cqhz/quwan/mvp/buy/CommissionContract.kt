package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.model.CommissionBean
import com.cqhz.quwan.model.CommissionInfoBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.mvp.IBasicView


interface CommissionContract {
    interface View : IBasicView {
        fun showCommissionInfo(commissionInfoBean: CommissionInfoBean)
        fun loadRecordList(pageNo: Int, pageResultBean: PageResultBean<CommissionBean>)
    }

    interface Presenter {
        fun getCommissionInfo(userId: String)
        fun getRecordList(userId: String, pageNo: Int, pageSize: Int)
    }
}