package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.model.PushOrderResultBean
import com.cqhz.quwan.mvp.IBasicView


interface MyPushOrderContract {
    interface View : IBasicView {
        fun loadOrderList(type: Int, pageNo: Int, bean: PushOrderResultBean)
    }

    interface Presenter {
        fun getOrderList(userId: String, type: Int, status: Int, pageNo: Int, pageSize: Int)
    }
}