package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.model.TransactionRecordBean
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView


interface DiamondsRecordContract {
    interface View : IBasicView {
        fun loadRecordList(pageNo: Int, pageResultBean: PageResultBean<TransactionRecordBean>)
        fun showUserInfo(userInfoBean: UserInfoBean)
    }

    interface Presenter {
        fun getRecordList(userId: String, pageNo: Int, pageSize: Int)
        fun getUserInfo(userId:String)
    }
}