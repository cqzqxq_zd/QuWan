package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisForecast;
import com.cqhz.quwan.mvp.IBasicView;

/**
 * @author whamu2
 * @date 2018/7/26
 */
public interface ForecastContract {
    interface View extends IBasicView {
        void getResult(AnalysisForecast data);

        void onFail();

        String getMatchId();
    }

    interface Presenter {
        void getAnalysisForecast();
    }
}
