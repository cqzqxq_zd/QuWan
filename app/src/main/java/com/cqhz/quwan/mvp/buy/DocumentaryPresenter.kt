package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class DocumentaryPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), DocumentaryContract.Presenter {

    /**
     * 获取跟单列表的数据
     * @param  type 1:人气 2：命中 3：盈利 4：关注
     */
    override fun getOrderList(pageNo: Int, pageSize: Int, type: Int, followUserId: String) {
        ModelAndView.create(view(DocumentaryContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).getFollowOrderList(pageNo, pageSize, when (type) {
                    0 -> 1
                    1 -> 2
                    2 -> 3
                    3 -> 4
                    else -> {
                        1
                    }
                }, followUserId), { view, data ->
                    view.loadOrderList(type, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 取消关注
     * @param userId
     * @param fansId 用户id
     * @param status 状态(0:取消关注,1:关注)
     */
    override fun updateFollowStatus(userId: String, fansId: String, status: String) {
        ModelAndView.create(view(DocumentaryContract.View::class.java), modelHelper())
                .request(service(UserService::class.java).followExpert(userId, fansId, status), { view, data ->
                    view.updateSuccess()
                }, { v, e -> v.showRequestError(e) })
    }
}