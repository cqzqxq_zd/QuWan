package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView

interface SettingsContract {
    interface View : IBasicView {
        fun showUserInfo(userInfoBean: UserInfoBean)
        fun refresh()
    }

    interface Presenter {
        fun getUserInfo(userId: String)
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
    }
}