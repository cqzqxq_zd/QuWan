package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.OrderService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

/**
 * Created by Guojing on 2018/9/19.
 */
class OrderListPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), OrderListContract.Presenter {
    /**
     * 获取订单列表
     * @param userId 用户id
     * @param status 空，2，4
     * @param pageNo 1..
     * @param pageSize 10
     */
    override fun getOrderList(userId: String, currentPage: Int, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(OrderListContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).orderPage(userId, when (currentPage) {
                    1 -> "2"
                    2 -> "4"
                    else -> ""
                }, pageNo, pageSize), { view, data ->
                    view.loadOrderList(currentPage, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}