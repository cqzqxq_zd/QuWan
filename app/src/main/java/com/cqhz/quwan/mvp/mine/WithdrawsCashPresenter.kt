package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class WithdrawsCashPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), WithdrawsCashContract.Presenter {

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(WithdrawsCashContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    override fun getCash(userId: String, money: String) {

    }

}