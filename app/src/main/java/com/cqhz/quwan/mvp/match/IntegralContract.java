package com.cqhz.quwan.mvp.match;

import com.cqhz.quwan.model.AnalysisIntegral;
import com.cqhz.quwan.mvp.IBasicView;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public interface IntegralContract {
    interface View extends IBasicView {
        void getData(AnalysisIntegral data);

        void onFail();

        String getMatchId();

        String getLeague();
    }

    interface Presenter {
        void getIntegralToType(int type);

    }
}
