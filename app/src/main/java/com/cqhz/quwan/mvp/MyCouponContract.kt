package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.CouponBean
import com.cqhz.quwan.model.PageResultBean


interface MyCouponContract {
    interface View : IBasicView {
        fun loadCouponList(currentPage: Int, pageNo: Int, pageResultBean: PageResultBean<CouponBean>)
    }

    interface Presenter {
        fun getCouponList(userId: Long, currentPage: Int, pageNo: Int, pageSize: Int)
    }
}