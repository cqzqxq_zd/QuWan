package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class FootBallBetPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), FootBallBetContract.Presenter {
    override fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(FootBallBetContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).bindAlipay(userId, alipayAuthData.userId, alipayAuthData.alipayOpenId, alipayAuthData.authCode), { _ -> "" },
                { view, _ ->
                    view.showToast("绑定成功")
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取专家信息
     * @param userId
     */
    override fun getExpertInfo(userId: String) {
        ModelAndView.create(view(FootBallBetContract.View::class.java), modelHelper())
                .request(service(UserService::class.java).getExpertInfo(userId.toLong()), { view, data ->
                    view.setExpertInfo(data)
                }, { v, e -> v.showRequestError(e) })
    }
}