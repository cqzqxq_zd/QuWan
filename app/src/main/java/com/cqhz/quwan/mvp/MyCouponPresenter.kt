package com.cqhz.quwan.mvp

import com.cqhz.quwan.service.APIService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class MyCouponPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), MyCouponContract.Presenter {

    /**
     * 获取优惠券列表
     * @param userId 用户id
     * @param status 优惠券状态 1:未使用 2:已使用、已过期 3:待派发
     * @param pageNo 1..
     * @param pageSize 10
     */
    override fun getCouponList(userId: Long, currentPage: Int, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(MyCouponContract.View::class.java), modelHelper())
                .request(service(APIService::class.java).getCouponList(userId, when (currentPage) {
                    0 -> 1
                    1 -> 3
                    2 -> 2
                    else -> 1
                }, pageNo, pageSize), { view, data ->
                    view.loadCouponList(currentPage, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}