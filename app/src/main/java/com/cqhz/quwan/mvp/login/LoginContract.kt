package com.cqhz.quwan.mvp.login

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.model.LoginBean
import com.cqhz.quwan.mvp.IBasicView


interface LoginContract {
    interface BasicView:IBasicView{
        fun lockInputVerCode()
        fun authLoginOk(loginBean: LoginBean)
    }
    interface Presenter {
        fun getVerCode(number:String)
        fun doLogin(number:String,verCode:String)
        fun login4alipay(alipayAuthData:AlipayAuthData)
    }
}