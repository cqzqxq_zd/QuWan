package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.mvp.IBasicView
import com.cqhz.quwan.util.AuthResult
import com.cqhz.quwan.util.PayResult


interface AlipayContract {
    interface View : IBasicView{
        fun callAlipayAuth(arg:String)
        fun callAlipay(arg:String)
        fun alipayCallBack()
        fun alipayFail(payResult:PayResult)
        fun alipayAuthCallBack(authData:AlipayAuthData)
        fun alipayAuthFail(authResult: AuthResult)
    }
    interface Presenter {
        fun auth4alipay()
    }
}