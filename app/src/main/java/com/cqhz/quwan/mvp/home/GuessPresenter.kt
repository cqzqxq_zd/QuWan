package com.cqhz.quwan.mvp.home

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.service.CommonService
import com.cqhz.quwan.service.HomeService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class GuessPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), GuessContract.Presenter {
    override fun saveReaderNum(num: String) {
        ModelAndView.create(view(GuessContract.View::class.java), modelHelper()).request(
                service(HomeService::class.java).saveReaderNum(num), { _ -> "" },
                { _, _ -> }, { v, e -> v.showRequestError(e) }
        )
    }

    override fun getHotLottery() {
        ModelAndView.create(view(GuessContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).hotLottery("1"), { view, item ->
                    view.showHotLottery(item)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取顶部广告的数据
     * @param  type 0：竞猜 1：首页中部 2：个人中心 3：弹窗 4:资讯首页
     * @return data 广告数据列表
     */
    override fun getBannerData(type: Int) {
        ModelAndView.create(view(GuessContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).queryBannerList(type), { view, data ->
                    view.loadBanner(type, data)
                }, { v, e -> v.showRequestError(e) })
    }

    override fun getNewsList() {
        ModelAndView.create(view(GuessContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).queryNewsList(), { view, item ->
                    view.showHotNews(item.records)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取弹窗信息
     */
    override fun getWindowList(userId: String) {
        ModelAndView.create(view(GuessContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).getWindowList(userId), { v, data ->
            v.setWindowList(data)
        }, { v, e -> v.showRequestError(e) })
    }

    override fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData) {
        ModelAndView.create(view(GuessContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).bindAlipay(userId, alipayAuthData.userId, alipayAuthData.alipayOpenId, alipayAuthData.authCode), { _ -> "" },
                { view, _ ->
                    view.showToast("绑定成功")
                }, { view, data -> view.showRequestError(data) })
    }
}