package com.cqhz.quwan.mvp

import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.model.ExpertInfoBean
import com.cqhz.quwan.model.OrderResp


interface FootballSelectedContract {
    interface View : IBasicView {
        fun setExpertInfo(bean: ExpertInfoBean)
        fun bindAlipay()
        fun setCommitEnable()
        fun toPay(orderResp: OrderResp)
    }

    interface Presenter {
        fun getExpertInfo(userId: String)
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
        fun postOrder(orderArgs: Map<String, String>)
    }
}