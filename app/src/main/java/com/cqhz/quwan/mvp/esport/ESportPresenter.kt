package com.cqhz.quwan.mvp.esport

import com.cqhz.quwan.service.HomeService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class ESportPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), ESportContract.Presenter {

    /**
     * 获取电竞标题列表
     */
    override fun getTitleList(type: String) {
        ModelAndView.create(view(ESportContract.View::class.java), modelHelper()).request(
                service(HomeService::class.java).getTitleList(type), { v, data ->
            v.setTitleList(data)
        }, { v, e ->
            v.requestTitleFail()
            v.showRequestError(e)
        })
    }

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(ESportContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.setBean(data.balance ?: "0")
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取电竞赛事列表的数据
     * @param  type 游戏类型
     */
    override fun getGameList(pageNo: Int, pageSize: Int, type: Int, pagePosition: Int) {
        ModelAndView.create(view(ESportContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).eSportGameList(pageNo, pageSize, type), { view, data ->
                    view.loadGameList(pagePosition, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}