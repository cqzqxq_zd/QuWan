package com.cqhz.quwan.mvp.home

import com.cqhz.quwan.model.*
import com.cqhz.quwan.mvp.IBasicView


interface GuessContract {
    interface View : IBasicView {
        fun loadBanner(type: Int, banner: List<BannerItem>)
        fun showHotLottery(hotLottery: List<HotLottery>)
        fun showHotNews(news: List<HotNews>)
        fun setWindowList(list: List<WindowBean>)
    }

    interface Presenter {
        fun saveReaderNum(num: String)
        fun getBannerData(type: Int)
        fun getHotLottery()
        fun getNewsList()
        fun getWindowList(userId: String)
        fun bindAlipay(userId: String, alipayAuthData: AlipayAuthData)
    }
}