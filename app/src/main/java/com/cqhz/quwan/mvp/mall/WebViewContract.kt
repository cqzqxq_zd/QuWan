package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.model.OrderResp
import com.cqhz.quwan.model.UserInfoBean
import com.cqhz.quwan.mvp.IBasicView


interface WebViewContract {
    interface View : IBasicView {
        fun showUserInfo(userInfoBean: UserInfoBean)
        fun showOrderInfo(orderInfo: OrderResp, periodId:String, typeId:String)
    }

    interface Presenter {
        fun getUserInfo(userId:String)
        fun getOrderInfo(orderId:String,periodId:String,typeId:String)
    }
}