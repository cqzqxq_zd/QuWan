package com.cqhz.quwan.mvp.news


import com.cqhz.quwan.service.CommonService
import com.cqhz.quwan.service.HomeService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

/**
 * @author Guojing
 * @date 2018/9/13
 */
class NewsPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), NewsContract.Presenter {

    /**
     * 获取顶部广告的数据
     * @param  banner类型 0：竞猜 1：首页中部 2：个人中心 3：弹窗 4:资讯首页
     */
    override fun getBannerData() {
        ModelAndView.create(view(NewsContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).queryBannerList(4), { view, data ->
                    view.loadBanner(data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取两场比赛的数据
     */
    override fun getNewsGames() {
        ModelAndView.create(view(NewsContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).newsGames(), { view, data ->
                    view.loadNewsGames(data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取资讯列表的数据
     * @param type 赛事类型0:热门赛事1:足球2:中超3:篮球4:电竞
     */
    override fun getNewsList(type: Int, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(NewsContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).newsList(type, pageNo, pageSize), { view, data ->
                    view.loadNewsList(type, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取弹窗信息
     */
    override fun getWindowList(userId: String) {
        ModelAndView.create(view(NewsContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).getWindowList(userId), { v, data ->
            v.setWindowList(data)
        }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 资讯点击统计
     */
    override fun newsClick(newsId: String) {
        ModelAndView.create(view(NewsContract.View::class.java), modelHelper())
                .request(service(HomeService::class.java).newsClick(newsId), { view, data ->
                    view.newsClickSuccess(newsId)
                }, { v, e -> v.showRequestError(e) })
    }
}