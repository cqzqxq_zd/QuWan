package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.service.CashService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class DiamondsRecordPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), DiamondsRecordContract.Presenter {

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(DiamondsRecordContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.showUserInfo(data)
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取商品列表
     */
    override fun getRecordList(userId: String, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(DiamondsRecordContract.View::class.java), modelHelper())
                .request(service(CashService::class.java).diamondsRecord(userId, pageNo, pageSize), { view, data ->
                    view.loadRecordList(pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}