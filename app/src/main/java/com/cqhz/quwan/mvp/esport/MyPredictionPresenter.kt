package com.cqhz.quwan.mvp.esport

import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class MyPredictionPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), MyPredictionContract.Presenter {

    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(MyPredictionContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { view, data ->
                    view.hintLoading()
                    view.setGuessStatistics(data)
                }, { view, data -> view.showRequestError(data) })
    }

    /**
     * 获取订单列表
     * @param userId 用户id
     * @param status 空，4
     * @param pageNo 1..
     * @param pageSize 10
     */
    override fun getOrderList(userId: String, currentPage: Int, pageNo: Int, pageSize: Int) {
        ModelAndView.create(view(MyPredictionContract.View::class.java), modelHelper())
                .request(service(OrderService::class.java).eSportOrderList(userId, when (currentPage) {
                    0 -> ""
                    1 -> "4"
                    else -> ""
                },"10".toLong(), pageNo, pageSize), { view, data ->
                    view.loadOrderList(currentPage, pageNo, data)
                }, { v, e -> v.showRequestError(e) })
    }
}