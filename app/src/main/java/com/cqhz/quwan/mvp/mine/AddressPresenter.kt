package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.AddressBean
import com.cqhz.quwan.service.AddressService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


/**
 * 收货地址
 */
class AddressPresenter @Inject constructor(modelHelper: ModelHelper?)
    : QuickPresenter(modelHelper), AddressContract.Presenter {
    override fun getUserInfo(userId: String) {
        ModelAndView.create(view(AddressContract.View::class.java), modelHelper()).request(
                service(UserService::class.java).getUserInfo(userId),
                { _, data ->
                        data.addressId?.let { getAddress(it) }
                }, { view, data -> view.showRequestError(data) })
    }

    override fun getAddress(userId: String) {
        ModelAndView.create(view(AddressContract.View::class.java), modelHelper()).request(
                service(AddressService::class.java).getAddress(userId),
                { view, data ->
                    view.setAddress(data)
                }, { view, data -> view.showRequestError(data) })
    }

    override fun saveAddress(userId: String, addressBean: AddressBean) {
        ModelAndView.create(view(AddressContract.View::class.java), modelHelper()).request(
                service(AddressService::class.java).saveAddress(addressBean.id, userId, addressBean.receiverName, addressBean.phoneNumber, addressBean.province, addressBean.city, addressBean.county, addressBean.street, addressBean.detailAddress),
                { view, data ->
                    view.updateSuccess()
                }, { view, data ->
            view.showRequestError(data)
        })
    }
}