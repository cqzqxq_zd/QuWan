package com.cqhz.quwan.mvp.home

import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.service.CommonService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject

class MainPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper), MainContract.Presenter {

    /**
     * 获取竞猜开关
     */
    override fun getSystemConfig() {
        ModelAndView.create(view(MainContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).systemConfig(KeyContract.GUESS_COMPETITION), { v, data ->
            v.setGuessButton(data)
        }, { v, e -> v.showRequestError(e) })
    }

    /**
     * 获取版本更新信息
     */
    override fun getVersionConfig() {
        ModelAndView.create(view(MainContract.View::class.java), modelHelper()).request(
                service(CommonService::class.java).getVersionConfig(), { v, data ->
            v.setVersionInfo(data)
        }, { v, e -> v.showRequestError(e) })
    }
}