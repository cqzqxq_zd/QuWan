package com.cqhz.quwan.mvp.buy

import com.cqhz.quwan.model.*
import com.cqhz.quwan.mvp.IBasicView


interface GoPaymentContract {
    interface View : IBasicView{
        fun showUserInfo(userInfoBean: UserInfoBean)
        fun loadAvailableCouponList(list: List<CouponBean>)
        fun showOrderInfo(orderInfo: OrderResp)
        fun setWindowList(list: List<WindowBean>)
    }

    interface Presenter {
        fun getUserInfo(userId:String)
        fun getAvailableCouponList(orderId: String)
        fun getWindowList(userId: String)
        fun orderPay(userId: String, orderId: String, couponId: String?)
    }
}