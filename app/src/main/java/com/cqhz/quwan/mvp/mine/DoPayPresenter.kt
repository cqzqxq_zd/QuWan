package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.APP
import com.cqhz.quwan.model.CouponConsume
import com.cqhz.quwan.service.OrderService
import com.cqhz.quwan.service.UserService
import me.militch.quickcore.execute.impl.ModelAndView
import me.militch.quickcore.mvp.model.ModelHelper
import me.militch.quickcore.mvp.presenter.QuickPresenter
import javax.inject.Inject


class DoPayPresenter @Inject constructor(modelHelper: ModelHelper?) : QuickPresenter(modelHelper),DoPayContract.Presenter {
    override fun doPay2(couponConsume: CouponConsume,payPassword:String) {
        ModelAndView.create(view(DoPayContract.View::class.java),modelHelper()).request(
                service(OrderService::class.java).
                        balanceConsume(
                                couponConsume.orderId,
                                couponConsume.couponId,
                                couponConsume.subBalanceMoney,
                                payPassword),{ _->""},
                {v,d->
                    v.hintLoading()
                    v.closePage()
                },
                {v,e->
                    v.showRequestError(e)
                    v.close4resultCode(0x99)
                }
        )
    }

    override fun doGetCash(money: Double,  payPassword: String,payWay:Int) {
        ModelAndView.create(view(DoPayContract.View::class.java),modelHelper()).request(
                service(UserService::class.java).getCash(APP.get()!!.loginInfo!!.userId!!,money,payWay,payPassword),{ _->""},
                {v,d->
                    v.hintLoading()
                    v.closePage()
                },
                {v,e->
                    v.showRequestError(e)
                    v.close4resultCode(0x99)
                }
        )
    }



    override fun doPay(money: Double, orderId: String, payPassword: String) {
        ModelAndView.create(view(DoPayContract.View::class.java),modelHelper()).request(
                service(OrderService::class.java).payOrder4Balance(APP.get()!!.loginInfo!!.userId!!,money,orderId,payPassword),{_->""},
                {v,d->
                    v.hintLoading()
                    v.closePage()
                },
                {v,e->
                    v.showRequestError(e)
                    v.close4resultCode(0x99)
                }
        )
    }
}