package com.cqhz.quwan.mvp.mall

import com.cqhz.quwan.model.ExchangeOrderBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.mvp.IBasicView


interface MyExchangeContract {
    interface View : IBasicView {
        fun loadOrderList(currentPage: Int, pageNo: Int, pageResultBean: PageResultBean<ExchangeOrderBean>)
    }

    interface Presenter {
        fun getOrderList(userId: Long, currentPage: Int, pageNo: Int, pageSize: Int)
    }
}