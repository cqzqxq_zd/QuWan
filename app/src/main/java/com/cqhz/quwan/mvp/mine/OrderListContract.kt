package com.cqhz.quwan.mvp.mine

import com.cqhz.quwan.model.OrderBean
import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.mvp.IBasicView


interface OrderListContract {
    interface View : IBasicView {
        fun loadOrderList(currentPage: Int, pageNo: Int, pageResultBean: PageResultBean<OrderBean>)
    }

    interface Presenter {
        fun getOrderList(userId: String, currentPage: Int, pageNo: Int, pageSize: Int)
    }
}