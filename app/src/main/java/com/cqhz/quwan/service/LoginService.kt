package com.cqhz.quwan.service

import com.cqhz.quwan.model.LoginBean
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface LoginService {
    @POST(API.sendVerifyCode)
    @FormUrlEncoded
    fun sendVerifyCode(
            @Field("mobile") mobile:String,
            @Field("codeType") codeType:Int
    ):Flowable<RespBase<String>>

    @POST(API.loginRegister)
    @FormUrlEncoded
    fun doLogin(
            @Field("mobile") mobile:String,
            @Field("verifyCode") verCode:String
    ):Flowable<RespBase<LoginBean>>

    @POST(API.bindMobilePhone)
    @FormUrlEncoded
    fun alipayAuthLogin(
            @Field("mobile") mobile:String,
            @Field("openId") openId:String,
            @Field("alipayAccount") alipayAccount:String,
            @Field("authCode") authCode:String,
            @Field("type") type:Int
    ): Flowable<RespBase<LoginBean>>

    @POST(API.bindMobilePhone)
    @FormUrlEncoded
    fun alipayAuthLogin(
            @Field("openId") openId:String,
            @Field("alipayAccount") alipayAccount:String,
            @Field("authCode") authCode:String,
            @Field("type") type:Int
    ): Flowable<RespBase<LoginBean>>


}