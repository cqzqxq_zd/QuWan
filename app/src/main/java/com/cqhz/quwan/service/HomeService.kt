package com.cqhz.quwan.service

import com.cqhz.quwan.model.*
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface HomeService {

    @POST(API.queryBannerList)
    @FormUrlEncoded
    fun queryBannerList(@Field("type") type: Int): Flowable<RespBase<List<BannerItem>>>

    @POST(API.hotLottery)
    @FormUrlEncoded
    fun hotLottery(@Field("status") status: String): Flowable<RespBase<List<HotLottery>>>

    @POST(API.news)
    fun queryNewsList(): Flowable<RespBase<NewsPageBean>>

    @POST(API.msg)
    @FormUrlEncoded
    fun msgList(@Field("type") type: Int): Flowable<RespBase<MsgPageBean>>


    @POST(API.msg2)
    @FormUrlEncoded
    fun msg2List(
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int,
            @Field("userId") userId: String): Flowable<RespBase<RecordBean<Msg2Entity>>>

    @POST(API.saveReaderNum)
    @FormUrlEncoded
    fun saveReaderNum(@Field("id") id: String): Flowable<RespBase<String>>

    @POST(API.main_page_switch)
    @FormUrlEncoded
    fun switchActivity(@Field("appVersion") appVersion: String,
                       @Field("clientId") clientId: String,
                       @Field("clientModel") clientModel: String,
                       @Field("clientOs") clientOs: String,
                       @Field("osVersion") osVersion: String,
                       @Field("networkType") networkType: String,
                       @Field("lat") lat: String,
                       @Field("lng") lng: String,
                       @Field("areaCode") areaCode: String,
                       @Field("langCode") langCode: String): Flowable<RespBase<String>>

//    @POST(API.main_cgj)
//    fun queryCGJBean(): Flowable<RespBase<CGJData>>

    @POST(API.hotMatchList)
    fun queryHotMatch(): Flowable<RespBase<List<HotBean>>>

    @POST(API.focusMatchList)
    fun queryFocusMatch(): Flowable<RespBase<List<FocusBean>>>

    //=================================================
    @POST(API.newsGames)
    fun newsGames(): Flowable<RespBase<List<NewsGameBean>>>

    //=================================================
    @POST(API.newsList)
    @FormUrlEncoded
    fun newsList(
            @Field("type") type: Int,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int): Flowable<RespBase<PageResultBean<NewsBean>>>

    //=================================================
    @POST(API.newsClick)
    @FormUrlEncoded
    fun newsClick(@Field("newsId") newsId: String): Flowable<RespBase<String>>


    //=================================================
    @POST(API.matchLiveList)
    @FormUrlEncoded
    fun matchLiveList(
            @Field("pageNo") pageNo: String,
            @Field("pageSize") pageSize: String,
            @Field("status") status: Int,
            @Field("type") type: Int): Flowable<RespBase<PageResultBean<MatchBean>>>


    @POST(API.eSportGameList)
    @FormUrlEncoded
    fun eSportGameList(
            @Field("pageNO") pageNo: Int,
            @Field("pageSize") pageSize: Int,
            @Field("type") type: Int): Flowable<RespBase<PageResultBean<ESportBean>>>


    @POST(API.eSportGameItemList)
    @FormUrlEncoded
    fun eSportGameItemList(
            @Field("matchId") matchId: String,
            @Field("round") round: String): Flowable<RespBase<ArrayList<ESportItemBean>>>


    @POST(API.eSportDetailHead)
    @FormUrlEncoded
    fun eSportDetailHead(@Field("matchId") matchId: String): Flowable<RespBase<ESportDetailBean>>


    @POST(API.getTitleList)
    @FormUrlEncoded
    fun getTitleList(@Field("type") type: String): Flowable<RespBase<ArrayList<TitleBean>>>
}
