package com.cqhz.quwan.service

import com.cqhz.quwan.model.PageResultBean
import com.cqhz.quwan.model.RecordBean
import com.cqhz.quwan.model.TransactionRecordBean
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface CashService {
    @POST(API.queryTransactionRecord)
    @FormUrlEncoded
    fun transactionRecord(
            @Field("userId") userId: String,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize:Int
    ):Flowable<RespBase<RecordBean<TransactionRecordBean>>>

    @POST(API.queryWonRecord)
    @FormUrlEncoded
    fun wonRecord(
            @Field("userId") userId: String,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize:Int
    ):Flowable<RespBase<RecordBean<TransactionRecordBean>>>

    @POST(API.diamondsRecord)
    @FormUrlEncoded
    fun diamondsRecord(
            @Field("userId") userId: String,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize:Int
    ):Flowable<RespBase<PageResultBean<TransactionRecordBean>>>
}