package com.cqhz.quwan.service

import com.cqhz.quwan.model.FootBallEntity
import com.cqhz.quwan.model.LeagueEntity
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface FootballService {
    @POST(API.getBallMatchList)
    @FormUrlEncoded
    fun list(
            /**
             * 1:胜平负
             * 2:让球胜平负
             * 3:比分
             * 4:总进球
             * 5:半全场
             * 6:混合投注
             * 7:单关固定
             * 8:猜一场
             * 9:2选1
             */
            @Field("playType") playType: String,
            @Field("leagueIds") leagues: String = "",
            @Field("matchType") matchType: String = "0"
    ): Flowable<RespBase<List<FootBallEntity>>>

    @POST(API.getLeagueList)
    fun leagues(): Flowable<RespBase<List<LeagueEntity>>>
}