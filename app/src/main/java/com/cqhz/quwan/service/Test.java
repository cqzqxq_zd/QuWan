package com.cqhz.quwan.service;

import io.reactivex.Flowable;
import me.militch.quickcore.util.RespBase;
import quickcore.annotation.Repository;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by WYZ on 2018/3/30.
 */
@Repository
public interface Test {
    @POST(API.realAuth)
    @FormUrlEncoded
    Flowable<RespBase<Object>> realAuth(
            @Field("userId") String userId,
            @Field("realName") String realName,
            @Field("idCard") String idCard
    );
}
