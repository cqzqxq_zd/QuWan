package com.cqhz.quwan.service

import com.cqhz.quwan.model.CouponConsume
import com.cqhz.quwan.model.CouponEntity
import com.cqhz.quwan.model.RecordBean
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface ActionService {
    @POST(API.getCouponList)
    @FormUrlEncoded
    fun getCouponList(
            @Field("userId") userId:String,
            @Field("status") status:Int,
            @Field("pageNo") pageNo:Int,
            @Field("pageSize") pageSize:Int)
            :Flowable<RespBase<RecordBean<CouponEntity>>>

    @POST(API.getAvailableCouponList)
    @FormUrlEncoded
    fun getAvailableCouponList(@Field("orderId") orderId:String):
            Flowable<RespBase<List<CouponEntity>>>

    @POST(API.couponConsume)
    @FormUrlEncoded
    fun couponConsume(
            @Field("couponId") couponId:String,
            @Field("orderId") orderId: String):Flowable<RespBase<CouponConsume>>
}