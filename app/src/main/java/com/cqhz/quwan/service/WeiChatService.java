package com.cqhz.quwan.service;

import com.cqhz.quwan.model.WeiChatAccessTokenData;

import io.reactivex.Flowable;
import me.militch.quickcore.util.RespBase;
import quickcore.annotation.Repository;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * @author whamu2
 * @date 2018/7/10
 */
@Repository
public interface WeiChatService {

    @POST(API.get_weichat_token)
    @FormUrlEncoded
    Flowable<RespBase<String>> getWeichatToken(@Field("userId") String userId, @Field("code") String code);
}
