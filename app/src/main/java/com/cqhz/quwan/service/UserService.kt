package com.cqhz.quwan.service

import com.cqhz.quwan.model.ExpertInfoBean
import com.cqhz.quwan.model.MineAd
import com.cqhz.quwan.model.UserInfoBean
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


@Repository
interface UserService {
    //=================================================
    @POST(API.userInfo)
    @FormUrlEncoded
    fun getUserInfo(@Field("userId") userId: String): Flowable<RespBase<UserInfoBean>>

    //=================================================
    @POST(API.auth4alipay)
    fun auth4alipay(): Flowable<RespBase<String>>

    //=================================================
    @POST(API.realAuth)
    @FormUrlEncoded
    fun realAuth(
            @Field("userId") userId: String,
            @Field("realName") realName: String,
            @Field("idCard") idCard: String
    ): Flowable<RespBase<String>>

    //=================================================
    @POST(API.bindAlipay)
    @FormUrlEncoded
    fun bindAlipay(
            @Field("userId") userId: String,
            @Field("alipayAccount") alipayAccount: String,
            @Field("openId") openId: String,
            @Field("authCode") authCode: String
    ): Flowable<RespBase<String>>

    //=================================================
    @POST(API.getCash)
    @FormUrlEncoded
    fun getCash(
            @Field("userId") userId: String,
            @Field("money") money: Double,
            @Field("payWay") payWay: Int,
            @Field("payPassword") payPassword: String
    ): Flowable<RespBase<String>>

    //=================================================
    @POST(API.hasMsgNew)
    @FormUrlEncoded
    fun hasNewMsg(@Field("userId") userId: String): Flowable<RespBase<Int>>

    //=================================================
    @POST(API.mine_get_ad)
    fun getAd(): Flowable<RespBase<MineAd>>

    //=================================================
    @POST(API.signReward)
    @FormUrlEncoded
    fun signReward(
            @Field("userId") userId: String
    ): Flowable<RespBase<String>>

    //=================================================
    @POST(API.updateUserInfo)
    @FormUrlEncoded
    fun updateUserInfo(
            @Field("userId") userId: String,
            @Field("headAddress") headAddress: String,
            @Field("nickName") nickName: String
    ): Flowable<RespBase<String>>


    @POST(API.getExpertInfo)
    @FormUrlEncoded
    fun getExpertInfo(@Field("user_id") userId: Long): Flowable<RespBase<ExpertInfoBean>>


    @POST(API.followExpert)
    @FormUrlEncoded
    fun followExpert(
            @Field("user_id") userId: String,
            @Field("fans_id") fansId: String,
            @Field("status") status: String
    ): Flowable<RespBase<String>>
}