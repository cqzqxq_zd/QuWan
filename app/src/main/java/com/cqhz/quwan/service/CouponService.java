package com.cqhz.quwan.service;

import com.cqhz.quwan.model.Coupon;

import java.util.List;

import io.reactivex.Flowable;
import me.militch.quickcore.util.RespBase;
import quickcore.annotation.Repository;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * @author whamu2
 * @date 2018/6/11
 */
@Repository
public interface CouponService {
    @POST(API.getAvailableCouponList)
    @FormUrlEncoded
    Flowable<RespBase<List<Coupon>>> getList(@Field("orderId") String orderId);
}
