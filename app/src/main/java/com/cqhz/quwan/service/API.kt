package com.cqhz.quwan.service

import com.cqhz.quwan.BuildConfig


interface API {
    companion object {
        const val WEB_HOST = BuildConfig.H5_HOST
        const val FOOTBALL_INFORMATION = "${WEB_HOST}football-information.html"
        // 开奖
        const val KaiJiangPage = "${WEB_HOST}football-information.html"
        const val FaXianPage = "${WEB_HOST}faixian.html"
        // 意见反馈
        const val LunTan = "https://support.qq.com/product/37702"
        const val Wanfa = "${WEB_HOST}shuoming.html"
        const val share = "${WEB_HOST}share.html"
        // 跟单规则
        const val followOrderRule = "${WEB_HOST}gendanguiuze.html"
        // 红包使用规则
        const val couponUseRule = "${WEB_HOST}youhuijuan/use_rule.html"
        // 竞猜篮球规则
        const val basketballRule = "${WEB_HOST}jclqgz.html"
        // 注册协议
        const val RegisterAgreement = "${WEB_HOST}QuWanregXieyi.html"
        // 买卖服务协议
        const val saleAgreement1 = "${WEB_HOST}daimaixieyi.html?__hbt=1539244157670"
        // 商品代卖代收协议
        const val saleAgreement2 = "${WEB_HOST}1daimaixieyi.html?__hbt=1539244129390"

        const val TzXieyi = "${WEB_HOST}wendang.html"
        const val YQYJ = "${WEB_HOST}invite.html"
        const val YHQ_SYGZ = "${WEB_HOST}youhuijuan/use_rule.html"
        const val LishiKaiJiang = "${WEB_HOST}historyKaijiang.html"
        const val ZXXQ = "${WEB_HOST}zixunXq.html"
        // 订单详情
        const val DDXQ = "${WEB_HOST}daigouDetail.html"
        // 电竞订单详情页
        const val eSportOrderDetail = "${WEB_HOST}dianjingDetail.html"
        // 帮助中心
        const val H5_Help = "${WEB_HOST}QuWanhelp.html"
        // 玩法
        const val rule = "${WEB_HOST}QuWanDJwanfa.html"
        // 趣豆寻宝
        const val findTreasure = "${WEB_HOST}zscj/index.html"
        // 资讯详情
        const val newsDetail = "${WEB_HOST}newlist/index.html"
        // 直播
        const val matchLiveDetail = "${WEB_HOST}zhibo/tongzhi.html"
        // 方案详情 ?orderId=137575083400101888&expertId=31791792047390724&followUserId=31791792047390724
        const val planDetail = "${WEB_HOST}td/faxqy.html"
        // 专家详情 ?userId=31791792047390724&followUserId=31791792047390724
        const val expertDetail = "${WEB_HOST}td/zjxq.html"


        const val getCouponList = "/apiActivity/getCouponList"
        //todo change url
        const val HOST = BuildConfig.API_HOST
        const val queryBannerList = "/banner/queryList"
        // 交易记录
        const val queryTransactionRecord = "/cash/pageJY"
        // 中奖记录
        const val queryWonRecord = "/cash/pageZJ"
        const val sendVerifyCode = "/sms/sendVerifyCode"
        const val loginRegister = "/login/loginRegister"
        const val userInfo = "/user/get"
        // 我的竞猜列表
        const val orderPage = "/order/page"
        const val orderAdd = "/order/add"
        const val hotLottery = "/lottery/kind/list"
        const val news = "/news/queryPageList"
        const val msg = "/msg/queryPageList"
        const val msg2 = "/msg/user/page"
        const val resetPayPassword = "/user/updatePayPassord"
        const val recharge4alipay = "/alipay/rechargeParams"
        const val recharge4wechat = "/wechat/rechargeParams"
        const val auth4alipay = "/alipay/appAuth"
        const val bindMobilePhone = "/login/other"
        const val realAuth = "/user/realAuth"
        const val bindAlipay = "/user/bindAlipay"
        const val balancePay = "/cash/balanceConsume"
        const val alipay = "/alipay/consumeParams"
        const val getCash = "/cash/drawCash"
        const val hasMsgNew = "/msg/user/new"
        const val getOrderInfo = "/order/get"
        const val balanceConsume = "/cash/balanceConsume"
        const val aliPayConsumeParams = "/alipay/consumeParams"
        const val wechatConsumeParams = "/wechat/consumeParams"
        const val getAlipayAuthUser = "/alipay/getUser"
        const val saveReaderNum = "/news/saveReaderNum"
        const val getAvailableCouponList = "/apiActivity/getAvailableCouponList"
        const val couponConsume = "/apiActivity/couponConsume"
        // 世界杯开关
        const val main_page_switch = "/app/which/activity"
        // 猜冠军
//        const val main_cgj = "/home/hot/banner"
        // 热门赛事
        const val hotMatchList = "/home/hot/match/list2"
        // 焦点赛事
        const val focusMatchList = "/home/hot/match/list"
        // 获取微信用户openid
        const val get_weichat_token = "/user/bindWechat"
        //
        const val mine_get_ad = "/home/center/banner"
        // 签到
        const val signReward = "/sign/signReward"
        // 上传文件
        const val updateFile = "/file/upload"
        // 上传头像
        const val updateHead = "/file/upload/head"
        // 更新用户头像和昵称
        const val updateUserInfo = "/user/updateUserInfo"
        // 获取地址信息
        const val getAddress = "/address/get"
        // 保存地址信息
        const val saveAddress = "/address/save"
        // 获取商品列表
        const val getProductList = "/product/page"
        // 支付页面数据获取（获取商品详情）
        const val payDataProcure = "/product/payDataProcure"
        // 提交订单
        const val addProductOrder = "/product/addProductOrder"
        // 首页资讯两场赛事
        const val newsGames = "/matchNews/match"
        // 首页资讯列表
        const val newsList = "/matchNews/home"
        // 首页资讯点击统计
        const val newsClick = "/matchNews/click"
        // 系统配置参数获取
        const val systemConfig = "/sysConfig/get"
        // 直播数据列表
        const val matchLiveList = "/jczq/match/result/list"
        // 钻石交易记录
        const val diamondsRecord = "/cash/pageZS"
        // 兑换详情
        const val exchangeDetail = "/userProduct/get"
        // 我的兑换列表
        const val exchangeList = "/userProduct/page"
        // 去支付
        const val orderPay = "/order/pay"
        // 获取更新版本信息
        const val getVersionConfig = "/app/upgrade"
        // 获取弹窗信息
        const val getWindowList = "/msg/queryWindowList"
        // 电竞赛事列表
        const val eSportGameList = "/eSports/homeList"
        // 赛事展开玩法列表
        const val eSportGameItemList = "/eSports/playwayList"
        // 电竞玩法详情头部
        const val eSportDetailHead = "/eSports/eSportsHead"
        // 电竞订单列表（我的预测）
        const val eSportOrderList = "/order/dj/page"
        // 跟单列表
        const val getFollowOrderList = "/follow/orderList"
        // 我的佣金
        const val getCommissionList = "/expert/commission/page"
        // 我的佣金-头部信息
        const val getCommissionInfo = "/expert/commissionInfo"
        // 推单列表
        const val getPushOrderList = "/follow/pushOrderList"
        // 跟单-专家认证
        const val expertAuth = "/expert/expertAuth"
        // 跟单-关注专家
        const val followExpert = "/expert/follow"
        // 查询专家信息
        const val getExpertInfo = "/expert/getExpertInfo"
        // 获取电竞玩法标题列表
        const val getTitleList = "/eSports/titleList"
        // 获取竞猜足球和篮球列表
        const val getBallMatchList = "/jczq/match/hot/list"
        // 获取联赛列表
        const val getLeagueList = "/jczq/league/hot/list"
        //获取订单详情
        const val getOrderDetail = "/order/get"
        // 获取订单详情二维码图片
        const val getCodeImg = "/app/userorder/share/rq"
        // 获取专家详情
        const val getExpertDetail = "/follow/expertDetail"
        // 获取方案详情
        const val getProgrammeDetail = "/follow/programmeDetail"
        // 跟单
        const val addFollowOrder = "/order/follow/add"
        // 更新订单状态
        const val updateOrderStatus = "/order/updateStatus"
    }
}