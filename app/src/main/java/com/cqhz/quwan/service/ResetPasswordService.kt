package com.cqhz.quwan.service

import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface ResetPasswordService{
    @POST(API.resetPayPassword)
    @FormUrlEncoded
    fun resetPayPassword(
            @Field("userId") userId:String,
            @Field("payPassword") pass:String,
            @Field("verifyCode") verifyCode:String
    ):Flowable<RespBase<String>>
}