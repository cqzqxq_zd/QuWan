package com.cqhz.quwan.service;

import com.cqhz.quwan.model.AnalysisEventData;
import com.cqhz.quwan.model.AnalysisForecast;
import com.cqhz.quwan.model.AnalysisIntegral;
import com.cqhz.quwan.model.AnalysisRecord;
import com.cqhz.quwan.model.AnalysisVote;
import com.cqhz.quwan.model.OddsData;
import com.cqhz.quwan.model.OddsEuropeData;

import java.util.List;

import io.reactivex.Flowable;
import me.militch.quickcore.util.RespBase;
import quickcore.annotation.Repository;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * 赛事分析
 *
 * @author whamu2
 * @date 2018/7/24
 */
@Repository
public interface AnalysisService {

    /**
     * 赛事分析-头部数据
     *
     * @param code matchId
     * @return {@link Flowable}
     */
    @POST("/analysis/getAnalysisMatch")
    @FormUrlEncoded
    Flowable<RespBase<AnalysisEventData>> getAnalysisEvent(@Field("code") String code);

    /**
     * 赛事分析-比賽积分信息查询
     *
     * @param code matchId
     * @return {@link Flowable}
     */
    @POST("/analysis/getAnalysisPoint")
    @FormUrlEncoded
    Flowable<RespBase<AnalysisIntegral>> getIntegralToType(@Field("point_type") Integer type,
                                                           @Field("code") String code);

    /**
     * 赛事分析-分析-投票
     *
     * @param code matchId
     * @return {@link Flowable}
     */
    @POST("/analysis/addAnalysisVote")
    @FormUrlEncoded
    Flowable<RespBase<AnalysisVote>> addAnalysisVote(@Field("user_id") String userId,
                                                     @Field("code") String code,
                                                     @Field("vote_type") Integer type);

    /**
     * 赛事分析-分析(比賽历史交锋，近期战绩，未来赛事)查询
     *
     * @param code matchId
     * @return {@link Flowable}
     */
    @POST("/analysis/getAnalysisRecord")
    @FormUrlEncoded
    Flowable<RespBase<AnalysisRecord>> getAnalysisRecord(@Field("user_id") String userId,
                                                         @Field("code") String code);

    /**
     * 赛事分析-赔率信息查询
     *
     * @param code matchId
     * @return {@link Flowable}
     */
    @POST("/analysis/getAnalysisOdds")
    @FormUrlEncoded
    Flowable<RespBase<List<OddsData>>> getAnalysisOdds(@Field("code") String code,
                                                       @Field("odds_type") Integer type);

    /**
     * 赛事分析-赔率信息查询
     *
     * @param code matchId
     * @return {@link Flowable}
     */
    @POST("/analysis/getAnalysisOdds")
    @FormUrlEncoded
    Flowable<RespBase<List<OddsEuropeData>>> getAnalysisEuropeOdds(@Field("code") String code,
                                                                   @Field("odds_type") Integer type);

    /**
     * 赛事分析-比賽预测信息查询
     *
     * @param code matchId
     * @return {@link Flowable}
     */
    @POST("/analysis/getAnalysisForecast")
    @FormUrlEncoded
    Flowable<RespBase<AnalysisForecast>> getAnalysisForecast(@Field("code") String code);
}
