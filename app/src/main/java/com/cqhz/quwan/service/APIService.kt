package com.cqhz.quwan.service

import com.cqhz.quwan.model.*
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface APIService {

    @POST(API.getBallMatchList)
    @FormUrlEncoded
    fun getBallMatchList(
            @Field("playType") playType: Int,
            @Field("leagueIds") leagueIds: String?,
            @Field("matchType") matchType: String
    ): Flowable<RespBase<List<BallMatchListBean>>>

    @POST(API.getLeagueList)
    fun getLeagueList(): Flowable<RespBase<List<LeagueBean>>>


    @POST(API.getCodeImg)
    @FormUrlEncoded
    fun getCodeImg(
            @Field("userId") userId: String
    ): Flowable<RespBase<CodeBean>>


    @POST(API.getExpertDetail)
    @FormUrlEncoded
    fun getExpertDetail(
            @Field("userId") userId: String,
            @Field("followUserId") followUserId: String
    ): Flowable<RespBase<ExpertDetailBean>>

    @POST(API.getProgrammeDetail)
    @FormUrlEncoded
    fun getProgrammeDetail(
            @Field("orderId") orderId: String,
            @Field("expertId") expertId: String,
            @Field("followUserId") followUserId: String
    ): Flowable<RespBase<ExpertDetailBean>>

    @POST(API.addFollowOrder)
    @FormUrlEncoded
    fun addFollowOrder(
            @Field("userId") userId: Long,
            @Field("orderId") orderId: Long,
            @Field("betTimes") betTimes: Int
    ): Flowable<RespBase<OrderInfoBean>>

    @POST(API.updateOrderStatus)
    @FormUrlEncoded
    fun updateOrderStatus(
            @Field("id") orderId: String,
            @Field("status") status: String
    ): Flowable<RespBase<OrderBean>>

    @POST(API.getCouponList)
    @FormUrlEncoded
    fun getCouponList(
            @Field("userId") userId: Long,
            @Field("status") status: Int,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int)
            : Flowable<RespBase<PageResultBean<CouponBean>>>

    @POST(API.getAvailableCouponList)
    @FormUrlEncoded
    fun getAvailableCouponList(@Field("orderId") orderId: String): Flowable<RespBase<List<CouponBean>>>
}