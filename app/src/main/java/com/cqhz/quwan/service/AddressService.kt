package com.cqhz.quwan.service

import com.cqhz.quwan.model.AddressBean
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * 地址服务
 * Created by Guojing on 2018/9/8.
 */
@Repository
interface AddressService {

    //=================================================
    @POST(API.getAddress)
    @FormUrlEncoded
    fun getAddress(
            @Field("id") id: String
    ): Flowable<RespBase<AddressBean>>

    //=================================================
    @POST(API.saveAddress)
    @FormUrlEncoded
    fun saveAddress(
            @Field("id") id: String,
            @Field("userId") userId: String,
            @Field("receiverName") receiverName: String,
            @Field("phoneNumber") phoneNumber: String,
            @Field("province") province: String,
            @Field("city") city: String,
            @Field("county") county: String,
            @Field("street") street: String,
            @Field("detailAddress") detailAddress: String
    ): Flowable<RespBase<AddressBean>>
}