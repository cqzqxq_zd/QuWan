package com.cqhz.quwan.service

import com.cqhz.quwan.model.*
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * 商城服务
 * Created by Guojing on 2018/9/12.
 */
@Repository
interface MallService {

    //=================================================
    @POST(API.getProductList)
    @FormUrlEncoded
    fun getProductList(
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int
    ): Flowable<RespBase<PageResultBean<ProductBean>>>

    //=================================================
    @POST(API.payDataProcure)
    @FormUrlEncoded
    fun payDataProcure(
            @Field("userId") userId: String,
            @Field("productId") pageSize: String
    ): Flowable<RespBase<ProductDetailBean>>

    //=================================================
    @POST(API.addProductOrder)
    @FormUrlEncoded
    fun addProductOrder(
            @Field("userId") userId: String,
            @Field("productId") productId: String,
            @Field("num") num: String,
            @Field("type") type: String,// 交易类型 1：兑换实物 2：代卖
            @Field("exchangeMethod") exchangeMethod: String// 兑换方式 0-支付宝；1-微信
    ): Flowable<RespBase<CommonResultBean>>

    //=================================================
    @POST(API.exchangeDetail)
    @FormUrlEncoded
    fun exchangeDetail(
            @Field("tradeId") tradeId: String
    ): Flowable<RespBase<OrderDetailBean>>

    //=================================================
    @POST(API.exchangeList)
    @FormUrlEncoded
    fun exchangeList(
            @Field("userId") userId: Long,
            @Field("status") status: String,// 空，0，1
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int
    ): Flowable<RespBase<PageResultBean<ExchangeOrderBean>>>
}