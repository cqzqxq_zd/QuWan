package com.cqhz.quwan.service

import io.reactivex.Flowable
import io.reactivex.Observable
import me.militch.quickcore.util.RespBase
import okhttp3.MultipartBody
import okhttp3.RequestBody
import quickcore.annotation.Repository
import retrofit2.Call
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * 上传文件服务
 * Created by Guojing on 2018/9/7.
 */
@Repository
interface UploadService {

    @Multipart
    @POST(API.updateFile)
    @FormUrlEncoded
    fun updateFile(
            @Part("file") files: List<MultipartBody.Part>
    ): Flowable<RespBase<String>>

    @Multipart
    @POST(API.updateHead)
    fun updateHead(@Part files: List<MultipartBody.Part>): Flowable<RespBase<String>>

    /**
     * 上传头像
     */
    @Multipart
    @POST(API.updateHead)
    fun updateHead2(@Part files: List<MultipartBody.Part>): Call<RespBase<Map<String, Any>>>

    /**
     * 上传头像
     */
    @Multipart
    @POST(API.updateHead)
    fun updateHead3(@Part files: List<MultipartBody.Part>): Observable<RespBase<Map<String, Any>>>

    /**
     * 上传头像
     */
    @Multipart
    @POST(API.updateHead)
    fun updateHead4(@Part("file\"; filename=\"avatar.jpg\"") imgs: RequestBody): Call<RespBase<Map<String, Any>>>
}