package com.cqhz.quwan.service

import com.google.gson.annotations.SerializedName

interface WechatPayContract {
    interface View{
        fun wechatPayCallBack(appId:String)
        fun callWechatPay(wechatPayResp: WechatPayResp)
    }
    interface Presenter{
        fun getWechatAppId()
        fun getWechatPayReq()
    }
    data class WechatPayResp(
            val appid:String,
            val partnerid:String,
            val prepayid:String,
            @SerializedName("package")
            val packageValue:String,
            val noncestr:String,
            val timestamp:String,
            val sign:String
    )
}