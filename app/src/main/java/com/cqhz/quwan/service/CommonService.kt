package com.cqhz.quwan.service

import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.model.SystemConfigBean
import com.cqhz.quwan.model.VersionBean
import com.cqhz.quwan.model.WindowBean
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * 公共服务
 * Created by Guojing on 2018/9/14.
 */
@Repository
interface CommonService {
    @POST(API.systemConfig)
    @FormUrlEncoded
    fun systemConfig(@Field("configKey") configKey: String): Flowable<RespBase<SystemConfigBean>>

    @POST(API.getVersionConfig)
    @FormUrlEncoded
    fun getVersionConfig(
            @Field("appKey") appKey: String = KeyContract.APP_KEY,
            @Field("source") source: Int = 1,
            @Field("channel") channel: String = "tencent"): Flowable<RespBase<VersionBean>>

    @POST(API.getWindowList)
    @FormUrlEncoded
    fun getWindowList(@Field("userId") userId: String): Flowable<RespBase<List<WindowBean>>>
}