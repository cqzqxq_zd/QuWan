package com.cqhz.quwan.service

import com.cqhz.quwan.model.*
import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


@Repository
interface OrderService {

    @POST(API.eSportOrderList)
    @FormUrlEncoded
    fun eSportOrderList(
            @Field("userId") userId: String,
            @Field("status") status: String,// 全部订单【空】 中奖订单【4】
            @Field("lotteryId") lotteryId: Long,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int
    ): Flowable<RespBase<ESportOrderResultBean>>

    @POST(API.orderPage)
    @FormUrlEncoded
    fun orderPage(
            @Field("userId") userId: String,
            @Field("status") status: String,// 全部订单【空】 待开奖订单【2】 中奖订单【4】
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int
    ): Flowable<RespBase<PageResultBean<OrderBean>>>

    @POST(API.orderAdd)
    @FormUrlEncoded
    fun postOrder(
            @FieldMap args: Map<String, String>
    ): Flowable<RespBase<OrderResp>>

    @POST(API.balancePay)
    @FormUrlEncoded
    fun payOrder4Balance(
            @Field("userId") userId: String,
            @Field("money") money: Double,
            @Field("orderId") orderId: String,
            @Field("payPassword") payPassword: String
    ): Flowable<RespBase<String>>

    @POST(API.alipay)
    @FormUrlEncoded
    fun alipay(
            @Field("userId") userId: String,
            @Field("orderId") orderId: String
    ): Flowable<RespBase<String>>


    @POST(API.getOrderInfo)
    @FormUrlEncoded
    fun getOrderInfo(
            @Field("id") orderId: String
    ): Flowable<RespBase<OrderResp>>


    @POST(API.balanceConsume)
    @FormUrlEncoded
    fun balanceConsume(
            @Field("orderId") orderId: String,
            @Field("couponId") couponId: String,
            @Field("money") money: Double,
            @Field("payPassword") payPassword: String
    ): Flowable<RespBase<String>>


    @POST(API.aliPayConsumeParams)
    @FormUrlEncoded
    fun alipay2(
            @Field("orderId") orderId: String,
            @Field("couponId") couponId: String,
            @Field("balance") balance: Double,
            @Field("money") money: Double
    ): Flowable<RespBase<String>>

    @POST(API.wechatConsumeParams)
    @FormUrlEncoded
    fun wechatPay(
            @Field("orderId") orderId: String,
            @Field("couponId") couponId: String,
            @Field("balance") balance: Double,
            @Field("money") money: Double
    ): Flowable<RespBase<WechatPayContract.WechatPayResp>>

    @POST(API.orderPay)
    @FormUrlEncoded
    fun orderPay(
            @Field("userId") userId: String,
            @Field("orderId") orderId: String,
            @Field("couponId") couponId: String?): Flowable<RespBase<OrderResp>>

    @POST(API.getCommissionList)
    @FormUrlEncoded
    fun getCommissionList(
            @Field("userId") userId: String,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int,
            @Field("type") type: String = "6"
    ): Flowable<RespBase<PageResultBean<CommissionBean>>>

    @POST(API.getCommissionInfo)
    @FormUrlEncoded
    fun getCommissionInfo(@Field("user_id") userId: String): Flowable<RespBase<CommissionInfoBean>>


    @POST(API.getFollowOrderList)
    @FormUrlEncoded
    fun getFollowOrderList(
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int,
            @Field("type") type: Int,
            @Field("followUserId") followUserId: String): Flowable<RespBase<PageResultBean<DocumentaryBean>>>


    @POST(API.getPushOrderList)
    @FormUrlEncoded
    fun getPushOrderList(
            @Field("userId") userId: String,
            @Field("type") type: Int,
            @Field("status") status: Int,
            @Field("pageNo") pageNo: Int,
            @Field("pageSize") pageSize: Int): Flowable<RespBase<PushOrderResultBean>>


    @POST(API.expertAuth)
    @FormUrlEncoded
    fun expertAuth(
            @Field("user_id") userId: Long,
            @Field("nick") nick: String,
            @Field("head_portrait") headPortrait: String,
            @Field("introduce") introduce: String): Flowable<RespBase<String>>


    @POST(API.getOrderDetail)
    @FormUrlEncoded
    fun getOrderDetail(
            @Field("id") orderId: String
    ): Flowable<RespBase<OrderInfoBean>>
}