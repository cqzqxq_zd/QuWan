package com.cqhz.quwan.service

import io.reactivex.Flowable
import me.militch.quickcore.util.RespBase
import quickcore.annotation.Repository
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@Repository
interface RechargeService {
    @POST(API.recharge4alipay)
    @FormUrlEncoded
    fun doRecharge(
            @Field("userId") userId:String,
            @Field("money") money:String
    ):Flowable<RespBase<String>>
    @POST(API.recharge4wechat)
    @FormUrlEncoded
    fun doRecharge4wechat(
            @Field("userId") userId:String,
            @Field("money") money:String
    ):Flowable<RespBase<WechatPayContract.WechatPayResp>>
}