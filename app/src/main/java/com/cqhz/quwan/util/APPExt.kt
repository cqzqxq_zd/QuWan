package com.cqhz.quwan.util

import android.app.Application
import android.content.pm.PackageManager


fun Application.getVersionName():String{
    var versionName = ""
    try {
        val packageInfo = this.packageManager.getPackageInfo(packageName, 0)
        versionName = packageInfo.versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return versionName
}

inline fun <reified T> Application.getMetaDataVersion(metaDataKey:String):T?{
    var result:T? = null
    try {
        val appInfo = this.packageManager.getApplicationInfo(packageName,
                PackageManager.GET_META_DATA)

        val value = appInfo.metaData.get(metaDataKey)
        if(value is T){
            result = value
        }
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return result
}