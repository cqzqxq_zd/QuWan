package com.cqhz.quwan.util

import android.text.Html
import android.text.Spanned
import java.text.SimpleDateFormat


fun String.md5(): String {
    return Md5Util.encryption(this)
}

fun String.dateFormat(template: String) {
    val x = SimpleDateFormat(template)
    x.parse(this)
}

fun String.htmlFontColor(color: String): String {
    return "<font color='$color'>$this</font>"
}

fun String.footBallOddText(v: String, ex: String = "<br/>", isChecked: Boolean = false, isEnabled: Boolean = true): Spanned {
    val textTemp = "${this.htmlFontColor(if (!isChecked) "#333333" else "#ffffff")}$ex${v.htmlFontColor(if (!isChecked) "#999999" else "#ffffff")}"
    val t = if (isEnabled) textTemp else "${this.htmlFontColor("#ffffff")}$ex${v.htmlFontColor("#fffffff")}"
    return Html.fromHtml(t)
}