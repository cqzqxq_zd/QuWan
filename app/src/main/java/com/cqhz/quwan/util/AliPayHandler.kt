package com.cqhz.quwan.util

import android.os.Handler
import android.os.Message
import android.text.TextUtils
import com.cqhz.quwan.model.AlipayAuthData
import com.cqhz.quwan.mvp.mine.AlipayContract


class AliPayHandler(val view: AlipayContract.View): Handler() {
    companion object {
        const val ALIPAY_AUTH_FLAG = 0x01
        const val ALIPAY_PAY_FLAG = 0X02
    }
    override fun handleMessage(msg: Message?) {
        super.handleMessage(msg)
        when (msg?.what) {
            ALIPAY_PAY_FLAG -> {
                val payResult = PayResult(msg.obj as Map<String, String>)
                /**
                 * 对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                 */
                val resultInfo = payResult.result// 同步返回需要验证的信息
                val resultStatus = payResult.resultStatus
                // 判断resultStatus 为9000则代表支付成功
                if (TextUtils.equals(resultStatus, "9000")) {
                    // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                    view.alipayCallBack()
                } else {
                    // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                    view.alipayFail(payResult)
                }
            }
            ALIPAY_AUTH_FLAG -> {
                val authResult = AuthResult(msg.obj as Map<String, String>,true)
                val resultStatus = authResult.resultStatus
                val resultCode = authResult.resultCode
                if(TextUtils.equals(                     resultStatus, "9000")&& TextUtils.equals(resultCode, "200")){
                    val alipayAuthData = AlipayAuthData(authResult.authCode,authResult.alipayOpenId,authResult.userId)
                    view.alipayAuthCallBack(alipayAuthData)
                }else{
                    view.alipayAuthFail(authResult)
                }
            }
        }
    }
}