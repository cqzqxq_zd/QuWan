package com.cqhz.quwan.util

import java.util.*

class MainEventObserver{
    companion object {
        private val obs = HashSet<MainEventSubscribe>()
        fun register(subscribe: MainEventSubscribe) {
            obs.add(subscribe)
        }
        fun remove(subscribe: MainEventSubscribe){
            obs.remove(subscribe)
        }
        fun pushDoCheck(checked:Int) {
            for (ls in obs) {
                ls.checkEvent(checked)
            }
        }
    }


}