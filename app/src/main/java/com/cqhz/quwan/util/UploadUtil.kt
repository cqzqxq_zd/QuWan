package com.cqhz.quwan.util

import android.util.Log
import com.cqhz.quwan.BuildConfig
import com.cqhz.quwan.service.API
import com.cqhz.quwan.service.UploadService
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.militch.quickcore.util.RespBase
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * 上传头像工具类
 * Created by Guojing on 2018/10/13.
 */
object UploadUtil {
    fun uploadImages(files: List<String>, callResult: CallResult<String>) {
        val partList = ArrayList<MultipartBody.Part>()
        for (i in files.indices) {
            val file = File(files[i])

            val builder = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)// 表单类型

            val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)

            builder.addFormDataPart("file", file.name, requestBody)// file 后台接收图片流的参数名

            partList.add(builder.build().part(i))
        }

//        val interceptor = InterceptorUtil.HeaderInterceptor()

        val builder = OkHttpClient.Builder()
        builder.connectTimeout(10, TimeUnit.SECONDS)
        builder.retryOnConnectionFailure(true)
//        builder.addInterceptor(interceptor)

        val gson = GsonBuilder().setLenient().create()

        val retrofit = Retrofit.Builder()
                .client(builder.build())
                .baseUrl(API.HOST)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

//        retrofit.create(UploadService::class.java).updateHead2(partList).enqueue(object : Callback<RespBase<Map<String, Any>>> {
//            override fun onResponse(call: Call<RespBase<Map<String, Any>>>, response: Response<RespBase<Map<String, Any>>>) {
//                Log.e("updateHead2", call.request().toString())
//                Log.e("updateHead2", response.body()!!.toString())
//                if (response.body()!!.data != null) {
//                    callResult.onResponse(response.body()!!.data["relativeUrl"].toString())
//                    return
//                }
//                callResult.onResponse(response.body()!!.msg)
//            }
//
//            override fun onFailure(call: Call<RespBase<Map<String, Any>>>, t: Throwable) {
//                callResult.onFailure(t.message!!)
//            }
//        })

        retrofit.create(UploadService::class.java).updateHead3(partList).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : BaseObserver<RespBase<Map<String, Any>>>() {
                    override fun onSuccess(response: RespBase<Map<String, Any>>) {
                        Log.e("updateHead3", partList[0].headers()!!.toString())
                        Log.e("updateHead3", response.toString())
                        if (response.data != null) {
                            callResult.onResponse(response.data["relativeUrl"].toString())
                            return
                        }
                        callResult.onResponse(response.msg)
                    }

                    override fun onFailure(e: Throwable, isNetWorkError: Boolean) {
                        callResult.onFailure(e.message!!)
                    }
                })

//        val file = File(files[0])
//        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
//
//        retrofit.create(UploadService::class.java).updateHead4(requestBody).enqueue(object : Callback<RespBase<Map<String, Any>>> {
//            override fun onResponse(call: Call<RespBase<Map<String, Any>>>, response: Response<RespBase<Map<String, Any>>>) {
//                Log.e("updateHead4", call.request().toString())
//                Log.e("updateHead4", response.body()!!.toString())
//                if (response.body()!!.data != null) {
//                    callResult.onResponse(response.body()!!.data["relativeUrl"].toString())
//                    return
//                }
//                callResult.onResponse(response.body()!!.msg)
//            }
//
//            override fun onFailure(call: Call<RespBase<Map<String, Any>>>, t: Throwable) {
//                callResult.onFailure(t.message!!)
//            }
//        })
    }

    interface CallResult<T> {
        fun onResponse(data: T)

        fun onFailure(data: T)
    }
}