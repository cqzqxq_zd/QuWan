package com.cqhz.quwan.util;

import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * ================================================
 * 框架要求框架中的每个 {@link } 都需要实现此类,以满足规范
 *
 * @function OkHttp3 日志拦截器
 * Created by GuoJing on 2018/11/06.
 * ================================================
 */

public class LoggerInterceptor implements Interceptor
{
    private final String  TAG = this.getClass().getSimpleName();
    private       String  tag;
    private       boolean showResponse;

    LoggerInterceptor(String tag, boolean showResponse)
    {
        if (TextUtils.isEmpty(tag))
        {
            tag = TAG;
        }
        this.showResponse = showResponse;
        this.tag = tag;
    }

    public LoggerInterceptor(String tag)
    {
        this(tag, false);
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();
        logForRequest(request);
        Response response = chain.proceed(request);
        return logForResponse(response);
    }

    private Response logForResponse(Response response)
    {
        try
        {
            Response.Builder builder = response.newBuilder();
            Response clone = builder.build();

            if (showResponse)
            {
                ResponseBody body = clone.body();
                if (body != null)
                {
                    MediaType mediaType = body.contentType();
                    if (mediaType != null)
                    {
                        if (isText(mediaType))
                        {
                            String resp = body.string();
                            LogUtils.eTag(tag, "responseBody's content : " + resp);

                            body = ResponseBody.create(mediaType, resp);
                            return response.newBuilder().body(body).build();
                        }
                        else
                        {
                            LogUtils.eTag(tag, "responseBody's content : " + " maybe [file part] , too large too print , ignored!");
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return response;
    }

    private void logForRequest(Request request)
    {
        try
        {
            String url = request.url().toString();

            LogUtils.eTag(tag, "url : " + url);
            RequestBody requestBody = request.body();
            if (requestBody != null)
            {
                MediaType mediaType = requestBody.contentType();
                if (mediaType != null)
                {
                    LogUtils.eTag(tag, "requestBody's contentType : " + mediaType.toString());
                    if (isText(mediaType))
                    {
                        LogUtils.json(tag, "requestBody's content : " + bodyToString(request));
                    }
                    else
                    {
                        LogUtils.eTag(tag, "requestBody's content : " + " maybe [file part] , too large too print , ignored!");
                    }
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private boolean isText(MediaType mediaType)
    {
        if (mediaType.type() != null && mediaType.type().equals("text"))
        {
            return true;
        }
        String subtype = mediaType.subtype();
        if (subtype != null)
        {
            if (subtype.equals("json") ||
                subtype.equals("xml") ||
                subtype.equals("html") ||
                subtype.equals("webviewhtml") ||
                subtype.equals("octet-stream"))
                return true;
        }
        return false;
    }

    private String bodyToString(final Request request)
    {
        try
        {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        }
        catch (final IOException e)
        {
            return "something error when show requestBody.";
        }
    }
}

