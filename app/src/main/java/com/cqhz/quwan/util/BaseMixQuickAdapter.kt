package com.cqhz.quwan.util

import android.support.annotation.LayoutRes
import android.util.SparseArray
import android.view.ViewGroup
import com.cqhz.quwan.model.news.MixItemEntity
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder


abstract class BaseMixQuickAdapter<T : MixItemEntity, K : BaseViewHolder>
/**
 * Same as QuickAdapter#QuickAdapter(Context,int) but with
 * some initialization data.
 *
 * @param data    A new list is created out of this one to avoid mutable list
 */
(data: List<T>) : BaseQuickAdapter<T, K>(data) {
    /**
     * layouts indexed with their types
     */
    private var layouts: SparseArray<Int>? = null

    override fun getDefItemViewType(position: Int): Int {
        val item = mData[position]
        return if (item is MixItemEntity) {
            (item as MixItemEntity).itemType
        } else DEFAULT_VIEW_TYPE
    }

    protected fun setDefaultViewTypeLayout(@LayoutRes layoutResId: Int) {
        addItemType(DEFAULT_VIEW_TYPE, layoutResId)
    }

    override fun onCreateDefViewHolder(parent: ViewGroup, viewType: Int): K {
        return createBaseViewHolder(parent, getLayoutId(viewType))
    }

    private fun getLayoutId(viewType: Int): Int {

        val tmp = layouts!!.get(viewType)

        return tmp!!
    }

    protected fun addItemType(type: Int, @LayoutRes layoutResId: Int) {
        if (layouts == null) {
            layouts = SparseArray()
        }
        layouts!!.put(type, layoutResId)
    }

    companion object {
        private val DEFAULT_VIEW_TYPE = -0xff
    }
}