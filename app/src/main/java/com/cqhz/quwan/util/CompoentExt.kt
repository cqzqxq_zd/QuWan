package com.cqhz.quwan.util

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ListView
import com.cqhz.quwan.ui.base.AbsAdapter




fun Window.lock(lock: Boolean){
    if(lock){
        this.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        this.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }else {
        this.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        this.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }
}

fun Context.hanlderPostRun(run:()->Unit){
    Handler().post(run)
}

fun <T: View>  Fragment.findView(@IdRes res: Int):Lazy<T?>{
    return lazy {
        view?.findViewById<T>(res)
    }
}
fun ListView.setBasedOnChildren(){
    val listAdapter = this.adapter ?: return
    var totalHeight = 0
    var maxWidth = 0
    for (i in 0 until listAdapter.count) {
        val listItem = listAdapter.getView(i, null, this)
        listItem.measure(0, 0)
        totalHeight += listItem.measuredHeight
        val width = listItem.measuredWidth
        if (width > maxWidth) maxWidth = width
    }
    val params = this.layoutParams
    params.height = totalHeight + this.dividerHeight * (listAdapter.count - 1)
    params.width = maxWidth
    this.layoutParams = params
}
fun <T> Fragment.getAdapter(
        layoutId:Int,
        handlerPre:((AbsAdapter.ViewHolder, Int, T?)->Unit)? = null,
        handler: ((AbsAdapter.ViewHolder, Int, T?)->Unit)?=null
):Lazy<AbsAdapter<T>>{
    return lazy {
        object : AbsAdapter<T>(context!!,layoutId){
            override fun handlerViewHolderPre(viewHolder: ViewHolder, position: Int, itemData: T?) {
                super.handlerViewHolderPre(viewHolder, position, itemData)
                handlerPre?.invoke(viewHolder,position,itemData)
            }

            override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: T?) {
                handler?.invoke(viewHolder,position,itemData)
            }
        }
    }
}
fun <T> Activity.getSimpleAdapter(
        layoutId:Int,
        handlerPre:((AbsAdapter.ViewHolder, Int, T?)->Unit)? = null,
        handler: ((AbsAdapter.ViewHolder, Int, T?)->Unit)?=null
):Lazy<AbsAdapter<T>>{
    return lazy {
        object : AbsAdapter<T>(this,layoutId){
            override fun handlerViewHolderPre(viewHolder: ViewHolder, position: Int, itemData: T?) {
                super.handlerViewHolderPre(viewHolder, position, itemData)
                handlerPre?.invoke(viewHolder,position,itemData)
            }

            override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: T?) {
                handler?.invoke(viewHolder,position,itemData)
            }
        }
    }
}