package com.cqhz.quwan.util

fun Boolean.run4true(run:()->Unit){
    if(this) run.invoke()
}
fun Boolean.run4true(run1:()->Unit,run2:()->Unit){
    if(this) run1.invoke() else run2.invoke()
}

fun Boolean.run4false(run:()->Unit){
    if(!this) run.invoke()
}
fun Boolean.run4false(run1:()->Unit,run2:()->Unit){
    if(!this) run1.invoke() else run2.invoke()
}