package com.cqhz.quwan.util

import android.app.Activity
import android.content.Context
import com.bumptech.glide.Glide
import com.yancy.gallerypick.inter.ImageLoader
import com.yancy.gallerypick.widget.GalleryImageView

class GlideImageLoader : ImageLoader
{
    private val TAG = "GlideImageLoader"

    override fun displayImage(activity: Activity, context: Context, path: String, galleryImageView: GalleryImageView, width: Int, height: Int) {
        Glide.with(context)
                .load(path)
                .into(galleryImageView)
    }

    override fun clearMemoryCache() {

    }
}