package com.cqhz.quwan.util

import com.google.gson.Gson
import java.net.URLEncoder


fun List<Int>.str(): String {
    val convert = this.map { it.slice(2) }
    return convert.toString().replace("[", "").replace("]", "")
}

fun <T> Collection<T>.strNoSlice(): String {
    return this.toString().replace("[", "").replace("]", "")
}

fun <T> Collection<T>.strCommaNoSlice(): String {
    return strNoSlice().replace(", ", ",")
}

fun List<Int>.strComma(): String {
    return str().replace(", ", ",")
}

fun List<Int>.strNoneComma(): String {
    return str().replace(", ", " ")
}

fun <T> List<T>.json(): String {
    val gson = Gson()
    return gson.toJson(this)
}

fun <T> List<List<T>>.findDuplicate(): List<T> {
    val temp = ArrayList<T>()
    for (i in indices) {
        for (ii in 0 until size - 1 - i) {
            if (this[ii].containsAll(this[ii + 1])) {
                temp.add(this[ii][0])
            }
        }
    }
    return temp
}


fun List<String>.toFootBallSelectedText(): String {
    var text = ""
    for (key in this) {
        text = "$text $key"
    }
    return text
}

fun List<Int>.sum(): Int {
    var sum = 0
    for (key in this) {
        sum += key
    }
    return sum
}

fun Map<String, String>.toSerialized(): String {
    val temp = StringBuilder()
    this.forEach {
        if (temp.isNotEmpty()) {
            temp.append("&")
        }
        temp.append("${it.key}=${it.value}")
    }
    return temp.toString()
}

fun Map<String, String>.kvEncode(): Map<String, String> {
    val temp = HashMap<String, String>()
    for (key in this.keys) {
        temp[URLEncoder.encode(key, "UTF-8")] = URLEncoder.encode(this[key], "UTF-8")
    }
    return temp
}