package com.cqhz.quwan.util

import android.content.Context
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import com.cqhz.quwan.R
import com.cqhz.quwan.common.SingletonHolderCreate


class LoadingToast private constructor(context: Context) {
    companion object : SingletonHolderCreate<LoadingToast,Context>(::LoadingToast)
    private var mToast: Toast
    private val mHandler = Handler()
    private var isHint = true
    private var mTextView: TextView
    init {
        val v = LayoutInflater.from(context).inflate(R.layout.layout_loading,null,false)
        mTextView = v.findViewById(R.id.tv_loading)
        mToast = Toast(context)
        mToast.duration = Toast.LENGTH_LONG
        mToast.setGravity(Gravity.CENTER,0,0)
        mToast.view = v
    }



    fun showing():Boolean{
        return !isHint
    }
    fun show(text:String){
        if(isHint&&text.isEmpty()){
            mTextView.text = text
            mToast.show()
            isHint = false
            looperShow()
        }

    }

    private fun looperShow(){
        if(!isHint){
            mToast.show()
            mHandler.postDelayed({
                looperShow()
            },1000)
        }
    }
    fun hint(){
        mToast.cancel()
        isHint = true
    }
}