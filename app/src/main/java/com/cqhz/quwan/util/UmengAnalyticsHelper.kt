package com.cqhz.quwan.util

import android.content.Context
import com.cqhz.quwan.APP.Companion.app
import com.cqhz.quwan.common.KeyContract
import com.umeng.analytics.MobclickAgent
import com.umeng.commonsdk.UMConfigure

/**
 * 友盟统计助手
 * Created by Guojing on 2018/9/27.
 */
object UmengAnalyticsHelper {
    var instance = UmengAnalyticsHelper()

    private fun UmengAnalyticsHelper() {}

    @Synchronized
    fun getInstance() {
        return instance
    }

    /**
     * 初始化common库
     * 参数1：上下文，不能为空
     * 参数2：【友盟+】 AppKey
     * 参数3：【友盟+】 Channel
     * 参数4：设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
     * 参数5：Push推送业务的secret
     */
    fun init(channel: String) {
        UMConfigure.init(app, KeyContract.UmengAppKey, channel, UMConfigure.DEVICE_TYPE_PHONE, null)
        UMConfigure.setLogEnabled(false)// 打开统计SDK调试模式
        UMConfigure.setEncryptEnabled(true)
        MobclickAgent.openActivityDurationTrack(false)// 手动页面统计模式
        MobclickAgent.setCatchUncaughtExceptions(true)// false-关闭错误统计功能；true-打开错误统计功能（默认打开）
    }

    /**
     * 开始，在onResume时调用
     * @param pageName 页面名称
     */
    fun onPageStart(pageName: String) {
        MobclickAgent.onPageStart(pageName)
    }

    /**
     * 结束，在onPause时调用
     * @param pageName 页面名称
     */
    fun onPageEnd(pageName: String) {
        MobclickAgent.onPageEnd(pageName)
    }

    /**
     * 开始统计时长，在onResume时调用
     * @param context 上下文
     */
    fun onResume(context: Context) {
        MobclickAgent.onResume(context)
    }

    /**
     * 结束统计时长，在onPause时调用
     * @param context 上下文
     */
    fun onPause(context: Context) {
        MobclickAgent.onPause(context)
    }

    /**
     * 事件方法1
     * @param context 上下文
     * @param eventID 事件id 可用英文或数字，不要使用中文和特殊字符且不能使用英文句号”.”您可以使用下划线”_”
     */
    fun onEvent(context: Context, eventID: String) {
        MobclickAgent.onEvent(context, eventID)
    }

    /**
     * 事件方法2
     *
     * @param context 上下文
     * @param eventID 事件id 可用英文或数字，不要使用中文和特殊字符且不能使用英文句号”.”您可以使用下划线”_”
     * @param label   事件的标签属性
     */
    fun onEvent(context: Context, eventID: String, label: String) {
        MobclickAgent.onEvent(context, eventID, label)
    }


    /**
     * 事件方法3
     *
     * @param context 上下文
     * @param eventID 事件id 可用英文或数字，不要使用中文和特殊字符且不能使用英文句号”.”您可以使用下划线”_”
     * @param map     map数据
     */
    fun onEvent(context: Context, eventID: String, map: Map<String, String>) {
        MobclickAgent.onEvent(context, eventID, map)
    }

    /**
     * 计算事件
     *
     * @param context 上下文
     * @param eventID 事件id 可用英文或数字，不要使用中文和特殊字符且不能使用英文句号”.”您可以使用下划线”_”
     * @param map     map数据
     * @param du      当前事件的数值
     */
    fun onEventValue(context: Context, eventID: String, map: Map<String, String>, du: Int) {
        MobclickAgent.onEventValue(context, eventID, map, du)
    }

    /**
     * 抛错记录方法
     *
     * @param error   错误提示
     */
    fun reportError(error: Throwable) {
        MobclickAgent.reportError(app, error)
    }

    /**
     * 抛错记录方法2
     *
     * @param error   错误提示
     */
    fun reportError(error: String) {
        MobclickAgent.reportError(app, error)
    }

    /**
     * 程序退出时，用于保存统计数据的API
     */
    fun onKillProcess() {
        MobclickAgent.onKillProcess(app)
    }
}