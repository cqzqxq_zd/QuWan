package com.cqhz.quwan.util;

import com.cqhz.quwan.model.UserInfoBean;

public interface UserInfoSubscribe {
    void userInfo(UserInfoBean bean);
}
