package com.cqhz.quwan.util;

import com.cqhz.quwan.model.UserInfoBean;

import java.util.HashSet;

public class UserInfoObserver {
    private static HashSet<UserInfoSubscribe> obs = new HashSet<>();

    public static void register(UserInfoSubscribe subscribe) {
        obs.add(subscribe);
    }

    public static void unregister(UserInfoSubscribe subscribe) {
        obs.remove(subscribe);
    }

    public static void push(UserInfoBean bean) {
        for (UserInfoSubscribe ls : obs) {
            ls.userInfo(bean);
        }
    }
}
