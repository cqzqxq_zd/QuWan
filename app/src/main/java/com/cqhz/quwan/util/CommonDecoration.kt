package com.cqhz.quwan.util

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * ================================================
 * 分割线
 *
 * @function 作用
 * Created by Guojing on 2018/10/29 6:56 PM
 * ================================================
 */
class CommonDecoration : RecyclerView.ItemDecoration {

    private var pxTop: Int = 0
    private var pxBottom: Int = 0
    private var pxLeft: Int = 0
    private var pxRight: Int = 0

    constructor(pxTop: Int, pxBottom: Int, pxLeft: Int, pxRight: Int) {
        this.pxTop = pxTop
        this.pxBottom = pxBottom
        this.pxLeft = pxLeft
        this.pxRight = pxRight
    }

    constructor(pxTop: Int) {
        this.pxTop = pxTop
    }

    constructor(pxTop: Int, pxBottom: Int) {
        this.pxTop = pxTop
        this.pxBottom = pxBottom
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(pxLeft, pxTop, pxRight, pxBottom)
    }
}