package com.cqhz.quwan.util

import android.util.Log

class CLogger constructor(clazz: Class<*>){
    private val classTag:String = clazz.simpleName
    fun i(){
        i("")
    }
    fun i(msg:String){
        val methodName = Thread.currentThread().stackTrace[2].methodName
        Log.i("$classTag=>$methodName", msg)
    }
    fun i(key:String,value:String){
        i("$key: $value")
    }
    fun i(key1:String,value1:String,key2:String,value2:String){
        i("$key1: $value1 $key2: $value2")
    }
    fun d(){
        d("")
    }
    fun d(msg:String){
        val methodName = Thread.currentThread().stackTrace[2].methodName
        Log.d("$classTag=>$methodName", msg)
    }
    fun d(key:String,value:String){
        d("$key: $value")
    }
    fun d(key1:String,value1:String,key2:String,value2:String){
        d("$key1: $value1 $key2: $value2")
    }
    fun e(){
        e("")
    }
    fun e(msg:String){
        val methods = Thread.currentThread().stackTrace
        val methodName = methods[4].methodName
        Log.e("$classTag=>$methodName", msg)
    }
    fun e(key:String,value:String){
        e("$key: $value")
    }
    fun e(key1:String,value1:String,key2:String,value2:String){
        e("$key1: $value1 $key2: $value2")
    }

}