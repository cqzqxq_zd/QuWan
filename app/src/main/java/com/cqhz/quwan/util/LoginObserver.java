package com.cqhz.quwan.util;

import com.cqhz.quwan.model.LoginBean;

import java.util.HashSet;

public class LoginObserver {
    private static HashSet<LoginSubscribe> obs = new HashSet<>();
    public static void register(LoginSubscribe subscribe){
        obs.add(subscribe);
    }
    public static void unregister(LoginSubscribe subscribe){
        obs.remove(subscribe);
    }
    public static void push(LoginBean loginBean){
        for (LoginSubscribe ls: obs){
            ls.login(loginBean);
        }
    }
}
