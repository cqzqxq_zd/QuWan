@file:Suppress("UNCHECKED_CAST")

package com.cqhz.quwan.util

import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View


fun <T: View> Fragment.find(id:Int):T{
    return view!!.findViewById(id)
}
fun <T: View> Fragment.inflate(layoutId:Int):T{
    return LayoutInflater.from(context).inflate(layoutId,null,false) as T
}
fun Fragment.lockWindow(lock: Boolean){
    this.activity?.lockWindow(lock)
}