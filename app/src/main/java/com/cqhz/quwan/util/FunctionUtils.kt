package com.cqhz.quwan.util

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.media.MediaScannerConnection
import android.support.annotation.DrawableRes
import android.support.v4.widget.NestedScrollView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.blankj.utilcode.constant.RegexConstants.REGEX_ID_CARD18
import com.blankj.utilcode.util.FileUtils.deleteFile
import com.blankj.utilcode.util.RegexUtils.isMatch
import com.blankj.utilcode.util.StringUtils
import com.blankj.utilcode.util.StringUtils.isEmpty
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.cqhz.quwan.BuildConfig.IMAGE_HOST
import java.io.File
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


/**
 * 省略显示数字
 *
 * @param number 待处理的数字字符串
 * @return StringBuffer
 */
fun String.displayNumber(): StringBuffer {
    val sb = StringBuffer(this.trim { it <= ' ' })

    when (sb.length) {
        11 -> sb.replace(3, 7, "****")
        16 -> sb.replace(0, 12, "************")
        17 -> sb.replace(0, 13, "*************")
        18 -> sb.replace(4, 14, "**********")
        19 -> sb.replace(0, 15, "***************")
    }
    return sb
}

/**
 * 加载图片公用方法
 *
 * @param context    上下文
 * @param url        图片链接
 * @param resourceId 默认图片
 */
fun ImageView.setLoadImage(context: Context, url: String?, @DrawableRes resourceId: Int?) {
    if (!StringUtils.isEmpty(url)) {
        Glide.with(context)
                .load(url!!.getImageUrl())
                .apply(RequestOptions().error(resourceId!!))
                .into(this)
    } else {
        if (resourceId != null) {
            this.setImageResource(resourceId)
        }
    }
}

/**
 * 获取图片路径的方法
 *
 * @param this        图片链接
 */
fun String.getImageUrl(): String {
    return if (this!!.startsWith("http")) this else IMAGE_HOST + this
}

/**
 * 规范化货值，保留两位小数
 * @param this 货值
 */
fun String?.formatMoney(): String {
    if (isEmpty(this)) return "0.00"
    val bg = BigDecimal(this!!.toDouble()).setScale(2, RoundingMode.HALF_UP)
    return bg.toString()
}

/**
 * 规范化货值，取整
 * @param this 货值
 */
fun String?.formatNoZero(): String {
    if (isEmpty(this)) return "0"
    val bg = BigDecimal(this!!.toDouble()).setScale(0, RoundingMode.HALF_UP)
    return bg.toString()
}

/**
 * 转换时间
 *
 * @param this 系统时间
 * @param format     要转换的格式
 * @return String
 */
fun String.formatSystemTime(format: String): String {
    val sdf1 = SimpleDateFormat(format)

    try {
        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this)
        val localeDate = Date(date.time)
        return sdf1.format(localeDate)

    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return ""
}

/**
 * 转换时间显示格式
 */
fun String.formatTime2Show(localTimestamp: String): String {
    var seconds = 0L
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var dateSystem: Date? = null
    var dateGame: Date? = null
    try {
        dateSystem = simpleDateFormat.parse(localTimestamp)
        dateGame = simpleDateFormat.parse(this)
    } catch (e: java.text.ParseException) {
        e.printStackTrace()
    }

    seconds = (dateSystem!!.time - dateGame!!.time) / 1000

    val days = (60 * 60 * 24).toLong()
    val hours = (60 * 60).toLong()
    val day = seconds / days// 天
    val hour = (seconds - days * day) / hours// 小时
    val minute = (seconds - days * day - hours * hour) / 60// 分

    if (day != 0L) {
        return Math.abs(day).toString() + "天" + if (day > 0) "前" else "后"
    }
    if (hour != 0L) {
        return Math.abs(hour).toString() + "小时" + if (hour > 0) "前" else "后"
    }
    return if (minute != 0L) {
        Math.abs(minute).toString() + "分钟" + if (minute > 0) "前" else "后"
    } else "刚刚"
}

/**
 * 转换时间
 *
 * @param this 系统时间
 * @param format     要转换的格式
 * @return String
 */
fun String.formatTime(format: String): String {
    val sdf1 = SimpleDateFormat(format)

    try {
        val date = SimpleDateFormat("yyyy-MM-dd HH:mm").parse(this)
        val localeDate = Date(date.time)
        return sdf1.format(localeDate)

    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return ""
}

/**
 * 将Double四舍五入转换成Int
 */
fun Double.getInt(): Int {
    var bd: BigDecimal = BigDecimal(this).setScale(0, BigDecimal.ROUND_HALF_UP)
    return Integer.parseInt(bd.toString())
}

/**
 * 将String四舍五入转换成Int
 */
fun String.getInt(): Int {
    var bd: BigDecimal = BigDecimal(this).setScale(0, BigDecimal.ROUND_HALF_UP)
    return Integer.parseInt(bd.toString())
}

/**
 * 将String四舍五入转换成Int
 */
fun String.getFloat(): Float {
    var bd: BigDecimal = BigDecimal(this).setScale(0, BigDecimal.ROUND_UP)
    return bd.toFloat()
}

/**
 * 获取屏幕的宽度
 */
fun Activity.getScreenWidth(): Int {
    val dm = DisplayMetrics()
    this.windowManager.defaultDisplay.getMetrics(dm)
    return dm.widthPixels
}

/**
 * 获取屏幕的高度
 */
fun Activity.getScreenHeight(): Int {
    val dm = DisplayMetrics()
    this.windowManager.defaultDisplay.getMetrics(dm)
    return dm.heightPixels
}

/**
 * 扫描图片
 *
 * @param context 上下文
 * @param path    文件的绝对路径
 */
fun Context.scanPhoto(path: String) {
    MediaScannerConnection.scanFile(this, arrayOf(path), arrayOf("image/jpeg")) { path1, uri ->

    }
}

/**
 * 验证身份证号码18位
 *
 * @param input 待验证文本
 * @return `true`: 匹配<br></br>`false`: 不匹配
 */
fun CharSequence.isIDCard18(): Boolean {
    // 对身份证号进行简单判断
    if (!isMatch(REGEX_ID_CARD18, this)) {
        return false
    }
    // 1-17位相乘因子数组
    val factor = intArrayOf(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2)
    // 18位随机码数组
    val random = "10X98765432".toCharArray()
    // 计算1-17位与相应因子乘积之和
    var total = 0
    for (i in 0..16) {
        total += Character.getNumericValue(this[i]) * factor[i]
    }
    // 判断随机码是否相等
    return random[total % 11] == this[17]
}


/**
 * 删除文件
 *
 * @param File 要删除的文件
 */
fun File.deleteFile() {
    if (this.exists()) {// 判断文件是否存在
        if (this.isFile) {// 判断是否是文件
            this.delete()// delete()方法 你应该知道 是删除的意思;
        } else if (this.isDirectory) {// 否则如果它是一个目录
            val files = this.listFiles()// 声明目录下所有的文件 files[]
            for (i in files.indices) {// 遍历目录下所有的文件
                deleteFile(files[i])// 把每个文件 用这个方法进行迭代
            }
        }
        this.delete()
    }
}

/**
 * 获取版本号
 *
 * @param this 上下文
 * @return 当前应用的版本号
 */
fun Context.getVersion(): String {
    return try {
        val manager = this.packageManager
        val info = manager.getPackageInfo(this.packageName, 0)
        info.versionName
    } catch (e: Exception) {
        e.printStackTrace()
        "1.0.0"
    }
}

/**
 * 判断字符串中是否包含中文
 * @param str
 * 待校验字符串
 * @return 是否为中文
 * @warn 不能校验是否为中文标点符号
 */
fun String.isContainChinese(): Boolean {
    val p = Pattern.compile("[\u4e00-\u9fa5]")
    val m = p.matcher(this)
    return m.find()
}

/**
 * 获取控件位置文字的值
 * @param this 控件的绝对位置
 * @param isBF 是否为比分布局
 *
 * 胜平负(0,1,2)
 * 让球胜平负(3,4,5)
 * 半全场(6,7,8,9,10,11,12,13,14)
 * 比分-胜(15,16,17,18,19,20,21,22,23,24,25,26,27)
 * 比分-平(28,29,30,31,32)
 * 比分-负(33,34,35,36,37,38,39,40,41,42,43,44,45)
 * 总进球数(46,47,48,49,50,51,52,53)
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckBoxTextValue2(): String {
    return when (this) {
        0 -> "胜"
        1 -> "平"
        2 -> "负"
        3 -> "让球胜"
        4 -> "让球平"
        5 -> "让球负"
        6 -> "胜胜"
        7 -> "胜平"
        8 -> "胜负"
        9 -> "平胜"
        10 -> "平平"
        11 -> "平负"
        12 -> "负胜"
        13 -> "负平"
        14 -> "负负"
        15 -> "1:0"
        16 -> "2:0"
        17 -> "2:1"
        18 -> "3:0"
        19 -> "3:1"
        20 -> "3:2"
        21 -> "4:0"
        22 -> "4:1"
        23 -> "4:2"
        24 -> "5:0"
        25 -> "5:1"
        26 -> "5:2"
        27 -> "胜其他"
        28 -> "0:0"
        29 -> "1:1"
        30 -> "2:2"
        31 -> "3:3"
        32 -> "平其他"
        33 -> "0:1"
        34 -> "0:2"
        35 -> "1:2"
        36 -> "0:3"
        37 -> "1:3"
        38 -> "2:3"
        39 -> "0:4"
        40 -> "1:4"
        41 -> "2:4"
        42 -> "0:5"
        43 -> "1:5"
        44 -> "2:5"
        45 -> "负其他"
        46 -> "0"
        47 -> "1"
        48 -> "2"
        49 -> "3"
        50 -> "4"
        51 -> "5"
        52 -> "6"
        53 -> "7+"
        else -> "None"
    }
}

/**
 * 获取控件位置文字的值
 * @param this 控件的绝对位置
 * @param isBF 是否为比分布局
 *
 * 胜平负(0,1,2)
 * 让球胜平负(3,4,5)
 * 半全场(6,7,8,9,10,11,12,13,14)
 * 比分-胜(15,16,17,18,19,20,21,22,23,24,25,26,27)
 * 比分-平(28,29,30,31,32)
 * 比分-负(33,34,35,36,37,38,39,40,41,42,43,44,45)
 * 总进球数(46,47,48,49,50,51,52,53)
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckBoxTextValue(isBF: Boolean): String {
    var position = this.getMapKeyByPosition(isBF)
    return when (position) {
        0 -> "胜"
        1 -> "平"
        2 -> "负"
        3 -> "胜"
        4 -> "平"
        5 -> "负"
        6 -> "胜胜"
        7 -> "胜平"
        8 -> "胜负"
        9 -> "平胜"
        10 -> "平平"
        11 -> "平负"
        12 -> "负胜"
        13 -> "负平"
        14 -> "负负"
        15 -> "1:0"
        16 -> "2:0"
        17 -> "2:1"
        18 -> "3:0"
        19 -> "3:1"
        20 -> "3:2"
        21 -> "4:0"
        22 -> "4:1"
        23 -> "4:2"
        24 -> "5:0"
        25 -> "5:1"
        26 -> "5:2"
        27 -> "胜其他"
        28 -> "0:0"
        29 -> "1:1"
        30 -> "2:2"
        31 -> "3:3"
        32 -> "平其他"
        33 -> "0:1"
        34 -> "0:2"
        35 -> "1:2"
        36 -> "0:3"
        37 -> "1:3"
        38 -> "2:3"
        39 -> "0:4"
        40 -> "1:4"
        41 -> "2:4"
        42 -> "0:5"
        43 -> "1:5"
        44 -> "2:5"
        45 -> "负其他"
        46 -> "0"
        47 -> "1"
        48 -> "2"
        49 -> "3"
        50 -> "4"
        51 -> "5"
        52 -> "6"
        53 -> "7+"
        else -> "None"
    }
}

/**
 * 获取赔率map的key值
 *
 * 胜平负(0,1,2)
 * 让球胜平负(3,4,5)
 * 半全场(6,7,8,9,10,11,12,13,14)
 * 比分-胜(15,16,17,18,19,20,21,22,23,24,25,26,27)
 * 比分-平(28,29,30,31,32)
 * 比分-负(33,34,35,36,37,38,39,40,41,42,43,44,45)
 * 总进球数(46,47,48,49,50,51,52,53)
 *
 * @param this 控件的绝对位置
 * @param isBF 是否为比分布局
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getMapKeyByPosition(isBF: Boolean): Int {
    if (isBF) {
        return this + 15
    }
    return when (this) {
        in 0..5 -> this// 胜平负和让球胜平负
        in 6..36 -> this + 9// 比分
        in 37..44 -> this + 9// 总进球数
        in 45..53 -> this - 39// 半全场
        else -> 0
    }
}

/**
 * 拼接CheckBox控件显示的文本值(一行显示)
 * @param this 控件的绝对位置
 * @param isBF 是否为比分布局
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckBoxText(oddsMap: Map<Int, String>, isBF: Boolean): String {
    return this.getCheckBoxTextValue(isBF) + " " + oddsMap[this.getMapKeyByPosition(isBF)]
}

/**
 * 拼接CheckBox控件显示的文本值（多行显示）
 * @param this      控件的绝对位置
 * @param context   上下文
 * @param oddsMap   数据
 * @param isChecked 是否选中
 * @param size1     文本1大小
 * @param size2     文本2大小
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckedStyleText(context: Context, isBF: Boolean, oddsMap: Map<Int, String>, isChecked: Boolean, size1: Int, size2: Int): SpannableStringBuilder {
    val str1 = this.getCheckBoxTextValue(isBF) + "\n"
    val str2 = oddsMap[this.getMapKeyByPosition(isBF)]

    val style = SpannableStringBuilder(str1 + str2)
    if (!isChecked) {
        style.setSpan(ForegroundColorSpan(Color.parseColor("#333333")), 0, str1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        style.setSpan(ForegroundColorSpan(Color.parseColor("#666666")), str1.length, str1.length + str2!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    } else {
        style.setSpan(ForegroundColorSpan(Color.parseColor("#ffffff")), 0, str1.length + str2!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    style.setSpan(AbsoluteSizeSpan(sp2px(context, size1)), 0, str1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    style.setSpan(AbsoluteSizeSpan(sp2px(context, size2)), str1.length, str1.length + str2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    return style
}

/**
 * 将sp值转换为px值，保证文字大小不变
 *
 * @param context 上下文
 * @param spValue （DisplayMetrics类中属性scaledDensity）
 * @return 转化后的像素值
 */
fun sp2px(context: Context, spValue: Int): Int {
    val fontScale = context.resources.displayMetrics.scaledDensity
    return (spValue * fontScale + 0.5f).toInt()
}

/**
 * 获取屏幕高度(px)
 */
fun getScreenHeight(context: Context): Int {
    return context.resources.displayMetrics.heightPixels
}

/**
 * 获取屏幕宽度(px)
 */
fun getScreenWidth(context: Context): Int {
    return context.resources.displayMetrics.widthPixels
}

/**
 * 计算出来的位置，y方向就在anchorView的上面和下面对齐显示，x方向就是与屏幕右边对齐显示
 * 如果anchorView的位置有变化，就可以适当自己额外加入偏移来修正
 *
 * @param anchorView  呼出window的view
 * @param contentView window的内容布局
 * @return window显示的左上角的xOff, yOff坐标
 */
fun calculatePopWindowPos(anchorView: View, contentView: View): IntArray {
    val windowPos = IntArray(2)
    val anchorLoc = IntArray(2)
    // 获取锚点View在屏幕上的左上角坐标位置
    anchorView.getLocationOnScreen(anchorLoc)
    val anchorHeight = anchorView.height
    // 获取屏幕的高宽
    val screenHeight = getScreenHeight(anchorView.context)
    val screenWidth = getScreenWidth(anchorView.context)
    contentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
    // 计算contentView的高宽
    val windowHeight = contentView.measuredHeight
    val windowWidth = contentView.measuredWidth
    // 判断需要向上弹出还是向下弹出显示
    val isNeedShowUp = screenHeight - anchorLoc[1] - anchorHeight < windowHeight
    if (isNeedShowUp) {
        windowPos[0] = screenWidth - windowWidth
        windowPos[1] = anchorLoc[1] - windowHeight
    } else {
        windowPos[0] = screenWidth - windowWidth
        windowPos[1] = anchorLoc[1] + anchorHeight
    }
    return windowPos
}

/**
 * 截取NestedScrollView的屏幕
 */
fun getNestedScrollViewBitmap(scrollView: NestedScrollView): Bitmap {
    var h = 0
    val bitmap: Bitmap
    for (i in 0 until scrollView.childCount) {
        h += scrollView.getChildAt(i).height
    }
    // 创建对应大小的bitmap
    bitmap = Bitmap.createBitmap(scrollView.width, h,
            Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    scrollView.draw(canvas)
    return bitmap
}

/**
 * 提供精确的乘法运算。
 *
 * @param v1
 * @param v2
 * @return 两个参数的积
 */
fun multiply(v1: Double, v2: Double): Double {
    val b1 = BigDecimal(java.lang.Double.toString(v1))
    val b2 = BigDecimal(java.lang.Double.toString(v2))
    return b1.multiply(b2).setScale(2, RoundingMode.HALF_UP).toDouble()
}

/**
 * 设置边距
 */
fun View.setMargins(l: Int, t: Int, r: Int, b: Int) {
    if (this.layoutParams is ViewGroup.MarginLayoutParams) {
        val p = this.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(l, t, r, b)
        this.requestLayout()
    }
}

/**
 * 处理文字颜色
 *
 * @param str1     整段字符串
 * @param str2     突出的字符串
 * @return SpannableStringBuilder
 */
fun displayTextColor(str1: String, str2: String): SpannableStringBuilder {
    val style = SpannableStringBuilder(str1)
    style.setSpan(ForegroundColorSpan(Color.parseColor("#333333")), 0, str1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    style.setSpan(ForegroundColorSpan(Color.parseColor("#D60000")), str1.indexOf(str2), str1.indexOf(str2) + str2.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
    return style
}

/**
 * 处理文字颜色
 *
 * @param str1     整段字符串
 * @param str2     突出的字符串1
 * @param str3     突出的字符串2
 * @return SpannableStringBuilder
 */
fun displayTextColor(str1: String, str2: String, str3: String): SpannableStringBuilder {
    val style = SpannableStringBuilder(str1)
    style.setSpan(ForegroundColorSpan(Color.parseColor("#333333")), 0, str1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    style.setSpan(ForegroundColorSpan(Color.parseColor("#D60000")), str1.indexOf(str2), str1.indexOf(str2) + str2.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
    style.setSpan(ForegroundColorSpan(Color.parseColor("#D60000")), str1.indexOf(str3), str1.indexOf(str3) + str3.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
    return style
}

/**
 * 拼接CheckBox控件显示的文本值(一行显示)
 * @param this 控件的绝对位置
 * @param isSFC 是否为胜分差布局
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckBoxText2Show(oddsMap: Map<Int, String>): String {
    return this.getCheckBoxTextValueShow() + " " + oddsMap[this]
}

/**
 * 获取控件位置文字的值
 * @param this 控件的绝对位置
 * 0：主负
 * 1：主胜
 * 2：让分主负
 * 3：让分主胜
 * 4：大分
 * 5：小分
 * 6：1-5
 * 7：6-10
 * 8：11-15
 * 9：16-20
 * 10：21-25
 * 11：26+
 * 12：1-5
 * 13：6-10
 * 14：11-15
 * 15：16-20
 * 16：21-25
 * 17：26+
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckBoxTextValueLabel(): String {
    return when (this) {
        0 -> "主负"
        1 -> "主胜"
        2 -> "让分主负"
        3 -> "让分主胜"
        4 -> "大分"
        5 -> "小分"
        6 -> "主负 (1-5)"
        7 -> "主负 (6-10)"
        8 -> "主负 (11-15)"
        9 -> "主负 (16-20)"
        10 -> "主负 (21-25)"
        11 -> "主负 (26+)"
        12 -> "主胜 (1-5)"
        13 -> "主胜 (6-10)"
        14 -> "主胜 (11-15)"
        15 -> "主胜 (16-20)"
        16 -> "主胜 (21-25)"
        17 -> "主胜 (26+)"
        else -> "None"
    }
}

/**
 * 获取控件位置文字的值
 * @param this 控件的绝对位置
 * 0：主负
 * 1：主胜
 * 2：让分主负
 * 3：让分主胜
 * 4：大分
 * 5：小分
 * 6：1-5
 * 7：6-10
 * 8：11-15
 * 9：16-20
 * 10：21-25
 * 11：26+
 * 12：1-5
 * 13：6-10
 * 14：11-15
 * 15：16-20
 * 16：21-25
 * 17：26+
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckBoxTextValueShow(): String {
    return when (this) {
        0 -> "主负"
        1 -> "主胜"
        2 -> "主负"
        3 -> "主胜"
        4 -> "大分"
        5 -> "小分"
        6 -> "主负 (1-5)"
        7 -> "主负 (6-10)"
        8 -> "主负 (11-15)"
        9 -> "主负 (16-20)"
        10 -> "主负 (21-25)"
        11 -> "主负 (26+)"
        12 -> "主胜 (1-5)"
        13 -> "主胜 (6-10)"
        14 -> "主胜 (11-15)"
        15 -> "主胜 (16-20)"
        16 -> "主胜 (21-25)"
        17 -> "主胜 (26+)"
        else -> "None"
    }
}

/**
 * 拼接CheckBox控件显示的文本值（多行显示）
 * @param this      控件的绝对位置
 * @param context   上下文
 * @param isSFC 是否为胜分差布局
 * @param oddsMap   数据
 * @param isChecked 是否选中
 * @param size1     文本1大小
 * @param size2     文本2大小
 */
@Suppress("UNREACHABLE_CODE")
fun Int.getCheckedStyleText2(context: Context, oddsMap: Map<Int, String>, isChecked: Boolean): SpannableStringBuilder {
    var isSFC = when (this) {
        in 6..17 -> true
        else -> false
    }
    val str1 = this.getCheckBoxTextValueShow() + (if (isSFC) "\n" else "")
    val str2 = oddsMap[this]

    val style = SpannableStringBuilder(str1 + str2)
    if (!isChecked) {
        if (isSFC) {
            style.setSpan(ForegroundColorSpan(Color.parseColor("#333333")), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            style.setSpan(ForegroundColorSpan(Color.parseColor("#999999")), 2, str1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            style.setSpan(ForegroundColorSpan(Color.parseColor("#666666")), str1.length, str1.length + str2!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        } else {
            style.setSpan(ForegroundColorSpan(Color.parseColor("#333333")), 0, str1.length + str2!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    } else {
        style.setSpan(ForegroundColorSpan(Color.parseColor("#ffffff")), 0, str1.length + str2!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    style.setSpan(AbsoluteSizeSpan(sp2px(context, 13)), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    style.setSpan(AbsoluteSizeSpan(sp2px(context, 12)), 2, str1.length + str2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    return style
}
