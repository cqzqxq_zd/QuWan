package com.cqhz.quwan.util;

import com.cqhz.quwan.model.UserInfoBean;

import java.util.HashSet;

public class PayEventObserver {
    private static HashSet<PayEventSubscribe> obs = new HashSet<>();
    public static void register(PayEventSubscribe subscribe){
        obs.add(subscribe);
    }
    public static void unregister(PayEventSubscribe subscribe){
        obs.remove(subscribe);
    }
    public static void pushPayFinish(){
        for (PayEventSubscribe ls: obs){
            ls.doPayFinish();
        }
    }
}
