package com.cqhz.quwan.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.os.CountDownTimer
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.blankj.utilcode.util.SizeUtils.dp2px
import com.blankj.utilcode.util.StringUtils
import com.blankj.utilcode.util.StringUtils.isEmpty
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.cqhz.quwan.R
import com.cqhz.quwan.model.*
import com.cqhz.quwan.ui.interfaces.SelectBallChangeListener
import com.cqhz.quwan.ui.interfaces.SelectSeriesChangeListener
import com.cqhz.quwan.ui.interfaces.SelectTeamItemClickListener
import com.cqhz.quwan.ui.widget.CircleImageView
import com.cqhz.quwan.ui.widget.GridSpacingItemDecoration
import java.util.*


/**
 * 通用适配器助手
 * Created by Guojing on 2018/9/17.
 */
object CommonAdapterHelper {

    /**
     * 获取商城商品列表数据适配器
     *
     * @param activity 当前活动
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getProductListAdapter(activity: Activity, data: List<ProductBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<ProductBean, BaseViewHolder>(R.layout.item_mall_product, data) {
            override fun convert(helper: BaseViewHolder, item: ProductBean) {
                if (item != null) {
                    (helper.getView(R.id.iv_product) as ImageView).setLoadImage(activity, item.imgUrl, R.drawable.mall_img_default)
                    helper.setText(R.id.tv_product_name, item.model)
                    var tvProductPrice = helper.getView(R.id.tv_product_price) as TextView
                    tvProductPrice.text = "参考价：¥" + item.price.toString().formatMoney()
                    tvProductPrice.paint.isAntiAlias = true// 抗锯齿
                    tvProductPrice.paint.flags = Paint.STRIKE_THRU_TEXT_FLAG or Paint.ANTI_ALIAS_FLAG// 设置中划线并加清晰
                    helper.setText(R.id.tv_price_diamonds, item.exchangeRate.toString().formatMoney() + "钻石")
                }
            }
        }
    }

    /**
     * 获取钻石记录列表数据适配器
     *
     * @param activity 当前活动
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getDiamondsRecordListAdapter(activity: Activity, data: List<TransactionRecordBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<TransactionRecordBean, BaseViewHolder>(R.layout.item_diamonds_record, data) {
            override fun convert(helper: BaseViewHolder, item: TransactionRecordBean) {
                if (item != null) {
                    helper.setText(R.id.tv_time, item.createTime)
                    helper.setText(R.id.tv_pay_amount, (if (item.type == "9" || item.type == "10") "-" else "+")
                    + item.payAmount.formatMoney() ?: "0.00")
                    helper.setTextColor(R.id.tv_pay_amount, when (item.type) {
                        "9", "11" -> 0XFF3333330.toInt()
                        else -> 0XFFd60000.toInt()
                    })


                    helper.setText(R.id.tv_type, when (item.type) {
                        "1" -> "充值"
                        "2" -> "提现"
                        "3" -> "奖金"
                        "4" -> "竞猜"
                        "5" -> "退款"
                        "6" -> "佣金"
                        "7" -> "赠送"
                        "8" -> "换钻"
                        "9" -> "换物"
                        "10" -> "抽奖"
                        "11" -> "兑换"
                        else -> "其他"
                    })
                    helper.setGone(R.id.line, helper.adapterPosition < data.size - 1)
                }
            }
        }
    }

    /**
     * 获取我的兑换列表数据适配器
     *
     * @param activity 当前活动
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getExchangeListAdapter(activity: Activity, data: List<ExchangeOrderBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<ExchangeOrderBean, BaseViewHolder>(R.layout.item_my_exchange, data) {
            override fun convert(helper: BaseViewHolder, item: ExchangeOrderBean) {
                if (item != null) {
                    helper.setText(R.id.tv_time, item.createTime)
                    helper.setText(R.id.tv_status, when (item.tradeType) {
                        "1" -> "等待发货"
                        "2" -> "已完成"
                        "3" -> "已取消"
                        else -> {
                            "其他状态"
                        }
                    })
                    (helper.getView(R.id.iv_product) as ImageView).setLoadImage(activity, item.productImgUrl, R.drawable.mall_img_default)
                    helper.setGone(R.id.iv_status, item.tradeType == "2" || (item.tradeType == "1" && !isEmpty(item.logisticsNum)))// 1：兑换实物 2：代卖
                    helper.setGone(R.id.rl_express, item.tradeType == "1" && !isEmpty(item.logisticsNum))// 1：兑换实物 2：代卖
                    helper.setText(R.id.tv_product_name, item.productVersion)
                    helper.setText(R.id.tv_product_number, "数量" + item.num + "个")
                    helper.setText(R.id.tv_exchange_type, if (item.tradeType == "1") "实物兑换" else "代卖换钱")
                    helper.setText(R.id.tv_diamonds, item.price.formatMoney() + "钻石")
                    helper.setText(R.id.tv_express_number, "物流单号：" + item.logisticsNum)
                    helper.setText(R.id.tv_express_name, "物流公司：" + item.logisticsCompany)
                    helper.setGone(R.id.tv_discount_price, item.tradeType == "2")// 1：兑换实物 2：代卖
                    helper.setText(R.id.tv_discount_price, "折扣价：" + item.exchangePrice.formatMoney() + "元")
                    helper.setGone(R.id.line, helper.adapterPosition < data.size - 1)
                }
            }
        }
    }

    /**
     * 获取我的竞猜订单列表数据适配器
     *
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getOrderListAdapter(data: List<OrderBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<OrderBean, BaseViewHolder>(R.layout.item_order_list, data) {
            override fun convert(helper: BaseViewHolder, item: OrderBean) {
                if (item != null) {
                    helper.setText(R.id.tv_title, item.lotteryName)
                    helper.setText(R.id.tv_amount, item.payAmount.formatNoZero() + "趣豆")
                    helper.setText(R.id.tv_status, when {
                        item.status.toInt() == 0 -> "待下单"
                        item.status.toInt() == 1 -> "待下单"
                        item.status.toInt() == 2 -> "待开奖"
                        item.status.toInt() == 3 -> "未中奖"
                        item.status.toInt() == 4 -> item.prize.formatMoney()// 已中奖
                        item.status.toInt() == 5 -> "待领奖"// 待领奖
                        item.status.toInt() == 6 -> item.prize.formatMoney()// 已领奖
                        item.status.toInt() == 7 -> "取消"
                        item.status.toInt() == 8 -> "支付中"
                        item.status.toInt() == 9 -> "已下单"
                        else -> "其他"
                    })
                    helper.setImageResource(R.id.iv_follow, when {
                        item.followStatus == "1" -> R.drawable.order_list_gen
                        item.followStatus == "2" -> R.drawable.order_list_tui
                        else -> 0
                    })
                    helper.setTextColor(R.id.tv_status, when (item.status) {
                        "4", "5", "6" -> 0XFFd60000.toInt()
                        else -> 0XFF333333.toInt()
                    })

                    helper.setText(R.id.tv_time, item.createTime)
                    helper.setGone(R.id.iv_draw, item.status.toInt() == 4 || item.status.toInt() == 6)
                }
            }
        }
    }

    /**
     * 获取我的预测列表数据适配器
     *
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getGuessListAdapter(data: List<ESportOrderItemBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<ESportOrderItemBean, BaseViewHolder>(R.layout.item_guess_list, data) {
            override fun convert(helper: BaseViewHolder, item: ESportOrderItemBean) {
                if (item != null) {
                    helper.setText(R.id.tv_title, item.djPlayDescribe + "（" + item.djSelectTeam + "）")
                    helper.setText(R.id.tv_amount, item.payAmount.formatNoZero() + "趣豆")
                    helper.setText(R.id.tv_status, when {
                        item.status.toInt() == 0 -> "待下单"
                        item.status.toInt() == 1 -> "待下单"
                        item.status.toInt() == 2 -> "待开奖"
                        item.status.toInt() == 3 -> "未中奖"
                        item.status.toInt() == 4 -> item.prize.formatMoney()// 已中奖
                        item.status.toInt() == 5 -> "待领奖"// 待领奖
                        item.status.toInt() == 6 -> item.prize.formatMoney()// 已领奖
                        item.status.toInt() == 7 -> "取消"
                        item.status.toInt() == 8 -> "支付中"
                        item.status.toInt() == 9 -> "已下单"
                        else -> "其他"
                    })
                    helper.setTextColor(R.id.tv_status, when (item.status) {
                        "4", "5", "6" -> 0XFFd60000.toInt()
                        else -> 0XFF333333.toInt()
                    })

                    helper.setText(R.id.tv_game_name, item.djMatchName)
                    helper.setGone(R.id.iv_draw, item.status.toInt() == 4 || item.status.toInt() == 6)
                    helper.setGone(R.id.line, helper.adapterPosition < data.size - 1)
                }
            }
        }
    }

    /**
     * 获取电竞详情预测列表数据适配器
     *
     * @param context  上下文
     * @param data     数据列表
     * @param listener 点击监听事件
     * @return adapter 返回的适配器
     */
    fun getESportDetailGuessListAdapter(context: Context, data: ArrayList<ESportItemBean>, listener: SelectTeamItemClickListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<ESportItemBean, BaseViewHolder>(R.layout.item_esport_detail_guess, data) {
            override fun convert(helper: BaseViewHolder, item: ESportItemBean) {
                if (item != null) {
                    helper.setText(R.id.tv_title, item.playDescribe)
                    helper.setText(R.id.tv_guest_number, "当前预测人数：" + item.betNum + "人")
                    helper.setText(R.id.tv_end_time, if (item.closeTime != null) item.closeTime!!.formatSystemTime("MM月dd日 HH:mm") + " 截止" else "")

                    var list = ArrayList<ESportItemSelectBean>()
                    for (i in 1..item.playOddsMap!!.size) {
                        list.add(ESportItemSelectBean(i.toString(), item.playOddsMap!![i.toString()]!!["partOdds"]!!, item.playOddsMap!![i.toString()]!!["partName"]!!))
                    }

                    var spanCount = if (list.size == 2 || list.size == 4) 2 else 3
                    val spanSpace = dp2px(14f)
                    var rvContent = helper.getView<RecyclerView>(R.id.rv_content)
                    rvContent.layoutManager = GridLayoutManager(context, spanCount)
                    // 需要在setLayoutManager()之后调用addItemDecoration()
                    if (rvContent.itemDecorationCount > 0) {
                        rvContent.removeItemDecorationAt(0)
                    }
                    rvContent.addItemDecoration(GridSpacingItemDecoration(spanCount, spanSpace, false))
                    rvContent.adapter = getESportDetailSelectListAdapter(list, item)
                    rvContent.addOnItemTouchListener(object : com.chad.library.adapter.base.listener.OnItemClickListener() {
                        override fun onSimpleItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
                            val itemSelectBean = adapter.getItem(position) as ESportItemSelectBean
                            if (item.betStatus == "0") {
                                listener?.onTeamClick(item, itemSelectBean)
                            }
                        }
                    })
                }
            }
        }
    }

    /**
     * 获取电竞详情预测选择列表数据适配器
     *
     * @param data     数据列表
     * @param listener 点击监听事件
     * @return adapter 返回的适配器
     */
    fun getESportDetailSelectListAdapter(data: ArrayList<ESportItemSelectBean>, bean: ESportItemBean): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<ESportItemSelectBean, BaseViewHolder>(R.layout.item_esport_detail_select, data) {
            override fun convert(helper: BaseViewHolder, item: ESportItemSelectBean) {
                if (item != null) {
                    helper.setText(R.id.tv_name, item.partName)
                    helper.setText(R.id.tv_odds, item.partOdds)

                    if ((data.size == 2 || data.size == 4) && (helper.adapterPosition + 1 == 2 || helper.adapterPosition + 1 == 4)) {
                        helper.setGone(R.id.iv_victory_right, bean.result == (helper.adapterPosition + 1).toString())
                    } else {
                        helper.setGone(R.id.iv_victory_left, bean.result == (helper.adapterPosition + 1).toString())
                    }

                    var maxPosition = 666
                    var maxVaue = 0.00
                    for (i in 0 until data.size) {
                        if (maxVaue < data[i].partOdds.toDouble()) {
                            maxVaue = data[i].partOdds.toDouble()
                            maxPosition = i
                        }
                    }

                    helper.setTextColor(R.id.tv_odds, if (maxPosition == helper.adapterPosition) Color.parseColor("#FF513A") else Color.parseColor("#5BB82A"))

                    var actionSelect = helper.getView(R.id.action_select) as RelativeLayout
                    actionSelect.setBackgroundResource(if (bean.betStatus == "0") R.drawable.esports_betting_default else R.drawable.sp_gray_f5_x2)
                    actionSelect.isEnabled = bean.betStatus == "0"
                }
            }
        }
    }

    /**
     * 获取电竞详情数据列表数据适配器
     *
     * @param activity 当前活动
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getESportDetailDataListAdapter(activity: Activity, data: List<OrderBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<OrderBean, BaseViewHolder>(R.layout.item_esport_detail_data, data) {
            override fun convert(helper: BaseViewHolder, item: OrderBean) {
                if (item != null) {
                    helper.setGone(R.id.iv_host_victory, helper.adapterPosition % 2 == 0)
                    helper.setGone(R.id.iv_guest_victory, helper.adapterPosition % 2 != 0)
                    helper.setGone(R.id.iv_host_yx, helper.adapterPosition % 2 == 0)
                    helper.setGone(R.id.iv_host_kill, helper.adapterPosition % 2 == 0)
                    helper.setGone(R.id.iv_guest_yx, helper.adapterPosition % 2 != 0)
                    helper.setGone(R.id.iv_guest_kill, helper.adapterPosition % 2 != 0)
                    helper.setText(R.id.tv_host_game, "RNG")
                    helper.setText(R.id.tv_guest_game, "G2")
                    helper.setText(R.id.tv_host_score, "8")
                    helper.setText(R.id.tv_guest_score, "5")
                    helper.setText(R.id.tv_game_times, "第" + helper.adapterPosition + "场")
                    helper.setText(R.id.tv_time1, " 3 ")
                    helper.setText(R.id.tv_time2, " 3 ")
                    helper.setText(R.id.tv_time3, ":")
                    helper.setText(R.id.tv_time4, " 3 ")
                    helper.setText(R.id.tv_time5, " 3 ")
                    helper.setTextColor(R.id.tv_time1, if (true) Color.parseColor("#FFFFFF") else Color.parseColor("#666666"))
                    helper.setTextColor(R.id.tv_time2, if (true) Color.parseColor("#FFFFFF") else Color.parseColor("#666666"))
                    helper.setTextColor(R.id.tv_time3, if (true) Color.parseColor("#FF513A") else Color.parseColor("#666666"))
                    helper.setTextColor(R.id.tv_time4, if (true) Color.parseColor("#FFFFFF") else Color.parseColor("#666666"))
                    helper.setTextColor(R.id.tv_time5, if (true) Color.parseColor("#FFFFFF") else Color.parseColor("#666666"))
                    (helper.getView(R.id.iv_host_hero1) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_host_hero2) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_host_hero3) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_host_hero4) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_host_hero5) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_guest_hero1) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_guest_hero2) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_guest_hero3) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_guest_hero4) as ImageView).setLoadImage(activity, "", 0)
                    (helper.getView(R.id.iv_guest_hero5) as ImageView).setLoadImage(activity, "", 0)
                    helper.setText(R.id.tv_game_status, "比赛中")
                }
            }
        }
    }


    /**
     * 获取跟单订单列表数据适配器
     *
     * @param context  上下文
     * @param data     数据列表
     * @param listener 点击监听事件
     * @return adapter 返回的适配器
     */
    fun getDocumentaryOrderListAdapter(context: Context, data: ArrayList<DocumentaryBean>, type: Int): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<DocumentaryBean, BaseViewHolder>(R.layout.item_documentary_order, data) {
            override fun convert(helper: BaseViewHolder, item: DocumentaryBean) {
                if (item != null) {
                    var headAddress = if (StringUtils.isEmpty(item.headUrl)) "http://h5.zhanguo.fun/td/img/img_tx.png" else item.headUrl!!// 暂时使用
                    var civAvatar = helper.getView(R.id.civ_avatar) as CircleImageView
                    civAvatar.setLoadImage(context, headAddress, R.drawable.default_head)

                    var tvName = helper.getView(R.id.tv_name) as TextView
                    val level = context.resources.getDrawable(
                            when (item.rank) {
                                1 -> R.drawable.details_level_1
                                2 -> R.drawable.details_level_2
                                3 -> R.drawable.details_level_3
                                else -> R.drawable.details_level_1
                            })
                    level.setBounds(0, 0, level.minimumWidth, level.minimumHeight)
                    tvName.setCompoundDrawables(null, null, level, null)
                    tvName.text = item.nick
                    helper.setText(R.id.tv_sp, "SP：" + item.sp)

                    helper.setText(R.id.tv_desc, when (type) {
                        0, 1, 2 -> item.betBunch + "（佣金" + item.commissionRate.formatNoZero() + "%）"
                        3 -> item.saleNum + "个方案在售"
                        else -> {
                            item.betBunch + "（佣金" + item.commissionRate.formatNoZero() + "%）"
                        }
                    })
                    helper.setText(R.id.tv_number, when (type) {
                        0, 3 -> "发" + item.createOrders + "中" + item.hitNums
                        1 -> item.hitOdds.formatNoZero() + "%"
                        2 -> item.profitOdds.formatNoZero() + "%"
                        else -> {
                            "发" + item.createOrders + "中" + item.hitNums
                        }
                    })
                    helper.setText(R.id.tv_date, when (type) {
                        0 -> "近7日"
                        1 -> "命中率"
                        2 -> "盈利率"
                        3 -> "近30日"
                        else -> {
                            "近7日"
                        }
                    })
                    helper.setText(R.id.tv_left_value, if (type != 3) item.payAmt.formatNoZero() else item.hitOdds.formatNoZero() + "%")
                    helper.setText(R.id.tv_left, if (type != 3) "自购额" else "命中率")
                    helper.setText(R.id.tv_middle_value, if (type != 3) item.minFollowAmt.formatNoZero() else item.profitOdds.formatNoZero() + "%")
                    helper.setText(R.id.tv_middle, if (type != 3) "起跟额" else "盈利率")
                    helper.setText(R.id.tv_right_value, item.reds + "连红")
                    helper.setText(R.id.tv_right, "近30日")
                    helper.setText(R.id.tv_follow_total, item.followAmt.formatNoZero())
                    helper.setText(R.id.tv_follow_number, item.follows)

                    helper.setGone(R.id.tv_sp, type != 3)
                    helper.setGone(R.id.action_follow_buy, type != 3)
                    helper.setGone(R.id.line_follow, type != 3)
                    helper.setGone(R.id.ll_follow, type != 3)
                    helper.setGone(R.id.tv_right_value, type == 3)
                    helper.setGone(R.id.tv_right, type == 3)

                    helper.addOnClickListener(R.id.civ_avatar)
                    helper.addOnClickListener(R.id.tv_name)
                    helper.addOnClickListener(R.id.tv_desc)
                    helper.addOnClickListener(R.id.action_follow_buy)
                }
            }
        }
    }

    /**
     * 获取佣金记录列表数据适配器
     *
     * @param activity 当前活动
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getCommissionRecordListAdapter(data: List<CommissionBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<CommissionBean, BaseViewHolder>(R.layout.item_my_commission, data) {
            override fun convert(helper: BaseViewHolder, item: CommissionBean) {
                if (item != null) {
                    helper.setText(R.id.tv_title, "佣金")
                    helper.setText(R.id.tv_time, item.createTime.formatSystemTime("yy-MM-dd HH:mm"))
                    helper.setText(R.id.tv_amount, item.payAmount.formatMoney())
                }
            }
        }
    }

    /**
     * 获取我的推单列表数据适配器
     *
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getPushOrderListAdapter(data: List<PushOrderItemBean>, type: Int): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<PushOrderItemBean, BaseViewHolder>(R.layout.item_push_order, data) {
            override fun convert(helper: BaseViewHolder, item: PushOrderItemBean) {
                if (item != null) {
                    helper.setText(R.id.tv_title, item.createTime.formatSystemTime("yy/MM/dd") + "   " + item.betType)
                    helper.setText(R.id.tv_profit_rate, when (type) {
                        1 -> item.profitOdds.formatNoZero() + "%"
                        2 -> if (item.followStatus == "2") "已封单" else "跟单中"
                        else -> ""
                    })
                    helper.setText(R.id.tv_follow_number, item.follows)
                    helper.setText(R.id.tv_follow_amount, item.followAmt.formatNoZero())
                    helper.setText(R.id.tv_commission, item.commission.formatNoZero())
                    helper.setImageResource(R.id.iv_prize, when (type) {
                        1 -> R.drawable.td_ic_red
                        3 -> R.drawable.td_ic_black
                        else -> 0
                    })
                }
            }
        }
    }

    /**
     * 获取直播赛事列表数据适配器
     *
     * @param context  上下文
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getLiveListAdapter(context: Context, data: List<MatchItemBean>, countDownMap: SparseArray<CountDownTimer>?): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<MatchItemBean, BaseViewHolder>(R.layout.item_live, data) {
            override fun convert(helper: BaseViewHolder, item: MatchItemBean) {
                if (item != null) {
                    var ivMatchHostFlag = helper.getView(R.id.iv_match_host_flag) as ImageView
                    var ivMatchGuestFlag = helper.getView(R.id.iv_match_guest_flag) as ImageView
                    ivMatchHostFlag!!.setLoadImage(context, item.hostLogo, R.drawable.team_ic_default)// 主队logo
                    ivMatchGuestFlag!!.setLoadImage(context, item.guestLogo, R.drawable.team_ic_default)// 客队logo

                    helper.setText(R.id.tv_match_date, item.matchField)
                    helper.setText(R.id.tv_match_time_and_type, item.matchTime.formatTime("HH:mm") + " " + item.league)
                    helper.setText(R.id.tv_match_time_end, if (!StringUtils.isEmpty(item.minute)) item.minute else "")

                    // 倒计时
                    val time = (if (!StringUtils.isEmpty(item.minute)) item.minute else "0").toLong() * 60 * 1000
                    // 将前一个缓存清除
                    var tvMinute = helper.getView(R.id.tv_minute) as TextView
                    if (countDownMap!!.get(tvMinute.hashCode()) != null) {
                        countDownMap.get(tvMinute.hashCode()).cancel()
                    }
                    if (time > 0) {
                        tvMinute.text = "'"
                        val countDownTimer = object : CountDownTimer(time, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                                tvMinute.visibility = if (tvMinute.visibility == View.VISIBLE) View.INVISIBLE else View.VISIBLE
                            }

                            override fun onFinish() {
                                helper.setGone(R.id.tv_match_time_end, false)
                                tvMinute.visibility = View.GONE
                            }
                        }.start()
                        countDownMap.put(tvMinute.hashCode(), countDownTimer)// 这里不行就在外面加入数据时生成
                    } else {
                        helper.setGone(R.id.tv_match_time_end, false)
                        tvMinute.text = ""
                        tvMinute.visibility = View.GONE
                    }

                    helper.setText(R.id.tv_match_half_score, if (!StringUtils.isEmpty(item.halfScore)) "半" + item.halfScore else "")
                    helper.setText(R.id.tv_match_host_name, item.hostTeam)
                    helper.setText(R.id.tv_match_guest_name, item.guestTeam)
                    helper.setText(R.id.tv_match_score, if (!StringUtils.isEmpty(item.score)) item.score else "VS")
                    // 赛事状态0：未开始 1：进行中2：暂停3：已完成4：推迟5：取消
                    var tvMatchStatus = helper.getView(R.id.tv_match_status) as TextView
                    tvMatchStatus.text = item.gameState
                    tvMatchStatus.visibility = if (item.status == "0") View.INVISIBLE else View.VISIBLE
                    tvMatchStatus.setTextColor(Color.parseColor(if (item.status == "1") "#D60000" else "#FFFFFF"))
                    tvMatchStatus.setBackgroundResource(if (item.status == "1") R.drawable.sp_stroke_red_x10 else R.drawable.sp_gray_x10)
                    helper.setGone(R.id.line1, item.groupPosition != item.groupSize - 1 && item.childPosition == item.childSize - 1)
                    helper.setGone(R.id.line2, item.groupPosition == item.groupSize - 1 || item.childPosition != item.childSize - 1)
                }
            }
        }
    }

    /**
     * 足彩混合投注列表数据适配器
     *
     * @param data     数据列表
     * @param listener 监听事件
     * @return adapter 返回的适配器
     */
    fun getFootballLotteryHHListAdapter(data: List<BallMatchBean>, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_lottery_hh, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    val passStatus = item.passStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    helper.setGone(R.id.iv_dan, passStatus[0] == 1 || passStatus[1] == 1)
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.drawable.soccer_ic_analyse_up else R.drawable.soccer_ic_analyse_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var tvMatchScore = helper.getView<TextView>(R.id.tv_match_score)
                    tvMatchScore.text = item.score
                    tvMatchScore.setBackgroundColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#d60000"))

                    var actionGotoExpand = helper.getView<TextView>(R.id.action_goto_expand)
                    actionGotoExpand.text = if (item.selectedNumber > 0) "已选\n" + item.selectedNumber + "项" else "展开\n全部"
                    actionGotoExpand.setTextColor(if (item.selectedNumber > 0) Color.parseColor("#ffffff") else Color.parseColor("#333333"))
                    actionGotoExpand.setBackgroundColor(if (item.selectedNumber > 0) Color.parseColor("#D60000") else Color.parseColor("#ffffff"))

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_game_numbers, item.overallRecord!!.substring(0, item.overallRecord!!.length - 6))
                        helper.setText(R.id.tv_host_win, arr[1] + "胜")
                        helper.setText(R.id.tv_host_dogfall, arr[2] + "平")
                        helper.setText(R.id.tv_host_defeat, arr[3] + "负")
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    var avgOdds: Array<String>? = null
                    if (item.avgOdds != null) {
                        avgOdds = item.avgOdds!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    }
                    helper.setText(R.id.tv_odds0, if (avgOdds != null && avgOdds.size > 0) avgOdds[0] else "0%")
                    helper.setText(R.id.tv_odds1, if (avgOdds != null && avgOdds.size > 1) avgOdds[1] else "0%")
                    helper.setText(R.id.tv_odds2, if (avgOdds != null && avgOdds.size > 2) avgOdds[2] else "0%")

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)
                    var actionSelect4 = helper.getView<CheckBox>(R.id.action_select4)
                    var actionSelect5 = helper.getView<CheckBox>(R.id.action_select5)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)
                    checkBoxes.add(actionSelect4)
                    checkBoxes.add(actionSelect5)

                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }

                    item.pagePosition = 0// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        var isEnable = when (i) {
                            in 0..2 -> detailStatus[0] == 0
                            in 3..5 -> detailStatus[1] == 0
                            else -> true
                        }
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = i.getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) i.getCheckBoxText(item.oddsMap, false) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            actionGotoExpand.text = if (item.selectedNumber > 0) "已选\n" + item.selectedNumber + "项" else "展开\n全部"
                            actionGotoExpand.setTextColor(if (item.selectedNumber > 0) Color.parseColor("#ffffff") else Color.parseColor("#333333"))
                            actionGotoExpand.setBackgroundColor(if (item.selectedNumber > 0) Color.parseColor("#D60000") else Color.parseColor("#ffffff"))

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 足彩单关固定列表数据适配器
     *
     * @param data     数据列表
     * @param listener 监听事件
     * @return adapter 返回的适配器
     */
    fun getFootballLotteryDGListAdapter(data: List<BallMatchBean>, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_lottery_dg, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    val passStatus = item.passStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    helper.setGone(R.id.iv_dan, passStatus[0] == 1 || passStatus[1] == 1)
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.drawable.soccer_ic_analyse_up else R.drawable.soccer_ic_analyse_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var actionGotoExpand = helper.getView<TextView>(R.id.action_goto_expand)
                    actionGotoExpand.text = if (item.selectedNumber > 0) "已选\n" + item.selectedNumber + "项" else "展开\n全部"
                    actionGotoExpand.setTextColor(if (item.selectedNumber > 0) Color.parseColor("#ffffff") else Color.parseColor("#333333"))
                    actionGotoExpand.setBackgroundColor(if (item.selectedNumber > 0) Color.parseColor("#D60000") else Color.parseColor("#ffffff"))

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_game_numbers, item.overallRecord!!.substring(0, item.overallRecord!!.length - 6))
                        helper.setText(R.id.tv_host_win, arr[1] + "胜")
                        helper.setText(R.id.tv_host_dogfall, arr[2] + "平")
                        helper.setText(R.id.tv_host_defeat, arr[3] + "负")
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    var avgOdds: Array<String>? = null
                    if (item.avgOdds != null) {
                        avgOdds = item.avgOdds!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    }
                    helper.setText(R.id.tv_odds0, if (avgOdds != null && avgOdds.size > 0) avgOdds[0] else "0%")
                    helper.setText(R.id.tv_odds1, if (avgOdds != null && avgOdds.size > 1) avgOdds[1] else "0%")
                    helper.setText(R.id.tv_odds2, if (avgOdds != null && avgOdds.size > 2) avgOdds[2] else "0%")

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)

                    // 禁用玩法
                    var isEnable = passStatus[0] == 1

                    item.pagePosition = 1// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = i.getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) i.getCheckBoxText(item.oddsMap, false) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            actionGotoExpand.text = if (item.selectedNumber > 0) "已选\n" + item.selectedNumber + "项" else "展开\n全部"
                            actionGotoExpand.setTextColor(if (item.selectedNumber > 0) Color.parseColor("#ffffff") else Color.parseColor("#333333"))
                            actionGotoExpand.setBackgroundColor(if (item.selectedNumber > 0) Color.parseColor("#D60000") else Color.parseColor("#ffffff"))

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 足彩胜平负&让球胜平负列表数据适配器
     *
     * @param data     数据列表
     * @param listener 监听事件
     * @param isRQSPF  是否为让球胜平负
     * @return adapter 返回的适配器
     */
    fun getFootballLotterySPFListAdapter(data: List<BallMatchBean>, listener: SelectBallChangeListener, isRQSPF: Boolean): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_lottery_spf, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.drawable.soccer_ic_analyse_up else R.drawable.soccer_ic_analyse_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    helper.setGone(R.id.tv_match_score, isRQSPF)
                    if (isRQSPF) {
                        var tvMatchScore = helper.getView<TextView>(R.id.tv_match_score)
                        tvMatchScore.text = if (isRQSPF) item.score else ""
                        tvMatchScore.setTextColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))
                    }

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_game_numbers, item.overallRecord!!.substring(0, item.overallRecord!!.length - 6))
                        helper.setText(R.id.tv_host_win, arr[1] + "胜")
                        helper.setText(R.id.tv_host_dogfall, arr[2] + "平")
                        helper.setText(R.id.tv_host_defeat, arr[3] + "负")
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    var avgOdds: Array<String>? = null
                    if (item.avgOdds != null) {
                        avgOdds = item.avgOdds!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    }
                    helper.setText(R.id.tv_odds0, if (avgOdds != null && avgOdds.size > 0) avgOdds[0] else "0%")
                    helper.setText(R.id.tv_odds1, if (avgOdds != null && avgOdds.size > 1) avgOdds[1] else "0%")
                    helper.setText(R.id.tv_odds2, if (avgOdds != null && avgOdds.size > 2) avgOdds[2] else "0%")

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)

                    // 禁用玩法
                    val detailStatus = item.detailStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    var isEnable = if (!isRQSPF) detailStatus[0] == 0 else detailStatus[1] == 0

                    item.pagePosition = (if (!isRQSPF) 2 else 3)// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = (if (!isRQSPF) i else i + 3).getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) (if (!isRQSPF) i else i + 3).getCheckBoxText(item.oddsMap, false) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 足彩总进球数列表数据适配器
     *
     * @param context  上下文
     * @param data     数据列表
     * @param listener 监听事件
     * @return adapter 返回的适配器
     */
    fun getFootballLotteryZJQSCListAdapter(context: Context, data: List<BallMatchBean>, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_lottery_zjqs, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.drawable.soccer_ic_analyse_up else R.drawable.soccer_ic_analyse_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_game_numbers, item.overallRecord!!.substring(0, item.overallRecord!!.length - 6))
                        helper.setText(R.id.tv_host_win, arr[1] + "胜")
                        helper.setText(R.id.tv_host_dogfall, arr[2] + "平")
                        helper.setText(R.id.tv_host_defeat, arr[3] + "负")
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    var avgOdds: Array<String>? = null
                    if (item.avgOdds != null) {
                        avgOdds = item.avgOdds!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    }
                    helper.setText(R.id.tv_odds0, if (avgOdds != null && avgOdds.size > 0) avgOdds[0] else "0%")
                    helper.setText(R.id.tv_odds1, if (avgOdds != null && avgOdds.size > 1) avgOdds[1] else "0%")
                    helper.setText(R.id.tv_odds2, if (avgOdds != null && avgOdds.size > 2) avgOdds[2] else "0%")

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)
                    var actionSelect4 = helper.getView<CheckBox>(R.id.action_select4)
                    var actionSelect5 = helper.getView<CheckBox>(R.id.action_select5)
                    var actionSelect6 = helper.getView<CheckBox>(R.id.action_select6)
                    var actionSelect7 = helper.getView<CheckBox>(R.id.action_select7)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)
                    checkBoxes.add(actionSelect4)
                    checkBoxes.add(actionSelect5)
                    checkBoxes.add(actionSelect6)
                    checkBoxes.add(actionSelect7)

                    // 禁用玩法
                    val detailStatus = item.detailStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    var isEnable = detailStatus[3] == 0

                    item.pagePosition = 4// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = (i + 37).getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) (i + 37).getCheckedStyleText(context, false, item.oddsMap, checkBoxes[i].isChecked, 13, 12) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked
                            checkBoxes[i].text = (i + 37).getCheckedStyleText(context, false, item.oddsMap, isChecked, 13, 12)

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 足彩比分列表数据适配器
     *
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getFootballLotteryBFListAdapter(data: List<BallMatchBean>, pagePosition: Int): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_lottery_bf, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.drawable.soccer_ic_analyse_up else R.drawable.soccer_ic_analyse_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var actionGotoExpand = helper.getView<TextView>(R.id.action_goto_expand)
                    actionGotoExpand.text = if (item.selectedLabel.isNotEmpty()) item.selectedLabel else "请选择投注内容"
                    actionGotoExpand.setTextColor(if (item.selectedLabel.isNotEmpty()) Color.parseColor("#ffffff") else Color.parseColor("#999999"))
                    actionGotoExpand.setBackgroundColor(if (item.selectedLabel.isNotEmpty()) Color.parseColor("#D60000") else Color.parseColor("#ffffff"))

                    item.pagePosition = pagePosition // 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_game_numbers, item.overallRecord!!.substring(0, item.overallRecord!!.length - 6))
                        helper.setText(R.id.tv_host_win, arr[1] + "胜")
                        helper.setText(R.id.tv_host_dogfall, arr[2] + "平")
                        helper.setText(R.id.tv_host_defeat, arr[3] + "负")
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    var avgOdds: Array<String>? = null
                    if (item.avgOdds != null) {
                        avgOdds = item.avgOdds!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    }
                    helper.setText(R.id.tv_odds0, if (avgOdds != null && avgOdds.size > 0) avgOdds[0] else "0%")
                    helper.setText(R.id.tv_odds1, if (avgOdds != null && avgOdds.size > 1) avgOdds[1] else "0%")
                    helper.setText(R.id.tv_odds2, if (avgOdds != null && avgOdds.size > 2) avgOdds[2] else "0%")

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 足彩半全场列表数据适配器
     *
     * @param data     数据列表
     * @param listener 监听事件
     * @return adapter 返回的适配器
     */
    fun getFootballLotteryBQCListAdapter(data: List<BallMatchBean>, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_lottery_bqc, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.drawable.soccer_ic_analyse_up else R.drawable.soccer_ic_analyse_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_game_numbers, item.overallRecord!!.substring(0, item.overallRecord!!.length - 6))
                        helper.setText(R.id.tv_host_win, arr[1] + "胜")
                        helper.setText(R.id.tv_host_dogfall, arr[2] + "平")
                        helper.setText(R.id.tv_host_defeat, arr[3] + "负")
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    var avgOdds: Array<String>? = null
                    if (item.avgOdds != null) {
                        avgOdds = item.avgOdds!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    }
                    helper.setText(R.id.tv_odds0, if (avgOdds != null && avgOdds.size > 0) avgOdds[0] else "0%")
                    helper.setText(R.id.tv_odds1, if (avgOdds != null && avgOdds.size > 1) avgOdds[1] else "0%")
                    helper.setText(R.id.tv_odds2, if (avgOdds != null && avgOdds.size > 2) avgOdds[2] else "0%")

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)
                    var actionSelect4 = helper.getView<CheckBox>(R.id.action_select4)
                    var actionSelect5 = helper.getView<CheckBox>(R.id.action_select5)
                    var actionSelect6 = helper.getView<CheckBox>(R.id.action_select6)
                    var actionSelect7 = helper.getView<CheckBox>(R.id.action_select7)
                    var actionSelect8 = helper.getView<CheckBox>(R.id.action_select8)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)
                    checkBoxes.add(actionSelect4)
                    checkBoxes.add(actionSelect5)
                    checkBoxes.add(actionSelect6)
                    checkBoxes.add(actionSelect7)
                    checkBoxes.add(actionSelect8)

                    // 禁用玩法
                    val detailStatus = item.detailStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    var isEnable = detailStatus[4] == 0

                    item.pagePosition = 6// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = (i + 45).getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) (i + 45).getCheckBoxText(item.oddsMap, false) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 足彩混合投注选择列表数据适配器
     *
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getFootballSelectedHHListAdapter(data: List<BallMatchBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_selected_hh, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var actionGotoExpand = helper.getView<TextView>(R.id.action_goto_expand)
                    actionGotoExpand.text = if (item.selectedLabel.isNotEmpty()) item.selectedLabel else "请选择投注内容"
                    actionGotoExpand.setTextColor(if (item.selectedLabel.isNotEmpty()) Color.parseColor("#ffffff") else Color.parseColor("#999999"))
                    actionGotoExpand.setBackgroundColor(if (item.selectedLabel.isNotEmpty()) Color.parseColor("#D60000") else Color.parseColor("#ffffff"))

                    item.adapterPosition = helper.adapterPosition

                    helper.setGone(R.id.line, helper.adapterPosition < data.size - 1)

                    helper.addOnClickListener(R.id.action_remove)
                    helper.addOnClickListener(R.id.action_goto_expand)
                }
            }
        }
    }

    /**
     * 足彩胜平负选择列表数据适配器
     *
     * @param data     数据列表
     * @param listener 监听事件
     * @param isRQSPF  是否为让球胜平负
     * @return adapter 返回的适配器
     */
    fun getFootballSelectedSPFListAdapter(data: List<BallMatchBean>, listener: SelectBallChangeListener, isRQSPF: Boolean): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_selected_spf, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    helper.setGone(R.id.tv_match_score, isRQSPF)
                    if (isRQSPF) {
                        var tvMatchScore = helper.getView<TextView>(R.id.tv_match_score)
                        tvMatchScore.text = if (isRQSPF) item.score else ""
                        tvMatchScore.setTextColor(if (!item.score.contains("-")) Color.parseColor("#F20C00") else Color.parseColor("#009D48"))
                    }

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)

                    // 禁用玩法
                    val detailStatus = item.detailStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    var isEnable = if (!isRQSPF) detailStatus[0] == 0 else detailStatus[1] == 0

                    item.pagePosition = (if (!isRQSPF) 2 else 3)// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = (if (!isRQSPF) i else i + 3).getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) (if (!isRQSPF) i else i + 3).getCheckBoxText(item.oddsMap, false) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.setGone(R.id.line, helper.adapterPosition < data.size - 1)
                    helper.addOnClickListener(R.id.action_remove)
                }
            }
        }
    }

    /**
     * 足彩总进球数选择列表数据适配器
     *
     * @param context  上下文
     * @param data     数据列表
     * @param listener 监听事件
     * @return adapter 返回的适配器
     */
    fun getFootballSelectedZJQSListAdapter(context: Context, data: List<BallMatchBean>, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_selected_zjqs, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)
                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)
                    var actionSelect4 = helper.getView<CheckBox>(R.id.action_select4)
                    var actionSelect5 = helper.getView<CheckBox>(R.id.action_select5)
                    var actionSelect6 = helper.getView<CheckBox>(R.id.action_select6)
                    var actionSelect7 = helper.getView<CheckBox>(R.id.action_select7)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)
                    checkBoxes.add(actionSelect4)
                    checkBoxes.add(actionSelect5)
                    checkBoxes.add(actionSelect6)
                    checkBoxes.add(actionSelect7)

                    // 禁用玩法
                    val detailStatus = item.detailStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    var isEnable = detailStatus[3] == 0

                    item.pagePosition = 4// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = (i + 37).getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) (i + 37).getCheckedStyleText(context, false, item.oddsMap, checkBoxes[i].isChecked, 13, 12) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked
                            checkBoxes[i].text = (i + 37).getCheckedStyleText(context, false, item.oddsMap, isChecked, 13, 12)

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.setGone(R.id.line, helper.adapterPosition < data.size - 1)

                    helper.addOnClickListener(R.id.action_remove)
                }
            }
        }
    }

    /**
     * 足彩半全场选择列表数据适配器
     *
     * @param data     数据列表
     * @param listener 监听事件
     * @return adapter 返回的适配器
     */
    fun getFootballSelectedBQCListAdapter(data: List<BallMatchBean>, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_football_selected_bqc, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)
                    var actionSelect4 = helper.getView<CheckBox>(R.id.action_select4)
                    var actionSelect5 = helper.getView<CheckBox>(R.id.action_select5)
                    var actionSelect6 = helper.getView<CheckBox>(R.id.action_select6)
                    var actionSelect7 = helper.getView<CheckBox>(R.id.action_select7)
                    var actionSelect8 = helper.getView<CheckBox>(R.id.action_select8)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)
                    checkBoxes.add(actionSelect4)
                    checkBoxes.add(actionSelect5)
                    checkBoxes.add(actionSelect6)
                    checkBoxes.add(actionSelect7)
                    checkBoxes.add(actionSelect8)

                    // 禁用玩法
                    val detailStatus = item.detailStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    var isEnable = detailStatus[4] == 0

                    item.pagePosition = 6// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition
                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        var realPosition = (i + 45).getMapKeyByPosition(false)
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) (i + 45).getCheckBoxText(item.oddsMap, false) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValue2() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.setGone(R.id.line, helper.adapterPosition < data.size - 1)

                    helper.addOnClickListener(R.id.action_remove)
                }
            }
        }
    }

    /**
     * 获取赛事塞选列表数据适配器
     *
     * @param activity 当前活动
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getFootballLotteryFilterListAdapter(data: List<LeagueBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<LeagueBean, BaseViewHolder>(R.layout.item_football_lottery_filter, data) {
            override fun convert(helper: BaseViewHolder, item: LeagueBean) {
                if (item != null) {
                    var ivSelectBg = helper.getView(R.id.iv_select_bg) as ImageView
                    var tvMatchName = helper.getView(R.id.tv_match_name) as TextView
                    tvMatchName.text = item.league

                    when (helper.adapterPosition) {
                        0 -> {
                            ivSelectBg.setImageResource(if (item.selected) R.drawable.soccer_bg_all else R.drawable.soccer_bg_default)
                            tvMatchName.setTextColor(if (item.selected) Color.parseColor("#ffffff") else Color.parseColor("#666666"))
                        }
                        1 -> {
                            ivSelectBg.setImageResource(if (item.selected) R.drawable.soccer_bg_all else R.drawable.soccer_bg_default)
                            tvMatchName.setTextColor(if (item.selected) Color.parseColor("#ffffff") else Color.parseColor("#666666"))
                        }
                        else -> {
                            ivSelectBg.setImageResource(if (item.selected) R.drawable.soccer_bg_selected else R.drawable.soccer_bg_default)
                            tvMatchName.setTextColor(if (item.selected) Color.parseColor("#d60000") else Color.parseColor("#666666"))
                        }
                    }
                }
            }
        }
    }

    /**
     * 足彩串法列表数据适配器
     *
     * @param type     0上面默认显示的列表 1更多列表
     * @param data     数据列表
     * @param listener 监听事件
     * @return adapter 返回的适配器
     */
    fun getFootballSeriesListAdapter(type: Int, data: List<BallBunch>, listener: SelectSeriesChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallBunch, BaseViewHolder>(R.layout.item_football_series, data) {
            override fun convert(helper: BaseViewHolder, item: BallBunch) {
                if (item != null) {
                    var cbName = helper.getView<CheckBox>(R.id.cb_name)
                    cbName.text = item.name
                    cbName.setOnCheckedChangeListener(null)// 重置选中事件
                    cbName.isChecked = item.selected
                    cbName.setOnCheckedChangeListener { _, isChecked ->
                        item.selected = isChecked
                        listener?.onItemSelectChange(type, helper.adapterPosition, item)
                    }
                }
            }
        }
    }

    /**
     * 获取竞猜足球订单详情列表数据适配器
     *
     * @param context  上下文
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getOrderDetailListAdapter(context: Context, data: ArrayList<OrderBetInfoBean>, lotteryId: String): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<OrderBetInfoBean, BaseViewHolder>(R.layout.item_order_detail, data) {
            override fun convert(helper: BaseViewHolder, item: OrderBetInfoBean) {
                if (item != null) {
                    helper.setText(R.id.tv_time, item.matchField)
                    helper.setText(R.id.tv_team, when (lotteryId) {
                        "24" -> item.guestTeam + "\nVS\n" + item.hostTeam
                        else -> item.hostTeam + "\nVS\n" + item.guestTeam
                    })

                    var rvContent = helper.getView<RecyclerView>(R.id.rv_content)
                    rvContent.layoutManager = LinearLayoutManager(context)
                    // 需要在setLayoutManager()之后调用addItemDecoration()
                    if (rvContent.itemDecorationCount > 0) {
                        rvContent.removeItemDecorationAt(0)
                    }
                    rvContent.addItemDecoration(CommonDecoration(0))
                    rvContent.adapter = getOrderDetailItemListAdapter(item.betList, item, lotteryId)
                }
            }
        }
    }

    /**
     * 获取竞猜足球订单详情子列表数据适配器
     *
     * @param data     数据列表
     * @param listener 点击监听事件
     * @return adapter 返回的适配器
     */
    fun getOrderDetailItemListAdapter(data: List<BetBean>, bean: OrderBetInfoBean, lotteryId: String): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BetBean, BaseViewHolder>(R.layout.item_order_detail_child, data) {
            override fun convert(helper: BaseViewHolder, item: BetBean) {
                if (item != null) {
                    var llOrderDetailItem = helper.getView<LinearLayout>(R.id.ll_order_detail_item)
                    var lp = llOrderDetailItem.layoutParams
                    var tvName = helper.getView<TextView>(R.id.tv_name)
                    var tvContent = helper.getView<TextView>(R.id.tv_content)
                    var tvResult = helper.getView<TextView>(R.id.tv_result)
                    tvName.text = ""
                    tvContent.text = ""
                    tvResult.text = ""

                    var matchName = ""
                    var resultPosition = 0

                    when (lotteryId) {
                        "21" -> {
                            // 设置布局宽高度
                            lp.height = dp2px(when (data.size) {
                                1 -> 90f
                                2 -> 45f
                                else -> 30f
                            })
                            llOrderDetailItem.layoutParams = lp

                            when (item.betNum) {
                                in 0..2 -> {
                                    matchName = "胜平负"
                                    resultPosition = 0
                                }
                                in 3..5 -> {
                                    matchName = "让球(" + bean.letNum + ")"
                                    resultPosition = 1
                                }
                                in 6..14 -> {
                                    matchName = "半全场"
                                    resultPosition = 2
                                }
                                in 15..45 -> {
                                    matchName = "比分"
                                    resultPosition = 3
                                }
                                in 46..53 -> {
                                    matchName = "进球数"
                                    resultPosition = 4
                                }
                                else -> {
                                    matchName = "其他"
                                    resultPosition = 0
                                }
                            }

                            tvContent.text = item.betNum.getCheckBoxTextValue2() + " " + item.odds

                            if (!isEmpty(bean.matchResultInfo) && bean.matchResultInfo != "-,-,-,-") {
                                var results = bean.matchResultInfo.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()// 用,分割
                                var result = results[resultPosition]
                                tvResult.text = result
                                tvContent.setTextColor(Color.parseColor(if (result == item.betNum.getCheckBoxTextValue2()) "#FF0000" else "#333333"))
                                tvResult.setTextColor(Color.parseColor(if (result == item.betNum.getCheckBoxTextValue2()) "#FF0000" else "#333333"))
                            }
                        }
                        "24" -> {
                            // 设置布局高度
                            lp.height = dp2px(when (data.size) {
                                1 -> 90f
                                2 -> 45f
                                else -> 40f
                            })
                            llOrderDetailItem.layoutParams = lp

                            val width0 = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                            val width1 = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                            val width2 = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                            width0.weight = 1f
                            width1.weight = 1f
                            width2.weight = 1f
                            tvName.layoutParams = width0
                            tvContent.layoutParams = width1
                            tvResult.layoutParams = width2

                            when (item.betNum) {
                                0, 1 -> {
                                    matchName = "胜负"
                                    resultPosition = 0
                                }
                                2, 3 -> {
                                    matchName = "让分(" + bean.letNum + ")"
                                    resultPosition = 1
                                }
                                4, 5 -> {
                                    matchName = "大小分(" + bean.totalScore + ")"
                                    resultPosition = 2
                                }
                                in 6..17 -> {
                                    matchName = "胜分差"
                                    resultPosition = 3
                                }
                                else -> {
                                    matchName = "其他"
                                    resultPosition = 0
                                }
                            }

                            tvContent.text = item.betNum.getCheckBoxTextValueShow() + (if (resultPosition == 3) "\n" else " ") + item.odds

                            if (!isEmpty(bean.matchResult) && bean.matchResult != "-,-,-,-") {
                                var results = bean.matchResult.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()// 用,分割
                                var result = results[resultPosition]
                                tvResult.text = result.toInt().getCheckBoxTextValueLabel()
                                tvContent.setTextColor(Color.parseColor(if (result == item.betNum.toString()) "#FF0000" else "#333333"))
                                tvResult.setTextColor(Color.parseColor(if (result == item.betNum.toString()) "#FF0000" else "#333333"))
                            }
                        }
                        else -> "其他"
                    }

                    if (isEmpty(bean.matchResultInfo) || bean.matchResultInfo == "-,-,-,-") {
                        tvResult.text = "—"
                        tvContent.setTextColor(Color.parseColor("#333333"))
                        tvResult.setTextColor(Color.parseColor("#333333"))
                    }
                    tvName.text = matchName
                }
            }
        }
    }

    /**
     * 获取推单命中状态列表数据适配器
     *
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getHitListAdapter(data: List<String>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_hit, data) {
            override fun convert(helper: BaseViewHolder, item: String) {
                if (item != null) {
                    helper.setImageResource(R.id.iv_hit, if (item == "1") R.drawable.details_ic_red else R.drawable.details_ic_black)
                }
            }
        }
    }

    /**
     * 获取在售方案列表数据适配器
     *
     * @param data     数据列表
     * @return adapter 返回的适配器
     */
    fun getPlanListAdapter(data: List<Order>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<Order, BaseViewHolder>(R.layout.item_plan, data) {
            override fun convert(helper: BaseViewHolder, item: Order) {
                if (item != null) {
                    helper.setText(R.id.tv_play_type, item.betType)
                    helper.setText(R.id.tv_deadline, "截止" + item.closeTime.formatSystemTime("yyyy.MM.dd HH:mm"))
                    helper.setText(R.id.tv_pay_amount, item.payAmt.toString())
                    helper.setText(R.id.tv_commission_rate, item.commissionRate.formatNoZero() + "%")
                    helper.setText(R.id.tv_follow_amount, item.minFollowAmt.toString() + "起跟")
                    helper.setText(R.id.tv_follow_total, item.followAmt.toString())
                    helper.setText(R.id.tv_follow_number, item.follows.toString())
                }
            }
        }
    }

    /**
     * 获取我的红包列表数据适配器
     *
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @return adapter 返回的适配器
     */
    fun getCouponListAdapter(data: List<CouponBean>, pagePosition: Int): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<CouponBean, BaseViewHolder>(R.layout.item_my_coupon, data) {
            override fun convert(helper: BaseViewHolder, item: CouponBean) {
                if (item != null) {
                    helper.setImageResource(R.id.iv_coupon_bg, if (pagePosition != 2) R.drawable.list_img_coupons else R.drawable.list_img_coupons_lose)

                    var rlCoupon = helper.getView<RelativeLayout>(R.id.rl_coupon)
                    rlCoupon.setMargins(0, dp2px(if (helper.adapterPosition == 0) 8f else 0f), 0, 0)

                    var tvTime = helper.getView<TextView>(R.id.tv_time)
                    var tvStatus = helper.getView<TextView>(R.id.tv_status)
                    // 0.未使用 1.已使用 2.已过期 3.待派发
                    tvStatus.text = when (item.status) {
                        0 -> "立即使用"
                        1 -> "已使用"
                        2 -> "已过期"
                        else -> ""
                    }
                    when (pagePosition) {
                        0 -> {
                            tvTime.text = "有效期限至" + item.endTime
                            tvTime.setTextColor(Color.parseColor("#999999"))
                            tvStatus.setTextColor(Color.parseColor("#F73A36"))
                            tvStatus.setBackgroundResource(R.drawable.sp_stroke_red_x12)
                        }
                        1 -> {
                            tvTime.text = "将于" + item.distributeTime + "派发"
                            tvTime.setTextColor(Color.parseColor("#d60000"))
                            tvStatus.setBackgroundResource(0)
                        }
                        2 -> {
                            tvTime.text = "有效期限至" + item.endTime
                            tvTime.setTextColor(Color.parseColor("#999999"))
                            tvStatus.setTextColor(Color.parseColor("#666666"))
                            tvStatus.setBackgroundResource(R.drawable.sp_stroke_gray2_x12)
                        }
                    }

                    helper.setText(R.id.tv_coupon_number, item.money.toString())
                    helper.setText(R.id.tv_desc, "限" + item.lotteryName + "使用")
                    helper.setText(R.id.tv_condition, "单笔订单满" + item.conditionMoney + "趣豆可用")
                }
            }
        }
    }

    /**
     * 获取选择红包列表数据适配器
     *
     * @param data     数据列表
     * @param type     列表类型
     * @return adapter 返回的适配器
     */
    fun getSelectCouponListAdapter(data: List<CouponBean>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<CouponBean, BaseViewHolder>(R.layout.item_coupon_select, data) {
            override fun convert(helper: BaseViewHolder, item: CouponBean) {
                if (item != null) {
                    helper.setImageResource(R.id.iv_checked, if (item.isSelected) R.drawable.checkout_choose_selected else R.drawable.checkout_choose_default)
                    helper.setText(R.id.tv_coupon_number, item.money.toString())
                    helper.setText(R.id.tv_desc, "限" + item.lotteryName + "使用")
                    helper.setText(R.id.tv_condition, "单笔订单满" + item.conditionMoney + "趣豆可用")
                    helper.setText(R.id.tv_time, "有效期限至" + item.endTime)
                }
            }
        }
    }

    /**
     * 竞猜篮球混合投注列表数据适配器
     *
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @param listener     监听事件
     * @return adapter     返回的适配器
     */
    fun getBasketballLotteryHHListAdapter(data: List<BallMatchBean>, pagePosition: Int, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_lottery_hh, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.mipmap.ic_analysis_upward else R.mipmap.ic_analysis_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var tvMatchScore = helper.getView<TextView>(R.id.tv_match_score)
                    tvMatchScore.text = "（" + item.score + "）"
                    tvMatchScore.setTextColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))
                    var tvMatchScore2 = helper.getView<TextView>(R.id.tv_match_score2)
                    tvMatchScore2.setBackgroundColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))

                    var actionGotoExpand = helper.getView<TextView>(R.id.action_goto_expand)
                    actionGotoExpand.text = if (item.selectedNumber > 0) "已选\n" + item.selectedNumber + "项" else "全部\n玩法"
                    actionGotoExpand.setTextColor(if (item.selectedNumber > 0) Color.parseColor("#ffffff") else Color.parseColor("#333333"))
                    actionGotoExpand.setBackgroundColor(if (item.selectedNumber > 0) Color.parseColor("#F20C00") else Color.parseColor("#ffffff"))

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_history_record, item.overallRecord)
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    helper.setText(R.id.tv_ranking, item.matchRank)

                    item.pagePosition = pagePosition// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    val passStatus = item.passStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    var isEnable0 = if (item.pagePosition == 1) passStatus[0] == 1 else detailStatus[0] == 0
                    var isEnable1 = if (item.pagePosition == 1) passStatus[1] == 1 else detailStatus[1] == 0

                    // 设置禁用状态
                    helper.setGone(R.id.line0, isEnable0)
                    helper.setGone(R.id.action_select0, isEnable0)
                    helper.setGone(R.id.action_select1, isEnable0)
                    helper.setGone(R.id.tv_line0, !isEnable0)
                    helper.setGone(R.id.line1, isEnable1)
                    helper.setGone(R.id.action_select2, isEnable1)
                    helper.setGone(R.id.action_select3, isEnable1)
                    helper.setGone(R.id.tv_line1, !isEnable1)

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)

                    for (i in 0 until checkBoxes.size) {
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        checkBoxes[i].isChecked = item.selected!![i] == true
                        checkBoxes[i].text = i.getCheckBoxText2Show(item.oddsMap)
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![i] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            actionGotoExpand.text = if (item.selectedNumber > 0) "已选\n" + item.selectedNumber + "项" else "全部\n玩法"
                            actionGotoExpand.setTextColor(if (item.selectedNumber > 0) Color.parseColor("#ffffff") else Color.parseColor("#333333"))
                            actionGotoExpand.setBackgroundColor(if (item.selectedNumber > 0) Color.parseColor("#F20C00") else Color.parseColor("#ffffff"))

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 竞猜篮球胜负列表数据适配器
     *
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @param listener     监听事件
     * @return adapter     返回的适配器
     */
    fun getBasketballLotterySFListAdapter(data: List<BallMatchBean>, pagePosition: Int, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_lottery_sf, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.mipmap.ic_analysis_upward else R.mipmap.ic_analysis_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    if (pagePosition == 3) {
                        var tvMatchScore = helper.getView<TextView>(R.id.tv_match_score)
                        tvMatchScore.text = "（" + item.score + "）"
                        tvMatchScore.setTextColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))
                    }

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_history_record, item.overallRecord)
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    helper.setText(R.id.tv_ranking, item.matchRank)

                    item.pagePosition = pagePosition// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)

                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }

                    for (i in 0 until checkBoxes.size) {
                        var isEnable = when (pagePosition) {
                            2 -> detailStatus[0] == 0
                            3 -> detailStatus[1] == 0
                            else -> detailStatus[0] == 0
                        }
                        checkBoxes[i].isEnabled = isEnable
                        var realPosition = when (pagePosition) {
                            2 -> i
                            3 -> i + 2
                            else -> i
                        }
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) realPosition.getCheckBoxText2Show(item.oddsMap) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 竞猜篮球大小分列表数据适配器
     *
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @param listener     监听事件
     * @return adapter     返回的适配器
     */
    fun getBasketballLotteryDXFListAdapter(data: List<BallMatchBean>, pagePosition: Int, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_lottery_dxf, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.mipmap.ic_analysis_upward else R.mipmap.ic_analysis_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)
                    helper.setText(R.id.tv_total_score, item.totalScore)

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_history_record, item.overallRecord)
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    helper.setText(R.id.tv_ranking, item.matchRank)

                    item.pagePosition = pagePosition// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)

                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }

                    for (i in 0 until checkBoxes.size) {
                        var isEnable = detailStatus[2] == 0
                        checkBoxes[i].isEnabled = isEnable
                        var realPosition = i + 4
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) realPosition.getCheckBoxText2Show(item.oddsMap) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            var list = arrayListOf<String>()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                                    if (j > 3) {
                                        list.add(checkBoxes[j - 4].text.toString())
                                    }
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)
                            item.selectedBasketString = list

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 竞猜篮球胜分差列表数据适配器
     *
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @return adapter     返回的适配器
     */
    fun getBasketballLotterySFCListAdapter(data: List<BallMatchBean>, pagePosition: Int): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_lottery_sfc, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setGone(R.id.ll_more, item.isShowAnalysis)
                    helper.setImageResource(R.id.iv_xi, if (item.isShowAnalysis) R.mipmap.ic_analysis_upward else R.mipmap.ic_analysis_down)

                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_match_league, item.league)
                    helper.setText(R.id.tv_match_deadline, item.closeTime + "截止")
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var actionGotoExpand = helper.getView<TextView>(R.id.action_goto_expand)
                    actionGotoExpand.text = if (item.selectedLabel.isNotEmpty()) item.selectedLabel else "点击投注"
                    actionGotoExpand.setTextColor(if (item.selectedLabel.isNotEmpty()) Color.parseColor("#ffffff") else Color.parseColor("#999999"))
                    actionGotoExpand.setBackgroundColor(if (item.selectedLabel.isNotEmpty()) Color.parseColor("#D60000") else Color.parseColor("#ffffff"))

                    item.pagePosition = pagePosition // 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    if (!isEmpty(item.overallRecord)) {
                        val sc = Scanner(item.overallRecord)
                        val str = sc.nextLine()
                        val arr = ArrayList<String>()
                        for (s in str.replace("[^0-9]".toRegex(), "").split("".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.isNotEmpty()) {
                                arr.add(s)
                            }
                        }
                        helper.setText(R.id.tv_history_record, item.overallRecord)
                    }
                    if (!isEmpty(item.recentRecord)) {
                        helper.setText(R.id.tv_recent_record, item.recentRecord)
                    }
                    helper.setText(R.id.tv_ranking, item.matchRank)

                    helper.addOnClickListener(R.id.action_expand)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_analysis)
                }
            }
        }
    }

    /**
     * 竞猜篮球混合选择列表数据适配器
     *
     * @param context      上下文
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @param listener     监听事件
     * @return adapter     返回的适配器
     */
    fun getBasketballSelectedHHListAdapter(context: Context, data: List<BallMatchBean>, pagePosition: Int, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_selected_hh, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    var tvMatchScore = helper.getView<TextView>(R.id.tv_match_score)
                    tvMatchScore.text = "（" + item.score + "）"
                    tvMatchScore.setTextColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))
                    var tvMatchScore2 = helper.getView<TextView>(R.id.tv_match_score2)
                    tvMatchScore2.setBackgroundColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))

                    item.pagePosition = pagePosition// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    val passStatus = item.passStatus?.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }
                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }

                    var isEnable0 = if (item.pagePosition == 1) passStatus[0] == 1 else detailStatus[0] == 0
                    var isEnable1 = if (item.pagePosition == 1) passStatus[1] == 1 else detailStatus[1] == 0

                    // 设置禁用状态
                    helper.setGone(R.id.line0, isEnable0)
                    helper.setGone(R.id.action_select0, isEnable0)
                    helper.setGone(R.id.action_select1, isEnable0)
                    helper.setGone(R.id.tv_line0, !isEnable0)
                    helper.setGone(R.id.line1, isEnable1)
                    helper.setGone(R.id.action_select2, isEnable1)
                    helper.setGone(R.id.action_select3, isEnable1)
                    helper.setGone(R.id.tv_line1, !isEnable1)

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)

                    for (i in 0 until checkBoxes.size) {
                        var isEnable = when (i) {
                            0, 1 -> if (item.pagePosition == 1) passStatus[0] == 1 else detailStatus[0] == 0
                            2, 3 -> if (item.pagePosition == 1) passStatus[1] == 1 else detailStatus[1] == 0
                            else -> true
                        }
                        checkBoxes[i].isEnabled = isEnable
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        checkBoxes[i].isChecked = item.selected!![i] == true
                        checkBoxes[i].text = if (isEnable) i.getCheckBoxText2Show(item.oddsMap) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![i] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.setGone(R.id.tv_line2, item.selectedBasketString == null || item.selectedBasketString!!.isEmpty())
                    helper.setGone(R.id.rv_content, item.selectedBasketString != null && !item.selectedBasketString!!.isEmpty())

                    if (item.selectedBasketString != null) {
                        var rvContent = helper.getView<RecyclerView>(R.id.rv_content)
                        var adapter = getBasketballSelectedChildListAdapter(item.selectedBasketString!!)
                        rvContent.layoutManager = GridLayoutManager(context, 2)
                        // 需要在setLayoutManager()之后调用addItemDecoration()
                        if (rvContent.itemDecorationCount > 0) {
                            rvContent.removeItemDecorationAt(0)
                        }
                        rvContent.addItemDecoration(GridSpacingItemDecoration(2, dp2px(0f), false))
                        rvContent.adapter = adapter
                    }

                    helper.addOnClickListener(R.id.action_remove)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.ll_match)
                }
            }
        }
    }

    /**
     * 竞猜篮球选择列表数据适配器
     *
     * @param data         数据列表
     * @return adapter     返回的适配器
     */
    fun getBasketballSelectedChildListAdapter(data: List<String>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_basketball_selected_hh_child, data) {
            override fun convert(helper: BaseViewHolder, item: String) {
                if (item != null) {
                    helper.setText(R.id.action_select, item)
                    helper.addOnClickListener(R.id.action_select)
                }
            }
        }
    }

    /**
     * 竞猜篮球胜负选择列表数据适配器
     *
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @param listener     监听事件
     * @return adapter     返回的适配器
     */
    fun getBasketballSelectedSFListAdapter(data: List<BallMatchBean>, pagePosition: Int, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_selected_sf, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    if (pagePosition == 3) {
                        var tvMatchScore = helper.getView<TextView>(R.id.tv_match_score)
                        tvMatchScore.text = "（" + item.score + "）"
                        tvMatchScore.setTextColor(if (item.score.contains("-")) Color.parseColor("#009D48") else Color.parseColor("#F20C00"))
                    }

                    item.pagePosition = pagePosition// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)

                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }

                    for (i in 0 until checkBoxes.size) {
                        var isEnable = when (pagePosition) {
                            2 -> detailStatus[0] == 0
                            3 -> detailStatus[1] == 0
                            else -> detailStatus[0] == 0
                        }
                        checkBoxes[i].isEnabled = isEnable
                        var realPosition = when (pagePosition) {
                            2 -> i
                            3 -> i + 2
                            else -> i
                        }
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) realPosition.getCheckBoxText2Show(item.oddsMap) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_remove)
                }
            }
        }
    }

    /**
     * 竞猜篮球大小分选择列表数据适配器
     *
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @param listener     监听事件
     * @return adapter     返回的适配器
     */
    fun getBasketballSelectedDXFListAdapter(data: List<BallMatchBean>, pagePosition: Int, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_selected_dxf, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)
                    helper.setText(R.id.tv_total_score, item.totalScore)

                    item.pagePosition = pagePosition// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)

                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }

                    for (i in 0 until checkBoxes.size) {
                        var isEnable = detailStatus[2] == 0
                        checkBoxes[i].isEnabled = isEnable
                        var realPosition = i + 4
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) realPosition.getCheckBoxText2Show(item.oddsMap) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_remove)
                }
            }
        }
    }

    /**
     * 竞猜篮球胜分差选择列表数据适配器
     *
     * @param context      上下文
     * @param data         数据列表
     * @param pagePosition 页面位置
     * @param listener     监听事件
     * @return adapter     返回的适配器
     */
    fun getBasketballSelectedSFCListAdapter(context: Context, data: List<BallMatchBean>, pagePosition: Int, listener: SelectBallChangeListener): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallMatchBean, BaseViewHolder>(R.layout.item_basketball_selected_sfc, data) {
            override fun convert(helper: BaseViewHolder, item: BallMatchBean) {
                if (item != null) {
                    helper.setText(R.id.tv_match_field, item.matchField)
                    helper.setText(R.id.tv_host_team, item.hostTeam)
                    helper.setText(R.id.tv_guest_team, item.guestTeam)

                    item.pagePosition = pagePosition// 每个适配器对应的页面都不相同
                    item.adapterPosition = helper.adapterPosition

                    var actionSelect0 = helper.getView<CheckBox>(R.id.action_select0)
                    var actionSelect1 = helper.getView<CheckBox>(R.id.action_select1)
                    var actionSelect2 = helper.getView<CheckBox>(R.id.action_select2)
                    var actionSelect3 = helper.getView<CheckBox>(R.id.action_select3)
                    var actionSelect4 = helper.getView<CheckBox>(R.id.action_select4)
                    var actionSelect5 = helper.getView<CheckBox>(R.id.action_select5)
                    var actionSelect6 = helper.getView<CheckBox>(R.id.action_select6)
                    var actionSelect7 = helper.getView<CheckBox>(R.id.action_select7)
                    var actionSelect8 = helper.getView<CheckBox>(R.id.action_select8)
                    var actionSelect9 = helper.getView<CheckBox>(R.id.action_select9)
                    var actionSelect10 = helper.getView<CheckBox>(R.id.action_select10)
                    var actionSelect11 = helper.getView<CheckBox>(R.id.action_select11)

                    var checkBoxes = ArrayList<CheckBox>()
                    checkBoxes.add(actionSelect0)
                    checkBoxes.add(actionSelect1)
                    checkBoxes.add(actionSelect2)
                    checkBoxes.add(actionSelect3)
                    checkBoxes.add(actionSelect4)
                    checkBoxes.add(actionSelect5)
                    checkBoxes.add(actionSelect6)
                    checkBoxes.add(actionSelect7)
                    checkBoxes.add(actionSelect8)
                    checkBoxes.add(actionSelect9)
                    checkBoxes.add(actionSelect10)
                    checkBoxes.add(actionSelect11)

                    val detailStatus = item.detailStatus.split(',')?.map {
                        try {
                            it.toInt()
                        } catch (e: NumberFormatException) {
                            1
                        }
                    }

                    for (i in 0 until checkBoxes.size) {
                        var isEnable = detailStatus[3] == 0
                        checkBoxes[i].isEnabled = isEnable
                        var realPosition = i + 6
                        checkBoxes[i].setOnCheckedChangeListener(null)// 重置选中事件
                        checkBoxes[i].isChecked = item.selected!![realPosition] == true
                        checkBoxes[i].text = if (isEnable) realPosition.getCheckedStyleText2(context, item.oddsMap, checkBoxes[i].isChecked) else "未开售"
                        checkBoxes[i].setOnCheckedChangeListener { _, isChecked ->
                            item.selected!![realPosition] = isChecked
                            checkBoxes[i].text = if (isEnable) realPosition.getCheckedStyleText2(context, item.oddsMap, checkBoxes[i].isChecked) else "未开售"

                            var selectedNum = 0
                            var sbLabel = StringBuffer()
                            for (j in 0 until item.selected!!.size) {
                                if (item.selected!![j] == true) {
                                    selectedNum++
                                    sbLabel.append(j.getCheckBoxTextValueLabel() + ", ")
                                }
                            }
                            item.selectedNumber = selectedNum
                            item.selectedLabel = sbLabel.toString().substring(0, if (item.selectedNumber > 0) sbLabel.length - 2 else 0)

                            listener?.onItemSelectChange(item)
                        }
                    }

                    helper.addOnClickListener(R.id.action_remove)
                }
            }
        }
    }


    /**
     * 奖金优化选择列表数据适配器
     *
     * @param context      上下文
     * @param data         数据列表
     * @param isBasketBall 是否为篮球
     * @return adapter     返回的适配器
     */
    fun getBonusOptimizationListAdapter(context: Context, data: List<BallSelectedBean>, isBasketBall: Boolean): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallSelectedBean, BaseViewHolder>(R.layout.item_bonus_optimization, data) {
            override fun convert(helper: BaseViewHolder, item: BallSelectedBean) {
                if (item != null) {
                    var etBunchNumber = helper.getView<EditText>(R.id.et_bunch_number)
                    var tvTotalProfit = helper.getView<TextView>(R.id.tv_total_profit)
                    var rvContent0 = helper.getView<RecyclerView>(R.id.rv_content0)
                    var rvContent1 = helper.getView<RecyclerView>(R.id.rv_content1)

                    if (etBunchNumber.tag != null) {
                        (etBunchNumber.removeTextChangedListener(etBunchNumber.getTag() as TextWatcher))
                    }
                    var textWatcher = object : TextWatcher {
                        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                        }

                        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                        }

                        override fun afterTextChanged(s: Editable) {
                            item.betNum = s.toString().trim { it <= ' ' }
                        }
                    }
                    etBunchNumber.addTextChangedListener(textWatcher)
                    etBunchNumber.tag = textWatcher

                    helper.setText(R.id.tv_profit, "单注" + item.singlePrize)
                    helper.setGone(R.id.ll_show, item.isShowList)
                    helper.setGone(R.id.rl_show, item.isShowList)

                    etBunchNumber.setText(item.betNum)
                    tvTotalProfit.text = "预计" + item.totalPrize

                    var list0 = ArrayList<String>()
                    for (i in 0 until item.selectedList.size) {
                        var item = item.selectedList[i]
                        list0.add(item.week + item.matchField + "|" + item.betName + "|" + item.profitOdds)
                    }
                    var adapter0 = getBonusOptimizationContent0ListAdapter(list0)
                    rvContent0.layoutManager = LinearLayoutManager(context)
                    // 需要在setLayoutManager()之后调用addItemDecoration()
                    if (rvContent0.itemDecorationCount > 0) {
                        rvContent0.removeItemDecorationAt(0)
                    }
                    rvContent0.adapter = adapter0

                    var adapter1 = getBonusOptimizationContent1ListAdapter(item.selectedList, isBasketBall)
                    rvContent1.layoutManager = LinearLayoutManager(context)
                    // 需要在setLayoutManager()之后调用addItemDecoration()
                    if (rvContent1.itemDecorationCount > 0) {
                        rvContent1.removeItemDecorationAt(0)
                    }
                    rvContent1.adapter = adapter1

                    helper.addOnClickListener(R.id.action_minus)
                    helper.addOnClickListener(R.id.action_add)
                    helper.addOnClickListener(R.id.action_goto_expand)
                    helper.addOnClickListener(R.id.action_goto_expand2)
                }
            }
        }
    }

    /**
     * 奖金优化选择列表1数据适配器
     *
     * @param data         数据列表
     * @return adapter     返回的适配器
     */
    fun getBonusOptimizationContent0ListAdapter(data: List<String>): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_bonus_optimization_content0, data) {
            override fun convert(helper: BaseViewHolder, item: String) {
                if (item != null) {
                    helper.setText(R.id.tv_content, item)
                }
            }
        }
    }

    /**
     * 竞猜篮球选择列表2数据适配器
     *
     * @param data         数据列表
     * @param isBasketBall 是否为篮球
     * @return adapter     返回的适配器
     */
    fun getBonusOptimizationContent1ListAdapter(data: List<BallSelectedItemBean>, isBasketBall: Boolean): BaseQuickAdapter<*, *> {
        return object : BaseQuickAdapter<BallSelectedItemBean, BaseViewHolder>(R.layout.item_bonus_optimization_content1, data) {
            override fun convert(helper: BaseViewHolder, item: BallSelectedItemBean) {
                if (item != null) {
                    helper.setText(R.id.tv_match_info, item.week + item.matchField)
                    helper.setText(R.id.tv_match_team, if (isBasketBall) item.guestTeam + " VS " + item.hostTeam else item.hostTeam + " VS " + item.guestTeam)
                    helper.setText(R.id.tv_profit_rate, item.betName + " " + item.profitOdds)
                }
            }
        }
    }
}