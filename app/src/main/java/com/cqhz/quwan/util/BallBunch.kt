package com.cqhz.quwan.util

data class BallBunch(
        val code: String, // 编码
        val name: String,// 名称
        var selected: Boolean = false// 是否选中
)