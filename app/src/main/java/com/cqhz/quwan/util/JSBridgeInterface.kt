package com.cqhz.quwan.util

import android.webkit.JavascriptInterface

class JSBridgeInterface {
    var gotoPageCallBack: ((String, String) -> Unit)? = null
    var toPayCallBack: ((String, String, String) -> Unit)? = null
    var toBettingCallBack: ((String, String) -> Unit)? = null
    var finishCallBack: (() -> Unit)? = null
    var goToTarget: ((String) -> Unit)? = null
    var goHomeCallBack: (() -> Unit)? = null
    var toLoginCallBack: (() -> Unit)? = null
    var postOrderCallBack: ((String) -> Unit)? = null
    var callMtchAnalysis: ((String, String) -> Unit)? = null // 开奖吊起赛事分析

    /**
     * web页面跳转的公用方法
     * 方法名：gotoPage
     * 参数名1：pageId 返回值为需要跳转的页面名称（如：1 —>充值页面）
     * 参数名2：params 返回值为携带的字符串参数（如："newsId=1092,userId=8667119885615109"）
     */
    @JavascriptInterface
    fun gotoPage(pageId: String, params: String) {
        gotoPageCallBack?.invoke(pageId, params)
    }

    @JavascriptInterface
    fun goPay(orderId: String, periodId: String, typeId: String) {
        toPayCallBack?.invoke(orderId, periodId, typeId)
    }

    @JavascriptInterface()
    fun touzhu(typeId: String, periodId: String) {
        toBettingCallBack?.invoke(periodId, typeId)
    }

    @JavascriptInterface()
    fun goBack() {
        finishCallBack?.invoke()
    }

    @JavascriptInterface()
    fun goToTarget(flag: String) {
        goToTarget?.invoke(flag)
    }

    @JavascriptInterface()
    fun goHome() {
        goHomeCallBack?.invoke()
    }

    @JavascriptInterface()
    fun toLogin() {
        toLoginCallBack?.invoke()
    }

    @JavascriptInterface
    fun postOrder(json: String) {
        postOrderCallBack?.invoke(json)
    }

    /**
     * // 开奖吊起赛事分析
     */
    @JavascriptInterface
    fun callMtchAnalysis(matchId: String, leagueId: String) {
        callMtchAnalysis?.invoke(matchId, leagueId)
    }
}