package com.cqhz.quwan.util;

import com.cqhz.quwan.model.LoginBean;


public interface LoginSubscribe {
    void login(LoginBean loginBean);
}
