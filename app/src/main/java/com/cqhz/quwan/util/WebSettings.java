package com.cqhz.quwan.util;

import android.annotation.SuppressLint;

import com.blankj.utilcode.util.NetworkUtils;
import com.tencent.smtt.sdk.WebView;

/**
 * @author whamu2
 * @date 2018/7/27
 */
public class WebSettings {

    @SuppressLint("SetJavaScriptEnabled")
    public static void initWebViewSettings(WebView webView) {
        com.tencent.smtt.sdk.WebSettings webSetting = webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(com.tencent.smtt.sdk.WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(true);
        webSetting.setGeolocationEnabled(true);
        // 缓存
        webSetting.setAppCacheEnabled(true);
        webSetting.setDatabaseEnabled(true);
        webSetting.setDomStorageEnabled(true); //开启DOM缓存，关闭的话H5自身的一些操作是无效的
        if (NetworkUtils.isConnected()) {
            webSetting.setCacheMode(com.tencent.smtt.sdk.WebSettings.LOAD_NO_CACHE);
        } else {
            webSetting.setCacheMode(com.tencent.smtt.sdk.WebSettings.LOAD_CACHE_ONLY); //不使用网络，只加载缓存
        }
        webSetting.setLoadsImagesAutomatically(true); // 网页加载完成过后在加载图片
    }
}