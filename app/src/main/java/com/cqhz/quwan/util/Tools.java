package com.cqhz.quwan.util;

import com.cqhz.quwan.service.API;

public class Tools {
    public static String urlCV(String url){
        if(url.startsWith("http:")||url.startsWith("https:")||url.startsWith("www."))
            return url;
        else
            return API.HOST+url;
    }
}
