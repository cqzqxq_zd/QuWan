package com.cqhz.quwan.wxapi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.ShowMessageFromWX;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.cqhz.quwan.APP;
import com.cqhz.quwan.ActivityInject;
import com.cqhz.quwan.R;
import com.cqhz.quwan.model.LoginBean;
import com.cqhz.quwan.service.WeiChatService;

import javax.inject.Inject;

import me.militch.quickcore.core.HasDaggerInject;
import me.militch.quickcore.event.RespEvent;
import me.militch.quickcore.mvp.model.ModelHelper;
import me.militch.quickcore.util.ApiException;
import me.militch.quickcore.util.ResPreHandler;
import me.militch.quickcore.util.RespBase;

import static com.cqhz.quwan.common.KeyContract.WEICHAT_APP_ID;


public class WXEntryActivity extends AppCompatActivity implements IWXAPIEventHandler, HasDaggerInject<ActivityInject> {
    private static final String TAG = "WXEntryActivity";

    private IWXAPI api;

    @Inject
    ModelHelper mHelper;

    @Override
    public void inject(ActivityInject activityInject) {
        activityInject.inject(this);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, WXEntryActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.weiapi_entry);
        api = WXAPIFactory.createWXAPI(this, WEICHAT_APP_ID, false);
        api.registerApp(WEICHAT_APP_ID);
        api.handleIntent(getIntent(), this);
        overridePendingTransition(R.anim.fade_in, 0);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
        switch (req.getType()) {
            case ConstantsAPI.COMMAND_GETMESSAGE_FROM_WX:
                break;
            case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
                ShowMessageFromWX.Req req1 = (ShowMessageFromWX.Req) req;
                break;
            case ConstantsAPI.COMMAND_LAUNCH_BY_WX:
                break;
            default:
                break;
        }
    }

    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
            SendAuth.Resp var0 = (SendAuth.Resp) resp;

            LogUtils.e(TAG, "code = " + var0.code);
            LogUtils.e(TAG, "state = " + var0.state);

            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    LoginBean loginInfo = APP.Companion.get().getLoginInfo();
                    if (loginInfo == null) {
                        return;
                    }
                    mHelper.requestByPreHandler(mHelper.getService(WeiChatService.class).getWeichatToken(loginInfo.getUserId(), var0.code),
                            new RespEvent<String>() {
                                @Override
                                public void isOk(String data) {
                                    ToastUtils.showLong("微信绑定成功");
                                    finish();
                                }

                                @Override
                                public void isError(ApiException apiException) {
                                    ToastUtils.showLong("微信绑定失败");
                                    finish();
                                }
                            }, new ResPreHandler<RespBase<String>, String>() {
                                @Override
                                public String process(RespBase<String> respBase) throws Exception {
                                    return "";
                                }
                            });
//                    mHelper.requestByPreHandler(
//                            mHelper.getService(WeiChatService.class).getWeichatToken(loginInfo.getUserId(), var0.code),
//                            new EventCall<WeiChatAccessTokenData>() {
//                                @Override
//                                public void call(WeiChatAccessTokenData data) {
//                                    ToastUtils.showLong("微信绑定成功");
//                                    finish();
//                                }
//                            },
//                            new EventCall<ApiException>() {
//                                @Override
//                                public void call(ApiException e) {
//                                    ToastUtils.showLong("微信绑定失败");
//                                    finish();
//                                }
//                            });
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    ToastUtils.showLong("已取消微信绑定");
                    finish();
                    break;
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                    ToastUtils.showLong("已取拒绝信绑定");
                    finish();
                    break;
                default:
                    finish();
                    break;
            }

        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.fade_out);
    }
}