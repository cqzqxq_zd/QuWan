package com.cqhz.quwan.wxapi

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import com.tencent.mm.opensdk.constants.ConstantsAPI
import com.tencent.mm.opensdk.modelbase.BaseReq
import com.tencent.mm.opensdk.modelbase.BaseResp
import com.tencent.mm.opensdk.openapi.IWXAPI
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.cqhz.quwan.R
import com.cqhz.quwan.common.KeyContract
import com.cqhz.quwan.ui.buy.event.EventObj
import com.cqhz.quwan.util.CLogger
import org.greenrobot.eventbus.EventBus

class WXPayEntryActivity:AppCompatActivity(), IWXAPIEventHandler {
    private lateinit var api:IWXAPI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.weiapi_entry)
        api = WXAPIFactory.createWXAPI(this, KeyContract.WEICHAT_APP_ID, false)
        api.registerApp(KeyContract.WEICHAT_APP_ID)
        api.handleIntent(intent, this)
        overridePendingTransition(R.anim.fade_in, 0)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        api.handleIntent(intent, this)
    }
    override fun onResp(p0: BaseResp?) {
        val log = CLogger(this::class.java)
        log.e("errorCode","${p0?.errCode}")
        if (p0?.type == ConstantsAPI.COMMAND_PAY_BY_WX) {
            when(p0.errCode){
                BaseResp.ErrCode.ERR_USER_CANCEL -> {
                }
                BaseResp.ErrCode.ERR_OK -> {
                    EventBus.getDefault().post(EventObj(0x998,null))
                }
            }
            finish()
        }
    }

    override fun onReq(p0: BaseReq?) {

    }

}