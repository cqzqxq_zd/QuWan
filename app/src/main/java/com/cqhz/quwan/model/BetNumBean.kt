package com.cqhz.quwan.model

data class BetNumBean(
        val oddsMap: Map<Int, String>?,//赔率 Map
        var selected: LinkedHashSet<Int>? = null
)