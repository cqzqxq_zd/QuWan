package com.cqhz.quwan.model;

/**
 * @author whamu2
 * @date 2018/7/11
 */
public class WeiChatAccessTokenData {

    /**
     * unionId : oIak1wGVpw_0IqWbLoFQXAIZUjHk
     * openId : oRY9-vwWYcvOEzGo_GPBTEVKMYeE
     */

    private String unionId;
    private String openId;

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
