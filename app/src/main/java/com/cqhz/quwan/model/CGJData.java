package com.cqhz.quwan.model;

/**
 * @author whamu2
 * @date 2018/6/12
 */
public class CGJData {


    /**
     * id : 107079228385787905
     * title : 竞猜足球
     * imgUrl : /ZQXQ-LOTTERY/image/201805231134247387071049239.jpg
     * linkUrl : http://192.168.1.107:8020/cpweb/active/judge-winner.html
     * sort : 999
     * status : 1
     * createTime : 2018-05-23 00:00:00
     * updateTime : 2018-06-12 15:06:18
     * clickNum : 43
     */

    private String id;
    private String title;
    private String imgUrl;
    private String linkUrl;
    private String sort;
    private String status;
    private String createTime;
    private String updateTime;
    private String clickNum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getClickNum() {
        return clickNum;
    }

    public void setClickNum(String clickNum) {
        this.clickNum = clickNum;
    }
}
