package com.cqhz.quwan.model


data class OrderBet (
        val redNum:String,
        val blueNum:String,
        val playType:String,
        val betNum:String
)