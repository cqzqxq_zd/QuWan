package com.cqhz.quwan.model

/**
 * 地址数据
 */
data class NewsGameBean(
        val league: String,
        val hostTeam: String,
        val hostLogo: String,
        val guestTeam: String,
        val guestLogo: String,
        val matchId: String,
        val status: String,// 赛事状态0：未开始 1：进行中2：暂停3：已完成4：推迟5：取消
        val matchTime: String,
        val score: String,// "1:1"
        val lid: String
)