package com.cqhz.quwan.model

/**
 * 分页数据
 * Created by Guojing on 2018/9/12.
 */
class PageResultBean<T>(
        val size: Int,
        var pages: Int,
        val total: Int,
        val records: List<T>
)