package com.cqhz.quwan.model

data class TransactionRecordBean(
        val id: String,
        val realName: String,
        val mobile: String,
        val payWay: String,// 支付方式 0-系统；1-支付宝；2-银行卡；3-奖金；4-中奖资金；5-充值资金；6-微信；7-平台赠送资金；8-钻石；99-其它
        val payAccount: String,
        val payAmount: String,
        val remainder: String,
        val type: String,// 交易类型 1-充值；2-提现；3-奖金；4-竞猜；5-退款；6-佣金；7-赠送；8-换钻；9-换物；10-抽奖；11-兑换；
        val status: String,// 状态 1-待审核；2-处理中；3-失败；4-成功
        val behavior: String,
        val updateTime: String,
        val createTime: String
)