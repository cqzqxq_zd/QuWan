package com.cqhz.quwan.model
import android.support.v4.app.Fragment
import com.cqhz.quwan.ui.buy.fragment.BettingCourageFragment
import com.cqhz.quwan.ui.buy.fragment.BettingNormalFragment
import java.io.Serializable
class LottoType {
    var blueBalls:Balls? = null
    var redBalls:Balls? = null
    fun redBalls(ball:Balls.() -> Unit){
        val wrapper = Balls()
        wrapper.ball()
        this.redBalls = wrapper
    }
    fun blueBalls(ball:Balls.() -> Unit){
        val wrapper = Balls()
        wrapper.ball()
        this.blueBalls = wrapper
    }
    fun genFragments():List<Fragment> {
        val normalFragment = BettingNormalFragment
                .new(redBalls,blueBalls)
        val courageFragment = BettingCourageFragment
                .new(redBalls,blueBalls)
        return arrayListOf(normalFragment,courageFragment)
    }
    class Balls:Serializable{
        var dmSelected:List<Int>? = null
        var tmSelected:List<Int>? = null
        var selected:List<Int>? = null
        var label:String? = null
        var tip: String? = null
        var range:List<Int>? = null
        var selectMin:Int? = null
        var selectMax:Int? = null
        var selectDMMax:Int? = null
        var selectDMMin:Int? = null
        var selectTMMax:Int? = null
        var selectTMMin:Int? = null
        var dmLabel:String? = null
        var tmLabel:String? = null
        var dmTip:String? = null
        var tmTip:String? = null
    }
}