package com.cqhz.quwan.model

/**
 * 系统配置数据
 */
data class SystemConfigBean(
        val ckey: String,// RECHARGE_LIST_KEY，GUESS_COMPETITION
        val cval: String,// {"10": "0","20": "5","50": "5","100": "8","200": "8","500": "10"}，"1"显示
        val type: Int// type为1时弹窗，为0时跳转h5，h5地址为cval字段值
)
