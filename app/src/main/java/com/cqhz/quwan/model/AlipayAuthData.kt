package com.cqhz.quwan.model

import java.io.Serializable


data class AlipayAuthData(
        val authCode:String,
        val alipayOpenId:String,
        val userId:String
):Serializable