package com.cqhz.quwan.model

class LotteryList {
    var data = ArrayList<Lottery>()
    fun lottery(init:(Lottery.() -> Unit)){
        val wrapper = Lottery()
        wrapper.init()
        data.add(wrapper)
    }
}