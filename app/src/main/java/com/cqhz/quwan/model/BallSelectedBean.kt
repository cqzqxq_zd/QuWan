package com.cqhz.quwan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * 奖金优化列表数据
 * Created by Guojing on 2018/12/26.
 */
@Parcelize
data class BallSelectedBean(
        var isShowList: Boolean = false,// 是否显示列表
        var betNum: String,// 注数
        var singlePrize: String,// 单注奖金 = 赔率 * 赔率 * 200
        var totalPrize: String,// 预计奖金 = 单注奖金 * 注数
        var selectedList: List<BallSelectedItemBean>
) : Parcelable


@Parcelize
data class BallSelectedItemBean(
        var week: String,
        var matchField: String,// 周场次
        var hostTeam: String,// 主队
        var guestTeam: String,// 客队
        var betName: String,
        var profitOdds: String,
        val redNum: String,
        val period: String
) : Parcelable
