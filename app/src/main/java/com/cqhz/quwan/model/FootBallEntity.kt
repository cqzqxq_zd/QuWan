package com.cqhz.quwan.model

import com.cqhz.quwan.util.toFootBallSelectedText
import java.io.Serializable

data class FootBallEntity(
        val lotteryId: String,
        val date: String,
        val week: String,
        val count: String,
        var match: List<Match>
) : Serializable {
    class Match(
            val code: String?,//赛事编号
            val matchTime: String?,//比赛开始时间
            val closeTime: String?,//购买截止时间
            val matchField: String?,//周场次
            val league: String?,//联赛名称
            val hostTeam: String?,//主队
            val guestTeam: String?,//客队
            val score: String?,//让球数 -1 +1
            val isHot: String?,//是否热门 0-否 1-是
            val odds: String?,//赔率 json字符串S
            val oddsMap: Map<String, String>?,//赔率 Map
            val passStatus: String?,//单关状态(1,1,1,1,0):(spf,rqspf,bf,zjq,bqc),1:有单关玩法 0:无单关玩法
            val detailStatus: String?,//详细状态(1,1,1,1,1):(spf,rqspf,bf,zjq,bqc),0:开售1:未开售
            val overallRecord: String?,//历史交锋
            val recentRecord: String?,//近期战绩
            val avgOdds: String?,//支持率
            val quizNum: String?,//竞猜人数
            val matchId: String?,
            val leagueId: String?//
    ) : Serializable {
        var selected: LinkedHashSet<Int>? = null
        var selectedLable: String? = null

        private fun getTypeOddCbKey(index: Int, t: Int): String {
            return when (index) {
                0 -> when (t) {
                    1 -> "1:0"
                    2 -> "胜"
                    else -> "None"
                }
                1 -> when (t) {
                    1 -> "2:0"
                    2 -> "平"
                    else -> "None"
                }
                2 -> when (t) {
                    1 -> "2:1"
                    2 -> "负"
                    else -> "None"
                }
                3 -> when (t) {
                    1 -> "3:0"
                    2 -> "胜"
                    else -> "None"
                }
                4 -> when (t) {
                    1 -> "3:1"
                    2 -> "平"
                    else -> "None"
                }
                5 -> when (t) {
                    1 -> "3:2"
                    2 -> "负"
                    else -> "None"
                }
                6 -> when (t) {
                    1 -> "4:0"
                    2 -> "1:0"
                    else -> "None"
                }
                7 -> when (t) {
                    1 -> "4:1"
                    2 -> "2:0"
                    else -> "None"
                }
                8 -> when (t) {
                    1 -> "4:2"
                    2 -> "2:1"
                    else -> "None"
                }
                9 -> when (t) {
                    1 -> "5:0"
                    2 -> "3:0"
                    else -> "None"
                }
                10 -> when (t) {
                    1 -> "5:1"
                    2 -> "3:1"
                    else -> "None"
                }
                11 -> when (t) {
                    1 -> "5:2"
                    2 -> "3:2"
                    else -> "None"
                }
                12 -> when (t) {
                    1 -> "胜其他"
                    2 -> "4:0"
                    else -> "None"
                }
                13 -> when (t) {
                    1 -> "0:0"
                    2 -> "4:1"
                    else -> "None"
                }
                14 -> when (t) {
                    1 -> "1:1"
                    2 -> "4:2"
                    else -> "None"
                }
                15 -> when (t) {
                    1 -> "2:2"
                    2 -> "5:0"
                    else -> "None"
                }
                16 -> when (t) {
                    1 -> "3:3"
                    2 -> "5:1"
                    else -> "None"
                }
                17 -> when (t) {
                    1 -> "平其他"
                    2 -> "5:2"
                    else -> "None"
                }
                18 -> when (t) {
                    1 -> "0:1"
                    2 -> "胜其他"
                    else -> "None"
                }
                19 -> when (t) {
                    1 -> "2:0"
                    2 -> "0:0"
                    else -> "None"
                }
                20 -> when (t) {
                    1 -> "1:2"
                    2 -> "1:1"
                    else -> "None"
                }
                21 -> when (t) {
                    1 -> "0:3"
                    2 -> "2:2"
                    else -> "None"
                }
                22 -> when (t) {
                    1 -> "1:3"
                    2 -> "3:3"
                    else -> "None"
                }
                23 -> when (t) {
                    1 -> "2:3"
                    2 -> "平其他"
                    else -> "None"
                }
                24 -> when (t) {
                    1 -> "0:4"
                    2 -> "0:1"
                    else -> "None"
                }
                25 -> when (t) {
                    1 -> "1:4"
                    2 -> "0:2"
                    else -> "None"
                }
                26 -> when (t) {
                    1 -> "2:4"
                    2 -> "1:2"
                    else -> "None"
                }
                27 -> when (t) {
                    1 -> "0:5"
                    2 -> "0:3"
                    else -> "None"
                }
                28 -> when (t) {
                    1 -> "1:5"
                    2 -> "1:3"
                    else -> "None"
                }
                29 -> when (t) {
                    1 -> "2:5"
                    2 -> "2:3"
                    else -> "None"
                }
                30 -> when (t) {
                    1 -> "负其他"
                    2 -> "0:4"
                    else -> "None"
                }
                31 -> when (t) {
                    2 -> "1:4"
                    else -> "None"
                }
                32 -> when (t) {
                    2 -> "2:4"
                    else -> "None"
                }
                33 -> when (t) {
                    2 -> "0:5"
                    else -> "None"
                }
                34 -> when (t) {
                    2 -> "1:5"
                    else -> "None"
                }
                35 -> when (t) {
                    2 -> "2:5"
                    else -> "None"
                }
                36 -> when (t) {
                    2 -> "负其他"
                    else -> "None"
                }
                37 -> when (t) {
                    2 -> "0"
                    else -> "None"
                }
                38 -> when (t) {
                    2 -> "1"
                    else -> "None"
                }
                39 -> when (t) {
                    2 -> "2"
                    else -> "None"
                }
                40 -> when (t) {
                    2 -> "3"
                    else -> "None"
                }
                41 -> when (t) {
                    2 -> "4"
                    else -> "None"
                }
                42 -> when (t) {
                    2 -> "5"
                    else -> "None"
                }
                43 -> when (t) {
                    2 -> "6"
                    else -> "None"
                }
                44 -> when (t) {
                    2 -> "7+"
                    else -> "None"
                }
                45 -> when (t) {
                    2 -> "胜胜"
                    else -> "None"
                }
                46 -> when (t) {
                    2 -> "胜平"
                    else -> "None"
                }
                47 -> when (t) {
                    2 -> "胜负"
                    else -> "None"
                }
                48 -> when (t) {
                    2 -> "平胜"
                    else -> "None"
                }
                49 -> when (t) {
                    2 -> "平平"
                    else -> "None"
                }
                50 -> when (t) {
                    2 -> "平负"
                    else -> "None"
                }
                51 -> when (t) {
                    2 -> "负胜"
                    else -> "None"
                }
                52 -> when (t) {
                    2 -> "负平"
                    else -> "None"
                }
                53 -> when (t) {
                    2 -> "负负"
                    else -> "None"
                }
                else -> "None"
            }
        }

        private fun findSelectedKey(value: Int, t: Int): Int {
            return when (value) {
                0 -> when (t) {
                    2 -> 0
                    else -> -1
                }
                1 -> when (t) {
                    2 -> 1
                    else -> -1
                }
                2 -> when (t) {
                    2 -> 2
                    else -> -1
                }
                3 -> when (t) {
                    2 -> 3
                    else -> -1
                }
                4 -> when (t) {
                    2 -> 4
                    else -> -1
                }
                5 -> when (t) {
                    2 -> 5
                    else -> -1
                }
                6 -> when (t) {
                    2 -> 45
                    else -> -1
                }
                7 -> when (t) {
                    2 -> 46
                    else -> -1
                }
                8 -> when (t) {
                    2 -> 47
                    else -> -1
                }
                9 -> when (t) {
                    2 -> 48
                    else -> -1
                }
                10 -> when (t) {
                    2 -> 49
                    else -> -1
                }
                11 -> when (t) {
                    2 -> 50
                    else -> -1
                }
                12 -> when (t) {
                    2 -> 51
                    else -> -1
                }
                13 -> when (t) {
                    2 -> 52
                    else -> -1
                }
                14 -> when (t) {
                    2 -> 53
                    else -> -1
                }
                15 -> when (t) {
                    1 -> 0
                    2 -> 6
                    else -> -1
                }
                16 -> when (t) {
                    1 -> 1
                    2 -> 7
                    else -> -1
                }
                17 -> when (t) {
                    1 -> 2
                    2 -> 8
                    else -> -1
                }
                18 -> when (t) {
                    1 -> 3
                    2 -> 9
                    else -> -1
                }
                19 -> when (t) {
                    1 -> 4
                    2 -> 10
                    else -> -1
                }
                20 -> when (t) {
                    1 -> 5
                    2 -> 11
                    else -> -1
                }
                21 -> when (t) {
                    1 -> 6
                    2 -> 12
                    else -> -1
                }
                22 -> when (t) {
                    1 -> 7
                    2 -> 13
                    else -> -1
                }
                23 -> when (t) {
                    1 -> 8
                    2 -> 14
                    else -> -1
                }
                24 -> when (t) {
                    1 -> 9
                    2 -> 15
                    else -> -1
                }
                25 -> when (t) {
                    1 -> 10
                    2 -> 16
                    else -> -1
                }
                26 -> when (t) {
                    1 -> 11
                    2 -> 17
                    else -> -1
                }
                27 -> when (t) {
                    1 -> 12
                    2 -> 18
                    else -> -1
                }
                28 -> when (t) {
                    1 -> 13
                    2 -> 19
                    else -> -1
                }
                29 -> when (t) {
                    1 -> 14
                    2 -> 20
                    else -> -1
                }
                30 -> when (t) {
                    1 -> 15
                    2 -> 21
                    else -> -1
                }
                31 -> when (t) {
                    1 -> 16
                    2 -> 22
                    else -> -1
                }
                32 -> when (t) {
                    1 -> 17
                    2 -> 23
                    else -> -1
                }
                33 -> when (t) {
                    1 -> 18
                    2 -> 24
                    else -> -1
                }
                34 -> when (t) {
                    1 -> 19
                    2 -> 25
                    else -> -1
                }
                35 -> when (t) {
                    1 -> 20
                    2 -> 26
                    else -> -1
                }
                36 -> when (t) {
                    1 -> 21
                    2 -> 27
                    else -> -1
                }
                37 -> when (t) {
                    1 -> 22
                    2 -> 28
                    else -> -1
                }
                38 -> when (t) {
                    1 -> 23
                    2 -> 29
                    else -> -1
                }
                39 -> when (t) {
                    1 -> 24
                    2 -> 30
                    else -> -1
                }
                40 -> when (t) {
                    1 -> 25
                    2 -> 31
                    else -> -1
                }
                41 -> when (t) {
                    1 -> 26
                    2 -> 32
                    else -> -1
                }
                42 -> when (t) {
                    1 -> 27
                    2 -> 33
                    else -> -1
                }
                43 -> when (t) {
                    1 -> 28
                    2 -> 34
                    else -> -1
                }
                44 -> when (t) {
                    1 -> 29
                    2 -> 35
                    else -> -1
                }
                45 -> when (t) {
                    1 -> 30
                    2 -> 36
                    else -> -1
                }
                46 -> when (t) {
                    2 -> 37
                    else -> -1
                }
                47 -> when (t) {
                    2 -> 38
                    else -> -1
                }
                48 -> when (t) {
                    2 -> 39
                    else -> -1
                }
                49 -> when (t) {
                    2 -> 40
                    else -> -1
                }
                50 -> when (t) {
                    2 -> 41
                    else -> -1
                }
                51 -> when (t) {
                    2 -> 42
                    else -> -1
                }
                52 -> when (t) {
                    2 -> 43
                    else -> -1
                }
                53 -> when (t) {
                    2 -> 44
                    else -> -1
                }
                else -> -1
            }
        }

        private fun findSelectedKeys(t: Int): List<String> {
            return selected?.sorted()!!.map {
                getTypeOddCbKey(findSelectedKey(it, t), t)
            }
        }

        fun buildSelectedLabel(t: Int): String {
            return findSelectedKeys(t).toFootBallSelectedText()
        }
    }
}