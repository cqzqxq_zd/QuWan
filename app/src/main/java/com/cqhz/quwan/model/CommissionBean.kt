package com.cqhz.quwan.model

data class CommissionBean(
    var auditor: String,
    var behavior: String,
    var createTime: String,
    var id: String,
    var idCard: String,
    var info: String,
    var mobile: String,
    var payAccount: String,
    var payAmount: String,
    var payWay: String,
    var realName: String,
    var remainder: String,
    var status: String,
    var type: String,
    var updateTime: String,
    var userId: String
)

data class CommissionInfoBean(
        var accumulatedCommission: Int,
        var thirtyCommission: Int,
        var todayCommission: Int
)