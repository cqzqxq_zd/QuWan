package com.cqhz.quwan.model.news

import com.cqhz.quwan.model.BannerItem

class NewsBannerBean(list: List<BannerItem>) : NewsBaseBean() {
    private var list: List<BannerItem> = list

    fun getList(): List<BannerItem> {
        return list
    }

    fun setList(list: List<BannerItem>) {
        this.list = list
    }

    override val itemType: Int
        get() = TYPE_AD
}