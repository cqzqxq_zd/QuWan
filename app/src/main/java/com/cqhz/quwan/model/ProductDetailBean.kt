package com.cqhz.quwan.model

/**
 * 商品详情数据
 * Created by Guojing on 2018/9/18.
 */

data class ProductDetailBean(
        val userInfo: UserInfo,
        val productInfo: ProductInfo,
        val addressInfo: AddressInfo
)

data class UserInfo(
        val userId: String,
        val equipmentId: String,
        val phoneModel: String,
        val mobile: String,
        val headAddress: String,
        val nickName: String,
        val realName: String,
        val idCard: String,
        val age: String,
        val sex: String,
        val city: String,
        val createTime: String,
        val updateTime: String,
        val alipayAccount: String,
        val balance: String,
//        val lotBean: String,
        val bonus: String,
        val diamonds: String,
        val realAuth: String,
        val alipayBind: String,
        val wechatBind: String,
        val consumeStatus: String,
        val consumeNum: String,
        val consumeSum: String,
        val prizeNum: String,
        val prizeSum: String,
        val paySum: String,
        val payPwd: String,
        val activityId: String,
        val source: String,
        val channel: String,
        val loginTime: String,
        val status: String,
        val inviterMobile: String,
        val inviterId: String,
        val couponNum: String,
        val cashAble: String,
        val enabled: String,
        val isExpert: String,
        val officialQQGroup: String,
        val toDaySignStatus: String,
        val addressId: String,
        val exchangeDiamondRatio: String
)

data class ProductInfo(
        val id: String,
        val calss: Int,
        val type: String,
        val detailType: String,
        val model: String,
        val color: String,
        val version: String,
        val description: String,
        val longDescription: String,
        val imgUrl: String,
        val price: String,
        val exchangeRate: String,
        val status: Int,
        val createTime: String,
        val updateTime: String
)

data class AddressInfo(
        val id: String,
        val receiver_name: String,
        val phone_number: String,
        val province: String,
        val city: String,
        val county: String,
        val street: String,
        val detail_address: String
)