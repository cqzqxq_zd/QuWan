package com.cqhz.quwan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * 订单详情数据
 * Created by Guojing on 2018/11/30.
 */
@Parcelize
data class OrderInfoBean(
        var afterTax: String,
        var append: String,
        var betAppend: String,
        var betBunch: String,
        var betNum: String,
        var betTimes: String,
        var buyWay: String,
        var commission: String,
        var commissionRate: String,
        var createTime: String,
        var djMatchName: String,
        var djPlayDescribe: String,
        var djSelectTeam: String,
        var expectedBonus: String,
        var followOrderId: String,
        var followStatus: Int,
        var guaranteeOdds: String,
        var heelMultiple: String,
        var id: String,
        var idCard: String,
        var isPublishFollow: String,
        var lotteryId: String,
        var lotteryName: String,
        var lotteryNum: String,
        var lotteryTime: String,
        var mobile: String,
        var orderBetJson: String,
        var orderBetList: List<OrderBetBean>,
        var payAccount: String,
        var payAmount: String,
        var payProfitOdds: String,
        var payWay: String,
        var period: String,
        var prize: String,
        var pushOrderDetailVO: PushOrderDetailInfoBean,
        var realName: String,
        var secrecySet: String,
        var status: Int,
        var ticketList: String,
        var ticketShow: String,
        var ticketStatus: Int,
        var updateTime: String,
        var userId: String,
        var userInfo: UserInfoBean,
        var warning: String,
        var winTime: String,
        var isBonusOptimize: Int,
        var bonusOptimizeType: Int
) : Parcelable

/**
 * 订单信息
 */
@Parcelize
data class OrderBetBean(
        var afterTax: String,
        var betNum: String,
        var betTimes: String,
        var blueNum: String,
        var createTime: String,
        var id: String,
        var lotteryId: String,
        var lotteryName: String,
        var lotteryTime: String,
        var orderId: String,
        var period: String,
        var playType: String,
        var prize: String,
        var redNum: String,
        var status: String,
        var updateTime: String,
        var userId: String,
        var winInfo: String
) : Parcelable

/**
 * 足彩投注信息
 */
@Parcelize
data class OrderBetInfoBean(
        var betList: List<BetBean>,
        var guestTeam: String,
        var hostTeam: String,
        var letNum: String,
        var totalScore: String,
        var matchCode: String,
        var matchField: String,
        var playType: Int,
        var matchResult: String,
        var matchResultInfo: String
) : Parcelable

/**
 * 足彩投注子项信息
 */
@Parcelize
data class BetBean(
        var betNum: Int,
        var odds: String
) : Parcelable

/**
 * 推单信息
 */
@Parcelize
data class PushOrderDetailInfoBean(
        var betTimes: Int,
        var betType: String,
        var closeTime: String,
        var commission: String,
        var commissionRate: String,
        var createTime: String,
        var expertName: String,
        var followAmt: Int,
        var followStatus: String,
        var follows: Int,
        var minFollowAmt: String,
        var openStatus: Int,
        var orderId: String,
        var payAmt: String,
        var profitOdds: String,
        var projectId: String,
        var singleAmt: String,
        var sp: String
) : Parcelable

/**
 * 电竞订单信息
 */
@Parcelize
data class ESportOrderBetInfoBean(
        var betDescribe: String,
        var betOdds: String,
        var leftTeam: String,
        var leftTeamLogo: String,
        var matchName: String,
        var playDescribe: String,
        var rightTeam: String,
        var rightTeamLogo: String,
        var round: String,
        var rounds: String,
        var status: String,
        var type: Int
) : Parcelable

