package com.cqhz.quwan.model

import java.io.Serializable

/**
 * 商品数据
 * Created by Guojing on 2018/9/12.
 */
class ProductBean(
        val id: String,
        val calss: String,// 商品种类 0-实体商品；1-数字商品；
        val type: String,
        val detailType: String,
        val model: String,
        val color: String,
        val version: String,
        val description: String,
        val longDescription: String,
        val imgUrl: String,
        val price: Double,
        val exchangeRate: Double,
        val status: Int
) : Serializable