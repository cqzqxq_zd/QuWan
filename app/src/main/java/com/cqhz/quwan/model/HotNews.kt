package com.cqhz.quwan.model

/**
 * 热门新闻-实体
 * Created by WYZ on 2018/3/14.
 */
data class HotNews(
        val id:String,
        // 图片地址
        val imgUrl:String,
        // 新闻标题
        val title:String,
        // 新闻作者
        val author:String,
        // 阅读量
        val reader:String
)