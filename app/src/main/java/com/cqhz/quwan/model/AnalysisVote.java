package com.cqhz.quwan.model;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class AnalysisVote {

    /**
     * code : 109029
     * vote_type : 0
     * user_id : 100101
     */

    private double voteFlat;
    private double voteWin;
    private double voteNegative;
    private VoteBean vote;

    public double getVoteFlat() {
        return voteFlat;
    }

    public void setVoteFlat(double voteFlat) {
        this.voteFlat = voteFlat;
    }

    public double getVoteWin() {
        return voteWin;
    }

    public void setVoteWin(double voteWin) {
        this.voteWin = voteWin;
    }

    public double getVoteNegative() {
        return voteNegative;
    }

    public void setVoteNegative(double voteNegative) {
        this.voteNegative = voteNegative;
    }

    public VoteBean getVote() {
        return vote;
    }

    public void setVote(VoteBean vote) {
        this.vote = vote;
    }

    public static class VoteBean {
        /**
         * id : 1021333987447664600
         * code : 109029
         * vote_type : 1
         * user_id : 100101
         */

        private long id;
        private String code;
        private int vote_type;
        private String user_id;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getVote_type() {
            return vote_type;
        }

        public void setVote_type(int vote_type) {
            this.vote_type = vote_type;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }

}
