package com.cqhz.quwan.model

/**
 * 版本数据
 */
data class VersionBean(
        val appKey: String,
        val os: String,
        val version: String,
        val downloadUrl: String,
        val upgrade: Boolean
)
