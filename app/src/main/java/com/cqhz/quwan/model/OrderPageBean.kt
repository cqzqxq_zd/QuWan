package com.cqhz.quwan.model


data class OrderPageBean(
        val records :List<OrderBean>
)