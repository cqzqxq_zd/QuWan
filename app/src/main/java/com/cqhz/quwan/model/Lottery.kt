package com.cqhz.quwan.model

import android.support.v4.app.Fragment
import java.io.Serializable

class Lottery :Serializable{
    var id:String? = null
    var name:String? = null
    companion object {
        const val TYPE_LOTTO_NORMAL = 0x01
        const val TYPE_LOTTO_COURAGE = 0x02
        const val TYPE_NUM_ZX = 0x03
        const val TYPE_NUM_Z3_SINGLE = 0x04
        const val TYPE_NUM_Z3_MULTI = 0x05
        const val TYPE_NUM_Z6 = 0x06
    }
    /**
     * 游戏类型
     * 0: 乐透型
     * 1: 数字型
     */
    var type:Int = -1
    val betTypes = ArrayList<String>()
    val betTypeFragments = ArrayList<Fragment>()
    fun lotto(init:(LottoType.() -> Unit)){
        val wrapper = LottoType()
        wrapper.init()
        type = 0
        betTypes.clear()
        betTypeFragments.clear()
        betTypes.addAll(arrayListOf("普通投注","胆拖投注"))
        betTypeFragments.addAll(wrapper.genFragments())
    }
    fun num(init:NumType.()->Unit){
        val wrapper = NumType()
        wrapper.init()
        type = 1
        betTypes.clear()
        betTypeFragments.clear()
        betTypes.addAll(wrapper.names())
        betTypeFragments.addAll(wrapper.fragments())
    }
    fun genTitles():List<String>{
        return betTypes.map {
            if (type == 0&&it == "普通投注"){
                "$name"
            }else if(type == 0){
                "$name$it"
            }else if(type == 1 && it == "直选"){
                "$name"
            }else if(type == 1){
                "$name$it"
            }else{
                "其他"
            }
        }
    }
}