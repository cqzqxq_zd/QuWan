package com.cqhz.quwan.model.news

import com.cqhz.quwan.model.NewsGameBean

class NewsMatchBean(list: List<NewsGameBean>) : NewsBaseBean() {
    private var list: List<NewsGameBean> = list

    fun getList(): List<NewsGameBean> {
        return list
    }

    fun setList(list: List<NewsGameBean>) {
        this.list = list
    }

    override val itemType: Int
        get() = TYPE_MATCH
}