package com.cqhz.quwan.model

import android.support.v4.app.Fragment
import com.cqhz.quwan.ui.buy.fragment.BettingNumberFragment
import java.io.Serializable

class NumType:Serializable{
    private val betMethods = ArrayList<BetMethod>()
    fun betMethod(init:BetMethod.()->Unit){
        val wrapper = BetMethod()
        wrapper.init()
        betMethods.add(wrapper)
    }
    fun names():List<String>{
        return betMethods.map {
            it.name!!
        }
    }
    fun fragments():List<Fragment>{
        return betMethods.map {
            BettingNumberFragment.new(it)
        }
    }
    class BetMethod:Serializable{
        var name:String? = null
        var id:String? = null
        var tipLabel:String? = null
        var linkages:List<Int>? = null
        val betLines = ArrayList<BitLine>()
        fun bitLine(init:BitLine.()->Unit){
            val wrapper = BitLine()
            wrapper.init()
            betLines.add(wrapper)
        }
    }

    class BitLine:Serializable{
        var label:String? = null
        var nums:List<Int>? = null
        var selectMin:Int? = null
        var selectMax:Int? = null
        var selected:List<Int>? = null
    }
}