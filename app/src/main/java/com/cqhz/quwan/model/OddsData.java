package com.cqhz.quwan.model;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/26
 */
public class OddsData {
    /**
     * id : 63
     * code : 108945
     * odds_type : 0
     * odds_result :
     * create_time : 1532159203000
     * update_time : 1532159203000
     */
    private int id;
    private String code;
    private int odds_type;
    private long create_time;
    private long update_time;
    private List<OddsResultBean> odds_result;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getOdds_type() {
        return odds_type;
    }

    public void setOdds_type(int odds_type) {
        this.odds_type = odds_type;
    }

    public long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(long create_time) {
        this.create_time = create_time;
    }

    public long getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(long update_time) {
        this.update_time = update_time;
    }

    public List<OddsResultBean> getOdds_result() {
        return odds_result;
    }

    public void setOdds_result(List<OddsResultBean> odds_result) {
        this.odds_result = odds_result;
    }

    public static class OddsResultBean {
        /**
         * cn : Bet 365
         * o2_s : 1.80
         * o1_s : 2.04
         * o3_s : 平手
         * o3 : 平手
         * o2 : 2.02
         * o1 : 1.82
         */

        private String cn;
        private String o2_s;
        private String o1_s;
        private String o3_s;
        private String o3;
        private String o2;
        private String o1;
        private String o1_change; // up down equal
        private String o2_change;

        public String getCn() {
            return cn;
        }

        public void setCn(String cn) {
            this.cn = cn;
        }

        public String getO2_s() {
            return o2_s;
        }

        public void setO2_s(String o2_s) {
            this.o2_s = o2_s;
        }

        public String getO1_s() {
            return o1_s;
        }

        public void setO1_s(String o1_s) {
            this.o1_s = o1_s;
        }

        public String getO3_s() {
            return o3_s;
        }

        public void setO3_s(String o3_s) {
            this.o3_s = o3_s;
        }

        public String getO3() {
            return o3;
        }

        public void setO3(String o3) {
            this.o3 = o3;
        }

        public String getO2() {
            return o2;
        }

        public void setO2(String o2) {
            this.o2 = o2;
        }

        public String getO1() {
            return o1;
        }

        public void setO1(String o1) {
            this.o1 = o1;
        }

        public String getO1_change() {
            return o1_change;
        }

        public void setO1_change(String o1_change) {
            this.o1_change = o1_change;
        }

        public String getO2_change() {
            return o2_change;
        }

        public void setO2_change(String o2_change) {
            this.o2_change = o2_change;
        }
    }
}
