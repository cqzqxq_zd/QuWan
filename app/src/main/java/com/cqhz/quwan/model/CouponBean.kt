package com.cqhz.quwan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CouponBean(
        var conditionMoney: Int,
        var couponId: String,
        var distributeTime: String,
        var endTime: String,
        var lotteryId: String,
        var lotteryName: String,
        var lotteryStatus: String,
        var money: Int,
        var startTime: String,
        var status: Int,// 0未使用
        var type: Int,
        var userId: String,
        var isSelected: Boolean = false
) : Parcelable