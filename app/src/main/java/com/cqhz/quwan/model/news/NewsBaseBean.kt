package com.cqhz.quwan.model.news

import com.cqhz.quwan.R


abstract class NewsBaseBean : MixItemEntity{
    // item类型
    val TYPE_AD = 1// 顶部广告
    val TYPE_MATCH = 2// 赛事
    val TYPE_NEWS = 3// 资讯

    // resId
    val TYPE_AD_LAYOUT = R.layout.layout_page_banner
    val TYPE_MATCH_LAYOUT = R.layout.layout_page_news_match
    val TYPE_NEWS_LAYOUT = R.layout.layout_page_news_info
}