package com.cqhz.quwan.model

import java.io.Serializable


data class CommonResultBean(
        val id: String,
        val userId: String,
        val productId: String,
        val num: String,
        val type: String,// 交易类型 1：兑换实物 2：代卖
        val exchangeMethod: String// 换方式 0-支付宝；1-微信
) : Serializable