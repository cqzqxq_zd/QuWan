package com.cqhz.quwan.model

data class OrderResp(
        val id: String?,
        val payAmount: String?,
        val status: Int,
        val lotteryId: String?
)