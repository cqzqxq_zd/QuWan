package com.cqhz.quwan.model

/**
 * 联赛数据
 * Created by Guojing on 2018/11/14.
 */
data class LeagueBean(
        val league: String,
        val leagueId: String,
        val matchNum: Int,
        var selected: Boolean = true
)