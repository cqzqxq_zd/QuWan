package com.cqhz.quwan.model


data class BannerItem(
        val id: String,
        val imgUrl: String,
        val linkUrl: String?
)