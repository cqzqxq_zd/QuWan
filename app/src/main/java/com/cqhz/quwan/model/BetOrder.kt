package com.cqhz.quwan.model

data class BetOrder(
        val period: String,
        val redNum: String,
        val playType: Int
)

data class bonusOptimizeBetOrder(
        val betTimes: String,
        var betList: List<bonusOptimizeBetOrderItem>
)

data class bonusOptimizeBetOrderItem(
        val redNum: String,
        val period: String
)