package com.cqhz.quwan.model

class LayoutInit {
    var layoutId:Int? = null
    fun initView(initTarget:()->Unit){
        initTarget.invoke()
    }
}