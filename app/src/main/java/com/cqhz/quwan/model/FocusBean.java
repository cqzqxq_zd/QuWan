package com.cqhz.quwan.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author whamu2
 * @date 2018/6/12
 */
public class FocusBean {

    /**
     * lotteryId : 21
     * matchTime : 2018-06-14
     * week : 周四
     * code : 201806144001
     * closeTime : 22:45
     * matchField : 001
     * league : 世界杯
     * hostTeam : 俄罗斯
     * hostTeamLogo : null
     * guestTeam : 沙特
     * guestTeamLogo : null
     * score : -1
     * odds :
     * oddsMap : {"1":"4.15","2":"9.30","3":"2.03"}
     */

    private Integer lotteryId;
    private String matchTime;
    private String week;
    private String code;
    private String closeTime;
    private String matchField;
    private String league;
    private String hostTeam;
    private String hostTeamLogo;
    private String guestTeam;
    private String guestTeamLogo;
    private String score;
    private String odds;
    private Integer passStatus;// 0让球胜平负（3，4，5），1胜平负（0，1，2）
    private OddsMapBean oddsMap;

    public Integer getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Integer lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getMatchField() {
        return matchField;
    }

    public void setMatchField(String matchField) {
        this.matchField = matchField;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getHostTeam() {
        return hostTeam;
    }

    public void setHostTeam(String hostTeam) {
        this.hostTeam = hostTeam;
    }

    public String getHostTeamLogo() {
        return hostTeamLogo;
    }

    public void setHostTeamLogo(String hostTeamLogo) {
        this.hostTeamLogo = hostTeamLogo;
    }

    public String getGuestTeam() {
        return guestTeam;
    }

    public void setGuestTeam(String guestTeam) {
        this.guestTeam = guestTeam;
    }

    public String getGuestTeamLogo() {
        return guestTeamLogo;
    }

    public void setGuestTeamLogo(String guestTeamLogo) {
        this.guestTeamLogo = guestTeamLogo;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getOdds() {
        return odds;
    }

    public void setOdds(String odds) {
        this.odds = odds;
    }

    public Integer getPassStatus() {
        return passStatus;
    }

    public void setPassStatus(Integer passStatus) {
        this.passStatus = passStatus;
    }

    public OddsMapBean getOddsMap() {
        return oddsMap;
    }

    public void setOddsMap(OddsMapBean oddsMap) {
        this.oddsMap = oddsMap;
    }

    public static class OddsMapBean {
        /**
         * "0": "3.25",
         * "1": "3.55",
         * "2": "1.85",
         * "3": "1.73",
         * "4": "3.70",
         * "5": "3.56"
         */

        @SerializedName("0")
        private Double _$0;
        @SerializedName("1")
        private Double _$1;
        @SerializedName("2")
        private Double _$2;
        @SerializedName("3")
        private Double _$3;
        @SerializedName("4")
        private Double _$4;
        @SerializedName("5")
        private Double _$5;

        public Double get_$0() {
            return _$0;
        }

        public void set_$0(Double _$0) {
            this._$0 = _$0;
        }

        public Double get_$1() {
            return _$1;
        }

        public void set_$1(Double _$1) {
            this._$1 = _$1;
        }

        public Double get_$2() {
            return _$2;
        }

        public void set_$2(Double _$2) {
            this._$2 = _$2;
        }

        public Double get_$3() {
            return _$3;
        }

        public void set_$3(Double _$3) {
            this._$3 = _$3;
        }

        public Double get_$4() {
            return _$4;
        }

        public void set_$4(Double _$4) {
            this._$4 = _$4;
        }

        public Double get_$5() {
            return _$5;
        }

        public void set_$5(Double _$5) {
            this._$0 = _$5;
        }
    }
}
