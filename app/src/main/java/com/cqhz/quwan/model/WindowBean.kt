package com.cqhz.quwan.model


data class WindowBean(
    var content: String,
    var createTime: String,
    var id: String,
    var status: String,
    var title: String,
    var type: String,// type 类型 1-中奖；2-邀请首单；3-邀请首冲；4-每日首单；5-新用户注册
    var userId: String
)