package com.cqhz.quwan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class UserInfoBean(
        val activityId: String,
        val addressId: String,
        val age: String,
        val alipayAccount: String,
        val alipayBind: String,
        val balance: String,
        val bonus: String,
        val cashAble: String,
        val channel: String,
        val city: String,
        val consumeNum: String,
        val consumeStatus: String,
        val consumeSum: String,
        val couponNum: String,
        val createTime: String,
        val diamonds: String,
        val enabled: String,
        val equipmentId: String,
        val exchangeDiamondRatio: String,
        val headAddress: String,
        val idCard: String,
        val inviterId: String,
        val inviterMobile: String,
        val isExpert: String,
        val loginTime: String,
//        val lotBean: String,
        val mobile: String,
        val nickName: String,
        val officialQQGroup: String,
        val officialQQ: String,
        val payPwd: String,
        val paySum: String,
        val phoneModel: String,
        val prizeNum: String,
        val prizeSum: String,
        val realAuth: String,
        val realName: String,
        val sex: String,
        val source: String,
        val status: String,
        val toDaySignStatus: String,
        val updateTime: String,
        val userId: String,
        val rechargeSign: String,
        val wechatBind: String
) : Parcelable