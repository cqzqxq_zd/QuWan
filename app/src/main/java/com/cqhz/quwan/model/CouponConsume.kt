package com.cqhz.quwan.model

import java.io.Serializable

data class CouponConsume(
        val subBalanceMoney:Double,
        val finalMoney:Double,
        val couponMoney:Double,
        val orderId:String,
        val couponId:String,
        val availableCouponSize:Int
): Serializable