package com.cqhz.quwan.model

/**
 * Created by WYZ on 2018/3/27.
 */
data class OrderBean(
        val id: String,
        val lotteryName: String,
        val payAmount: String,
        val status: String,
        val followStatus: String,
        val prize: String,
        val afterTax: String,
        val lotteryNum: String,
        val createTime: String
)