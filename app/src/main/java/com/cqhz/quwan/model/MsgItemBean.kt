package com.cqhz.quwan.model

data class MsgItemBean (
        val theme:String?,
        val content:String?,
        val createTime:String?
)