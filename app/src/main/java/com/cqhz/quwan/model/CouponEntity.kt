package com.cqhz.quwan.model

data class CouponEntity(
        val conditionMoney:Double,
        val distributeTime:String,
        val money:Double?,
        val startTime:String,
        val endTime:String,
        val lotteryStatus:String,
        val lotteryName:String?,
        val couponId:String,
        val type:Int,
        val userId:String,
        val status:Int,
        val lotteryId:String,
        val period:String
)