package com.cqhz.quwan.model

/**
 * 比赛数据
 */

data class MatchBean(
        val matchResultList: List<MatchItemBean>,
        val matchTime: String,
        val matchField: String
)

data class MatchItemBean(
        val id: String,
        var groupPosition: Int,
        var childPosition: Int,
        var groupSize: Int,
        var childSize: Int,
        var groupName: String,
        val headName: String,
        val matchCode: String,
        val matchField: String,
        val league: String,
        val leagueId: String,
        val hostTeam: String,
        val guestTeam: String,
        val result: String,
        val info: String,
        val matchId: String,
        val status: String,
        val matchTime: String,
        val createTime: String,
        val minute: String,
        val score: String,
        val gameState: String,
        val goalLine: String,
        val hostLogo: String,
        val guestLogo: String,
        val lid: String,
        val halfScore: String
)