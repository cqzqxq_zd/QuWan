package com.cqhz.quwan.model

data class WonRecordBean(
        val id:String,
        val payAmount:String,
        val createTime:String
)