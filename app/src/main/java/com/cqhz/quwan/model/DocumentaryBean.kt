package com.cqhz.quwan.model

/**
 * 跟单订单数据
 * Created by Guojing on 2018/10/24.
 */

data class DocumentaryBean(
        var id: String?,
        var sp: String?,
        var betBunch: String?,// 投注方式
        var betTimes: String?,// 起投倍数
        var closeTime: String?,// 截止时间
        var commissionRate: String?,// 佣金比例
        var createOrders: Int,// 发单数
        var createTime: String?,// 发布时间
        var followAmt: String?,// 跟单金额
        var followOrderId: String?,// 跟单金额
        var follows: String?,// 人气（跟单人数）
        var followStatus: String?,
        var funs: String?,// 关注数
        var headUrl: String?,// 头像
        var hitNums: Int,// 中奖单数
        var hitOdds: String?,// 命中率
        var introduce: String?,// 介绍
        var lastStatus: String?,// 近期状态（近7天中奖状态）
        var minFollowAmt: String?,// 起购金额
        var nick: String?,// 用户名
        var openStatus: String?,// 跟单公开状态 保密设置 0：开赛后公开；1：开奖后公开；2：保密
        var orderId: String?,// 发单ID
        var payAmt: String?,// 自购金额
        var profitOdds: String?,// 盈利率
        var rank: Int?,// 专家等级(1:一级专家,2:二级专家,3:三级专家)
        var reds: String?,// 连红数
        var userId: String?,
        var saleNum: String?// 方案在售

)