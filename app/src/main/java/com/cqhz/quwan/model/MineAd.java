package com.cqhz.quwan.model;

/**
 * @author whamu2
 * @date 2018/7/19
 */
public class MineAd {


    /**
     * clickNum : string
     * createTime : string
     * id : string
     * imgUrl : string
     * linkUrl : string
     * sort : string
     * status : string
     * title : string
     * updateTime : string
     */

    private String clickNum;
    private String createTime;
    private String id;
    private String imgUrl;
    private String linkUrl;
    private String sort;
    private String status;
    private String title;
    private String updateTime;

    public String getClickNum() {
        return clickNum;
    }

    public void setClickNum(String clickNum) {
        this.clickNum = clickNum;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "MineAd{" +
                "clickNum='" + clickNum + '\'' +
                ", createTime='" + createTime + '\'' +
                ", id='" + id + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", linkUrl='" + linkUrl + '\'' +
                ", sort='" + sort + '\'' +
                ", status='" + status + '\'' +
                ", title='" + title + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
