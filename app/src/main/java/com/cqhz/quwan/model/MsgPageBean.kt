package com.cqhz.quwan.model

/**
 * Created by WYZ on 2018/3/27.
 */
data class MsgPageBean (
        val records:List<MsgItemBean>
)