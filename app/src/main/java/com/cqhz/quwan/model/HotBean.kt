package com.cqhz.quwan.model

import com.google.gson.annotations.SerializedName


class HotBean {
    private var lotteryId: Int = 0
    private var matchTime: String = ""
    private var week: String = ""
    private var code: String = ""
    private var closeTime: String = ""
    private var matchField: String = ""
    private var league: String = ""
    private var hostTeam: String = ""
    private var hostTeamLogo: String = ""
    private var guestTeam: String = ""
    private var guestTeamLogo: String = ""
    private var score: String = ""
    private var odds: String = ""
    private var passStatus: Int = 0// 1,0,-1
    private var oddsMap: OddsMapBean? = null
    private var peopleJoin: String = ""
    private var matchId: String = ""
    private var lid: String = ""

    fun getLotteryId(): Int? {
        return lotteryId
    }

    fun setLotteryId(lotteryId: Int) {
        this.lotteryId = lotteryId
    }

    fun getMatchTime(): String {
        return matchTime
    }

    fun setMatchTime(matchTime: String) {
        this.matchTime = matchTime
    }

    fun getWeek(): String {
        return week
    }

    fun setWeek(week: String) {
        this.week = week
    }

    fun getCode(): String {
        return code
    }

    fun setCode(code: String) {
        this.code = code
    }

    fun getCloseTime(): String {
        return closeTime
    }

    fun setCloseTime(closeTime: String) {
        this.closeTime = closeTime
    }

    fun getMatchField(): String {
        return matchField
    }

    fun setMatchField(matchField: String) {
        this.matchField = matchField
    }

    fun getLeague(): String {
        return league
    }

    fun setLeague(league: String) {
        this.league = league
    }

    fun getHostTeam(): String {
        return hostTeam
    }

    fun setHostTeam(hostTeam: String) {
        this.hostTeam = hostTeam
    }

    fun getHostTeamLogo(): String {
        return hostTeamLogo
    }

    fun setHostTeamLogo(hostTeamLogo: String) {
        this.hostTeamLogo = hostTeamLogo
    }

    fun getGuestTeam(): String {
        return guestTeam
    }

    fun setGuestTeam(guestTeam: String) {
        this.guestTeam = guestTeam
    }

    fun getGuestTeamLogo(): String {
        return guestTeamLogo
    }

    fun setGuestTeamLogo(guestTeamLogo: String) {
        this.guestTeamLogo = guestTeamLogo
    }

    fun getScore(): String {
        return score
    }

    fun setScore(score: String) {
        this.score = score
    }

    fun getOdds(): String {
        return odds
    }

    fun setOdds(odds: String) {
        this.odds = odds
    }

    fun getPassStatus(): Int {
        return passStatus
    }

    fun setPassStatus(passStatus: Int) {
        this.passStatus = passStatus
    }

    fun getOddsMap(): OddsMapBean? {
        return oddsMap
    }

    fun setOddsMap(oddsMap: OddsMapBean) {
        this.oddsMap = oddsMap
    }

    fun getPeopleJoin(): String {
        return peopleJoin
    }

    fun setPeopleJoin(peopleJoin: String) {
        this.peopleJoin = peopleJoin
    }

    fun getMatchId(): String {
        return matchId
    }

    fun setMatchId(matchId: String) {
        this.matchId = matchId
    }

    fun getLid(): String {
        return lid
    }

    fun setLid(lid: String) {
        this.lid = lid
    }

    class OddsMapBean {
        /**
         * 1 : 4.15
         * 2 : 9.30
         * 3 : 2.03
         */

        @SerializedName("1")
        var `_$1`: Double? = null
        @SerializedName("2")
        var `_$2`: Double? = null
        @SerializedName("3")
        var `_$3`: Double? = null
    }
}