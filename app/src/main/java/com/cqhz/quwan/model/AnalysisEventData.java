package com.cqhz.quwan.model;

/**
 * @author whamu2
 * @date 2018/7/24
 */
public class AnalysisEventData {


    /**
     * id : 116
     * code : 109049
     * match_time : 1532512800000
     * league : 日职
     * score :
     * host_team : 大阪樱花
     * guest_team : 鹿岛鹿角
     * host_logo : http://static.sporttery.cn/images/2018/07/03/36_2048263061.png
     * guest_logo : http://static.sporttery.cn/images/2018/07/03/36_2110188131.png
     * status : 0
     * game_state : 未开始
     * create_time : 1532484520000
     * update_time : 1532484520000
     * goal_line : -1
     */

    private int id;
    private String code;
    private long match_time;
    private String league;
    private String score;
    private String host_team;
    private String guest_team;
    private String host_logo;
    private String guest_logo;
    private int status;
    private String game_state;
    private long create_time;
    private long update_time;
    private String goal_line;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getMatch_time() {
        return match_time;
    }

    public void setMatch_time(long match_time) {
        this.match_time = match_time;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getHost_team() {
        return host_team;
    }

    public void setHost_team(String host_team) {
        this.host_team = host_team;
    }

    public String getGuest_team() {
        return guest_team;
    }

    public void setGuest_team(String guest_team) {
        this.guest_team = guest_team;
    }

    public String getHost_logo() {
        return host_logo;
    }

    public void setHost_logo(String host_logo) {
        this.host_logo = host_logo;
    }

    public String getGuest_logo() {
        return guest_logo;
    }

    public void setGuest_logo(String guest_logo) {
        this.guest_logo = guest_logo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getGame_state() {
        return game_state;
    }

    public void setGame_state(String game_state) {
        this.game_state = game_state;
    }

    public long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(long create_time) {
        this.create_time = create_time;
    }

    public long getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(long update_time) {
        this.update_time = update_time;
    }

    public String getGoal_line() {
        return goal_line;
    }

    public void setGoal_line(String goal_line) {
        this.goal_line = goal_line;
    }
}
