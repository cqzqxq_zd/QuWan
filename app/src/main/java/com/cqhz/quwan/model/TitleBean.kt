package com.cqhz.quwan.model

import android.os.Parcel
import android.os.Parcelable

data class TitleBean(var name: String, var type: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TitleBean> {
        override fun createFromParcel(parcel: Parcel): TitleBean {
            return TitleBean(parcel)
        }

        override fun newArray(size: Int): Array<TitleBean?> {
            return arrayOfNulls(size)
        }
    }
}