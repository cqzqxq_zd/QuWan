package com.cqhz.quwan.model

data class Msg2Entity(
        val id:String,
        val userId:String,
        val type:Int,
        val title:String,
        val logo:String,
        val content:String,
        val status:Int,
        val createTime:String
)