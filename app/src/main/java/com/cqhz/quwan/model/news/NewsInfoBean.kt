package com.cqhz.quwan.model.news

import com.cqhz.quwan.model.NewsBean

class NewsInfoBean(newsBean: NewsBean) : NewsBaseBean() {

    private var newsBean: NewsBean = newsBean

    fun getNewsBean(): NewsBean {
        return newsBean
    }

    fun setNewsBean(newsBean: NewsBean) {
        this.newsBean = newsBean
    }

    override val itemType: Int
        get() = TYPE_NEWS
}