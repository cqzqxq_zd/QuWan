package com.cqhz.quwan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CodeBean(
        var image: String
) : Parcelable


