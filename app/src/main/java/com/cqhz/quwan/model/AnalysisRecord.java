package com.cqhz.quwan.model;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class AnalysisRecord {

    /**
     * recordPOList :
     * votePOList :
     */

    private AnalysisVote votePOList;
    private List<RecordPOListBean> recordPOList;

    public AnalysisVote getVotePOList() {
        return votePOList;
    }

    public void setVotePOList(AnalysisVote votePOList) {
        this.votePOList = votePOList;
    }

    public List<RecordPOListBean> getRecordPOList() {
        return recordPOList;
    }

    public void setRecordPOList(List<RecordPOListBean> recordPOList) {
        this.recordPOList = recordPOList;
    }

    public static class RecordPOListBean {
        /**
         * id : 3
         * code : 109029
         * record_type : 2
         * record_simple_result :
         * record_detail_result :
         * create_time : 1532328084000
         * update_time : 1532499377000
         */

        private int id;
        private String code;
        private int record_type;
        private long create_time;
        private long update_time;
        private RecordSimpleResult record_simple_result;
        private List<RecordDetailResultBean> record_detail_result;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getRecord_type() {
            return record_type;
        }

        public void setRecord_type(int record_type) {
            this.record_type = record_type;
        }

        public RecordSimpleResult getRecord_simple_result() {
            return record_simple_result;
        }

        public void setRecord_simple_result(RecordSimpleResult record_simple_result) {
            this.record_simple_result = record_simple_result;
        }

        public long getCreate_time() {
            return create_time;
        }

        public void setCreate_time(long create_time) {
            this.create_time = create_time;
        }

        public long getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(long update_time) {
            this.update_time = update_time;
        }

        public List<RecordDetailResultBean> getRecord_detail_result() {
            return record_detail_result;
        }

        public void setRecord_detail_result(List<RecordDetailResultBean> record_detail_result) {
            this.record_detail_result = record_detail_result;
        }

        public static class RecordSimpleResult {

            /**
             * record : {"win":5,"draw":3,"lose":2}
             * total : 10
             */

            private RecordBean record;
            private int total;
            private HostRecord h_record;
            private HostRecord g_record;

            public RecordBean getRecord() {
                return record;
            }

            public void setRecord(RecordBean record) {
                this.record = record;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public HostRecord getH_record() {
                return h_record;
            }

            public void setH_record(HostRecord h_record) {
                this.h_record = h_record;
            }

            public HostRecord getG_record() {
                return g_record;
            }

            public void setG_record(HostRecord g_record) {
                this.g_record = g_record;
            }

            public static class RecordBean {
                /**
                 * win : 5
                 * draw : 3
                 * lose : 2
                 */

                private int win;
                private int draw;
                private int lose;

                public int getWin() {
                    return win;
                }

                public void setWin(int win) {
                    this.win = win;
                }

                public int getDraw() {
                    return draw;
                }

                public void setDraw(int draw) {
                    this.draw = draw;
                }

                public int getLose() {
                    return lose;
                }

                public void setLose(int lose) {
                    this.lose = lose;
                }
            }

            public static class HostRecord {

                /**
                 * win : 0
                 * draw : 3
                 * lose : 1
                 */

                private int win;
                private int draw;
                private int lose;

                public int getWin() {
                    return win;
                }

                public void setWin(int win) {
                    this.win = win;
                }

                public int getDraw() {
                    return draw;
                }

                public void setDraw(int draw) {
                    this.draw = draw;
                }

                public int getLose() {
                    return lose;
                }

                public void setLose(int lose) {
                    this.lose = lose;
                }
            }
        }

        public static class RecordDetailResultBean {
            /**
             * league : 瑞超
             * score : {"host":"埃尔夫斯堡","sco":" 0:0 ","guest":"哈马比"}
             * result : 平
             * time : 2018-07-17
             */

            private String league;
            private ScoreBean score;
            private TeamBean team;
            private String result;
            private String time;
            private int record_type;

            public String getLeague() {
                return league;
            }

            public void setLeague(String league) {
                this.league = league;
            }

            public ScoreBean getScore() {
                return score;
            }

            public void setScore(ScoreBean score) {
                this.score = score;
            }

            public String getResult() {
                return result;
            }

            public void setResult(String result) {
                this.result = result;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public TeamBean getTeam() {
                return team;
            }

            public void setTeam(TeamBean team) {
                this.team = team;
            }

            public int getRecord_type() {
                return record_type;
            }

            public void setRecord_type(int record_type) {
                this.record_type = record_type;
            }

            public static class ScoreBean {
                /**
                 * host : 埃尔夫斯堡
                 * sco :  0:0
                 * guest : 哈马比
                 */

                private String host;
                private String sco;
                private String guest;

                public String getHost() {
                    return host;
                }

                public void setHost(String host) {
                    this.host = host;
                }

                public String getSco() {
                    return sco;
                }

                public void setSco(String sco) {
                    this.sco = sco;
                }

                public String getGuest() {
                    return guest;
                }

                public void setGuest(String guest) {
                    this.guest = guest;
                }
            }


            public static class TeamBean {

                /**
                 * host : 巴黎圣曼
                 * sco : VS
                 * guest : 昂热
                 */

                private String host;
                private String sco;
                private String guest;

                public String getHost() {
                    return host;
                }

                public void setHost(String host) {
                    this.host = host;
                }

                public String getSco() {
                    return sco;
                }

                public void setSco(String sco) {
                    this.sco = sco;
                }

                public String getGuest() {
                    return guest;
                }

                public void setGuest(String guest) {
                    this.guest = guest;
                }
            }

        }
    }
}
