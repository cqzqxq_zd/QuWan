package com.cqhz.quwan.model


data class LoginBean(
        val userId:String?,
        val phone:String?,
        var phoneNumber:String?
)