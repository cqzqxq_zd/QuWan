package com.cqhz.quwan.model

import com.cqhz.quwan.util.strComma
import com.cqhz.quwan.util.strCommaNoSlice
import com.cqhz.quwan.util.strNoneComma
import java.io.Serializable

/**
 * Created by WYZ on 2018/3/26.
 */

data class BettingItem(
        val redBalls:List<Int>?,
        val blueBalls:List<Int>?,
        val typeId:Int,
        val allStake:Long
):Serializable{
    var redDMIndex = -1
    var redTMIndex = -1
    var blueDMIndex = -1
    var blueTMIndex = -1
    var betBits:List<List<Int>>? = null
    fun str():String{
        return if (typeId == Lottery.TYPE_LOTTO_NORMAL||
                typeId == Lottery.TYPE_LOTTO_COURAGE){
            betLottoStr()
        }else if(typeId == Lottery.TYPE_NUM_ZX
                ||typeId == Lottery.TYPE_NUM_Z3_SINGLE
                ||typeId == Lottery.TYPE_NUM_Z3_MULTI
                ||typeId == Lottery.TYPE_NUM_Z6){
            betNumStr()
        }else{
            ""
        }
    }

    fun betName():String{
        return if (typeId == Lottery.TYPE_LOTTO_NORMAL){
            if (redBalls!!.size + blueBalls!!.size
                    > 7) "复式" else "单式"
        }else if(typeId == Lottery.TYPE_LOTTO_COURAGE){
            "胆拖"
        }else if(typeId == Lottery.TYPE_NUM_ZX){
            if (betBits!![0].size>1
                    ||betBits!![1].size>1
                    ||betBits!![2].size>1) "直选复式" else "直选单式"
        }else if(typeId == Lottery.TYPE_NUM_Z3_SINGLE){
            "组三单式"
        }else if(typeId == Lottery.TYPE_NUM_Z3_MULTI){
            "组三复式"
        }else if(typeId == Lottery.TYPE_NUM_Z6){
            "组六"
        }else{
            ""
        }
    }

    private fun betLottoStr():String{
        val temp = if (redDMIndex != -1){
            val redDMs = redBalls?.slice(0..redDMIndex)?.sorted()
            val dmStr = "（${redDMs?.strNoneComma()}）"
            val redTMs = redBalls?.slice(redDMIndex+1..redTMIndex)?.sorted()
            "$dmStr${redTMs?.strNoneComma()}</font>"
        } else {
            "${redBalls?.sorted()?.strNoneComma()}</font>"
        }
        val text = "<font color='#EC0411'>$temp"
        return "$text <font color='#1c7ee3'>${if(blueDMIndex != -1){
            val blueDMs = blueBalls?.slice(0..blueDMIndex)?.sorted()
            val dmStr = "（${blueDMs?.strNoneComma()}）"
            val blueTMs = blueBalls?.slice(blueDMIndex+1..blueTMIndex)?.sorted()
            "$dmStr${blueTMs?.strNoneComma()}</font>"
        }else{
            "${blueBalls?.sorted()?.strNoneComma()}</font>"
        }}"
    }
    private fun betNumStr():String{
        var text = "<font color='#EC0411'>"
        for (betBit in betBits!!){
            var innerText = ""
            for (bit in betBit){
                innerText = "$innerText$bit"
            }
            text = "$text$innerText "
        }
        var outText = text.replace("| ","|")
        if (outText.endsWith("|")){
            outText = outText.removeRange(outText.length-1,outText.length)
        }
        return "$outText</font>"
    }
    fun redDMSelected():List<Int>?{
        return if(redDMIndex != -1){
            redBalls?.slice(0..redDMIndex)?.sorted()
        }else {
            null
        }
    }
    fun redTMSelected():List<Int>?{
        return if (redDMIndex != -1){
            redBalls?.slice(redDMIndex+1..redTMIndex)?.sorted()
        }else {
            null
        }
    }
    fun blueDMSelected():List<Int>?{
        return if(blueDMIndex != -1){
            blueBalls?.slice(0..blueDMIndex)?.sorted()
        }else {
            null
        }
    }
    fun blueTMSelected():List<Int>?{
        return if(blueDMIndex != -1){
            blueBalls?.slice(blueDMIndex+1..blueTMIndex)?.sorted()
        }else {
            null
        }
    }

    fun betStrWrapper():OrderBet{
        return if (typeId == Lottery.TYPE_LOTTO_NORMAL
                ||typeId == Lottery.TYPE_LOTTO_COURAGE){
            val payType = when(typeId){
                Lottery.TYPE_LOTTO_NORMAL -> "1"
                Lottery.TYPE_LOTTO_COURAGE -> "2"
                else -> "1"
            }
            OrderBet(betLottoRedStrWrapper(),betLottoBlueStrWrapper(),payType,allStake.toString())
        }else if(typeId == Lottery.TYPE_NUM_ZX
                ||typeId == Lottery.TYPE_NUM_Z3_SINGLE
                ||typeId == Lottery.TYPE_NUM_Z3_MULTI
                ||typeId == Lottery.TYPE_NUM_Z6){
            val payType = when(typeId){
                Lottery.TYPE_NUM_ZX -> "1"
                Lottery.TYPE_NUM_Z3_SINGLE -> "2"
                Lottery.TYPE_NUM_Z3_MULTI -> "3"
                Lottery.TYPE_NUM_Z6 -> "4"
                else -> "1"
            }
            OrderBet(betNumStrWrapper(),"",payType,allStake.toString())
        }else{
            OrderBet("","","",allStake.toString())
        }
    }




    private fun betLottoRedStrWrapper():String{
        return if (redDMIndex != -1){
            val redDMs = redBalls?.slice(0..redDMIndex)?.sorted()
            val dmStr = "(${redDMs?.strComma()}),"
            val redTMs = redBalls?.slice(redDMIndex+1..redTMIndex)?.sorted()
            "$dmStr${redTMs?.strComma()}"
        } else {

            "${redBalls?.sorted()?.strComma()}"
        }
    }

    private fun betLottoBlueStrWrapper():String{
        return if(blueDMIndex != -1){
            val blueDMs = blueBalls?.slice(0..blueDMIndex)?.sorted()
            val dmStr = "(${blueDMs?.strComma()}),"
            val blueTMs = blueBalls?.slice(blueDMIndex+1..blueTMIndex)?.sorted()
            "$dmStr${blueTMs?.strComma()}"
        }else{
            "${blueBalls?.sorted()?.strComma()}"
        }
    }


    private fun betNumStrWrapper():String{
        var text = ""
        for (betBit in betBits!!){
            val innerText = "${betBit.strCommaNoSlice()}|"
            text += innerText
        }
        var outText = text.replace("| ","|")
        if (outText.endsWith("|")){
            outText = outText.removeRange(outText.length-1,outText.length)
        }
        return outText
    }
}