package com.cqhz.quwan.model

/**
 * 订单列表项实体类
 * Created by WYZ on 2018/3/21.
 */
data class OrderItem(
        val title: String,
        val type: String,
        val status: String,
        val date: String
)