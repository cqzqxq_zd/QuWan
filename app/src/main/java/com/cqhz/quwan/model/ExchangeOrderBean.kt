package com.cqhz.quwan.model

/**
 * 我的兑换列表兑换数据
 */
data class ExchangeOrderBean(
        val id: String,
        val createTime: String,
        val status: String,
        val productId: String,
        val productCalss: String,
        val productType: String,
        val productDetailType: String,
        val productModel: String,
        val productColor: String,
        val productVersion: String,
        val productImgUrl: String,
        val tradeType: String,
        val num: String,
        val price: String,
        val totalPrice: String,
        val logisticsNum: String,
        val logisticsCompany: String,
        val exchangePrice: String
)