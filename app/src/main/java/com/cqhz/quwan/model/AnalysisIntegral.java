package com.cqhz.quwan.model;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/25
 */
public class AnalysisIntegral {

    /**
     * name : 国冠杯
     * point :
     */

    private String name;
    private List<PointBean> point;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PointBean> getPoint() {
        return point;
    }

    public void setPoint(List<PointBean> point) {
        this.point = point;
    }

    public static class PointBean {
        /**
         * id : 27
         * league_id : 241
         * point_type : 0
         * point_result :
         */

        private int id;
        private String league_id;
        private int point_type;
        private String league;
        private List<PointResultBean> point_result;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLeague_id() {
            return league_id;
        }

        public void setLeague_id(String league_id) {
            this.league_id = league_id;
        }

        public int getPoint_type() {
            return point_type;
        }

        public void setPoint_type(int point_type) {
            this.point_type = point_type;
        }

        public List<PointResultBean> getPoint_result() {
            return point_result;
        }

        public void setPoint_result(List<PointResultBean> point_result) {
            this.point_result = point_result;
        }

        public String getLeague() {
            return league;
        }

        public void setLeague(String league) {
            this.league = league;
        }

        public static class PointResultBean {
            /**
             * matchTimes : 25
             * draw : 8
             * score : 47
             * goal : 37
             * lose_goal : 23
             * team : 松本山雅
             * profit : 14
             * win : 13
             * lose : 4
             * rank : 1
             */

            private String matchTimes;
            private String draw;
            private String score;
            private String goal;
            private String lose_goal;
            private String team;
            private String profit;
            private String win;
            private String lose;
            private String rank;

            public String getMatchTimes() {
                return matchTimes;
            }

            public void setMatchTimes(String matchTimes) {
                this.matchTimes = matchTimes;
            }

            public String getDraw() {
                return draw;
            }

            public void setDraw(String draw) {
                this.draw = draw;
            }

            public String getScore() {
                return score;
            }

            public void setScore(String score) {
                this.score = score;
            }

            public String getGoal() {
                return goal;
            }

            public void setGoal(String goal) {
                this.goal = goal;
            }

            public String getLose_goal() {
                return lose_goal;
            }

            public void setLose_goal(String lose_goal) {
                this.lose_goal = lose_goal;
            }

            public String getTeam() {
                return team;
            }

            public void setTeam(String team) {
                this.team = team;
            }

            public String getProfit() {
                return profit;
            }

            public void setProfit(String profit) {
                this.profit = profit;
            }

            public String getWin() {
                return win;
            }

            public void setWin(String win) {
                this.win = win;
            }

            public String getLose() {
                return lose;
            }

            public void setLose(String lose) {
                this.lose = lose;
            }

            public String getRank() {
                return rank;
            }

            public void setRank(String rank) {
                this.rank = rank;
            }
        }
    }
}
