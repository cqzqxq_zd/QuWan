package com.cqhz.quwan.model

/**
 * 专家和方案详情公用数据
 * Created by Guojing on 2018/12/07.
 */
data class ExpertDetailBean(
    var createOrders: String,
    var expertId: String,
    var followStatus: Int,
    var headUrl: String,
    var hitNums: String,
    var hitOdds: String,
    var introduce: String,
    var lastStatus: String,
    var nick: String,
    var orderList: List<Order>,
    var profitOdds: String,
    var pushOrderDetailVO: PushOrderDetailInfoBean,
    var rank: Int
)

data class Order(
    var betTimes: Int,
    var betType: String,
    var closeTime: String,
    var commission: String,
    var commissionRate: String,
    var createTime: String,
    var expertName: String,
    var followAmt: Int,
    var followStatus: Int,
    var follows: Int,
    var minFollowAmt: Int,
    var openStatus: String,
    var orderId: String,
    var payAmt: Int,
    var profitOdds: String,
    var projectId: String,
    var singleAmt: String,
    var sp: String
)