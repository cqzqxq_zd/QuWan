package com.cqhz.quwan.model

/**
 * 电竞订单数据
 * Created by Guojing on 2018/10/24.
 */

data class ESportOrderResultBean(
        var competitionNumber: String,
        var totalIncome: String,
        var winnProbability: String,
        var page: PageResultBean<ESportOrderItemBean>
)

data class ESportOrderItemBean(
    var id: String,
    var userId: String,
    var lotteryId: String,
    var lotteryName: String,
    var period: String,
    var buyWay: String,
    var payWay: String,
    var payAccount: String,
    var payAmount: String,
    var expectedBonus: String,
    var prize: String,
    var afterTax: String,
    var status: String,
    var ticketList: String,
    var ticketShow: String,
    var ticketStatus: String,
    var lotteryNum: String,
    var betNum: String,
    var betTimes: String,
    var betBunch: String,
    var betAppend: String,
    var append: String,
    var lotteryTime: String,
    var createTime: String,
    var updateTime: String,
    var winTime: String,
    var realName: String,
    var mobile: String,
    var followOrderId: String,
    var userInfo: String,
    var orderBetList: String,
    var orderBetJson: String,
    var idCard: String,
    var isPublishFollow: String,
    var secrecySet: String,
    var commissionRate: String,
    var heelMultiple: String,
    var guaranteeOdds: String,
    var followStatus: String,
    var pushOrderDetailVO: String,
    var commission: String,
    var payProfitOdds: String,
    var warning: String,
    var djPlayDescribe: String,
    var djSelectTeam: String,
    var djMatchName: String
)