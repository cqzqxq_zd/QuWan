package com.cqhz.quwan.model

data class LeagueEntity(
        val league:String,
        val leagueId:String,
        val matchNum:Int
)