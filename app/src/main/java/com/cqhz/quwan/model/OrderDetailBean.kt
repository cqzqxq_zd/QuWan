package com.cqhz.quwan.model


/**
 * 兑换详情数据
 */
data class OrderDetailBean(
        val tradeId: String,
        val receiverMobile: String,
        val receiverName: String,
        val address: String,
        val productName: String,
        val num: Int,
        val color: String,
        val version: String,
        val diamonds: String,
        val status: Int,
        val createTime: String
)