package com.cqhz.quwan.model;

/**
 * @author whamu2
 * @date 2018/7/26
 */

public class AnalysisForecast {

    /**
     * id : 35
     * code : 109058
     * spf_forecast : {"win_per":"48%","draw_per":"23%","lose_per":"29%"}
     * rqspf_forecast : {"win_per":"32%","draw_per":"34%","lose_per":"34%"}
     * create_time : 1532484765000
     * update_time : 1532529511000
     * "goal_line": "-1"
     */

    private int id;
    private String code;
    private SpfForecastBean spf_forecast;
    private RqspfForecastBean rqspf_forecast;
    private String goal_line;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public SpfForecastBean getSpf_forecast() {
        return spf_forecast;
    }

    public void setSpf_forecast(SpfForecastBean spf_forecast) {
        this.spf_forecast = spf_forecast;
    }

    public RqspfForecastBean getRqspf_forecast() {
        return rqspf_forecast;
    }

    public void setRqspf_forecast(RqspfForecastBean rqspf_forecast) {
        this.rqspf_forecast = rqspf_forecast;
    }

    public String getGoal_line() {
        return goal_line;
    }

    public void setGoal_line(String goal_line) {
        this.goal_line = goal_line;
    }

    public static class SpfForecastBean {
        /**
         * win_per : 48%
         * draw_per : 23%
         * lose_per : 29%
         */

        private String win_per;
        private String draw_per;
        private String lose_per;

        public String getWin_per() {
            return win_per;
        }

        public void setWin_per(String win_per) {
            this.win_per = win_per;
        }

        public String getDraw_per() {
            return draw_per;
        }

        public void setDraw_per(String draw_per) {
            this.draw_per = draw_per;
        }

        public String getLose_per() {
            return lose_per;
        }

        public void setLose_per(String lose_per) {
            this.lose_per = lose_per;
        }
    }

    public static class RqspfForecastBean {
        /**
         * win_per : 32%
         * draw_per : 34%
         * lose_per : 34%
         */

        private String win_per;
        private String draw_per;
        private String lose_per;

        public String getWin_per() {
            return win_per;
        }

        public void setWin_per(String win_per) {
            this.win_per = win_per;
        }

        public String getDraw_per() {
            return draw_per;
        }

        public void setDraw_per(String draw_per) {
            this.draw_per = draw_per;
        }

        public String getLose_per() {
            return lose_per;
        }

        public void setLose_per(String lose_per) {
            this.lose_per = lose_per;
        }
    }
}
