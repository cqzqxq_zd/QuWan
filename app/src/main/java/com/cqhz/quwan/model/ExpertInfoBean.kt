package com.cqhz.quwan.model

/**
 * 专家信息数据
 * Created by Guojing on 2018/10/30.
 */
data class ExpertInfoBean(
        var create_time: Long,
        var head_portrait: String,
        var id: Long,
        var introduce: String,
        var max: Int,// 最大佣金率
        var min: Int,// 最小佣金率
        var nick: String,
        var nick_status: Int,
        var rank: Int,// 等级(1:一级专家,2:二级专家,3:三级专家)
        var remark: String,// 默认起投倍数
        var status: Int,
        var type: Int,// 0：真人；1：机器人；
        var update_time: Long,
        var user_id: Long
)