package com.cqhz.quwan.model

/**
 * 地址数据
 */
class AddressBean {
    var id: String = ""
    var receiverName: String = ""
    var phoneNumber: String = ""
    var province: String = ""
    var city: String = ""
    var county: String = ""
    var street: String = ""
    var detailAddress: String = ""

    constructor()

    constructor(province: String, city: String, county: String) {
        this.province = province
        this.city = city
        this.county = county
    }
}