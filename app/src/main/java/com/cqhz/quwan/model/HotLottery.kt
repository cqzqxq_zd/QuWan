package com.cqhz.quwan.model

/**
 * 热门彩票
 */
data class HotLottery(
        val id:String,
        // 图标
        val imgUrl:String,
        // 彩票名称
        val name:String,
        // 奖池
        val totalMoney:String,
        // 期号
        val period:String,
        // 开奖时间
        val lotteryTimeSuffix:String,
        val saleStatus:Int?,
        val tags:String?,
        val hot:Int,
        val playType:Int?
)