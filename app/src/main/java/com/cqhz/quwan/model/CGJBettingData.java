package com.cqhz.quwan.model;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/6/12
 */
public class CGJBettingData {

    /**
     * orderBetJson : [{"redNum":1,"period":"2018","playType":10},{"redNum":2,"period":"2018","playType":10}]
     * lotteryId : 21
     * period : 2018
     * payAmount : 40
     * betNum : 2
     * betTimes : 10
     * betBunch : 1-1
     */

    private Integer lotteryId;
    private Integer period;
    private Double payAmount;
    private Integer betNum;
    private Integer betTimes;
    private String betBunch;
    private List<OrderBetJsonBean> orderBetJson;

    public Integer getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Integer lotteryId) {
        this.lotteryId = lotteryId;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Double getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Double payAmount) {
        this.payAmount = payAmount;
    }

    public Integer getBetNum() {
        return betNum;
    }

    public void setBetNum(Integer betNum) {
        this.betNum = betNum;
    }

    public Integer getBetTimes() {
        return betTimes;
    }

    public void setBetTimes(Integer betTimes) {
        this.betTimes = betTimes;
    }

    public String getBetBunch() {
        return betBunch;
    }

    public void setBetBunch(String betBunch) {
        this.betBunch = betBunch;
    }

    public List<OrderBetJsonBean> getOrderBetJson() {
        return orderBetJson;
    }

    public void setOrderBetJson(List<OrderBetJsonBean> orderBetJson) {
        this.orderBetJson = orderBetJson;
    }

    public static class OrderBetJsonBean {
        /**
         * redNum : 1
         * period : 2018
         * playType : 10
         */

        private Integer redNum;
        private String period;
        private Integer playType;

        public Integer getRedNum() {
            return redNum;
        }

        public void setRedNum(Integer redNum) {
            this.redNum = redNum;
        }

        public String getPeriod() {
            return period;
        }

        public void setPeriod(String period) {
            this.period = period;
        }

        public Integer getPlayType() {
            return playType;
        }

        public void setPlayType(Integer playType) {
            this.playType = playType;
        }
    }
}
