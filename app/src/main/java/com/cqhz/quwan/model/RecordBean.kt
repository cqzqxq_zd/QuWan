package com.cqhz.quwan.model

data class RecordBean<out T> (
        val records:List<T>
)