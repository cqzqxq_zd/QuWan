package com.cqhz.quwan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * 联赛数据
 * Created by Guojing on 2018/11/14.
 */
@Parcelize
data class BallMatchListBean(
        val lotteryId: Int,
        val date: String,
        val week: String,
        val count: Int,
        val match: List<BallMatchBean>
) : Parcelable

@Parcelize
data class BallMatchBean(
        var week: String = "",
        var avgOdds: String?,// 支持率
        var closeTime: String,// 购买截止时间
        var code: String,// 赛事编号
        var detailStatus: String,// 详细状态(1,1,1,1,1):(spf,rqspf,bf,zjq,bqc),0:开售1:未开售
        var dgStatus: String,
        var fid: String?,// 篮球的matchId
        var guestLogo: String?,
        var guestTeam: String,// 客队
        var hostLogo: String?,
        var hostTeam: String,// 主队
        var isHot: String,
        var league: String,// 联赛名称
        var leagueId: String,
        var matchField: String,// 周场次
        var matchId: String?,
        var matchRank: String?,
        var matchType: String?,
        var matchTime: String,// 比赛开始时间
        var odds: String,// 赔率 json字符串S
        /**
         * 胜平负(0,1,2)
         * 让球胜平负(3,4,5)
         * 半全场(6,7,8,9,10,11,12,13,14)
         * 比分-胜(15,16,17,18,19,20,21,22,23,24,25,26,27)
         * 比分-平(28,29,30,31,32)
         * 比分-负(33,34,35,36,37,38,39,40,41,42,43,44,45)
         * 总进球-负(46,47,48,49,50,51,52,53)
         */
        var oddsMap: Map<Int, String>,// 赔率 Map
        var overallRecord: String?,// 历史交锋
        var passStatus: String,// 单关状态(1,1,1,1,0):(spf,rqspf,bf,zjq,bqc),1:有单关玩法 0:无单关玩法
        var quizNum: String?,// 竞猜人数
        var recentRecord: String?,// 近期战绩
        var score: String,// 让球数 -1 +1
        var totalScore: String?,
        var isShowAnalysis: Boolean = false,// 支持率
        var pagePosition: Int,// 页面位置
        var adapterPosition: Int,// 适配器位置
        var selectedNumber: Int,// 选中的数目
        var selected: MutableMap<Int, Boolean>? = null,// 选中的位置
        var selectedLabel: String = "",// 选中的场次拼接字符串
        var selectedBasketString: List<String>? = null,// 选中的场次字符串列表
        var groupName: String
) : Parcelable
