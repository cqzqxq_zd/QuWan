package com.cqhz.quwan.model;

import java.util.List;

/**
 * @author whamu2
 * @date 2018/7/26
 */
public class OddsEuropeData {
    /**
     * id : 63
     * code : 108945
     * odds_type : 0
     * odds_result :
     * create_time : 1532159203000
     * update_time : 1532159203000
     */
    private int id;
    private String code;
    private int odds_type;
    private long create_time;
    private long update_time;
    private List<OddsResultBean> odds_result;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getOdds_type() {
        return odds_type;
    }

    public void setOdds_type(int odds_type) {
        this.odds_type = odds_type;
    }

    public long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(long create_time) {
        this.create_time = create_time;
    }

    public long getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(long update_time) {
        this.update_time = update_time;
    }

    public List<OddsResultBean> getOdds_result() {
        return odds_result;
    }

    public void setOdds_result(List<OddsResultBean> odds_result) {
        this.odds_result = odds_result;
    }

    public static class OddsResultBean {

        /**
         * draw : 3.15
         * cn : 最大值
         * win : 2.85
         * draw_s : 3.30
         * lose_index : -
         * win_s_index : -
         * draw_index : -
         * lose_s : 2.65
         * lose_s_index : -
         * win_s : 3.10
         * lose : 3.10
         * draw_s_index : -
         * win_index : -
         */

        private String draw;
        private String cn;
        private String win;
        private String draw_s;
        private String lose_index;
        private String win_s_index;
        private String draw_index;
        private String lose_s;
        private String lose_s_index;
        private String win_s;
        private String lose;
        private String draw_s_index;
        private String win_index;
        private String win_change;
        private String lose_change;
        private String draw_change;

        public String getDraw() {
            return draw;
        }

        public void setDraw(String draw) {
            this.draw = draw;
        }

        public String getCn() {
            return cn;
        }

        public void setCn(String cn) {
            this.cn = cn;
        }

        public String getWin() {
            return win;
        }

        public void setWin(String win) {
            this.win = win;
        }

        public String getDraw_s() {
            return draw_s;
        }

        public void setDraw_s(String draw_s) {
            this.draw_s = draw_s;
        }

        public String getLose_index() {
            return lose_index;
        }

        public void setLose_index(String lose_index) {
            this.lose_index = lose_index;
        }

        public String getWin_s_index() {
            return win_s_index;
        }

        public void setWin_s_index(String win_s_index) {
            this.win_s_index = win_s_index;
        }

        public String getDraw_index() {
            return draw_index;
        }

        public void setDraw_index(String draw_index) {
            this.draw_index = draw_index;
        }

        public String getLose_s() {
            return lose_s;
        }

        public void setLose_s(String lose_s) {
            this.lose_s = lose_s;
        }

        public String getLose_s_index() {
            return lose_s_index;
        }

        public void setLose_s_index(String lose_s_index) {
            this.lose_s_index = lose_s_index;
        }

        public String getWin_s() {
            return win_s;
        }

        public void setWin_s(String win_s) {
            this.win_s = win_s;
        }

        public String getLose() {
            return lose;
        }

        public void setLose(String lose) {
            this.lose = lose;
        }

        public String getDraw_s_index() {
            return draw_s_index;
        }

        public void setDraw_s_index(String draw_s_index) {
            this.draw_s_index = draw_s_index;
        }

        public String getWin_index() {
            return win_index;
        }

        public void setWin_index(String win_index) {
            this.win_index = win_index;
        }

        public String getWin_change() {
            return win_change;
        }

        public void setWin_change(String win_change) {
            this.win_change = win_change;
        }

        public String getLose_change() {
            return lose_change;
        }

        public void setLose_change(String lose_change) {
            this.lose_change = lose_change;
        }

        public String getDraw_change() {
            return draw_change;
        }

        public void setDraw_change(String draw_change) {
            this.draw_change = draw_change;
        }
    }
}
