package com.cqhz.quwan.model

/**
 * 推单列表数据
 * Created by Guojing on 2018/10/30.
 */

data class PushOrderResultBean(
        var hitOdds: String,
        var profitOdds: String,
        var reds: String,
        var hitOrders: String,
        var prizeStatusStr: String,
        var orderPage: PageResultBean<PushOrderItemBean>
)

data class PushOrderItemBean(
        var betTimes: String,
        var betType: String,
        var commission: String,
        var commissionRate: String,
        var createTime: String,
        var followAmt: String,
        var follows: String,
        var minFollowAmt: String,
        var openStatus: String,
        var orderId: String,
        var payAmt: String,
        var profitOdds: String,
        var projectId: String,
        var followStatus: String
)