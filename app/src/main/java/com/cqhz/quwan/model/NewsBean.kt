package com.cqhz.quwan.model

/**
 * 资讯数据
 */
data class NewsBean(
        val id: String,
        val title: String,
        val titlePic: String,
        val source: String,
        val clickNums: Int
)