package com.cqhz.quwan.model

import com.chad.library.adapter.base.entity.AbstractExpandableItem
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.cqhz.quwan.ui.adapter.ESportExpandableQuickAdapter

/**
 * 电竞数据
 * Created by Guojing on 2018/10/23.
 */
class ESportBean : AbstractExpandableItem<ESportItemBean>(), MultiItemEntity {
    var id: String? = null
    var matchId: String? = null// 赛事id
    var type: String? = null// 游戏类型 0:全部 1:LOL 2:DOTA2 3:CSGO 4:守望先锋 5:篮球 6:足球 7:王者荣耀 8:绝地求生 9:魔兽争霸3 10:炉石传说 11:彩虹6号  12:星际争霸1 13:星际争霸2 14:风暴英雄 15:堡垒之夜 16:FIFA Online 17:穿越火线 18:传说对决 19:使命召唤 20:火箭联盟 21:NBA2K'
    var round: String? = null// 1:bo1 2:bo2 3:bo3 4:bo5
    var leftTeamLogo: String? = null// 左队logo
    var rightTeamLogo: String? = null// 右队logo
    var leftTeam: String? = null// 左队
    var rightTeam: String? = null// 右队
    var leftSupport: String? = null// 左队支持率
    var rightSupport: String? = null// 右队支持率
    var leftTeamId: String? = null// 左队id
    var rightTeamId: String? = null// 右队id
    var schedule: String? = null
    var status: String? = null// 状态0:未开始 1:进行中 2:已完成 3:取消（胜利的判断条件为 status=2 && 比分高 方为胜）
    var result: String? = null// 结果 1:左队胜 2:右队胜
    var matchName: String? = null// 联赛名称
    var matchLogo: String? = null
    var score: String? = null// 比分
    var matchTime: String? = null// 开赛时间
    var localTimestamp: String? = null
    var closeTime: String? = null// 禁止投注时间
    var createTime: String? = null
    var updateTime: String? = null
    var betStatus: String? = null// 预测状态 0:可预测 1:不可预测
    var playList: List<ESportItemBean>? = null

    /**
     * Get the level of this item. The level start from 0.
     * If you don't care about the level, just return a negative.
     */
    override fun getLevel(): Int {
        return 0
    }

    override fun getItemType(): Int {
        return ESportExpandableQuickAdapter.TYPE_LEVEL_0
    }
}

class ESportItemBean : MultiItemEntity {
    var id: String? = null
    var matchId: String? = null
    var type: String? = null// 游戏类型 0:全部 1:LOL 2:DOTA2 3:CSGO 4:守望先锋 5:篮球 6:足球 7:王者荣耀 8:绝地求生 9:魔兽争霸3 10:炉石传说 11:彩虹6号  12:星际争霸1 13:星际争霸2 14:风暴英雄 15:堡垒之夜 16:FIFA Online 17:穿越火线 18:传说对决 19:使命召唤 20:火箭联盟 21:NBA2K'
    var playId: String? = null// 玩法id
    var leftTeamLogo: String? = null// 左队logo
    var rightTeamLogo: String? = null// 右队logo
    var leftTeam: String? = null// 左队
    var rightTeam: String? = null// 右队
    var leftTeamId: String? = null
    var rightTeamId: String? = null
    var betNum: String? = null
    var betStatus: String? = null// 预测状态 0:可预测 1:不可预测
    var status: String? = null// 状态 0:未开始 1:进行中 2:已完成 3:取消
    var playDescribe: String? = null// 玩法描述（获取数字显示）
    var playName: String? = null// 玩法名称
    var handicap: String? = null// 让分1.5
    var handicapTeam: String? = null// 让分队 left:1 right:2
    var leftOdds: String? = null// 第一队赔率
    var rightOdds: String? = null// 第二队赔率
    var playType: String? = null// 玩法类型 1:猜输赢 2:让分局 3:第一局10杀 4:第二局10杀 17: 大小分 19:第一局胜者 20:第一局让分 22:第二局胜者 23:第二局让分 25:第三局胜者 26:第三局让分 28:第四局胜者 31:第五局胜者 34:第一局大小分 35:第二局大小分 36:第三局大小分 41:第一局5杀 42:第二局5杀 43:第三局5杀 44:第四局5杀 45:第五局5杀
    var result: String? = null// 结果 1:左队胜 2:右队胜
    var score: String? = null// 比分
    var showScore: String? = null// 是否展示比分 0:展示 1:不展示
    var showOdds: String? = null// 是否展示赔率 1:展示 0:不展示
    var matchTime: String? = null// 开赛时间
    var localTimestamp: String? = null
    var closeTimestamp: String? = null
    var closeTime: String? = null// 禁止下注时间
    var createTime: String? = null
    var updateTime: String? = null
    var remainTime: String? = null
    var tag: String? = null// 0:展示常规 1：展示大小分
    var isSelected: Boolean? = null
    var matchName: String? = null// 联赛名称
    var startTimestamp: String? = null
    var playOdds: String? = null
    var playOddsMap: Map<String, Map<String, String>>? = null // 赔率 Map
    var round: String? = null

    constructor()

    override fun getItemType(): Int {
        return ESportExpandableQuickAdapter.TYPE_LEVEL_1
    }
}

data class ESportItemSelectBean(
        val position: String,
        val partOdds: String,
        val partName: String
)

data class ESportDetailBean(
        val id: String,
        val matchId: String,
        val type: String,
        val round: String,
        val rounds: String,// "0,1,2,3"
        val leftTeamLogo: String,
        val rightTeamLogo: String,
        val leftTeam: String,
        val rightTeam: String,
        val leftSupport: String,
        val rightSupport: String,
        val leftTeamId: String,
        val rightTeamId: String,
        val schedule: String,
        val status: String,
        val matchName: String,
        val matchLogo: String,
        val score: String,
        val matchTime: String,
        val closeTime: String,
        val createTime: String,
        val updateTime: String,
        val betStatus: String
)

class ESportOrderBean {

    var redNum: String = ""
    var playType: String = ""
    var period: String = ""

    constructor()

    constructor(redNum: String, playType: String, period: String) {
        this.redNum = redNum
        this.playType = playType
        this.period = period
    }
}