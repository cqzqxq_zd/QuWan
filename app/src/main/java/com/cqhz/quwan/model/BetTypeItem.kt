package com.cqhz.quwan.model

data class BetTypeItem (
        val id:Int,
        val text:String
)