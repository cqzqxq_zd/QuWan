package com.cqhz.quwan.common

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import cn.jpush.android.api.JPushInterface
import com.cqhz.quwan.APP
import com.cqhz.quwan.ui.main.SplashActivity
import com.cqhz.quwan.ui.mine.MessageActivity
import com.cqhz.quwan.util.CLogger


class NotificationReceiver:BroadcastReceiver() {
    companion object {
        const val TAG = "JPUSH_RECEIVER"
    }
    private val log = CLogger(this::class.java)
    private var nm:NotificationManager? = null
    override fun onReceive(context: Context?, intent: Intent?) {
        if (nm == null){
            nm = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        val bundle = intent?.extras
        if (JPushInterface.ACTION_NOTIFICATION_OPENED == intent?.action){
            log.e("用户点击了通知内容")
            openNotification(context)
        }
    }
    private fun openNotification(context: Context?){
        val loginInfo = APP.get()?.loginInfo
        if (loginInfo == null){
            val mainIntent = Intent(context, SplashActivity::class.java)
            mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context?.startActivity(mainIntent)
            return
        }
        val mainIntent = Intent(context, SplashActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        val messageIntent = Intent(context, MessageActivity::class.java)
        context?.startActivities(arrayOf(mainIntent,messageIntent))
    }
}