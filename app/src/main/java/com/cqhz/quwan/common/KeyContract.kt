package com.cqhz.quwan.common

import com.cqhz.quwan.BuildConfig


class KeyContract {
    companion object {
        const val UserId = "userId"
        const val PhoneNumber = "phoneNumber"
        const val LoginInfo = "loginInfo"
        const val RedBalls = "redBalls"
        const val BetMethod = "betMethod"
        const val RedSelectedBalls = "redSelectedBalls"
        const val BlueSelectedBalls = "redSelectedDdBalls"
        const val BetId = "betId"
        const val Lottery = "lottery"
        const val IsReset = "isReset"
        const val ResetIndex = "resetIndex"
        const val LotteryName = "lotteryName"
        const val PlayType = "playType"
        const val PeriodNum = "periodNum"
        const val LotteryId = "lotteryId"
        const val BlueBalls = "blueBalls"
        const val Amount = "amount"
        const val ActualAmount = "actualAmount"
        const val CouponConsume = "CouponConsume"
        const val OrderId = "orderId"

        const val ExpertId = "expertId"

        const val Money = "money"
        const val Balance = "balance"
        const val AlipayAuthData = "alipayAuthData"
        const val LotteryType = "lotteryType"
        const val Balls = "balls"
        const val Gen = "gen"
        const val IsGetCash = "isGetCash"
        const val PAY_WAY_ALIPAY = 0x99
        const val PAY_WAY_WECHAT = 0x98
        const val PAY_WAY = "payWay"
        const val PayAccount = "payAccount"
        const val Checked = "checked"

        // web页面跳转key
        const val String = "string"
        const val List = "list"
        const val Position = "position"
        const val Type = "type"
        const val Title = "title"
        const val Url = "url"
        const val BannerId = "bannerId"
        const val Bean = "bean"
        const val EnableRefresh = "isEnableRefresh"

        const val APP_KEY = "APP_KEY"
        const val APP_SOURCE = "APP_SOURCE"
        const val APP_CHANNEL = "APP_CHANNEL"
        const val BUGLY_APPID = "BUGLY_APPID"
        const val BettingItem = "bettingItem"
        const val BettingItems = "bettingItems"
        const val Selected = "selected"
        const val IsLock = "isLock"
        const val LotteryType_SSQ = 0
        const val LotteryType_DLT = 1

        const val STATE = "state"

        // 首页活动开关-1：热门赛事；2：焦点赛事
        const val STATE_HOT_MATCH = 1
        const val STATE_FOCUS_MATCH = 2

        const val WEICHAT_APP_ID = "wxb31081254a9f0993"

        // 文件路径
        const val FILE_PROVIDER = BuildConfig.APPLICATION_ID + ".fileProvider"
        const val FILE_PATH = "/sdcard/DCIM/ttqw/"

        const val pageSize = 30

        const val UmengAppKey = "5bac8e38b465f5ccdd000132"

        const val RECHARGE_GIFT_RATIO = "RECHARGE_GIFT_RATIO"
        const val GUESS_COMPETITION = "GUESS_COMPETITION"
        const val PRODUCT_DISCOUNT_RATE = "PRODUCT_DISCOUNT_RATE"
        // 个人中心我的竞猜开关1:开0:关
        const val MY_QUIZ_SWITCH = "MY_QUIZ_SWITCH"
        // 个人中心签到开关 type为1时弹窗，为0时跳转h5，h5地址为cval字段值
        const val SIGN_IN_SWITCH = "SIGN_IN_SWITCH"
        // 代卖换钱开关1:开0:关
        const val EXCHANGE_MONEY_FOR_SALE_SWITCH = "EXCHANGE_MONEY_FOR_SALE_SWITCH"

        const val RECHARGE_HINTS = "RECHARGE_HINTS"

        // app下载链接
        const val DOWNLOAD_URL = "http://h5.zhanguo.fun/reg/apps/lottery.apk"
    }
}