package com.cqhz.quwan.common

import android.widget.ListView
import com.cqhz.quwan.ActivityInject
import com.cqhz.quwan.R
import com.cqhz.quwan.ui.base.AbsAdapter
import com.cqhz.quwan.ui.base.AbsFragment
import com.cqhz.quwan.ui.widget.StateLayout
import com.cqhz.quwan.util.findView
import io.reactivex.Flowable
import me.militch.quickcore.core.HasDaggerInject
import me.militch.quickcore.mvp.model.ModelHelper
import javax.inject.Inject

abstract class AbsListFragment<T>:AbsFragment(),HasDaggerInject<ActivityInject> {
    private val stateLayout by findView<StateLayout>(R.id.stateLayout)
    private val mListView by findView<ListView>(R.id.lv_content)
    private val mAdapter by initAdapter(this.itemLayout())
    @Inject
    lateinit var modelHolder:ModelHelper
    override fun layout(): Int {
        return R.layout.layout_listview
    }
    override fun initView() {
         modelHolder.getService(requestClass())

    }
    abstract fun requestClass():Class<*>
    abstract fun request():Flowable<T>
    abstract fun itemLayout():Int
    abstract fun handlerViewHolder(viewHolder: AbsAdapter.ViewHolder, position: Int, itemData: T?)
    private fun initAdapter(itemLayout:Int):Lazy<AbsAdapter<T>>{
        return lazy {
            object :AbsAdapter<T>(context(),itemLayout){
                override fun handlerViewHolder(viewHolder: ViewHolder, position: Int, itemData: T?) {
                    this@AbsListFragment.handlerViewHolder(viewHolder,position,itemData)
                }
            }
        }
    }
}